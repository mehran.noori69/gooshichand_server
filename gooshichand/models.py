from django.contrib.auth.models import User
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.utils import timezone
from datetime import datetime, timedelta

from website import jalali


class UserProfile(User):
    user = models.OneToOneField(User, on_delete=models.CASCADE, parent_link=True)
    nickName = models.CharField(max_length=20)
    verified = models.BooleanField(default=False)
    verifiedCode = models.IntegerField(default=000000)
    mode = models.CharField(default="user", max_length=20)

    class Meta:
        ordering = ('-date_joined',)
        verbose_name = _("User Profile")
        verbose_name_plural = _("User Profiles")


class MobileItem(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    ids = models.CharField(max_length=1000, blank=True, null=True)
    cats = models.CharField(max_length=1000, blank=True, null=True)
    model = models.CharField(max_length=1000, blank=True, null=True)
    title = models.CharField(max_length=1000, blank=True, null=True)
    shortdesc = models.CharField(max_length=1000, blank=True, null=True)
    price = models.IntegerField(default=0, blank=True, null=True)
    img = models.CharField(max_length=1000, blank=True, null=True)
    store = models.CharField(max_length=1000, blank=True, null=True)
    city = models.CharField(max_length=1000, blank=True, null=True)
    path = models.CharField(max_length=1000, blank=True, null=True)
    link = models.CharField(max_length=1000, blank=True, null=True)
    tags = models.TextField(blank=True, null=True)

    @property
    def store_underscore(self):
        return self.store.replace(' ', '_')

    @classmethod
    def get_mobile_item(cls):
        return cls.objects.all()

    @classmethod
    def get_min_price_of_item_by_title(cls, title_without_brand):
        return cls.objects.filter(model=title_without_brand).order_by('price')[0].price

    @classmethod
    def get_mobile_item_before_today(cls):
        startday = datetime.today() - timedelta(hours=datetime.today().hour - 1)
        return cls.objects.filter(created__lte=startday).order_by('-created')

    @classmethod
    def get_mobile_item_after_today(cls):
        startday = datetime.today() - timedelta(hours=datetime.today().hour - 1)
        return cls.objects.filter(created__gte=startday).order_by('-created')

    @classmethod
    def get_mobile_item_sorted_by_store(cls):
        return cls.objects.all().order_by('store')

    @classmethod
    def get_mobile_item_by_store_name(cls, name):
        return cls.objects.filter(store=name)

    @classmethod
    def get_stores_for_search_suggestion(cls):
        stores = [store.encode("utf8") for store in
                  cls.objects.values_list('store', flat=True).distinct().order_by()]
        return stores

    @classmethod
    def get_cities_for_search_suggestion(cls):
        cities = [city.encode("utf8") for city in
                  cls.objects.values_list('city', flat=True).distinct().order_by()]
        return cities

    @classmethod
    def get_length_of_mobile_item_by_store_name(cls, name):
        return len(cls.objects.filter(store=name))

    @classmethod
    def get_mobile_item_with_model(cls, title_without_brand):
        return cls.objects.filter(model=title_without_brand)

    @classmethod
    def see_if_mobile_spec_has_price(cls, title_without_brand):
        return len(cls.objects.filter(model=title_without_brand))

    @classmethod
    def get_min_price_of_spec_in_mobile_items(cls, title_without_brand):
        return cls.objects.filter(model=title_without_brand).order_by('price')[0].price

    class Meta:
        ordering = ('-created',)
        verbose_name = _("Mobile Item")
        verbose_name_plural = _("Mobile Items")


class MobileBrand(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    name = models.CharField(max_length=1000, blank=True, null=True)
    alt = models.CharField(max_length=1000, blank=True, null=True)
    brandUrl = models.CharField(max_length=1000, blank=True, null=True)
    brandDesc = models.CharField(max_length=1000, blank=True, null=True)
    imgUrl = models.CharField(max_length=1000, blank=True, null=True)
    visibility = models.BooleanField(default=False)

    @classmethod
    def get_mobile_brand(cls):
        return cls.objects.all()

    @classmethod
    def get_visible_mobile_brand(cls):
        return cls.objects.filter(visibility=True).order_by('name')

    @classmethod
    def get_brands_for_search_suggestion(cls):
        return cls.objects.values_list('name', flat=True).order_by('name')

    class Meta:
        ordering = ('-created',)
        verbose_name = _("Mobile Brand")
        verbose_name_plural = _("Mobile Brands")


class MobileStore(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    name = models.CharField(max_length=1000, blank=True, null=True)
    city = models.CharField(max_length=1000, blank=True, null=True)
    address = models.CharField(max_length=1000, blank=True, null=True)
    phone = models.CharField(max_length=1000, blank=True, null=True)
    lat = models.CharField(max_length=1000, blank=True, null=True)
    long = models.CharField(max_length=1000, blank=True, null=True)
    website = models.CharField(max_length=1000, blank=True, null=True)
    visibility = models.BooleanField(default=False)

    @classmethod
    def get_mobile_stores(cls):
        return cls.objects.all()

    @classmethod
    def get_mobile_stores_phone(cls, store):
        return cls.objects.get(name__icontains=store).phone

    @classmethod
    def get_store_by_name(cls, store_name):
        return cls.objects.get(name__icontains=store_name)

    class Meta:
        ordering = ('-created',)
        verbose_name = _("Mobile Store")
        verbose_name_plural = _("Mobile Stores")


class MobileSpecs(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    title = models.CharField(max_length=1000, blank=True, null=True)
    modelalt = models.CharField(max_length=1000, blank=True, null=True)
    img = models.CharField(max_length=1000, blank=True, null=True)
    link = models.CharField(max_length=1000, blank=True, null=True)
    camera_back = models.FloatField(blank=True, null=True)
    camera_front = models.FloatField(blank=True, null=True)
    release_date = models.DateTimeField(blank=True, null=True)
    os = models.CharField(max_length=1000, blank=True, null=True)
    dims = models.CharField(max_length=1000, blank=True, null=True)
    weight = models.FloatField(blank=True, null=True)
    inch = models.FloatField(blank=True, null=True)
    reso = models.CharField(max_length=1000, blank=True, null=True)
    pd = models.IntegerField(blank=True, null=True)
    stbratio = models.FloatField(blank=True, null=True)
    cpu_chip = models.CharField(max_length=1000, blank=True, null=True)
    cpu_processor = models.CharField(max_length=1000, blank=True, null=True)
    cpu_freq = models.IntegerField(default=0, blank=True, null=True)
    cpu_core = models.IntegerField(default=1, blank=True, null=True)
    gpu = models.CharField(max_length=1000, blank=True, null=True)
    memory = models.FloatField(blank=True, null=True)
    storage = models.FloatField(blank=True, null=True)
    battery_capacity = models.CharField(max_length=1000, blank=True, null=True)
    battery_type = models.CharField(max_length=1000, blank=True, null=True)
    battery_talk_time = models.CharField(max_length=1000, blank=True, null=True)
    data = models.CharField(max_length=1000, blank=True, null=True)
    nano = models.BooleanField(default=False)
    bluetooth = models.FloatField(blank=True, null=True)
    gallery = models.TextField(blank=True, null=True)
    path = models.CharField(max_length=1000, blank=True, null=True)

    @classmethod
    def get_mobile_spec(cls):
        return cls.objects.all()

    @classmethod
    def get_mobile_spec_by_path(cls, path):
        return cls.objects.filter(path=path)

    @classmethod
    def get_newest_mobile_spec_from_date(cls, start_day):
        return cls.objects.filter(created__gte=start_day).order_by('-release_date')

    @classmethod
    def get_latest_mobile_spec_from_date(cls, start_day):
        return cls.objects.filter(release_date__gte=start_day).order_by('-release_date')

    @property
    def title_with_underline(self):
        return self.title.replace(' ', '_')

    @property
    def show_date(self):
        return self.release_date.strftime('%B %Y')

    @property
    def show_camera(self):
        return self.camera_back

    @property
    def show_cpu(self):
        return self.cpu_freq

    @property
    def show_storage(self):
        return self.storage

    @property
    def show_inch(self):
        return self.inch

    @property
    def title_without_brand(self):
        title = self.title.replace(self.path, "")
        title = ' '.join(title.split())
        return title

    @classmethod
    def get_title_for_search_suggestion(cls):
        titles = [titles.encode("utf8") for titles in
                  cls.objects.values_list('title', flat=True).distinct().order_by('-title')]
        return titles

    @classmethod
    def get_mobile_brand(cls):
        return [brand.encode("utf8") for brand in MobileSpecs.objects.values_list('path', flat=True).distinct()
                .order_by('path')]

    @property
    def link_of_page(self):
        return "/item/" + self.title.encode('utf-8').replace(' ', "_")

    @property
    def link_of_page_to_share(self):
        return "https://gooshichand.com/item/" + self.title.encode('utf-8').replace(' ', "_")

    @property
    def first_image_in_gallery(self):
        image = ''
        if self.gallery is not None:
            images = self.gallery.split(",")
            if len(images) > 0:
                image = images[0]
        return image

    def get_first_image_from_gallery_of_mobile_spec(self):
        image = ''
        if self.gallery is not None:
            images = self.gallery.split(",")
            if len(images) > 0:
                image = images[0]
        return image

    @classmethod
    def get_mobile_spec_items_all(cls, order_by, has_page, start_index, end_index):
        if has_page:
            return cls.objects.all().order_by(order_by)[start_index: end_index]
        else:
            return cls.objects.all().order_by(order_by)

    @classmethod
    def get_mobile_spec_items_by_brand(cls, brand_name, order_by, has_page, start_index, end_index):
        if has_page:
            return cls.objects.filter(path=brand_name).order_by(order_by)[start_index: end_index]
        else:
            return cls.objects.filter(path=brand_name).order_by(order_by)

    @classmethod
    def get_mobile_spec_items_by_title(cls, title, order_by, has_page, start_index, end_index):
        if has_page:
            return cls.objects.filter(title__icontains=title).order_by(order_by)[start_index: end_index]
        else:
            return cls.objects.filter(title__icontains=title).order_by(order_by)

    @classmethod
    def est_mobile_item_by_brand_function(cls, brand, which, start, pagesize):
        mobile_specs = []
        if brand == 'all':
            if which == "back_camera":
                mobile_specs = cls.objects.filter(camera_back__gt=0).order_by('-camera_back')[
                               start:start + pagesize]
            elif which == "front_camera":
                mobile_specs = cls.objects.filter(camera_front__gt=0).order_by('-camera_front')[start:start + pagesize]
            elif which == "newest":
                mobile_specs = cls.objects.all().order_by('-release_date')[start:start + pagesize]
            elif which == "cpu":
                mobile_specs = cls.objects.filter(cpu_freq__gt=0).order_by('-cpu_freq')[start:start + pagesize]
            elif which == "ram":
                mobile_specs = cls.objects.filter(memory__gt=0).order_by('-memory')[start:start + pagesize]
            elif which == "inch":
                mobile_specs = cls.objects.filter(inch__gt=0).order_by('-inch')[start:start + pagesize]
            elif which == "weight":
                mobile_specs = cls.objects.filter(weight__gt=0).order_by('weight')[start:start + pagesize]
        else:
            if which == "back_camera":
                mobile_specs = cls.objects.filter(path=brand, camera_back__gt=0) \
                                   .order_by('-camera_back')[start:start + pagesize]
            elif which == "front_camera":
                mobile_specs = cls.objects.filter(path=brand, camera_front__gt=0) \
                                   .order_by('-camera_front')[start:start + pagesize]
            elif which == "newest":
                mobile_specs = cls.objects.filter(path=brand).order_by('-release_date')[start:start + pagesize]
            elif which == "cpu":
                mobile_specs = cls.objects.filter(path=brand, cpu_freq__gt=0) \
                                   .order_by('-cpu_freq')[start:start + pagesize]
            elif which == "ram":
                mobile_specs = cls.objects.filter(path=brand, memory__gt=0).order_by('-memory')[start:start + pagesize]
            elif which == "inch":
                mobile_specs = cls.objects.filter(path=brand, inch__gt=0).order_by('-inch')[start:start + pagesize]
            elif which == "weight":
                mobile_specs = cls.objects.filter(path=brand, weight__gt=0).order_by('weight')[start:start + pagesize]
        return mobile_specs

    class Meta:
        ordering = ('-created',)
        verbose_name = _("Mobile Spec")
        verbose_name_plural = _("Mobile Specs")


class MobileVideos(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    video_ids = models.AutoField(primary_key=True)
    link = models.CharField(max_length=1000, blank=True, null=True)
    tags = models.TextField(blank=True, null=True)
    count = models.IntegerField(default=0)
    verified = models.BooleanField(default=True)

    @classmethod
    def get_mobile_videos(cls):
        return cls.objects.all()

    class Meta:
        ordering = ('-created',)
        verbose_name = _("Mobile Video")
        verbose_name_plural = _("Mobile Videos")


class MobilePrice(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    title = models.CharField(max_length=1000, blank=True, null=True)
    price = models.IntegerField(default=0, blank=True, null=True)
    brand = models.CharField(max_length=1000, blank=True, null=True)

    @classmethod
    def get_mobile_price(cls):
        return cls.objects.all()

    @classmethod
    def list_price_of_mobile_item_for_last_month(cls, title_without_brand, path):
        return cls.objects.filter(title=title_without_brand, brand=path,
                                  created__gte=timezone.now() - timedelta(days=30)).order_by('created')

    class Meta:
        ordering = ('-created',)
        verbose_name = _("Mobile Min Price")
        verbose_name_plural = _("Mobile Min Prices")


class WebRequest(models.Model):
    time = models.DateTimeField(auto_now_add=True)
    section = models.CharField(blank=True, max_length=1000)
    ipaddress = models.CharField(blank=True, max_length=50)
    ref = models.CharField(null=True, blank=True, max_length=1000)
    browser = models.CharField(blank=True, max_length=1000)

    class Meta:
        ordering = ('-time',)
        verbose_name = _("Web Request")
        verbose_name_plural = _("Web Requests")


class PaymentGW(models.Model):
    type = models.CharField(max_length=64, blank=True, null=True, default="zpal", verbose_name=_("Gateway Type"))
    gateway_webservice = models.CharField(max_length=200, blank=True, null=True, verbose_name=_("Web Service Url"))
    gateway_starturl = models.CharField(max_length=200, blank=True, null=True, verbose_name=_("Start Url"))
    gateway_callbackurl = models.CharField(max_length=200, blank=True, null=True, verbose_name=_("Callback Url"))
    gateway_api = models.CharField(max_length=512, blank=True, null=True, verbose_name=_("Gateway API"))
    is_selected = models.BooleanField(default=False, verbose_name=_("Gateway Select"))

    @classmethod
    def get_gw(cls):
        return cls.objects.all()

    class Meta:
        verbose_name = _("Payment Gateway")
        verbose_name_plural = _("Payment Gateways")


class AdsTgSubs(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    ids = models.CharField(max_length=1000, blank=True, null=True)
    chat_ids = models.CharField(max_length=1000, blank=True, null=True)
    email = models.CharField(max_length=1000, blank=True, null=True)
    phone = models.CharField(max_length=1000, blank=True, null=True)
    telegram = models.CharField(max_length=1000, blank=True, null=True)
    fields = models.CharField(max_length=1000, blank=True, null=True)
    keywords = models.CharField(max_length=1000, blank=True, null=True)
    payed = models.BooleanField(default=False)
    locked = models.BooleanField(default=False)
    verified = models.BooleanField(default=False)
    test_by_user = models.BooleanField(default=False, verbose_name=_("Tested By User"))
    expired = models.DateTimeField(blank=True, null=True)
    comment = models.TextField(blank=True, null=True)
    authority = models.CharField(max_length=100, blank=True, null=True, verbose_name=_("Authority"))
    ref_id = models.CharField(max_length=100, blank=True, null=True, verbose_name=_("Reference ID"))
    payment_time = models.DateTimeField(blank=True, null=True, verbose_name=_("Payment Time"))
    ingroup = models.BooleanField(default=True)
    invalid_tg = models.BooleanField(default=False, verbose_name=_("Invalid Telegram ACC"))
    payment_page = models.CharField(max_length=1000, blank=True, null=True, verbose_name=_("Pay Link"))

    class Meta:
        ordering = ('-created',)
        verbose_name = _("Ad In Telegram Sub")
        verbose_name_plural = _("Ad In Telegram Subs")


class MobileModelDict(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    name = models.CharField(max_length=1000, blank=True, null=True)
    alts = models.TextField(blank=True, null=True)
    brand = models.TextField(blank=True, null=True)
    has_config = models.BooleanField(default=True)

    @classmethod
    def get_mobile_model_dict(cls):
        return cls.objects.all()

    class Meta:
        ordering = ('-created',)
        verbose_name = _("Mobile Models Dictionary")
        verbose_name_plural = _("Mobile Models Dictionaries")


class WebsiteReports(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    where = models.CharField(max_length=1000, blank=True, null=True)
    which = models.CharField(max_length=1000, blank=True, null=True)
    comment = models.CharField(max_length=1000, blank=True, null=True)

    class Meta:
        ordering = ('-created',)
        verbose_name = _("Website Report")
        verbose_name_plural = _("Website Reports")


class UserComment(models.Model):
    code = models.AutoField(primary_key=True)
    created = models.DateTimeField(auto_now_add=True)
    name = models.CharField(null=True, blank=True, max_length=65)
    email = models.CharField(null=True, blank=True, max_length=65)
    section = models.CharField(null=True, blank=True, max_length=200)
    question = models.TextField(null=True, blank=True)
    response = models.TextField(null=True, blank=True)
    deviceid = models.CharField(blank=True, null=True, max_length=50)

    @property
    def date(self):
        return self.created.strftime('%a %d %B %Y')

    @property
    def fadate(self):
        return jalali.Gregorian(self.created.strftime('%Y-%m-%d')).persian_string()

    @classmethod
    def get_user_comments_by_section(cls, section):
        return cls.objects.filter(section=section).order_by('-created')

    @classmethod
    def get_user_comments(cls):
        return cls.objects.all()

    class Meta:
        ordering = ('-code',)
        verbose_name = _("User Comment On Spec")
        verbose_name_plural = _("User Comments On Spec")


class MobileNews(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    ids = models.CharField(max_length=500, blank=True, null=True)
    title = models.CharField(max_length=500, blank=True, null=True)
    short_desc = models.TextField(blank=True, null=True)
    big_desc = models.TextField(blank=True, null=True)
    link = models.CharField(max_length=500, blank=True, null=True)
    img = models.CharField(max_length=1000, blank=True, null=True)
    source_name = models.CharField(max_length=1000, blank=True, null=True)
    source_name_file = models.CharField(max_length=1000, blank=True, null=True)
    tags = models.CharField(max_length=1000, blank=True, null=True)
    view = models.IntegerField(default=0)

    @property
    def link_of_news_to_share(self):
        return "https://gooshichand.com/news/" + self.ids + '/' + self.title_with_underline[0:5] + '/'

    @property
    def link_of_news(self):
        return "/news/" + self.ids + '/' + self.title_with_underline + '/'

    @property
    def share_link(self):
        return "/news/" + self.ids + '/' + self.title_with_underline + '/'

    @classmethod
    def mobile_news_set_tags(cls, news_item, brands):
        obj = cls.objects.get(ids=news_item.ids)
        tags = ''
        obj.tags = ''
        for mb in brands:
            alts = mb.alt.split('-')
            for alt in alts:
                if alt.encode('utf-8') in obj.title.encode('utf-8') or alt.encode('utf-8') in obj.short_desc.encode(
                        'utf-8') or alt.encode('utf-8') in obj.big_desc.encode('utf-8'):
                    if mb.name.encode('utf-8') not in tags:
                        tags += mb.name.encode('utf-8') + '-'
        obj.tags = tags
        obj.save()
        return obj

    @classmethod
    def increase_view_counter(cls, news_id):
        obj = cls.objects.get(ids=news_id)
        obj.view += 1
        obj.save()
        return True

    @property
    def title_with_underline(self):
        return self.title.replace(' ', "_")

    @classmethod
    def get_mobile_news(cls):
        return cls.objects.all()

    @classmethod
    def get_mobile_news_by_title(cls, title):
        return cls.objects.get(title=title)

    @classmethod
    def get_mobile_news_after_date(cls, from_date):
        return cls.objects.filter(created__gte=from_date).order_by('-created')

    @classmethod
    def get_mobile_news_after_date_with_tag(cls, start_day, start_index, end_index, tag, sort_string):
        return cls.objects.filter(tags__contains=tag, created__gte=start_day).order_by(sort_string)[start_index:
                                                                                                    end_index]

    class Meta:
        ordering = ('-created',)
        verbose_name = _("Mobile News")
        verbose_name_plural = _("Mobile News")


class MobileStoreRedirect(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    ids = models.CharField(max_length=1000, blank=True, null=True)
    title = models.CharField(max_length=1000, blank=True, null=True)
    cats = models.CharField(max_length=1000, blank=True, null=True)
    model = models.CharField(max_length=1000, blank=True, null=True)
    price = models.CharField(max_length=1000, blank=True, null=True)
    store = models.CharField(max_length=1000, blank=True, null=True)
    city = models.CharField(max_length=1000, blank=True, null=True)
    link = models.CharField(max_length=1000, blank=True, null=True)

    @classmethod
    def get_mobile_store_redirect(cls):
        return cls.objects.all()

    class Meta:
        ordering = ('-created',)
        verbose_name = _("Mobile Store Redirect")
        verbose_name_plural = _("Mobile Store Redirects")


class MisspelledModelKeyword(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    first = models.CharField(max_length=1000, blank=True, null=True)
    second = models.CharField(max_length=1000, blank=True, null=True)
    third = models.TextField(blank=True, null=True)

    @classmethod
    def get_misspelled(cls):
        return cls.objects.all()

    class Meta:
        ordering = ('-created',)
        verbose_name = _("Misspelled Model Keyword")
        verbose_name_plural = _("Misspelled Model Keywords")


class WebSettings(models.Model):
    admin_email = models.CharField(max_length=1000, blank=True, null=True)
    item_pg_size = models.IntegerField(default=0)
    allow_ips = models.TextField(blank=True, null=True)

    @classmethod
    def get_settings(cls):
        return cls.objects.all()

    class Meta:
        ordering = ('-admin_email',)
        verbose_name = _("Web Setting")
        verbose_name_plural = _("Web Setting")
