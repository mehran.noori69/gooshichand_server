from __future__ import unicode_literals

from django.apps import AppConfig
from watson import search as watson


class DoresaappConfig(AppConfig):
    name = 'gooshichand'

    def ready(self):
        mobile_item = self.get_model("MobileItem")
        watson.register(mobile_item, fields=("cats", "title", "price", "city"))
