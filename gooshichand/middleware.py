import requests


def sending_email(destination, subject, body):
    requests.post(
        "https://gooshichand.com/sendemail/",
        data={"destination": [destination],
              "subject": subject,
              "body": body})


class SendExceptionMiddleware(object):
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        return self.get_response(request)

    def process_exception(self, request, exception):
        intitle = u'{}: {}'.format(exception.__class__.__name__,  exception.message)
        print(intitle)
        # sending_email("mehran.noori2070@gmail.com", intitle, request.path)
        return None
