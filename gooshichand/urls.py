from django.conf.urls import url, include
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    url(r'^grappelli/', include('grappelli.urls')),
    url(r'^json/', include('backup.urls')),
    url(r'^android/', include('android.urls')),
    url(r'^sendemail/', include('sendemail.urls')),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^admin/', admin.site.urls),
    # analytics panel
    url(r'^tools/', include('analytics.urls')),
    # tour APIs
    url(r'^tours/', include('tours.urls')),
    # social media
    url(r'^socialmedia/', include('socialmedia.urls')),
    url(r'^', include('website.urls')),
]
if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
