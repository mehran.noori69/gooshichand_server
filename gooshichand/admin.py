from django.contrib import admin

from gooshichand.models import MobileItem, MobileBrand, MobileStore, MobileSpecs, MobileModelDict,\
    MobilePrice, UserComment, MobileNews, MobileStoreRedirect, MisspelledModelKeyword, MobileVideos, WebSettings


def ads_in_tg_sub_payed(queryset):
    queryset.update(payed=True)


def ads_in_tg_sub_no_payed(queryset):
    queryset.update(payed=False)


def ads_in_tg_sub_locked(queryset):
    queryset.update(locked=True)


def ads_in_tg_sub_no_locked(queryset):
    queryset.update(locked=False)


def ads_in_tg_sub_verified(queryset):
    queryset.update(verified=True)


def ads_in_tg_sub_no_verified(queryset):
    queryset.update(verified=False)


def ads_in_tg_sub_tested(queryset):
    queryset.update(test_by_user=True)


def ads_in_tg_sub_no_tested(queryset):
    queryset.update(test_by_user=False)


def ads_in_tg_sub_invalid_tg(queryset):
    queryset.update(invalid_tg=True)


def ads_in_tg_sub_no_invalid_tg(queryset):
    queryset.update(invalid_tg=False)


ads_in_tg_sub_payed.short_description = 'Mark as payed'
ads_in_tg_sub_no_payed.short_description = 'Mark as Not payed'
ads_in_tg_sub_locked.short_description = 'Mark as locked'
ads_in_tg_sub_no_locked.short_description = 'Mark as Not locked'
ads_in_tg_sub_verified.short_description = 'Mark as verified'
ads_in_tg_sub_no_verified.short_description = 'Mark as Not verified'
ads_in_tg_sub_tested.short_description = 'Mark as tested'
ads_in_tg_sub_no_tested.short_description = 'Mark as Not tested'
ads_in_tg_sub_invalid_tg.short_description = 'Mark as invalid TG ACC'
ads_in_tg_sub_no_invalid_tg.short_description = 'Mark as Valid TG ACC'


class UserProfilesAdmin(admin.ModelAdmin):
    list_display = ('date_joined', 'username', 'email', 'nickName', 'verified', 'verifiedCode', 'mode')
    list_filter = ['date_joined', 'username', 'email', 'nickName', 'verified', 'mode']
    search_fields = ['username', 'email', 'nickName', 'verifiedCode', 'mode']


class WebRequestsAdmin(admin.ModelAdmin):
    list_display = ('time', 'ipaddress', 'browser', 'section', 'ref')
    list_filter = ['time', 'ipaddress', 'browser', 'section', 'ref']
    search_fields = ['time', 'ipaddress', 'browser', 'section', 'ref']


class UserCommentAdmin(admin.ModelAdmin):
    list_display = ('code', 'created', 'name', 'email', 'section', 'question', 'response', 'deviceid')
    list_filter = ['code', 'created', 'name', 'email', 'section', 'question', 'response', 'deviceid']
    search_fields = ['code', 'created', 'name', 'email', 'section', 'question', 'response', 'deviceid']


class MobileItemAdmin(admin.ModelAdmin):
    list_display = (
        'created', 'ids', 'cats', 'model', 'title', 'price', 'img', 'store', 'city', 'path', 'link')
    list_filter = (
        'created', 'ids', 'cats', 'model', 'title', 'price', 'img', 'store', 'city', 'path', 'link')
    search_fields = (
        'created', 'ids', 'cats', 'model', 'title', 'price', 'img', 'store', 'city', 'path', 'link')


class MobileBrandAdmin(admin.ModelAdmin):
    list_display = ('created', 'name', 'alt', 'brandUrl', 'brandDesc', 'visibility', 'imgUrl')
    list_filter = ('created', 'name', 'alt', 'brandUrl', 'brandDesc', 'visibility', 'imgUrl')
    search_fields = ('created', 'name', 'alt', 'brandUrl', 'brandDesc', 'visibility', 'imgUrl')


class MobileStoreAdmin(admin.ModelAdmin):
    list_display = ('created', 'name', 'city', 'address', 'phone', 'website', 'lat', 'long', 'visibility')
    list_filter = ('created', 'name', 'city', 'address', 'phone', 'website', 'lat', 'long', 'visibility')
    search_fields = ('created', 'name', 'city', 'address', 'phone', 'website', 'lat', 'long', 'visibility')


class MobileSpecsAdmin(admin.ModelAdmin):
    list_display = (
        'created', 'title', 'modelalt', 'camera_back', 'camera_front', 'release_date', 'os', 'weight', 'inch',
        'pd', 'stbratio', 'cpu_processor', 'cpu_freq', 'cpu_core', 'memory',
        'storage', 'bluetooth', 'battery_capacity', 'battery_type', 'battery_talk_time', 'data', 'nano',
        'img', 'link', 'path')
    list_filter = (
        'created', 'title', 'modelalt', 'camera_back', 'camera_front', 'release_date', 'os', 'dims', 'weight', 'inch',
        'reso', 'pd', 'stbratio', 'cpu_chip', 'cpu_processor', 'cpu_freq', 'cpu_core', 'gpu', 'memory',
        'storage', 'bluetooth', 'battery_capacity', 'battery_type', 'battery_talk_time', 'data', 'nano',
        'img', 'link', 'path')
    search_fields = (
        'created', 'title', 'modelalt', 'camera_back', 'camera_front', 'release_date', 'os', 'dims', 'weight', 'inch',
        'reso', 'pd', 'stbratio', 'cpu_chip', 'cpu_processor', 'cpu_freq', 'cpu_core', 'gpu', 'memory',
        'storage', 'bluetooth', 'battery_capacity', 'battery_type', 'battery_talk_time', 'data', 'nano',
        'img', 'link', 'path')


class MobileModelDictAdmin(admin.ModelAdmin):
    list_display = ('created', 'name', 'brand', 'has_config', 'alts')
    list_filter = ['created', 'name', 'brand', 'has_config', 'alts']
    search_fields = ['created', 'name', 'brand', 'has_config', 'alts']


class WebsiteReportsAdmin(admin.ModelAdmin):
    list_display = ('created', 'where', 'which', 'comment')
    list_filter = ['created', 'where', 'which', 'comment']
    search_fields = ['created', 'where', 'which', 'comment']


class MobilePriceAdmin(admin.ModelAdmin):
    list_display = ('created', 'title', 'price', 'brand')
    list_filter = ['created', 'title', 'price', 'brand']
    search_fields = ['created', 'title', 'price', 'brand']


class PaymentGWAdmin(admin.ModelAdmin):
    list_display = ('type', 'is_selected', 'gateway_callbackurl', 'gateway_webservice', 'gateway_starturl',
                    'gateway_api')
    list_filter = ['type', 'is_selected']
    search_fields = ['type', 'is_selected', 'gateway_callbackurl', 'gateway_webservice', 'gateway_starturl',
                     'gateway_api']


class AdsTgSubsAdmin(admin.ModelAdmin):
    list_display = ('created', 'ids', 'chat_ids', 'keywords', 'phone', 'fields', 'payed', 'locked', 'expired',
                    'verified', 'test_by_user', 'authority', 'payment_time', 'ref_id', 'email', 'telegram', 'comment',
                    'ingroup', 'invalid_tg', 'payment_page')
    list_filter = ('created', 'ids', 'chat_ids', 'keywords', 'phone', 'fields', 'payed', 'locked', 'expired',
                   'verified', 'test_by_user', 'authority', 'payment_time', 'ref_id', 'email', 'telegram', 'comment',
                   'ingroup', 'invalid_tg', 'payment_page')
    search_fields = ('created', 'ids', 'chat_ids', 'keywords', 'phone', 'fields', 'payed', 'locked', 'expired',
                     'verified', 'test_by_user', 'authority', 'payment_time', 'ref_id', 'email', 'telegram', 'comment',
                     'ingroup', 'invalid_tg', 'payment_page')
    actions = [ads_in_tg_sub_verified, ads_in_tg_sub_no_verified, ads_in_tg_sub_locked, ads_in_tg_sub_no_locked,
               ads_in_tg_sub_payed, ads_in_tg_sub_no_payed, ads_in_tg_sub_tested, ads_in_tg_sub_no_tested,
               ads_in_tg_sub_invalid_tg, ads_in_tg_sub_no_invalid_tg]


class MobileVideosAdmin(admin.ModelAdmin):
    list_display = ('created', 'video_ids', 'link', 'tags', 'count', 'verified')
    list_filter = ['created', 'video_ids', 'link', 'tags', 'count', 'verified']
    search_fields = ['created', 'video_ids', 'link', 'tags', 'count', 'verified']


class MobileNewsAdmin(admin.ModelAdmin):
    list_display = ('created', 'ids', 'title', 'view', 'tags', 'link', 'img', 'source_name', 'source_name_file')
    list_filter = ['created', 'ids', 'title', 'view', 'tags', 'link', 'img', 'source_name', 'source_name_file']
    search_fields = ['created', 'ids', 'title', 'view', 'tags', 'short_desc', 'big_desc', 'source_name',
                     'source_name_file']


class MobileStoreRedirectAdmin(admin.ModelAdmin):
    list_display = ('created', 'ids', 'title', 'cats', 'model', 'price', 'store', 'city', 'link')
    list_filter = ['created', 'ids', 'title', 'cats', 'model', 'price', 'store', 'city', 'link']
    search_fields = ['created', 'ids', 'title', 'cats', 'model', 'price', 'store', 'city', 'link']


class MisspelledModelKeywordsAdmin(admin.ModelAdmin):
    list_display = ('created', 'first', 'second', 'third')
    list_filter = ['created', 'first', 'second', 'third']
    search_fields = ['created', 'first', 'second', 'third']


class WebSettingsAdmin(admin.ModelAdmin):
    list_display = ('admin_email', 'item_pg_size', 'allow_ips')
    list_filter = ['admin_email', 'item_pg_size', 'allow_ips']
    search_fields = ['admin_email', 'item_pg_size', 'allow_ips']


admin.site.register(MobilePrice, MobilePriceAdmin)
admin.site.register(MobileItem, MobileItemAdmin)
admin.site.register(MobileBrand, MobileBrandAdmin)
admin.site.register(MobileStore, MobileStoreAdmin)
admin.site.register(MobileSpecs, MobileSpecsAdmin)
admin.site.register(MobileModelDict, MobileModelDictAdmin)
admin.site.register(UserComment, UserCommentAdmin)
admin.site.register(MobileNews, MobileNewsAdmin)
admin.site.register(MobileVideos, MobileVideosAdmin)
admin.site.register(MobileStoreRedirect, MobileStoreRedirectAdmin)
admin.site.register(MisspelledModelKeyword, MisspelledModelKeywordsAdmin)
admin.site.register(WebSettings, WebSettingsAdmin)
