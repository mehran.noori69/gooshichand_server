#!/usr/bin/env python
# -*- coding: utf-8 -*-
from gooshichand.models import MisspelledModelKeyword


class InputChecker:
    text = ''

    def __init__(self, text):
        self.text = text

    @staticmethod
    def remove_underline_in_text(text):
        return text.replace("_", " ")

    @staticmethod
    def spell_correct(text):
        text = ' '.join(text.split())
        text = text.replace('ي'.decode('utf-8'), 'ی'.decode('utf-8'))
        text = text.replace('آ'.decode('utf-8'), 'ا'.decode('utf-8'))
        text = text.replace('ك'.decode('utf-8'), 'ک'.decode('utf-8'))
        text = text.replace('ي'.decode('utf-8'), 'ی'.decode('utf-8'))
        text = text.replace('ئ'.decode('utf-8'), 'ی'.decode('utf-8'))
        text = text.replace('ك'.decode('utf-8'), 'ک'.decode('utf-8'))
        text = text.replace('ﻚ'.decode('utf-8'), 'ک'.decode('utf-8'))
        text = text.replace('ﺑ'.decode('utf-8'), 'ب'.decode('utf-8'))
        text = text.replace('ﺎ'.decode('utf-8'), 'ا'.decode('utf-8'))
        text = text.replace('ﻧ'.decode('utf-8'), 'ن'.decode('utf-8'))
        text = text.replace('ﺧ'.decode('utf-8'), 'خ'.decode('utf-8'))
        text = text.replace('٠'.decode('utf-8'), '۰'.decode('utf-8'))
        text = text.replace('١'.decode('utf-8'), '۱'.decode('utf-8'))
        text = text.replace('٢'.decode('utf-8'), '۲'.decode('utf-8'))
        text = text.replace('٣'.decode('utf-8'), '۳'.decode('utf-8'))
        text = text.replace('۴'.decode('utf-8'), '۴'.decode('utf-8'))
        text = text.replace('۵'.decode('utf-8'), '۵'.decode('utf-8'))
        text = text.replace('۶'.decode('utf-8'), '۶'.decode('utf-8'))
        text = text.replace('٧'.decode('utf-8'), '۷'.decode('utf-8'))
        text = text.replace('٨'.decode('utf-8'), '۸'.decode('utf-8'))
        text = text.replace('٩'.decode('utf-8'), '۹'.decode('utf-8'))
        text = text.replace('إ'.decode('utf-8'), 'ا'.decode('utf-8'))
        text = text.replace('ؤ'.decode('utf-8'), 'و'.decode('utf-8'))
        text = text.replace('ۀ'.decode('utf-8'), 'ه'.decode('utf-8'))
        text = text.replace('أ'.decode('utf-8'), 'ا'.decode('utf-8'))
        text = text.replace('%'.decode('utf-8'), '٪'.decode('utf-8'))
        text = text.replace('?'.decode('utf-8'), '؟'.decode('utf-8'))
        text = text.replace('ً'.decode('utf-8'), ''.decode('utf-8'))
        text = text.replace('ٌ'.decode('utf-8'), ''.decode('utf-8'))
        text = text.replace('ٍ'.decode('utf-8'), ''.decode('utf-8'))
        text = text.replace('َ'.decode('utf-8'), ''.decode('utf-8'))
        text = text.replace('ُ'.decode('utf-8'), ''.decode('utf-8'))
        text = text.replace('ِ'.decode('utf-8'), ''.decode('utf-8'))
        text = text.replace(u"\uFEFF", " ")  # removing zero width non breakable space character
        text = text.replace(u"\u200F", "")  # removing rtl mark character
        text = text.replace(u"\u200E", "")  # removing ltr mark character
        text = text.replace(u"\u200D", "")  # removing zero width joiner
        text = text.replace(u"\u200B", " ")  # removing zero width space
        text = text.replace(u"\u200A", " ")  # removing hair space
        text = text.replace(u"\u200C", " ")  # removing zero width non joiner space character
        text = text.replace(u"\u00A0", " ")  # removing non break space
        text = text.replace(u"\u0640", "")  # removing arabic tatweel
        text = ' '.join(text.split())
        text = text.replace('۱'.decode('utf-8'), '1')
        text = text.replace('۲'.decode('utf-8'), '2')
        text = text.replace('۳'.decode('utf-8'), '3')
        text = text.replace('۴'.decode('utf-8'), '4')
        text = text.replace('۵'.decode('utf-8'), '5')
        text = text.replace('۶'.decode('utf-8'), '6')
        text = text.replace('۷'.decode('utf-8'), '7')
        text = text.replace('۸'.decode('utf-8'), '8')
        text = text.replace('۹'.decode('utf-8'), '9')
        text = text.replace('۰'.decode('utf-8'), '0')

        text = text.replace('/', '')
        # text = text.replace('.', '')
        text = ' '.join(text.split())

        text = text.lower()

        mjk = MisspelledModelKeyword.objects.all().order_by('created')
        for mj in mjk:
            text = text.replace(mj.first.replace('-', ' '), mj.second.replace('-', ' '))
            text = ' '.join(text.split())
            text = text.replace('  ', ' ')

        text = ' '.join(text.split())
        text = text.replace('  ', ' ')
        return text

    @staticmethod
    def check_input_size(text, size):
        if len(text) > size:
            return True
        return False


