#!/usr/bin/env python
# -*- coding: utf-8 -*-
from datetime import timedelta
from unittest import skipIf

from django.core.urlresolvers import reverse
from django.test import TestCase
from django.utils import timezone

from website import views


class YourTestClass(TestCase):
    @classmethod
    def setUpTestData(cls):
        # print("setUpTestData: Run once to set up non-modified data for all class methods.")
        pass

    def setUp(self):
        # print("setUp: Run once for every test method to setup clean data.")
        pass

    @skipIf(True, "Don't want to test")
    def test_false_is_false(self):
        self.assertFalse(False)
        self.assertEqual(1 + 1, 2)
        self.assertIn("salam", "salam khobi")

    @skipIf(True, "Don't want to test")
    def test_homepage(self):
        url = reverse(views.Home)
        print(url)
        resp = self.client.get(url)
        self.assertEqual(resp.status_code, 200)

    @skipIf(True, "Don't want to test")
    def test_get_mobile_brand_visible_cats(self):
        items = views.get_mobile_brand_visible_cats()
        print(len(items))
        self.assertEqual(len(items), 8)

    @skipIf(True, "Don't want to test")
    def test_get_title_for_search_suggestion(self):
        items = views.get_title_for_search_suggestion()
        self.assertEqual(len(items), 1348)

    @skipIf(True, "Don't want to test")
    def test_get_brands_for_search_suggestion(self):
        items = views.get_brands_for_search_suggestion()
        print(len(items))
        self.assertEqual(len(items), 25)

    @skipIf(True, "Don't want to test")
    def test_get_stores_for_search_suggestion(self):
        items = views.get_stores_for_search_suggestion()
        array = []
        pre_array = ['\xd8\xaa\xd8\xa8\xd9\x84\xd8\xaa \xd9\x81\xd9\x88\xd9\x86', '\xd8\xa8\xd8\xa7\xd9\x85\xdb\x8c\xd9\x84\xd9\x88', '\xd9\x85\xd9\x88\xd8\xa8\xd8\xa7\xdb\x8c\xd9\x84 \xd8\xa8\xd8\xa7\xd9\x85\xd8\xa7', '\xd8\xa8\xd8\xa7\xd9\x86\xd9\x87 \xd8\xaa\xda\xa9', '\xd9\x86\xda\xaf\xdb\x8c\xd9\x86 \xd8\xae\xd8\xa7\xd9\x88\xd8\xb1\xd9\x85\xdb\x8c\xd8\xa7\xd9\x86\xd9\x87', '19 \xda\xa9\xd8\xa7\xd9\x84\xd8\xa7', '\xd8\xa8\xd8\xa7\xd9\x86\xd9\x87 \xda\xa9\xd8\xa7\xd9\x84\xd8\xa7', '\xd8\xb9\xd9\x85\xd9\x88 \xdb\x8c\xd8\xa7\xd8\xaf\xda\xaf\xd8\xa7\xd8\xb1', '\xd8\xa8\xd9\x87\xdb\x8c\xd9\x86\xd9\x87 \xd9\xbe\xd8\xb1\xd8\xaf\xd8\xa7\xd8\xb2\xd8\xb4', '\xd8\xaf\xd8\xa7\xd8\xaa\xdb\x8c\xd8\xb3 \xd8\xa2\xd9\x86\xd9\x84\xd8\xa7\xdb\x8c\xd9\x86', '\xd8\xaf\xdb\x8c\xd8\xac\xdb\x8c \xda\xa9\xd8\xa7\xd9\x84\xd8\xa7', '\xd8\xa2\xd8\xaa\xd8\xb1\xd8\xa7\xd9\x85\xd8\xa7\xd8\xb1\xd8\xaa', '\xd9\x85\xd9\x82\xd8\xaf\xd8\xa7\xd8\xaf \xd8\xa2\xdb\x8c \xd8\xaa\xdb\x8c', '\xda\xa9\xd8\xa7\xd9\x84\xd8\xa7\xd8\xa8\xd8\xa7\xd9\x84\xd8\xa7', '\xd8\xa2\xd9\x84 \xd8\xaf\xdb\x8c\xd8\xac\xdb\x8c\xd8\xaa\xd8\xa7\xd9\x84', '\xda\xaf\xd9\x88\xd8\xb4\xdb\x8c \xd8\xb4\xd8\xa7\xd9\xbe', '\xd8\xaa\xd9\x8f\xd9\x85\xd9\x86\xda\xa9']
        for item in items:
            array.append(item)
        print(len(items))
        self.assertEqual(len(items), 17)
        self.assertEqual(array, pre_array)

    @skipIf(True, "Don't want to test")
    def test_get_cities_for_search_suggestion(self):
        items = views.get_cities_for_search_suggestion()
        print(len(items))
        self.assertEqual(len(items), 5)

    @skipIf(True, "Don't want to test")
    def test_detect_remove_brand_from_fullname(self):
        brand = "Apple"
        model = 'اپل ایفون ایکس'.decode('utf-8')
        brand, model = views.detect_remove_brand_from_fullname(brand, model)
        self.assertEqual(brand, "Apple")

    @skipIf(True, "Don't want to test")
    def test_spell_correct(self):
        model = 'آپل ایفون ایکس'.decode('utf-8')
        model = views.spellcorrect(model)
        self.assertEqual(model, 'apple iphone ایکس'.decode('utf-8'))

    @skipIf(True, "Don't want to test")
    def test_get_mobile_news_last_week(self):
        start_day = timezone.now() - timedelta(days=7)
        news = views.get_mobile_news(start_day, 0, 5, '', '-created')
        newslength = len(news)
        self.assertEqual(newslength, 5)

    @skipIf(True, "Don't want to test")
    def test_define_title_desc_keyword_author_footer_of_page(self):
        title, desc, author, keywords, footer = views.\
            define_title_desc_keyword_author_footer_of_page('دیجیکالا'.decode('utf-8'), '', '')
        print(title)
        print(desc)
        print(author)
        print(keywords)
        print(footer)
        self.assertEqual(5, 5)

    @skipIf(True, "Don't want to test")
    def test_create_spec_link_for_items(self):
        cats = 'apple'
        model = 'iphone x'
        link = views.create_spec_link_for_items(cats, model)
        print(link)
        self.assertEqual(5, 5)

    @skipIf(True, "Don't want to test")
    def test_put_underline_in_text(self):
        model = 'iphone x'
        model = views.put_underline_in_text(model)
        print(model)
        self.assertEqual(model, 'iphone_x')

    @skipIf(True, "Don't want to test")
    def test_remove_underline_in_text(self):
        model = 'iphone_x'
        model = views.remove_underline_in_text(model)
        print(model)
        self.assertEqual(model, 'iphone x')
