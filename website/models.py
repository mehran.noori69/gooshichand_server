# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.utils.translation import ugettext_lazy as _


class WebsiteAddr(models.Model):
    time = models.DateTimeField(auto_now_add=True)
    address = models.CharField(blank=True, max_length=1000)
    ipaddress = models.CharField(blank=True, max_length=50)
    browser = models.CharField(blank=True, max_length=1000)

    class Meta:
        ordering = ('-time',)
        verbose_name = _("Website Address")
        verbose_name_plural = _("Website Address")
