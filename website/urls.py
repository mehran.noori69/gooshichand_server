from django.conf.urls import url

from . import views

urlpatterns = [

    # website pages
    url(r'^$', views.Home.as_view(), name='homepage'),
    url(r'^web/', views.GetToHomeRedirect.as_view()),
    url(r'^items/$', views.GetItems.as_view(), name='mobileitems'),
    url(r'^filterbyspec/$', views.MobileFilterBySpec.as_view(), name='mobilefilterbyspec'),
    # there must not be a slash in title section of url,
    # must have at least one title in url
    # if we have a slash after title then there must not be anything after the slash
    url(r'^item/(?P<title>[^/]+)/$', views.MobileSpec.as_view(), name='mobilespec'),
    url(r'^compare/$', views.MobileSpecCompare.as_view(), name='mobilespeccompare'),
    url(r'^newslist/$', views.MobileNewsList.as_view(), name='newslist'),
    url(r'^news/?(?P<ids>[^/]+)/?(?P<title>[^/]+)/', views.MobileNewsDetail.as_view(), name='newsdetail'),
    url(r'^redirect/(?P<ids>[^/]+)/$', views.GetToStoreRedirect.as_view(), name='gotostorewebsite'),

    # increase item video played
    url(r'^itemvideoplayed/$', views.SpecVideo.as_view(), name='playspecvideo'),

    # contact us
    url(r'^contactus/$', views.ContactUsPage.as_view(), name='contactus'),
    url(r'^sendfeedback/$', views.ContactUsPageSendFeedback.as_view(), name='sendfeedback'),
    url(r'^commentonspec/$', views.CommentOnSpec.as_view(), name='commentonspec'),
    url(r'^websiteaddress/$', views.WebsiteAddress.as_view(), name='registerwebsite'),

    # Cat, Brand, model adjusting
    url(r'^addmodeldict/$', views.AddMobileDict.as_view(), name='addmodeldict'),
    url(r'^setitemsbrand/$', views.SetItemBrand.as_view(), name='setitemsbrand'),
    url(r'^setitemsmodels/$', views.ItemModelAlt.as_view(), name='setitemsmodels'),
    url(r'^setitemsmodel/$', views.ItemModel.as_view(), name='setitemsmodel'),

    # item and spec save and del
    url(r'^testmobilesaveitem/$', views.TestMobileItemSave.as_view(), name='TestMobileItemSave'),
    url(r'^mobilesaveitem/$', views.MobileItemSaveItem.as_view(), name='MobileItemSaveItem'),
    url(r'^mobiledelitem/$', views.MobileItemDelItem.as_view(), name='MobileItemDelItem'),
    url(r'^specsaveitem/$', views.MobileSpecSaveItem.as_view(), name='MobileSpecSaveItem'),
    url(r'^specchangeimg/$', views.MobileSpecChangeImage.as_view(), name='MobileSpecChangeImage'),
    url(r'^specdelitem/$', views.MobileSpecDeleteItem.as_view(), name='MobileSpecDeleteItem'),
    url(r'^getitemsbrand/$', views.MobileBrandList.as_view(), name='mobilebrandlist'),
    url(r'^getitemsspec/$', views.MobileSpecList.as_view(), name='mobilespeclist'),
    url(r'^setminprice/$', views.MinPrice.as_view(), name='setminprice'),

    # news save and del
    url(r'^mobilenewssaveitem/$', views.MobileNewsItemSave.as_view(), name='mobilenewssave'),
    url(r'^mobilenewsdelitem/$', views.MobileNewsItemDelete.as_view(), name='mobilenewsdelete'),

]
