#!/usr/bin/env python
# -*- coding: utf-8 -*-
import json
import operator
import shutil
from datetime import datetime, timedelta

import requests
from django.core.exceptions import ObjectDoesNotExist
from django.http import HttpResponse, JsonResponse, HttpResponseRedirect
from django.utils import timezone
from django.utils.http import urlunquote
from django.views.generic import TemplateView, RedirectView
from rest_framework import status
from rest_framework.views import APIView

from gooshichand.models import MobileItem, MobileBrand, MobileSpecs, MobileStore, \
    MobileModelDict, MobilePrice, UserComment, MobileNews, MobileStoreRedirect, \
    MobileVideos
from gooshichand.settings import PAGINATION_SIZE
from website.InputChecker import InputChecker
from website.Utility import Utility
from website.models import WebsiteAddr


class MobileSpec(TemplateView):
    template_name = "gc/item.html"
    item = ''
    items = []
    mvs = []
    cats = []
    brands = []
    comments = []
    ck = ''
    news = []
    newslength = ''
    prices = []
    priceslen = []
    newitems = []
    cameraitems = []
    cpuitems = []
    storageitems = []
    inchitems = []
    titles = []
    title = ''
    desc = ''
    author = ''
    keywords = ''
    years = ['2015', '2016', '2017', ]
    oss = ['Android', 'iOS', 'Windows', 'Tizen', 'Symbian']
    fcameras = [0, 2, 5, 8, 13, 20, 24]
    bcameras = [0, 2, 5, 8, 13, 20, 24]
    storages = [0, 1, 4, 8, 16, 32, 64, 128, 256, 512]
    rams = [0, 0.5, 1, 2, 4, 8, 16]
    freqs = [0, 500, 1000, 1500, 2000, 2500, 3000, 3500]
    cores = [0, 1, 2, 4, 8, 16]
    inchs = [0, 4, 5, 6, 8, 10, 13, 20]
    pds = [0, 100, 200, 300, 400, 500, 600, 900]

    def dispatch(self, *args, **kwargs):
        title = kwargs['title']
        self.ck = title
        title = Utility.remove_underline_in_text(title)
        Utility.registerrequest(self.request, 'item-spec-' + title)
        self.item = MobileSpecs.objects.filter(title__iexact=title)[0]
        self.cats = MobileBrand.get_visible_mobile_brand()
        self.brands = MobileSpecs.get_mobile_brand()
        self.item.rel_date = self.item.release_date.strftime("%B") + '-' + str(self.item.release_date.year)

        self.item.tit = self.item.title_without_brand.replace(" ", "_")
        self.prices = MobileItem.get_mobile_item_with_model(self.item.title_without_brand).order_by('price')[0:2]
        for price in self.prices:
            price.phone = MobileStore.get_mobile_stores_phone(price.store)
        self.priceslen = len(self.prices)

        temps = MobileVideos.objects.filter(tags__icontains=self.item.title)
        self.item.has_video = False
        self.mvs = []
        for mv in temps:
            tags = mv.tags.split("//")
            for tag in tags:
                if self.item.title == tag:
                    self.item.has_video = True
                    self.mvs.append(mv)

        self.newitems = Utility.get_mobile_spec('filter', 0, 6, self.item.path, '-release_date')
        for obj in self.newitems:
            Utility.get_url_prices_minprice_of_items(obj)
        self.cameraitems = Utility.get_mobile_spec('filter', 0, 6, self.item.path, '-camera_back')
        for obj in self.cameraitems:
            Utility.get_url_prices_minprice_of_items(obj)
        self.cpuitems = Utility.get_mobile_spec('filter', 0, 6, self.item.path, '-cpu_freq')
        for obj in self.cpuitems:
            Utility.get_url_prices_minprice_of_items(obj)
        self.storageitems = Utility.get_mobile_spec('filter', 0, 6, self.item.path, '-storage')
        for obj in self.storageitems:
            Utility.get_url_prices_minprice_of_items(obj)
        self.inchitems = Utility.get_mobile_spec('filter', 0, 6, self.item.path, '-inch')
        for obj in self.inchitems:
            Utility.get_url_prices_minprice_of_items(obj)

        footer, temp = Utility.create_footer_based_brand(self.item.path)
        for f in temp:
            self.items.append(f[0].replace(" ", "_"))

        self.title = "گوشی " + self.item.title.replace("(", "").replace(")", "").encode(
            "utf-8") + " چند؟ | ارزانترین قیمت و مشخصات"
        self.desc = "مشخصات " + self.item.title.replace("(", "").replace(")", "").encode(
            "utf-8") + " و قیمت آن در بیش از 30 فروشگاه های مختلف با اطلاعات تماس فروشندگان"
        self.author = ""
        self.keywords = ""
        for f in footer:
            self.keywords += f + ', '
        self.item.urltitle = self.item.title_with_underline
        if self.item.gallery is not None:
            self.item.images = self.item.gallery.split(',')

        # calculate mobile price to draw a scale
        # scales = MobilePrice.objects.filter(title=extract_model_from_fullname(self.item),
        #  brand=self.item.path).order_by('created')
        # self.item.price_label = list(t[0].day for t in scales.values_list('created').distinct())
        # self.item.price_value = list(t[0] / 10 for t in scales.values_list('price').distinct())

        titles = MobileSpecs.get_title_for_search_suggestion()
        self.titles = json.dumps(titles)

        self.comments = UserComment.get_user_comments_by_section(self.item.title)

        start_day = timezone.now() - timedelta(days=7)
        self.news = MobileNews.get_mobile_news_after_date_with_tag(start_day, 0, 10, self.item.path, '?')
        self.newslength = len(self.news)
        if self.newslength > 0:
            self.news = self.news.first()

        return super(MobileSpec, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(MobileSpec, self).get_context_data(**kwargs)
        context['item'] = self.item
        context['items'] = self.items
        context['mvs'] = self.mvs
        context['cats'] = self.cats
        context['brands'] = self.brands
        context['comments'] = self.comments
        context['ck'] = self.ck
        context['news'] = self.news
        context['newslength'] = self.newslength

        context['inchs'] = self.inchs
        context['pds'] = self.pds
        context['years'] = self.years
        context['oss'] = self.oss
        context['storages'] = self.storages
        context['rams'] = self.rams
        context['bcameras'] = self.bcameras
        context['fcameras'] = self.fcameras
        context['freqs'] = self.freqs
        context['cores'] = self.cores
        context['prices'] = self.prices
        context['priceslen'] = self.priceslen

        context['newitems'] = self.newitems
        context['cameraitems'] = self.cameraitems
        context['cpuitems'] = self.cpuitems
        context['storageitems'] = self.storageitems
        context['inchitems'] = self.inchitems

        context['titles'] = self.titles
        context['title'] = self.title
        context['desc'] = self.desc
        context['author'] = self.author
        context['keywords'] = self.keywords
        return context


class MobileFilterBySpec(TemplateView):
    template_name = "gc/finder.html"
    items = []
    length = ''
    cats = []
    brands = []
    inchs = []
    pds = []
    years = []
    oss = []
    storages = []
    rams = []
    bcameras = []
    fcameras = []
    freqs = []
    cores = []
    titles = []
    title = ''
    desc = ''
    author = ''
    keywords = []
    footer = ''

    def dispatch(self, *args, **kwargs):
        brand = Utility.remove_underline_in_text(self.request.GET.get('brand', ''))
        mininch = self.request.GET.get('mininch', '0')
        maxinch = self.request.GET.get('maxinch', '20')
        minpd = self.request.GET.get('minpd', '0')
        maxpd = self.request.GET.get('maxpd', '1000')
        minyear = self.request.GET.get('minyear', '2014')
        maxyear = self.request.GET.get('maxyear', '2019')
        minram = self.request.GET.get('minram', '0')
        maxram = self.request.GET.get('maxram', '10000')
        minstorage = self.request.GET.get('minstorage', '0')
        maxstorage = self.request.GET.get('maxstorage', '1000')
        mincore = self.request.GET.get('mincore', '0')
        maxcore = self.request.GET.get('maxcore', '100')
        minfreq = self.request.GET.get('minfreq', '0')
        maxfreq = self.request.GET.get('maxfreq', '10000')
        minbcamera = self.request.GET.get('minbcamera', '0')
        maxbcamera = self.request.GET.get('maxbcamera', '32')
        minfcamera = self.request.GET.get('minfcamera', '0')
        maxfcamera = self.request.GET.get('maxfcamera', '32')
        os = self.request.GET.get('os', '')
        page = self.request.GET.get('page', '0')
        sort = Utility.remove_underline_in_text(self.request.GET.get('sort', 'سال'.decode('utf-8')))
        sortdir = self.request.GET.get('dir', '')
        if InputChecker(brand).check_input_size(brand, 15) or InputChecker(brand).check_input_size(mininch, 5) or \
                InputChecker(brand).check_input_size(maxinch, 5) or InputChecker(brand).check_input_size(minpd, 10) or \
                InputChecker(brand).check_input_size(maxpd, 10) or InputChecker(brand).check_input_size(minyear, 5) or \
                InputChecker(brand).check_input_size(maxyear, 5) or InputChecker(brand).check_input_size(minram, 5) or \
                InputChecker(brand).check_input_size(maxram, 5) or \
                InputChecker(brand).check_input_size(minstorage, 5) or \
                InputChecker(brand).check_input_size(maxstorage, 5) or \
                InputChecker(brand).check_input_size(mincore, 5) or \
                InputChecker(brand).check_input_size(maxcore, 5) or \
                InputChecker(brand).check_input_size(minfreq, 10) or \
                InputChecker(brand).check_input_size(maxfreq, 10) or \
                InputChecker(brand).check_input_size(minbcamera, 5) or \
                InputChecker(brand).check_input_size(maxbcamera, 5) or \
                InputChecker(brand).check_input_size(minfcamera, 5) or \
                InputChecker(brand).check_input_size(maxfcamera, 5) or InputChecker(brand).check_input_size(os, 10) or \
                InputChecker(brand).check_input_size(page, 2) or InputChecker(brand).check_input_size(sort, 20) or \
                InputChecker(brand).check_input_size(sortdir, 20):
            return Utility.unauthorized()

        org_brand, org_mininch, org_maxinch, org_minpd, org_maxpd, org_minyear, org_maxyear, org_minram,\
            org_maxram, org_minstorage, org_maxstorage, org_mincore, org_maxcore, org_minfreq, org_maxfreq, org_minbc,\
            org_maxbc, org_minfc, org_maxfc, org_os, org_page, org_sort, org_sortdir =\
            brand, mininch, maxinch, minpd, maxpd, minyear, maxyear, minram, maxram, minstorage, maxstorage, mincore,\
            maxcore, minfreq, maxfreq, minbcamera, maxbcamera, minfcamera, maxfcamera, os, page, sort, sortdir

        if sortdir == 'صعودی'.decode('utf-8'):
            dirs = ''
        else:
            dirs = '-'
        index = int(page) * 21

        minyear = minyear + '-01-01'
        minyear = datetime.strptime(minyear, '%Y-%m-%d')
        maxyear = maxyear + '-12-31'
        maxyear = datetime.strptime(maxyear, '%Y-%m-%d')
        items = Utility.get_mobile_spec('all', -1, -1, '', '-release_date')
        items = items.filter(path__icontains=brand, inch__gte=mininch, inch__lte=maxinch, os__icontains=os,
                             memory__gte=minram, memory__lte=maxram, storage__gte=minstorage, storage__lte=maxstorage,
                             cpu_core__gte=mincore, cpu_core__lte=maxcore, camera_back__gte=minbcamera,
                             camera_back__lte=maxbcamera, camera_front__gte=minfcamera, camera_front__lte=maxfcamera,
                             cpu_freq__gte=minfreq, cpu_freq__lte=maxfreq, pd__gte=minpd, pd__lte=maxpd,
                             release_date__gte=minyear, release_date__lte=maxyear)

        sortitem = {'سال'.decode('utf-8'): 'release_date', 'سایز صفحه'.decode('utf-8'): 'inch',
                    'پردازنده'.decode('utf-8'): 'cpu_freq', 'رَم'.decode('utf-8'): 'memory',
                    'حافظه داخلی'.decode('utf-8'): 'storage', 'دوربین عقب'.decode('utf-8'): 'camera_back',
                    'دوربین جلو'.decode('utf-8'): 'camera_front'}
        items = items.order_by(dirs + sortitem[sort])

        self.items = items[index:index + PAGINATION_SIZE]
        self.length = len(self.items)
        for item in self.items:
            Utility.get_url_prices_minprice_of_items(item)
            item.year = item.release_date.year

        self.cats = MobileBrand.get_visible_mobile_brand()
        self.brands = [brand.encode("utf8") for brand in
                       MobileSpecs.objects.values_list('path', flat=True).distinct().order_by('path')]
        self.years = ['2015', '2016', '2017', '2018', '2019']
        self.oss = ['Android', 'iOS', 'Windows', 'Tizen', 'Symbian']
        self.fcameras = [0, 2, 5, 8, 13, 20, 24]
        self.bcameras = [0, 2, 5, 8, 13, 20, 24]
        self.storages = [0, 1, 4, 8, 16, 32, 64, 128, 256, 512]
        self.rams = [0, 0.5, 1, 2, 4, 8, 16]
        self.freqs = [0, 500, 1000, 1500, 2000, 2500, 3000, 3500]
        self.cores = [0, 1, 2, 4, 8, 16]
        self.inchs = [0, 4, 5, 6, 8, 10, 13, 20]
        self.pds = [0, 100, 200, 300, 400, 500, 600, 900]

        self.footer, temp = Utility.create_footer_based_brand(brand)

        self.title = "گوشی چند ؟ در جریان قیمت روز گوشی موبایل باشید"
        self.desc = "قیمت روزانه گوشی های اپل، سامسونگ، هواوی، سونی و... را در 30 فروشگاه" \
                    " در شهرهای مختلف هر روز دنبال کنید و براساس مشخصات مورد نظر خود گوشی خود را بیابید "
        self.author = ""
        self.keywords = ""

        titles = MobileSpecs.get_title_for_search_suggestion()
        self.titles = json.dumps(titles)

        Utility.registerrequest(self.request, 'advancedsearch-brand=' + org_brand + '&mininch=' + org_mininch +
                                '&maxinch=' + org_maxinch + '&minpd=' + org_minpd + '&maxpd=' + org_maxpd +
                                '&minyear=' + org_minyear + '&maxyear=' + org_maxyear + '&minram=' + org_minram +
                                '&maxram=' + org_maxram + '&minstorage=' + org_minstorage + '&maxstorage=' +
                                org_maxstorage + '&mincore=' + org_mincore + '&maxcore=' + org_maxcore + '&minfreq=' +
                                org_minfreq + '&maxfreq=' + org_maxfreq + '&minbcamera=' + org_minbc + '&maxbcamera=' +
                                org_maxbc + '&minfcamera=' + org_minfc + '&maxfcamera=' + org_maxfc + '&os=' + org_os +
                                '&page=' + org_page + '&sort=' + org_sort + '&sortdir=' + org_sortdir + '&result=' +
                                str(self.length))
        return super(MobileFilterBySpec, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(MobileFilterBySpec, self).get_context_data(**kwargs)
        context['specs'] = self.items
        context['length'] = self.length
        context['cats'] = self.cats
        context['brands'] = self.brands
        context['inchs'] = self.inchs
        context['pds'] = self.pds
        context['years'] = self.years
        context['oss'] = self.oss
        context['storages'] = self.storages
        context['rams'] = self.rams
        context['bcameras'] = self.bcameras
        context['fcameras'] = self.fcameras
        context['freqs'] = self.freqs
        context['cores'] = self.cores
        context['titles'] = self.titles
        context['title'] = self.title
        context['desc'] = self.desc
        context['author'] = self.author
        context['keywords'] = self.keywords
        context['footer'] = self.footer
        return context


class MobileSpecCompare(TemplateView):
    template_name = "gc/compare.html"
    items = []
    cats = []
    length = ''
    footer = ''
    titles = ''
    title = ''
    desc = ''
    author = ''
    keywords = ''

    def dispatch(self, *args, **kwargs):
        models = self.request.GET.get('models', None)
        if InputChecker(models).check_input_size(models, 200):
            return Utility.unauthorized()
        Utility.registerrequest(self.request, 'items-compare-' + models)
        models = models.split(",")
        headers = [['rel_date', 'زمان انتشار:'], ['os', 'سیستم عامل:'], ['inch', 'صفحه نمایش:(اینچ)'],
                   ['data', 'شبکه:'],
                   ['back_camera', 'دوربین عقب:(مگاپیکسل)'], ['front_camera', 'دوربین جلو:(مگاپیکسل)'],
                   ['cpu_freq', 'فرکانس پردازنده:(هرتز)'],
                   ['cpu_core', 'تعداد هسته: '], ['memory', 'حافظه (RAM) رَم:(گیگ)'], ['storage', 'حافظه داخلی:(گیگ)'],
                   ['dims', 'ابعاد:'], ['weight', 'وزن:(گرم)'], ['reso', 'وضوح:'], ['pd', 'تراکم صفحه:'],
                   ['stbratio', 'نسبت صفحه به بدنه:(درصد)'], ['cpu_chip', 'چیپ پردازنده:'],
                   ['cpu_processor', 'پردازنده:'],
                   ['gpu', 'پردازنده گرافیکی:'], ['battery_capacity', 'ظرفیت باتری:(mAh)'],
                   ['battery_type', 'مدل باتری:'],
                   ['bluetooth', 'ورژن بلوتوث:'], ['title', 'مدل:'], ['img', 'عکس:'], ['price', 'قیمت:(ریال)'],
                   ['link', 'فروشندگان:']]
        self.items = []
        self.items.append(headers)

        for model in models:
            ms = MobileSpecs.objects.filter(title=model.replace("_", " "))
            if len(ms) > 0:
                item = ms[0]
                item.tit = item.title_without_brand.replace(" ", "_")
                item.price = 'نامشخص'.decode('utf-8')
                temp = MobileItem.objects.filter(model=item.title_without_brand).order_by('price')
                if len(temp) > 0:
                    temp = temp[0]
                    item.price = temp.price
                item.seller = '<a href="/items/?model=' + item.title.replace(' ', '_') + '"> ' \
                              + 'لیست فروشندگان'.decode('utf-8') + ' </a>'
                obj = [['rel_date', item.show_date], ['os', item.os], ['inch', item.inch], ['data', item.data],
                       ['back_camera', item.camera_back], ['front_camera', item.camera_front],
                       ['cpu_freq', item.cpu_freq], ['cpu_core', item.cpu_core], ['memory', item.memory],
                       ['storage', item.storage], ['dims', item.dims], ['weight', item.weight], ['reso', item.reso],
                       ['pd', item.pd], ['stbratio', item.stbratio], ['cpu_chip', item.cpu_chip],
                       ['cpu_processor', item.cpu_processor], ['gpu', item.gpu],
                       ['battery_capacity', item.battery_capacity], ['battery_type', item.battery_type],
                       ['bluetooth', item.bluetooth], ['title', item.title], ['img', item.img], ['price', item.price],
                       ['link', item.seller]]
                self.items.append(obj)

        titles = MobileSpecs.get_title_for_search_suggestion()
        self.titles = json.dumps(titles)
        self.cats = MobileBrand.get_visible_mobile_brand()
        self.title, self.desc, self.author, self.keywords, self.footer = \
            "گوشی چند ؟ در جریان قیمت روز گوشی موبایل باشید", "", "", "", ""
        self.length = len(self.items)
        return super(MobileSpecCompare, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(MobileSpecCompare, self).get_context_data(**kwargs)
        context['models'] = self.items
        context['cats'] = self.cats
        context['length'] = self.length
        context['footer'] = self.footer
        context['titles'] = self.titles
        context['title'] = self.title
        context['desc'] = self.desc
        context['author'] = self.author
        context['keywords'] = self.keywords
        return context


class GetToHomeRedirect(RedirectView):
    def get_redirect_url(self, *args, **kwargs):
        self.url = '/'
        return super(GetToHomeRedirect, self).get_redirect_url(self, *args, **kwargs)


class Home(TemplateView):
    template_name = "gc/homepage.html"
    cats = []
    titles = []
    title = ''
    desc = ''
    author = ''
    keywords = ''
    items = []

    def dispatch(self, *args, **kwargs):
        Utility.get_elements(self.request, 'home-page')
        self.cats = MobileBrand.get_visible_mobile_brand()
        titles = MobileSpecs.get_title_for_search_suggestion()
        for title in titles:
            self.items.append(title.replace(" ", "_"))
        self.titles = json.dumps(titles)
        self.title = "گوشی چند ؟ در جریان قیمت روز گوشی موبایل باشید"
        self.desc = "قیمت روزانه گوشی های اپل، سامسونگ، هواوی، سونی و... را در 30 فروشگاه در شهرهای مختلف هر روز " \
                    "دنبال کنید و براساس مشخصات مورد نظر خود گوشی خود را بیابید "
        self.author = ""
        self.keywords = ""
        return super(Home, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(Home, self).get_context_data(**kwargs)
        context['cats'] = self.cats
        context['titles'] = self.titles
        context['title'] = self.title
        context['desc'] = self.desc
        context['author'] = self.author
        context['keywords'] = self.keywords
        context['items'] = self.items
        return context


class GetItems(TemplateView):
    template_name = "gc/items.html"
    items = None
    cats = []
    length = 0
    brands = []
    cities = []
    stores = []
    title = ''
    desc = ''
    author = ''
    keywords = []
    footer = []
    item_specs = []
    titles = []
    newslength = 0
    news = []
    mvs = []

    def dispatch(self, *args, **kwargs):
        Utility.get_elements(self.request, 'home-page')
        model = Utility.remove_underline_in_text(self.request.GET.get('model', ''))
        minprice = self.request.GET.get('minprice', 0)
        maxprice = self.request.GET.get('maxprice', 1000000000)
        store = Utility.remove_underline_in_text(self.request.GET.get('store', ''))
        brand = Utility.remove_underline_in_text(self.request.GET.get('brand', ''))
        city = Utility.remove_underline_in_text(self.request.GET.get('city', ''))
        page = self.request.GET.get('page', '0')

        if InputChecker(model).check_input_size(model, 100) or InputChecker(model).check_input_size(store, 50) or \
                InputChecker(model).check_input_size(brand, 20) or InputChecker(model).check_input_size(city, 15) or \
                InputChecker(model).check_input_size(page, 2):
            return Utility.unauthorized()

        org_model, org_minprice, org_maxprice, org_store, org_brand, org_city, org_page = model, minprice, maxprice,\
            store, brand, city, page
        model = InputChecker(model).spell_correct(model)

        brand, model = Utility.detect_remove_brand_from_fullname(brand, model)

        start_day = timezone.now() - timedelta(days=7)
        self.news = MobileNews.get_mobile_news_after_date_with_tag(start_day, 0, 5, '', '-created')
        self.newslength = len(self.news)

        self.title, self.desc, self.author, self.keywords, self.footer = \
            Utility.define_title_desc_keyword_author_footer_of_page(store, model, brand)

        self.mvs = Utility.get_videos_in_items_page()

        if len(brand) > 0:
            brand = brand.split(" - ")[0]
        self.items = MobileItem.objects.filter(price__gte=int(minprice)).filter(price__lte=int(maxprice),
                                                                                city__icontains=city,
                                                                                store__icontains=store,
                                                                                cats__icontains=brand).order_by('price')
        if len(model) > 0:
            mmds = MobileModelDict.objects.all()
            models_array = []
            for mmd in mmds:
                if model.lower() in mmd.alts.lower().replace('//', ' // '):
                    models_array.append(mmd.name)
                if model.lower() in mmd.name.lower():
                    models_array.append(mmd.name)
            # detect the other names of a model in mobiles model dict
            self.items = self.items.filter(model__in=models_array)
            if len(self.items) < 1:
                self.items = MobileItem.objects.filter(title__icontains=model)  # if there is no alt name for a model,
                #  just search its title

        for item in self.items:
            item.hasconfig = False
            item.phone = MobileStore.get_mobile_stores_phone(item.store)
            if item.cats is not None and item.model is not None:
                its = MobileSpecs.objects.filter(title=item.cats + ' ' + item.model)
                item.itslen = len(its)
                item.org_img = item.img
                if len(its) > 0:
                    item.hasconfig = True
                    item.release_date = str(its[0].release_date)
                    item.urltitle = its[0].title_with_underline
                    item.org_img = its[0].img
                    item.spec_link = its[0].link_of_page
                else:
                    item.release_date = '2014-01-01 00:00:00+00:00'
        self.items = sorted(self.items, key=operator.attrgetter('release_date'), reverse=True)
        index = int(page) * 21
        self.items = self.items[index:index + PAGINATION_SIZE]
        self.length = len(self.items)

        self.cats = MobileBrand.get_visible_mobile_brand()
        self.brands = MobileBrand.get_brands_for_search_suggestion()
        self.stores = MobileItem.get_stores_for_search_suggestion()
        self.cities = MobileItem.get_cities_for_search_suggestion()

        Utility.registerrequest(self.request, 'model=' + org_model + '&minprice=' + str(org_minprice) + '&maxprice=' +
                                str(org_maxprice) + '&store=' + org_store + '&brand=' + org_brand + '&city=' +
                                org_city + '&page=' + org_page + '&result=' + str(self.length))

        self.item_specs = []
        # if there is no mobile item available in store, we need to show its Spec page
        if self.length == 0 and len(model) > 0:
            mmds = MobileModelDict.objects.all()
            models_array = []
            for mmd in mmds:
                if model.lower() in mmd.alts.lower().replace('//', ' // '):
                    models_array.append(mmd.brand + ' ' + mmd.name)
                if model.lower() in mmd.name.lower():
                    models_array.append(mmd.brand + ' ' + mmd.name)
            self.item_specs = MobileSpecs.objects.filter(path=brand, title__in=models_array)
            if len(self.item_specs) < 1:
                self.item_specs = MobileSpecs.objects.filter(path=brand, title__icontains=model)
            for item_spec in self.item_specs:
                item_spec.cats = item_spec.path
                item_spec.org_img = item_spec.img
                item_spec.org_img = item_spec.img
                item_spec.urltitle = item_spec.title_with_underline
                item_spec.spec_link = item_spec.link_of_page

        titles = MobileSpecs.get_title_for_search_suggestion()
        self.titles = json.dumps(titles)
        return super(GetItems, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(GetItems, self).get_context_data(**kwargs)
        context['mobile_items'] = self.items
        context['cats'] = self.cats
        context['length'] = self.length
        context['brands'] = self.brands
        context['cities'] = self.cities
        context['stores'] = self.stores
        context['title'] = self.title
        context['desc'] = self.desc
        context['author'] = self.author
        context['keywords'] = self.keywords
        context['footer'] = self.footer
        context['item_specs'] = self.item_specs
        context['titles'] = self.titles
        context['newslength'] = self.newslength
        context['news'] = self.news
        context['mvs'] = self.mvs
        return context


class MobileNewsList(TemplateView):
    template_name = "gc/newslist.html"
    news_items = []
    cats = []
    length = 0

    def dispatch(self, *args, **kwargs):
        cats = self.request.GET.get('cats', 'all')
        page = self.request.GET.get('page', '0')
        Utility.registerrequest(self.request, 'item-newslist-cats-' + cats + '-page-' + page)
        index = int(page) * PAGINATION_SIZE
        start_day = timezone.now() - timedelta(days=365)
        self.news_items = MobileNews.get_mobile_news_after_date_with_tag(start_day, index, index + PAGINATION_SIZE, '',
                                                                    '-created')
        for news in self.news_items:
            news.tags = news.tags.split("-")
            news.title = news.title[:35] + ' ...'
            news.short_desc = news.short_desc[:120] + ' ...'
            news.tags.pop()
        self.length = len(self.news_items)
        self.cats = MobileBrand.get_visible_mobile_brand()
        return super(MobileNewsList, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(MobileNewsList, self).get_context_data(**kwargs)
        context['news'] = self.news_items
        context['cats'] = self.cats
        context['length'] = self.length
        return context


class MobileNewsDetail(TemplateView):
    template_name = "gc/newsitem.html"
    item = None
    news_lists = []
    cats = []
    comments = []
    titles = []
    title = ''
    desc = ''
    keywords = ''
    tags = []

    def dispatch(self, *args, **kwargs):
        ids = self.kwargs.get('ids', '')
        title = self.kwargs.get('title', '')
        Utility.registerrequest(self.request, 'item-news-ids-' + ids + '-title-' + title)
        end_day = timezone.now()
        start_day = timezone.now() - timedelta(days=7, hours=end_day.hour - 1)
        try:
            self.item = MobileNews.objects.get(ids=ids)
        except ObjectDoesNotExist:
            return HttpResponseRedirect('/newslist/')
        self.item.increase_view_counter(self.item.ids)
        self.news_lists = MobileNews.get_mobile_news_after_date_with_tag(start_day, 0, 10, '', '?')
        self.cats = MobileBrand.get_visible_mobile_brand()
        self.title, self.desc, self.keywords, self.tags = self.item.title, self.item.short_desc, \
            self.item.source_name, self.item.tags.split("-")
        self.tags.pop()
        self.comments = UserComment.get_user_comments_by_section(self.item.title)
        titles = MobileSpecs.get_title_for_search_suggestion()
        self.titles = json.dumps(titles)
        return super(MobileNewsDetail, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(MobileNewsDetail, self).get_context_data(**kwargs)
        context['item'] = self.item
        context['cats'] = self.cats
        context['newss'] = self.news_lists
        context['title'] = self.title
        context['desc'] = self.desc
        context['keywords'] = self.keywords
        context['tags'] = self.tags
        context['comments'] = self.comments
        context['titles'] = self.titles
        return context


class GetToStoreRedirect(RedirectView):
    def get_redirect_url(self, *args, **kwargs):
        ids = kwargs['ids']
        Utility.registerrequest(self.request, 'item-redirect-ids-' + ids)
        mi = MobileItem.objects.get(ids=ids)
        MobileStoreRedirect(ids=mi.ids, title=mi.title, cats=mi.cats, model=mi.model, price=mi.price, store=mi.store,
                            city=mi.city, link=mi.link).save()
        self.url = urlunquote(mi.link)
        return super(GetToStoreRedirect, self).get_redirect_url(self, *args, **kwargs)


class SpecVideo(APIView):
    @staticmethod
    def post(request):
        video_ids = request.POST.get('ids', '')
        Utility.registerrequest(request, 'video-is-played-ids-' + video_ids)
        mvs = MobileVideos.objects.filter(video_ids=video_ids)
        if len(mvs) > 0:
            mv = mvs.first()
            mv.count += 1
            mv.save()
        return HttpResponse('ok', status=status.HTTP_200_OK)


class ContactUsPage(TemplateView):
    template_name = "gc/contactus.html"
    titles = []
    comments = []

    def dispatch(self, *args, **kwargs):
        Utility.registerrequest(self.request, 'contact-us')
        self.comments = UserComment.get_user_comments_by_section('global')
        for comment in self.comments:
            if comment.name is None:
                comment.name = 'ناشناس'

        items = []
        titles = MobileSpecs.get_title_for_search_suggestion()
        for title in titles:
            items.append(title.replace(' ', '_'))
        self.titles = json.dumps(titles)
        return super(ContactUsPage, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(ContactUsPage, self).get_context_data(**kwargs)
        context['titles'] = self.titles
        context['comments'] = self.comments
        return context


class ContactUsPageSendFeedback(APIView):
    @staticmethod
    def post(request):
        name = request.POST.get('name', '')
        if name == '':
            return HttpResponse("failed", status.HTTP_200_OK)
        comment = request.POST.get('comment', '')
        section = 'global'
        ipaddress = Utility.get_client_ip(request)
        UserComment(name=name, email='', question=comment, response='', deviceid=ipaddress, section=section).save()
        return HttpResponse("success", status.HTTP_200_OK)


class TestMobileItemSave(APIView):
    @staticmethod
    def get(request):
        title = request.GET.get('title', None)
        temp = ''
        cats = ''
        model = ''

        found = False
        mbss = MobileBrand.objects.all()
        for mbs in mbss:
            if not found:
                mbsalt = mbs.alt
                mbsalts = mbsalt.split("-")
                for mb in mbsalts:
                    if not found:
                        if Utility.find_whole_word(mb.lower(), title.lower()):
                            cats = mbs.name
                            found = True
                    else:
                        break
            else:
                break

        mss = MobileModelDict.objects.filter(brand=cats)
        for ms in mss:
            alts = ms.alts.split("//")
            for alt in alts:
                alt = alt.replace('+', 'plus')
                title = title.replace('+', 'plus')
                if len(alt) > 0 and len(alt) > len(temp) and \
                        Utility.find_whole_word(alt.lower().replace("(", "").replace(")", ""),
                                                title.lower().replace("(", "").replace(")", "")):
                    temp = alt
                    model = alt

        mss = MobileModelDict.objects.filter(brand=cats, alts__icontains=model)
        for ms in mss:
            head = ms.name
            alts = ms.alts.split("//")
            for alt in alts:
                if len(alt) > 0 and alt.lower() == model.lower():
                    model = head

        res = dict()
        res['model'] = model
        res['cat'] = cats
        return HttpResponse(json.dumps(res), status.HTTP_200_OK)


class MobileItemSaveItem(APIView):
    @staticmethod
    def post(request):
        ids = request.POST.get('ids', None)
        title = request.POST.get('title', None)
        img = request.POST.get('img', None)
        link = request.POST.get('link', None)
        price = request.POST.get('price', None)
        path = request.POST.get('path', None)
        city = request.POST.get('city', None)
        store = request.POST.get('store', None)
        mi = MobileItem(ids=ids, model='', cats='', title=title, img=img, link=link, price=price, path=path, city=city,
                        store=store)
        mi.save()
        temp = ''

        found = False
        mbss = MobileBrand.objects.all()
        for mbs in mbss:
            if not found:
                mbsalt = mbs.alt
                mbsalts = mbsalt.split("-")
                for mb in mbsalts:
                    if not found:
                        if Utility.find_whole_word(mb.lower(), mi.title.lower()):
                            mi.cats = mbs.name
                            mi.save()
                            found = True
                    else:
                        break
            else:
                break

        mss = MobileModelDict.objects.filter(brand=mi.cats)
        for ms in mss:
            alts = ms.alts.split("//")
            for alt in alts:
                alt = alt.replace('+', 'plus')
                mi.title = mi.title.replace('+', 'plus')
                if len(alt) > 0 and len(alt) > len(temp) and \
                        Utility.find_whole_word(alt.lower().replace("(", "").replace(")", ""),
                                                mi.title.lower().replace("(", "").replace(")", "")):
                    alt = alt.replace('plus', '+')
                    mi.title = mi.title.replace('plus', '+')
                    temp = alt
                    mi.model = alt
                    mi.save()
                mi.title = mi.title.replace('plus', '+')

        mss = MobileModelDict.objects.filter(brand=mi.cats, alts__icontains=mi.model)
        for ms in mss:
            head = ms.name
            alts = ms.alts.split("//")
            for alt in alts:
                if len(alt) > 0 and alt.lower() == mi.model.lower():
                    mi.model = head
                    mi.tags = ms.alts
                    mi.save()

        return HttpResponse("Done", status.HTTP_200_OK)


class MobileItemDelItem(APIView):
    @staticmethod
    def post(request):
        path = request.POST.get('path', None)
        MobileItem.objects.filter(path=path).delete()
        return HttpResponse("Done", status.HTTP_200_OK)


class AddMobileDict(APIView):
    @staticmethod
    def get(request):
        mss = Utility.get_mobile_spec('all', -1, -1, '', '-release_date')
        for ms in mss:
            title = ms.title.replace(ms.path, '')
            title = ' '.join(title.split())
            if len(MobileModelDict.objects.filter(name=title, brand=ms.path)) == 0:
                mmd = MobileModelDict(name=title, alts=title, brand=ms.path)
                mmd.save()

        return HttpResponse("Done", status.HTTP_200_OK)


class SetItemBrand(APIView):
    @staticmethod
    def get(request):
        mis = MobileItem.objects.all()
        mbss = MobileBrand.objects.all()
        for mbs in mbss:
            mbsalt = mbs.alt
            mbsalts = mbsalt.split("-")
            for mb in mbsalts:
                for mi in mis:
                    if Utility.find_whole_word(mb.lower(), mi.title.lower()):
                        mi.cats = mbs.name
                        mi.save()

        return HttpResponse("Done", status.HTTP_200_OK)


class ItemModelAlt(APIView):
    @staticmethod
    def get(request):
        mss = MobileModelDict.objects.all()
        mis = MobileItem.objects.all()
        for mi in mis:
            for ms in mss:
                alts = ms.alts.split("//")
                for alt in alts:
                    if len(alt) > 0 and mi.cats == ms.brand and \
                            Utility.find_whole_word(alt.lower().replace("(", "").replace(")", ""),
                                                    mi.title.lower().replace("(", "").replace(")", "")):
                        if len(alt) > len(mi.model):
                            mi.model = alt
                            mi.save()

        return HttpResponse("Done", status.HTTP_200_OK)


class ItemModel(APIView):
    @staticmethod
    def get(request):
        mis = MobileItem.objects.all()
        for mi in mis:
            mss = MobileModelDict.objects.filter(brand=mi.cats, alts__icontains=mi.model)
            for ms in mss:
                head = ms.name
                alts = ms.alts.split("//")
                for alt in alts:
                    if len(alt) > 0 and alt.lower() == mi.model.lower():
                        mi.model = head
                        mi.save()

        return HttpResponse("Done", status.HTTP_200_OK)


class MobileSpecSaveItem(APIView):
    @staticmethod
    def post(request):
        title = request.POST.get('title', None)
        img = request.POST.get('img', None)
        link = request.POST.get('link', None)
        camera_back = request.POST.get('camera_back', None)
        camera_front = request.POST.get('camera_front', None)
        release_date = request.POST.get('release_date', None)
        os = request.POST.get('os', None)
        dims = request.POST.get('dims', None)
        weight = request.POST.get('weight', None)
        inch = request.POST.get('inch', None)
        reso = request.POST.get('reso', None)
        pd = request.POST.get('pd', None)
        stbratio = request.POST.get('stbratio', None)
        cpuchip = request.POST.get('cpuchip', None)
        cpu_processor = request.POST.get('cpu_processor', None)
        gpu = request.POST.get('gpu', None)
        memory = request.POST.get('memory', None)
        storage = request.POST.get('storage', None)
        battery_capacity = request.POST.get('battery_capacity', None)
        battery_type = request.POST.get('battery_type', None)
        battery_talk_time = request.POST.get('battery_talk_time', None)
        data = request.POST.get('data', None)
        nano = request.POST.get('nano', None)
        bluetooth = request.POST.get('bluetooth', None)
        cpu_core = request.POST.get('cpu_core', None)
        cpu_freq = request.POST.get('cpu_freq', None)
        path = request.POST.get('path', None)

        if len(MobileSpecs.objects.filter(path=path, title=title)) < 1:
            MobileSpecs(title=title, img=img, link=link, camera_back=camera_back, camera_front=camera_front, path=path,
                        release_date=release_date, os=os, dims=dims, weight=weight, inch=inch, reso=reso, pd=pd,
                        stbratio=stbratio, cpu_chip=cpuchip, cpu_processor=cpu_processor, gpu=gpu, memory=memory,
                        battery_capacity=battery_capacity, battery_type=battery_type,
                        battery_talk_time=battery_talk_time, data=data, nano=nano, bluetooth=bluetooth, storage=storage,
                        cpu_freq=cpu_freq, cpu_core=cpu_core, gallery='').save()
        else:
            return HttpResponse("Already Exist", status.HTTP_302_FOUND)
        return HttpResponse("Done", status.HTTP_200_OK)


class MobileSpecChangeImage(APIView):
    @staticmethod
    def post(request):
        gallery = request.POST.get('gallery', None)
        title = request.POST.get('title', None)
        ms = MobileSpecs.objects.get(title=title)
        ms.gallery = gallery
        ms.save()
        return HttpResponse("Done", status.HTTP_200_OK)


class MobileSpecDeleteItem(APIView):
    @staticmethod
    def post(request):
        path = request.POST.get('path', None)
        MobileSpecs.objects.filter(path=path).delete()
        return HttpResponse("Done", status.HTTP_200_OK)


class MobileSpecList(APIView):
    @staticmethod
    def get(request):
        path = request.GET.get('path', None)
        end_day = timezone.now()
        start_day = timezone.now() - timedelta(hours=end_day.hour)
        items = MobileSpecs.objects.filter(created__gte=start_day, created__lte=end_day, path=path) \
            .values_list('title', 'link')
        return JsonResponse({'results': list(items)})


class MinPrice(APIView):
    @staticmethod
    def get(request):
        items = MobileModelDict.objects.all()
        for item in items:
            temp = item.name
            minprice = MobileItem.objects.filter(model=temp, cats=item.brand).order_by('price')
            if len(minprice) > 0:
                minprice = minprice[0].price
                mp = MobilePrice(title=temp, price=int(minprice), brand=item.brand)
                mp.save()
        return HttpResponse("Done", status.HTTP_200_OK)


class MobileBrandList(APIView):
    @staticmethod
    def get(request):
        items = MobileBrand.objects.all().values_list('name')
        return JsonResponse({'results': list(items)})


class WebsiteAddress(APIView):
    @staticmethod
    def post(request):
        website = request.POST.get('address', None)
        ipaddress = Utility.get_client_ip(request)
        if website is None:
            return HttpResponse("failed", status.HTTP_200_OK)
        uc = WebsiteAddr(address=website, ipaddress=ipaddress, browser=request.META['HTTP_USER_AGENT'])
        uc.save()
        return HttpResponse("success", status.HTTP_200_OK)


class CommentOnSpec(APIView):
    @staticmethod
    def post(request):
        name = request.POST.get('name', None)
        comment = request.POST.get('comment', None)
        section = request.POST.get('section', None)
        section = section.replace("_", " ")
        if InputChecker(name).check_input_size(name, 20) or InputChecker(name).check_input_size(comment, 300) or \
                InputChecker(name).check_input_size(section, 50):
            return Utility.unauthorized()
        ipaddress = Utility.get_client_ip(request)
        UserComment(name=name, email='', question=comment, response='', deviceid=ipaddress, section=section).save()
        return HttpResponse("success", status.HTTP_200_OK)


class MobileNewsItemSave(APIView):
    @staticmethod
    def post(request):
        ids = request.POST.get('our_news_id', None)
        title = request.POST.get('title', None)
        img = request.POST.get('img', None)
        link = request.POST.get('link', None)
        source_name_file = request.POST.get('source_name_file', None)
        source_name = request.POST.get('source_name', None)
        big_desc = request.POST.get('big_desc', None)
        short_desc = request.POST.get('short_desc', None)
        folder = request.POST.get('folder', None)
        if len(MobileNews.objects.filter(title=title, source_name_file=source_name_file)) < 1:
            r = requests.get(img, stream=True, headers={'User-agent': 'Mozilla/5.0'})
            with open('/root/gooshichand_server/static/website/img/news/' + folder + '/' + ids + '.jpg',
                      'wb') as output:
                r.raw.decode_content = True
                shutil.copyfileobj(r.raw, output)
            img = "https://gooshichand.com/static/website/img/news/" + folder + '/' + ids + ".jpg"
            mn = MobileNews(ids=ids, title=title, img=img, link=link, source_name_file=source_name_file,
                            source_name=source_name, big_desc=big_desc, short_desc=short_desc)
            mn.save()
            mn.mobile_news_set_tags(mn, MobileBrand.objects.all())
            return HttpResponse("News Is Saved", status.HTTP_200_OK)
        else:
            return HttpResponse("News Exists", status.HTTP_200_OK)


class MobileNewsItemDelete(APIView):
    @staticmethod
    def post(request):
        path = request.POST.get('path', None)
        MobileNews.objects.filter(source_name_file=path).delete()
        return HttpResponse("News Is Deleted", status.HTTP_200_OK)
