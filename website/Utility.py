#!/usr/bin/env python
# -*- coding: utf-8 -*-
import re
import urllib
from datetime import datetime

from django.http import HttpResponse
# from celery import shared_task
from rest_framework import status

from analytics.mongomodels import MongoWebRequest
from gooshichand import settings
from gooshichand.models import MobileModelDict, MobileSpecs, MobileItem, MobileVideos


class Utility:
    def __init__(self):
        pass

    @staticmethod
    def remove_underline_in_text(text):
        if len(text) > 1 and text[len(text) - 1] == '_':
            text = text[:-1]
        return text.replace("_", " ")

    @staticmethod
    def convert_to_csv(date):
        response = HttpResponse(date, content_type='application/vnd.ms-excel')
        response['Content-Disposition'] = 'attachment; filename="foo.xls"'
        return response

    @staticmethod
    def get_client_ip(request):
        x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
        if x_forwarded_for:
            ip = x_forwarded_for.split(',')[0]
        else:
            ip = request.META.get('REMOTE_ADDR')
        return ip

    @staticmethod
    def registerrequest(request, where):
        ipaddress = Utility.get_client_ip(request)
        try:
            browser = request.META['HTTP_USER_AGENT']
            ref = request.META.get("HTTP_REFERER")
        except KeyError:
            browser = ''
            ref = ''
        if ref is not None:
            ref = urllib.unquote(ref).decode('utf8')
        if not settings.WINDOWS_ENV:
            obj = MongoWebRequest(time=datetime.now(), section=where, ipaddress=ipaddress, ref=ref, browser=browser)
            obj.save()

    @staticmethod
    def get_elements(request, where):
        ipaddress = Utility.get_client_ip(request)
        try:
            browser = request.META['HTTP_USER_AGENT']
            ref = request.META.get("HTTP_REFERER")
        except KeyError:
            browser = ''
            ref = ''
        if ref is not None:
            ref = urllib.unquote(ref).decode('utf8')
        if not settings.WINDOWS_ENV:
            # registerrequest_d.delay(ipaddress, ref, browser, where)
            Utility.registerrequest_d(ipaddress, ref, browser, where)

    # @shared_task()
    @staticmethod
    def registerrequest_d(ipaddress, ref, browser, where):
        # time.sleep(10)
        obj = MongoWebRequest(time=datetime.now(), section=where, ipaddress=ipaddress, ref=ref, browser=browser)
        obj.save()

    @staticmethod
    def detect_remove_brand_from_fullname(brand, model):
        brands_array = MobileModelDict.objects.order_by().values_list('brand').distinct()
        for b in brands_array:
            if b[0].encode('utf-8').lower() in model.lower():
                model = model.lower().replace(b[0].encode('utf-8').lower(), '')
                model = ' '.join(model.split())
                brand = b[0].encode('utf-8')
        return brand, model

    @staticmethod
    def get_mobile_spec(which, start_index, end_index, path, sort_string):
        if which == 'all':
            if start_index == -1 and end_index == -1:
                specs = MobileSpecs.get_mobile_spec().order_by(sort_string)
            else:
                specs = MobileSpecs.get_mobile_spec().order_by(sort_string)[start_index:end_index]
        else:
            specs = MobileSpecs.get_mobile_spec_by_path(path).order_by(sort_string)[start_index:end_index]
        return specs

    @staticmethod
    def get_url_prices_minprice_of_items(obj):
        obj.urltitle = obj.title_with_underline
        obj.hasprice = 0
        rel_prices = MobileItem.get_mobile_item_with_model(obj.title_without_brand)
        obj.hasprice = len(rel_prices)
        if obj.hasprice > 0:
            obj.minprice = MobileItem.get_min_price_of_item_by_title(obj.title_without_brand)
        else:
            obj.minprice = 0
        return obj

    @staticmethod
    def create_footer_based_brand(path):
        temp = MobileSpecs.objects.filter(path__icontains=path).order_by().values_list('title').distinct()
        footer = []
        for f in temp:
            footer.append(f[0].encode('utf-8'))
        return footer, temp

    @staticmethod
    def get_videos_in_items_page():
        mvs = MobileVideos.objects.all().order_by('-created')[0:5]
        for mv in mvs:
            tags = mv.tags.split("//")
            for tag in tags:
                if len(tag) > 0:
                    mv.title = tag
        return mvs

    @staticmethod
    def define_title_desc_keyword_author_footer_of_page(store, model, brand):
        if len(store) > 0 and len(model) == 0 and len(brand) == 0:
            title = store.encode('utf-8') + " | مشخصات و قیمت روز گوشی های " + store.encode('utf-8') + \
                    " در وبسایت گوشی چند"
            desc = "قیمت روز گوشی های موبایل فروشی در فروشگاه " + store.encode(
                'utf-8') + " را با سایر فروشگاه ها مقایسه کنید"
            author = ""
            keywords = "قیمت روز گوشی در " + store.encode('utf-8') + ", قیمت گوشی در " + store.encode('utf-8') + \
                       ", موبایل در " + store.encode('utf-8') + ", ارزانترین قیمت در " + store.encode('utf-8') + \
                       ", حراج گوشی موبایل در " + store.encode('utf-8') + ", قیمت گوشی موبایل و تبلت در " + \
                       store.encode('utf-8') + ", قیمت گوشی اپل apple آیفون iphone در " + store.encode('utf-8') + \
                       ", قیمت گوشی گلکسی galaxy سامسونگ samsung در " + store.encode('utf-8') + \
                       ", قیمت گوشی شیائومی xiaomi در " + store.encode('utf-8') + ", قیمت گوشی ال جی LG در " + \
                       store.encode('utf-8') + ", قیمت گوشی موتورولا motorola در " + store.encode('utf-8') + \
                       ", قیمت گوشی سونی Sony در " + store.encode('utf-8') + ", قیمت گوشی اچ تی سی HTC در " + \
                       store.encode('utf-8') + ", قیمت گوشی هواوی Huawei در " + store.encode('utf-8')
            footer = keywords
        elif len(brand) > 0 and len(model) == 0 and len(store) == 0:
            brand = brand.split(" - ")[0]
            title = brand.encode('utf-8') + " | مشخصات و قیمت روز گوشی های " + brand.encode('utf-8')
            desc = "قیمت روز گوشی های موبایل " + brand.encode('utf-8') + " در فروشگاه های " \
                                                                         "دیجی کالا، بامیلو، گوشی شاپ، بانه کالا، " \
                                                                         "ردکالا، سایمان دیجیتال، کیمیا آنلاین، " \
                                                                         "داتیس آنلاین، مقداد آیتی، آل دیجیتال، زنبیل"
            author = ""
            keywords = "قیمت روز گوشی " + brand.encode('utf-8') + ", قیمت گوشی " + brand.encode('utf-8') + \
                       ", موبایل " + brand.encode('utf-8') + ", ارزانترین قیمت " + brand.encode('utf-8') + \
                       ", حراج گوشی موبایل " + brand.encode('utf-8') + ", قیمت گوشی موبایل و تبلت " + \
                       brand.encode('utf-8')
            footer = keywords
        else:
            title = "گوشی چند ؟ در جریان قیمت روز گوشی موبایل باشید"
            desc = "قیمت روزانه گوشی های اپل، سامسونگ، هواوی، سونی و... را در" \
                   " 30 فروشگاه در شهرهای مختلف هر روز دنبال کنید و براساس مشخصات مورد نظر خود گوشی خود را بیابید "
            author = ""
            keywords = ""
            footer = []
        return title, desc, author, keywords, footer

    @staticmethod
    def find_whole_word(w, s):
        regex = r"(\s+|^)(" + w + r")(\s+|$)"
        test_str = s
        matches = re.search(regex, test_str)
        return matches

    @staticmethod
    def unauthorized():
        HttpResponse('Unauthorized', status=status.HTTP_401_UNAUTHORIZED)
