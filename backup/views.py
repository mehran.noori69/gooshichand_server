#!/usr/bin/env python
# -*- coding: utf-8 -*-
import json

import requests
from django.core.exceptions import ObjectDoesNotExist
from django.http import HttpResponse
from rest_framework.decorators import api_view

from backup.middleware import filter_ip_access_to_analytics
from gooshichand.models import MobileItem, MobileBrand, MobileStore, PaymentGW,\
    MobileModelDict, MobilePrice, UserComment, MobileNews, MobileStoreRedirect, \
    MisspelledModelKeyword, MobileVideos, WebSettings, MobileSpecs


@filter_ip_access_to_analytics
def json_backup(request):
    number = request.GET.get('number', 0)
    number = int(number)
    result = dict()

    if number == 0:
        art = MisspelledModelKeyword.get_misspelled()
        arts = list(art.values('created', 'first', 'second', 'third'))
        result['name'] = 'MisspelledModelKeyword'
        result['MisspelledModelKeyword'] = arts

    elif number == 1:
        item = MobileBrand.get_mobile_brand()
        items = list(item.values('created', 'name', 'alt', 'brandUrl', 'brandDesc', 'imgUrl', 'visibility'))
        result['name'] = 'MobileBrand'
        result['MobileBrand'] = items

    elif number == 2:
        item = MobileItem.get_mobile_item()
        items = list(item.values('created', 'ids', 'cats', 'model', 'title', 'shortdesc', 'price', 'img', 'store',
                                 'city', 'path', 'link', 'tags'))
        result['name'] = 'MobileItem'
        result['MobileItem'] = items

    elif number == 3:
        item = MobilePrice.get_mobile_price()
        items = list(item.values('created', 'title', 'price', 'brand'))
        result['name'] = 'MobilePrice'
        result['MobilePrice'] = items

    elif number == 4:
        item = MobileModelDict.get_mobile_model_dict()
        items = list(item.values('created', 'name', 'alts', 'brand', 'has_config'))
        result['name'] = 'MobileModelDict'
        result['MobileModelDict'] = items

    elif number == 5:
        item = MobileNews.get_mobile_news()
        items = list(item.values('created', 'ids', 'title', 'short_desc', 'big_desc', 'link', 'img', 'source_name',
                                 'source_name_file', 'tags', 'view'))
        result['name'] = 'MobileNews'
        result['MobileNews'] = items

    elif number == 6:
        item = MobileSpecs.get_mobile_spec()
        items = list(item.values('created', 'title', 'img', 'link', 'camera_back', 'camera_front', 'release_date',
                                 'os', 'dims', 'weight', 'inch', 'reso', 'pd', 'stbratio', 'cpu_chip', 'cpu_processor',
                                 'cpu_freq', 'cpu_core', 'gpu',  'memory',  'storage',  'battery_capacity',
                                 'battery_type', 'battery_talk_time', 'data', 'nano', 'bluetooth', 'gallery', 'path'))
        result['name'] = 'MobileSpecs'
        result['MobileSpecs'] = items

    elif number == 7:
        item = MobileStoreRedirect.get_mobile_store_redirect()
        items = list(item.values('created', 'ids', 'title', 'cats', 'model', 'price', 'store', 'city', 'link'))
        result['name'] = 'MobileStoreRedirect'
        result['MobileStoreRedirect'] = items

    elif number == 8:
        at = MobileStore.get_mobile_stores()
        ats = list(at.values('created', 'name', 'city', 'address', 'phone', 'lat', 'long', 'website', 'visibility'))
        result['name'] = 'MobileStore'
        result['MobileStore'] = ats

    elif number == 9:
        ap = MobileVideos.get_mobile_videos()
        aps = list(ap.values('created', 'video_ids', 'link', 'tags', 'count', 'verified'))
        result['name'] = 'MobileVideos'
        result['MobileVideos'] = aps

    elif number == 10:
        item = UserComment.get_user_comments()
        items = list(item.values('code', 'created', 'name', 'email', 'section', 'question', 'response', 'deviceid'))
        result['name'] = 'UserComment'
        result['UserComment'] = items

    elif number == 11:
        item = WebSettings.get_settings()
        items = list(item.values('admin_email', 'item_pg_size', 'allow_ips'))
        result['name'] = 'WebSettings'
        result['WebSettings'] = items

    elif number == 12:
        item = PaymentGW.get_gw()
        items = list(item.values('type', 'gateway_webservice', 'gateway_starturl', 'gateway_callbackurl',
                                 'gateway_api', 'is_selected'))
        result['name'] = 'PaymentGW'
        result['PaymentGW'] = items

    result = json.dumps(result, sort_keys=True, default=str)
    return HttpResponse(result, content_type='application/json')


@filter_ip_access_to_analytics
@api_view(['GET'])
def json_restore(request):
    date = request.GET.get('date', None)
    number = request.GET.get('number', 0)
    number = int(number)
    result = dict()

    if number == 0:
        url = "http://m1.silverv.in/gooshichand/"+date+"/MobileBrand"
        items = requests.get(url=url)
        items = json.loads(items.content)['MobileBrand']
        for item in items:
            mb = MobileBrand(created=item['created'], name=item['name'], alt=item['alt'], brandUrl=item['brandUrl'],
                             brandDesc=item['brandDesc'], imgUrl=item['imgUrl'], visibility=item['visibility'])
            mb.save()

    elif number == 1:
        url = "http://m1.silverv.in/gooshichand/"+date+"/MisspelledModelKeyword"
        items = requests.get(url=url)
        items = json.loads(items.content)['MisspelledModelKeyword']
        for item in items:
            mmk = MisspelledModelKeyword(created=item['created'], first=item['first'], second=item['second'],
                                         third=item['third'])
            mmk.save()

    elif number == 2:
        item = MobileItem.get_mobile_item()
        items = list(item.values('created', 'ids', 'cats', 'model', 'title', 'shortdesc', 'price', 'img', 'store',
                                 'city', 'path', 'link', 'tags'))
        result['name'] = 'MobileItem'
        result['MobileItem'] = items

    elif number == 3:
        url = "http://m1.silverv.in/gooshichand/"+date+"/MobilePrice"
        root_items = requests.get(url=url)
        items = json.loads(root_items.content)['MobilePrice']
        for item in items:
            mmk = MobilePrice(created=item['created'], title=item['title'], price=item['price'], brand=item['brand'])
            mmk.save()
            mmk.created = item['created']
            mmk.save()

    elif number == 4:
        url = "http://m1.silverv.in/gooshichand/"+date+"/MobileModelDict"
        items = requests.get(url=url)
        items = json.loads(items.content)['MobileModelDict']
        for item in items:
            mmk = MobileModelDict(created=item['created'], name=item['name'], alts=item['alts'], brand=item['brand'],
                                  has_config=item['has_config'])
            mmk.save()

    elif number == 5:
        url = "http://m1.silverv.in/gooshichand/"+date+"/MobileNews"
        items = requests.get(url=url)
        items = json.loads(items.content)['MobileNews']
        for item in items:
            try:
                mmk = MobileNews.get_mobile_news_by_title(item['title'])
                mmk.crea = item['created']
                mmk.ids = item['ids']
                mmk.save()
            except ObjectDoesNotExist:
                pass

    elif number == 6:
        item = MobileStoreRedirect.get_mobile_store_redirect()
        items = list(item.values('created', 'ids', 'title', 'cats', 'model', 'price', 'store', 'city', 'link'))
        result['name'] = 'MobileStoreRedirect'
        result['MobileStoreRedirect'] = items

    elif number == 7:
        url = "http://m1.silverv.in/gooshichand/"+date+"/MobileStore"
        root_items = requests.get(url=url)
        items = json.loads(root_items.content)['MobileStore']
        for item in items:
            mmk = MobileStore(created=item['created'], name=item['name'], city=item['city'], address=item['address'],
                              phone=item['phone'], lat=item['lat'], long=item['long'],
                              website=item['website'], visibility=item['visibility'])
            mmk.save()

        items = json.loads(root_items.content)['MobileVideos']
        for item in items:
            mmk = MobileVideos(created=item['created'], video_ids=item['video_ids'], link=item['link'],
                               tags=item['tags'], count=item['count'], verified=item['verified'])
            mmk.save()

        items = json.loads(root_items.content)['PaymentGW']
        for item in items:
            mmk = PaymentGW(type=item['type'], gateway_webservice=item['gateway_webservice'],
                            gateway_starturl=item['gateway_starturl'], gateway_callbackurl=item['gateway_callbackurl'],
                            gateway_api=item['gateway_api'], is_selected=item['is_selected'])
            mmk.save()

    elif number == 8:
        url = "http://m1.silverv.in/gooshichand/"+date+"/UserComment"
        root_items = requests.get(url=url)
        items = json.loads(root_items.content)['UserComment']
        for item in items:
            mmk = UserComment(created=item['created'], code=item['code'], name=item['name'], email=item['email'],
                              section=item['section'], question=item['question'], response=item['response'],
                              deviceid=item['deviceid'])
            mmk.save()
            mmk.created = item['created']
            mmk.save()

    elif number == 9:
        url = "http://m1.silverv.in/gooshichand/"+date+"/MobileSpecs"
        root_items = requests.get(url=url)
        items = json.loads(root_items.content)['MobileSpecs']
        for item in items:
            mmk = MobileSpecs(created=item['created'], title=item['title'], img=item['img'], link=item['link'],
                              camera_back=item['camera_back'], camera_front=item['camera_front'],
                              release_date=item['release_date'], os=item['os'], dims=item['dims'],
                              weight=item['weight'], inch=item['inch'], reso=item['reso'], pd=item['pd'],
                              stbratio=item['stbratio'], cpu_chip=item['cpu_chip'], cpu_processor=item['cpu_processor'],
                              cpu_freq=item['cpu_freq'], cpu_core=item['cpu_core'], gpu=item['gpu'],
                              memory=item['memory'], storage=item['storage'], battery_capacity=item['battery_capacity'],
                              battery_type=item['battery_type'], battery_talk_time=item['battery_talk_time'],
                              data=item['data'], nano=item['nano'], bluetooth=item['bluetooth'],
                              gallery=item['gallery'], path=item['path'])
            mmk.save()
            mmk.created = item['created']
            mmk.save()

    return HttpResponse("ok")
