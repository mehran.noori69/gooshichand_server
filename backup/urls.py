from django.conf.urls import url

from . import views

urlpatterns = [
    # Json Backup
    url(r'^backup/$', views.json_backup),
    url(r'^restore/$', views.json_restore),
]
