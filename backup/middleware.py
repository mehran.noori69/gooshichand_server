from django.core.exceptions import PermissionDenied

from gooshichand.models import WebSettings


def get_client_ip(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip


def filter_ip_access_to_analytics(f):
    def _filter_ip_access_to_analytics(*args, **kwargs):
        return f(*args, **kwargs)
    return _filter_ip_access_to_analytics


class FilterIPMiddleware(object):
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        return self.get_response(request)

    def process_view(self, request, view_func, view_args, view_kwargs):
        if view_func.func_name == "_filter_ip_access_to_analytics":
            allow_ips = WebSettings.objects.all().last().allow_ips
            if get_client_ip(request) not in allow_ips:
                raise PermissionDenied
            return view_func(request, *view_args, **view_kwargs)
        return None
