# coding=utf-8
import json
import requests
import datetime

# !/usr/bin/python
import os, sys

index = 0
url = "https://gooshichand.com/json/backup/?number="
now = datetime.datetime.now()
path = '/var/www/html/gooshichand/' + str(now.year)+str(now.month)+str(now.day)
os.makedirs(path, 0755)
while index < 12:
    link = url + str(index)
    response = requests.request("GET", link)
    events = json.loads(response.text)
    file = open('/var/www/html/gooshichand/' + str(now.year)+str(now.month)+str(now.day)+'/'+events['name'], "w")
    file.write(response.text)
    file.close()
    index += 1
