# coding=utf-8
import json
import requests
import datetime

# !/usr/bin/python
import os, sys

index = 9
url = "https://doresa.ir/tools/jsonbackup/?number="
now = datetime.datetime.now()
path = '/root/backup/doresa/' + str(now.year)+str(now.month)+str(now.day)
path1 = path + '/2'
os.makedirs(path1, 0755)
while index < 11:
    print(index)
    link = url + str(index)
    response = requests.request("GET", link)
    events = json.loads(response.text)
    file = open('/root/backup/doresa/' + str(now.year)+str(now.month)+str(now.day)+'/2/'+events['name'], "w")
    file.write(response.text)
    file.close()
    index += 1
