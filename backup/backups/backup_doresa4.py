# coding=utf-8
import json
import requests
import datetime

# !/usr/bin/python
import os, sys

index = 20
url = "https://doresa.ir/tools/jsonbackup/?number="
now = datetime.datetime.now()
path = '/root/backup/doresa/' + str(now.year)+str(now.month)+str(now.day)
path1 = path + '/4'
os.makedirs(path1, 0755)
while index < 32:
    print(index)
    link = url + str(index)
    response = requests.request("GET", link)
    events = json.loads(response.text)
    file = open('/root/backup/doresa/' + str(now.year)+str(now.month)+str(now.day)+'/4/'+events['name'], "w")
    file.write(response.text)
    file.close()
    index += 1
