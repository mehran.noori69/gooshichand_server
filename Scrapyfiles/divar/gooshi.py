#!/usr/bin/env python
# -*- coding: utf-8 -*-
import json
from utility import *


class ScrapySpider(scrapy.Spider):
    name = "gooshi"
    start_urls = [
        "https://divar.ir/tehran/%D8%AA%D9%87%D8%B1%D8%A7%D9%86/browse/%DA%AF%D9%88%D8%B4%DB%8C-%D9%85%D9%88%D8%A8%D8%A"
        "7%DB%8C%D9%84-%D8%AA%D8%A8%D9%84%D8%AA/%D9%84%D9%88%D8%A7%D8%B2%D9%85-%D8%A7%D9%84%DA%A9%D8%AA%D8%B1%D9%88%D9%"
        "86%DB%8C%DA%A9%DB%8C/%DA%AF%D9%88%D8%B4%DB%8C-%D9%85%D9%88%D8%A8%D8%A7%DB%8C%D9%84/"
    ]

    def parse(self, response):
        last_post_date = ''
        res = ''
        body = response.xpath('.//body').extract()
        for b in body:
            if '<script>' in b:
                c = b.split("lastPostDate")
                if len(c) > 1:
                    last_post_date = c[1].split(':')[1].split(',')[0]
                    last_post_date = last_post_date.encode('utf-8')
                    res = get_data(last_post_date)

        new_last_post_date = res['result']['last_post_date']
        posts = res['result']['post_list']
        print(new_last_post_date)
        for post in posts:
            title = post['title']
            token = post['token']
            price = post['v09']
            img = "https://s100.divarcdn.com/static/thumbnails/1512061559/"+token+".jpg"
            r = get_number(token)
            phone = r['result']['phone']
            email = r['result']['email']
            print(title)
            print(token)
            print(price)
            print(phone)
            print(email)
            print("----------------------")

