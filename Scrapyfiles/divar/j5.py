#!/usr/bin/env python
# -*- coding: utf-8 -*-
from utility import *
import telegram

TOKEN = "595255997:AAHcV-leW1X0vksOv-l2lu0kQQC9H6oee8w"
bot = telegram.Bot(token=TOKEN)


def send_to_telegram(title, city, time, image, link):
    content = title.encode('utf-8') + '\n'
    content += city.encode('utf-8') + '\n'
    content += time.encode('utf-8') + '\n'
    content += link.encode('utf-8') + '\n'
    if image == '':
        res = bot.sendMessage(chat_id='-384336544', text=content)
    else:
        res = bot.sendPhoto(chat_id='-384336544', photo=image, caption=content)
    print(res['message_id'])


class ScrapySpider(scrapy.Spider):
    name = "j5"
    start_urls = [
        "https://divar.ir/tehran/تهران/browse/استخدام-کاریابی/?query=0,استخدامی"
    ]

    def parse(self, response):
        blocks = response.xpath('//div[contains(@class, "post-card")] ')
        print(len(blocks))
        for block in blocks:
            time = block.xpath('.//div[contains(@class, "two")]//div[contains(@class, "description")]//'
                               'div[contains(@class, "meta")]//text()').extract()
            if len(time) < 1:
                continue
            time = time[0]
            time = ' '.join(time.split())
            if 'لحظاتی'.decode('utf-8') not in time and 'دقایقی'.decode('utf-8') not in time and \
                                                        'ربع'.decode('utf-8') not in time:
                continue
            image = block.xpath('.//div[contains(@class, "two")]//div[contains(@class, "image")]//img//@src').extract()
            if len(image) == 0:
                image = ''
            else:
                image = image[0]
            title = block.xpath('.//div[contains(@class, "two")]//h2//text()').extract()[0]
            title = ' '.join(title.split())
            city = block.xpath('.//div[contains(@class, "two")]//div[contains(@class, "description")]//label//'
                               'text()').extract()[0]
            city = ' '.join(city.split())
            link = block.xpath('.//a//@href').extract()[0]
            link = 'https://divar.ir/v/' + link.split('/')[3]
            # price = block.xpath('.//div[contains(@class, "one")]//label[contains(@class, "item")]//text()')
            # .extract()[0]
            # price = ' '.join(price.split())
            # price = fatoen(price)
            # if 'نیم'.decode('utf-8') in time or 'ربع'.decode('utf-8') in time or 'لحظاتی'.decode('utf-8') in time\
            #         or 'دقایقی'.decode('utf-8') in time:
            #     token = link.split("/")[4]
            #     r = get_number(token)
            #     phone = r['result']['phone']
            #     email = r['result']['email']
            send_to_telegram(title, city, time, image, link)
