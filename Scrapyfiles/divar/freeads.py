#!/usr/bin/env python
# -*- coding: utf-8 -*-
import json
from utility import *


class ScrapySpider(scrapy.Spider):
    name = "gooshi"
    start_urls = [
        "https://divar.ir/tehran/%D8%AA%D9%87%D8%B1%D8%A7%D9%86/browse/?query=0,%D8%B1%D8%A7%DB%8C%DA%AF%D8%A7%D9%86"
    ]

    def parse(self, response):
        last_post_date = ''
        # print(response.url)
        res = ''
        body = response.xpath('.//body').extract()
        for b in body:
            if '<script>' in b:
                c = b.split("lastPostDate")
                if len(c) > 1:
                    last_post_date = c[1].split(':')[1].split(',')[0]
                    last_post_date = last_post_date.encode('utf-8')
                    # print(last_post_date)

        new_last_post_date = last_post_date
        index = 0
        while index < 50:
            res = get_free_data(new_last_post_date)
            new_last_post_date = str(res['result']['last_post_date'])
            posts = res['result']['post_list']
            # print(new_last_post_date)
            for post in posts:
                title = post['title']

                s = ['ارسال رایگان'.decode('utf-8'), 'حمل رایگان'.decode('utf-8'), 'تحویل رایگان'.decode('utf-8'),
                     'نصب رایگان'.decode('utf-8'), 'ارسال تهران رایگان' .decode('utf-8'),
                     'شوینده رایگان' .decode('utf-8'), 'استخدام رایگان' .decode('utf-8'),
                     'رایگان ارسال' .decode('utf-8'), 'محل رایگان' .decode('utf-8'),
                     'تحویل درب منزل رایگان'.decode('utf-8'), 'توزیع به تمام نقاط رایگان'.decode('utf-8'),
                     'ارسال فقط تهران رایگان'.decode('utf-8'), 'روکش رایگان'.decode('utf-8'),
                     'کارورزی رایگان'.decode('utf-8'), ]

                if s[0] not in title and s[1] not in title and s[2] not in title and s[3] not in title\
                        and s[4] not in title and s[5] not in title and s[6] not in title and s[7] not in title\
                        and s[8] not in title and s[9] not in title and s[10] not in title and s[11] not in title\
                        and s[12] not in title and s[13] not in title:
                    print(title)
                    # print("----------------------")

            index += 1

# token = post['token']
# price = post['v09']
# img = "https://s100.divarcdn.com/static/thumbnails/1512061559/"+token+".jpg"
# r = get_number(token)
# phone = r['result']['phone']
# email = r['result']['email']
# print(token)
# print(price)
# print(phone)
# print(email)
