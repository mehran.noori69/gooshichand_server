#!/usr/bin/env python
# -*- coding: utf-8 -*-
import json
from random import randint
from datetime import datetime, timedelta
import urlparse
import scrapy
from scrapy import Spider, Item, Request
import re
import requests
import shutil


def fatoen(inputs):
    inputs = inputs.replace('۱'.decode('utf-8'), '1')
    inputs = inputs.replace('۲'.decode('utf-8'), '2')
    inputs = inputs.replace('۳'.decode('utf-8'), '3')
    inputs = inputs.replace('۴'.decode('utf-8'), '4')
    inputs = inputs.replace('۵'.decode('utf-8'), '5')
    inputs = inputs.replace('۶'.decode('utf-8'), '6')
    inputs = inputs.replace('۷'.decode('utf-8'), '7')
    inputs = inputs.replace('۸'.decode('utf-8'), '8')
    inputs = inputs.replace('۹'.decode('utf-8'), '9')
    inputs = inputs.replace('۰'.decode('utf-8'), '0')
    return inputs


def monthofyear(inputs):
    inputs = inputs.replace('فروردین'.decode('utf-8'), '01')
    inputs = inputs.replace('اردیبهشت'.decode('utf-8'), '02')
    inputs = inputs.replace('خرداد'.decode('utf-8'), '03')
    inputs = inputs.replace('تیر'.decode('utf-8'), '04')
    inputs = inputs.replace('مرداد'.decode('utf-8'), '05')
    inputs = inputs.replace('شهریور'.decode('utf-8'), '06')
    inputs = inputs.replace('شهريور'.decode('utf-8'), '06')
    inputs = inputs.replace('مهر'.decode('utf-8'), '07')
    inputs = inputs.replace('آبان'.decode('utf-8'), '08')
    inputs = inputs.replace('آذر'.decode('utf-8'), '09')
    inputs = inputs.replace('دی'.decode('utf-8'), '10')
    inputs = inputs.replace('بهمن'.decode('utf-8'), '11')
    inputs = inputs.replace('اسفند'.decode('utf-8'), '12')
    return inputs


def isEnglish(s):
    try:
        s.encode(encoding='utf-8').decode('ascii')
    except UnicodeDecodeError:
        return False
    else:
        return True


def hasNumbers(inputString):
    return any(char.isdigit() for char in inputString)


def cleanhtml(raw_html):
    cleanr = re.compile('<.*?>')
    cleantext = re.sub(cleanr, '', raw_html)
    return cleantext


def get_image(img, ids):
    r = requests.get(img, stream=True, headers={'User-agent': 'Mozilla/5.0'})
    with open('/root/gooshichand_server/static/website/img/items/' + ids + '.jpg', 'wb') as output:
        r.raw.decode_content = True
        shutil.copyfileobj(r.raw, output)
    img = "http://files.gooshichand.ir/static/website/img/items/" + ids + ".jpg"
    return img


def get_image(img, title):
    r = requests.get(img, stream=True, headers={'User-agent': 'Mozilla/5.0'})
    with open('/root/gooshichand_server/static/website/img/mobile/' + title + '.jpg', 'wb') as output:
        r.raw.decode_content = True
        shutil.copyfileobj(r.raw, output)
    img = "http://files.gooshichand.ir/static/website/img/mobile/" + title + ".jpg"
    return img


def get_data(last_post_date):
    url = "https://search.divar.ir/json/"
    payload = "{\r\n\"id\":0,\r\n\"jsonrpc\":\"2.0\",\r\n\"method\":\"getPostList\",\r\n\"params\":[[[\"place2\"" \
              ", 0, [\"1\"]], [\"cat3\", 0, [117]], [\"cat2\", 0, [37]], [\"cat1\", 0, [1]]], " + last_post_date + \
              "]\r\n}"
    headers = {
        'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) '
                      'Chrome/39.0.2171.95 Safari/537.36',
        'content-type': "application/x-www-form-urlencoded",
        'cache-control': "no-cache",
    }
    r = requests.request("POST", url, data=payload, headers=headers)
    js = json.loads(r.text)
    return js


def get_free_data(last_post_date):
    url = "https://search.divar.ir/json/"

    payload = "{\"jsonrpc\":\"2.0\",\"id\":0,\"method\":\"getPostList\",\"params\":[[[\"place2\",0,[\"1\"]],[\"query\",0,[\"رایگان\"]]],"+last_post_date+"]}"

    headers = {
        'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) '
                      'Chrome/39.0.2171.95 Safari/537.36',
        'content-type': "application/x-www-form-urlencoded",
        'cache-control': "no-cache",
    }
    r = requests.request("POST", url, data=payload, headers=headers)
    js = json.loads(r.text)
    return js


def get_arrayesh_data(last_post_date):
    url = "https://search.divar.ir/json/"

    payload = "{\n\"id\":0,\n\"jsonrpc\":\"2.0\",\n\"method\":\"getPostList\",\n\"params\":[[[\"place2\", 0, [\"1\"]]," \
              " [\"cat2\", 0, [138]], [\"cat1\", 0, [125]]], "+last_post_date+"]\n}"

    headers = {
        'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) '
                      'Chrome/39.0.2171.95 Safari/537.36',
        'content-type': "application/x-www-form-urlencoded",
        'cache-control': "no-cache",
    }
    r = requests.request("POST", url, data=payload, headers=headers)
    js = json.loads(r.text)
    return js


def get_number(token):
    url = "https://r.divar.ir/json/"
    payload = "{\"id\":0, \"jsonrpc\":\"2.0\",\"method\":\"getContact\", \"params\":[\""+token+"\"]}"
    headers = {
        'content-type': "application/json",
        'cache-control': "no-cache",
        'postman-token': "23b68eda-b849-81ff-2fc9-7c09527bc706"
    }
    r = requests.request("POST", url, data=payload, headers=headers)
    js = json.loads(r.text)
    return js


# start = monthofyear(start)
# now = str(datetime.now()).split(" ")[0]
# year = jalali.Gregorian(now).persian_year
# start = start.replace(' ', '-')
# start += "-" + str(year)
# price = temps[1]
# price = price.encode('utf-8')
# price = price.split(" + ")[0]
# price = price.replace(" تومان", "")
# price = price.replace("قیمت از ", "")
# price = price.replace(",", "")
# price = ' '.join(price.split())
# price += "0"
# days = temps[2]
# days = days.encode('utf-8')
# days = days.replace(' شب', '')
# airline = temps[3]
# while '  ' in price:
#     price = price.replace('  ', ' ')
# while '  ' in days:
#     days = days.replace('  ', ' ')
# while '  ' in airline:
#     airline = airline.replace('  ', ' ')
# day = start.split("-")[0]
# mon = start.split("-")[1]
# year = start.split("-")[2]
# startdate = year + '-' + mon + '-' + day
# startdate = str(startdate)
# startdate = jalali.Persian(startdate).gregorian_string()
