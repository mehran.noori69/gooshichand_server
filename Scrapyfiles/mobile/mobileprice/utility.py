#!/usr/bin/env python
# -*- coding: utf-8 -*-
from random import randint
from datetime import datetime, timedelta
import urlparse
import scrapy
from scrapy import Spider, Item, Request
import re, json
import requests
import shutil


def fatoen(inputs):
    inputs = inputs.replace('۱'.decode('utf-8'), '1')
    inputs = inputs.replace('۲'.decode('utf-8'), '2')
    inputs = inputs.replace('۳'.decode('utf-8'), '3')
    inputs = inputs.replace('۴'.decode('utf-8'), '4')
    inputs = inputs.replace('۵'.decode('utf-8'), '5')
    inputs = inputs.replace('۶'.decode('utf-8'), '6')
    inputs = inputs.replace('۷'.decode('utf-8'), '7')
    inputs = inputs.replace('۸'.decode('utf-8'), '8')
    inputs = inputs.replace('۹'.decode('utf-8'), '9')
    inputs = inputs.replace('۰'.decode('utf-8'), '0')
    return inputs


def monthofyear(inputs):
    inputs = inputs.replace('فروردین'.decode('utf-8'), '01')
    inputs = inputs.replace('اردیبهشت'.decode('utf-8'), '02')
    inputs = inputs.replace('خرداد'.decode('utf-8'), '03')
    inputs = inputs.replace('تیر'.decode('utf-8'), '04')
    inputs = inputs.replace('مرداد'.decode('utf-8'), '05')
    inputs = inputs.replace('شهریور'.decode('utf-8'), '06')
    inputs = inputs.replace('شهريور'.decode('utf-8'), '06')
    inputs = inputs.replace('مهر'.decode('utf-8'), '07')
    inputs = inputs.replace('آبان'.decode('utf-8'), '08')
    inputs = inputs.replace('آذر'.decode('utf-8'), '09')
    inputs = inputs.replace('دی'.decode('utf-8'), '10')
    inputs = inputs.replace('بهمن'.decode('utf-8'), '11')
    inputs = inputs.replace('اسفند'.decode('utf-8'), '12')
    return inputs


def isEnglish(s):
    try:
        s.encode(encoding='utf-8').decode('ascii')
    except UnicodeDecodeError:
        return False
    else:
        return True


def hasNumbers(inputString):
    return any(char.isdigit() for char in inputString)


def cleanhtml(raw_html):
    cleanr = re.compile('<.*?>')
    cleantext = re.sub(cleanr, '', raw_html)
    return cleantext


def get_image(img, ids, folder):
    r = requests.get(img, stream=True, headers={'User-agent': 'Mozilla/5.0'})
    with open('/root/gooshichand_server/static/website/img/items/' + folder + '/' + ids + '.jpg', 'wb') as output:
        r.raw.decode_content = True
        shutil.copyfileobj(r.raw, output)
    img = "https://gooshichand.com/static/website/img/items/" + folder + '/' + ids + ".jpg"
    return img


def del_item(filename):
    r = requests.post("https://gooshichand.com/mobiledelitem/", data={'path': filename})


def save_item(ids, title, price, img, link, filename, store, city):
    r = requests.post("https://gooshichand.com/mobilesaveitem/", data={'ids': ids, 'title': title, 'price': price
        , 'img': img, 'link': link, 'path': filename, 'store': store, 'city': city})


def spellcorrect(text):
    text = ' '.join(text.split())
    text = text.replace('ي'.decode('utf-8'), 'ی'.decode('utf-8'))
    text = text.replace('آ'.decode('utf-8'), 'ا'.decode('utf-8'))
    text = text.replace('ك'.decode('utf-8'), 'ک'.decode('utf-8'))
    text = text.replace('ي'.decode('utf-8'), 'ی'.decode('utf-8'))
    text = text.replace('ئ'.decode('utf-8'), 'ی'.decode('utf-8'))
    text = text.replace('ك'.decode('utf-8'), 'ک'.decode('utf-8'))
    text = text.replace('ﻚ'.decode('utf-8'), 'ک'.decode('utf-8'))
    text = text.replace('ﺑ'.decode('utf-8'), 'ب'.decode('utf-8'))
    text = text.replace('ﺎ'.decode('utf-8'), 'ا'.decode('utf-8'))
    text = text.replace('ﻧ'.decode('utf-8'), 'ن'.decode('utf-8'))
    text = text.replace('ﺧ'.decode('utf-8'), 'خ'.decode('utf-8'))
    text = text.replace('٠'.decode('utf-8'), '۰'.decode('utf-8'))
    text = text.replace('١'.decode('utf-8'), '۱'.decode('utf-8'))
    text = text.replace('٢'.decode('utf-8'), '۲'.decode('utf-8'))
    text = text.replace('٣'.decode('utf-8'), '۳'.decode('utf-8'))
    text = text.replace('۴'.decode('utf-8'), '۴'.decode('utf-8'))
    text = text.replace('۵'.decode('utf-8'), '۵'.decode('utf-8'))
    text = text.replace('۶'.decode('utf-8'), '۶'.decode('utf-8'))
    text = text.replace('٧'.decode('utf-8'), '۷'.decode('utf-8'))
    text = text.replace('٨'.decode('utf-8'), '۸'.decode('utf-8'))
    text = text.replace('٩'.decode('utf-8'), '۹'.decode('utf-8'))
    text = text.replace('إ'.decode('utf-8'), 'ا'.decode('utf-8'))
    text = text.replace('ؤ'.decode('utf-8'), 'و'.decode('utf-8'))
    text = text.replace('ۀ'.decode('utf-8'), 'ه'.decode('utf-8'))
    text = text.replace('أ'.decode('utf-8'), 'ا'.decode('utf-8'))
    text = text.replace('%'.decode('utf-8'), '٪'.decode('utf-8'))
    text = text.replace('?'.decode('utf-8'), '؟'.decode('utf-8'))
    text = text.replace('ً'.decode('utf-8'), ''.decode('utf-8'))
    text = text.replace('ٌ'.decode('utf-8'), ''.decode('utf-8'))
    text = text.replace('ٍ'.decode('utf-8'), ''.decode('utf-8'))
    text = text.replace('َ'.decode('utf-8'), ''.decode('utf-8'))
    text = text.replace('ُ'.decode('utf-8'), ''.decode('utf-8'))
    text = text.replace('ِ'.decode('utf-8'), ''.decode('utf-8'))
    text = text.replace(u"\uFEFF", " ")  # removing zero width non breakable space character
    text = text.replace(u"\u200F", "")  # removing rtl mark character
    text = text.replace(u"\u200E", "")  # removing ltr mark character
    text = text.replace(u"\u200D", "")  # removing zero width joiner
    text = text.replace(u"\u200B", " ")  # removing zero width space
    text = text.replace(u"\u200A", " ")  # removing hair space
    text = text.replace(u"\u200C", " ")  # removing zero width non joiner space character
    text = text.replace(u"\u00A0", " ")  # removing non break space
    text = text.replace(u"\u0640", "")  # removing arabic tatweel
    text = ' '.join(text.split())
    return text


def correction(name):
    name = spellcorrect(name)
    # Huawei
    name = name.replace('Mate 10-', 'Mate 10 ')
    name = name.replace('smart-', 'smart ')
    name = name.replace('P9-', 'P9 ')
    name = name.replace('P10-', 'P10 ')
    name = name.replace('6X-', '6X ')
    name = name.replace('7X-', '7X ')
    name = name.replace('Y5-', 'Y5 -')
    name = name.replace('Y6-', 'Y6 -')
    name = name.replace('Y5 II-', 'Y5 II -')
    name = name.replace('Honor 9-', 'Honor 9 ')
    # Sony
    name = name.replace('Xperia C4-', 'Xperia C4 ')
    name = name.replace('Xperia M5-', 'Xperia M5 ')
    name = name.replace('Xperia E5-', 'Xperia E5 ')
    name = name.replace('Xperia Z5-', 'Xperia Z5 ')
    name = name.replace('Xperia X-', 'Xperia X ')
    name = name.replace('Xperia XA1-', 'Xperia XA1 ')
    name = name.replace('XA1Ultra-', 'XA1 Ultra ')
    name = name.replace('C5 Ultra-', 'C5 Ultra ')
    # HTC
    name = name.replace('U11-', 'U11 ')
    name = name.replace('U20-', 'U20 ')
    name = name.replace('U Play-', 'U Play ')
    name = name.replace('U Ultra-', 'U Ultra ')
    name = name.replace('HTC 10-', 'HTC 10 ')
    name = name.replace('Desire 830-', 'Desire 830 ')
    # Nokia
    name = name.replace('Nokia 5-', 'Nokia 5 ')
    name = name.replace('Nokia 3-', 'Nokia 3 ')
    name = name.replace('Nokia 150-', 'Nokia 150 ')
    name = name.replace('Nokia 3310-', 'Nokia 3310 ')
    # Samsung
    name = name.replace('Galexy', 'Galaxy')
    name = name.replace('(Samsung Galaxy A8+ (2018', 'Samsung Galaxy A8+ (2018)')
    name = name.replace('Galaxy Note Fan Edition-', 'Galaxy Note Fan Edition -')
    name = name.replace('J1-', 'J1 ')
    name = name.replace('J2-J200', 'J2 - J200')
    name = name.replace('J3 2016-', 'J3 2016')
    name = name.replace('J3 (2016)-', 'J3 (2016) ')
    name = name.replace('J3-', 'J3 ')
    name = name.replace('J3 Pro-', 'J3 Pro ')
    name = name.replace('-J320F/DS', 'J320F/DS')
    name = name.replace('J5-16GB-2017', 'J5 2017 16GB')
    name = name.replace('S7-', 'S7 ')
    name = name.replace('S8-', 'S8 ')
    name = name.replace('S9-', 'S9 ')
    name = name.replace('A3-', 'A3 ')
    name = name.replace('A5-', 'A5 ')
    name = name.replace('A7-', 'A7 ')
    name = name.replace('A8+', 'A8 Plus')
    name = name.replace('A9-', 'A9 ')
    name = name.replace('Pro-', 'Pro ')
    name = name.replace('C5-', 'C5 ')
    name = name.replace('C7-', 'C7 ')
    name = name.replace('C8-', 'C8 ')
    name = name.replace('Note5-', 'Note5 ')
    name = name.replace('Note 5-', 'Note 5 ')
    name = name.replace('Note 8-', 'Note 8 ')
    # Xiaomi
    name = name.replace('Mi 5S-', 'Mi 5S ')
    name = name.replace('Mi4', 'Mi 4')
    name = name.replace('Mi5c', 'Mi 5c')
    name = name.replace('Mi 5s-', 'Mi 5s ')
    name = name.replace('Mi6', 'Mi 6')
    name = name.replace('Redmi 5A-', 'Redmi 5A -')
    name = name.replace('Mi A1-', 'Mi A1 ')
    name = name.replace('Mi Mix-', 'Mi Mix ')
    name = name.replace('Mi Max-', 'Mi Max ')
    name = name.replace('Mi 5X-', 'Mi 5X ')
    name = name.replace('Redmi 4X-', 'Redmi 4X ')
    name = name.replace('Redmi 4a-', 'Redmi 4a ')
    name = name.replace('Redmi 5-', 'Redmi 5 ')
    # Year
    name = name.replace('2016-', '2016 ')
    name = name.replace('2017-', '2017 ')
    name = name.replace('-2017', ' 2017')
    # Apple
    name = name.replace('SE-', 'SE ')
    name = name.replace('8 plus-', '8 plus ')
    name = name.replace('8-', '8 ')
    name = name.replace('7-', '7 ')
    name = name.replace('7s-', '7s ')
    name = name.replace('6-', '6 ')
    name = name.replace('6s-', '6s ')
    name = name.replace('iPhone X-', 'iPhone X - ')
    # LG
    name = name.replace('LG G4-', 'LG G4 - ')
    name = name.replace('V10-', 'V10 ')
    name = name.replace('K10-', 'K10 ')
    name = name.replace('V20-', 'V20 ')
    name = name.replace('Stylus 2-', 'Stylus 2 ')
    name = name.replace('Stylus 3-', 'Stylus 3 ')
    name = name.replace('X power-', 'X power ')
    # Motorola
    name = name.replace('Moto Z-', 'Moto Z ')
    name = name.replace('Moto M-', 'Moto M ')
    name = name.replace('Moto X-', 'Moto X ')
    # Lenovo
    name = name.replace('Vibe Shot-', 'Vibe Shot ')
    name = name.replace('Vibe P1m-', 'Vibe P1m ')
    name = name.replace('Vibe S1-', 'Vibe S1- ')

    name = name.replace('', '')
    return name
