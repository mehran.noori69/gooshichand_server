#!/usr/bin/env python
# -*- coding: utf-8 -*-
from utility import *


class ScrapySpider(scrapy.Spider):
    name = "esara"
    start_urls = [
        "https://www.esara.ir/Search/Electronic-Devices/Mobile/Mobile-Phone/~/Status-1/PageSize-150/"
    ]

    def parse(self, response):
        filename = "es-esara"
        del_item(filename)
        blocks = response.xpath('//div[contains(@class, "product-container")]')
        store = "ایسرا"
        city = "تهران"
        folder = '18'
        print(len(blocks))
        if len(blocks) < 1:
            return
        for block in blocks:
            img = block.xpath('.//div[@class="left-block h300"]/a/img/@src').extract()[0]
            img = urlparse.urljoin(response.url, img)
            title = block.xpath('.//div[@class="right-block"]/h5/a/text()').extract()[0]
            title = ' '.join(title.split())
            link = block.xpath('./div[@class="left-block h300"]/a/@href').extract()[0]
            link = urlparse.urljoin(response.url, link)
            price = block.xpath('.//div[@class="right-block"]'
                                '/div[@class="content_price h30"]/span[@class="price product-price pull-left"]'
                                '/span//text()').extract()
            if len(price) > 0:
                price = price[0]
                price = fatoen(price)
                price = price.replace(",".decode('utf-8'), "")
                price = price.replace("تومان".decode('utf-8'), "")
                price = ' '.join(price.split())
                price += '0'
            else:
                continue
            rand = '18'
            rand += str(randint(10000, 999999))
            ids = rand
            img = img.replace('https', 'http')
            img = get_image(img, ids, folder)
            save_item(ids.encode('utf-8'), title.encode('utf-8'), price.encode('utf-8'), img.encode('utf-8')
                      , link.encode('utf-8'), filename, store, city)
