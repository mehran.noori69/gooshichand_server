#!/usr/bin/env python
# -*- coding: utf-8 -*-
from utility import *


class ScrapySpider(scrapy.Spider):
    name = "ighab"
    start_urls = [
        "https://ighab.com/"
    ]

    def parse(self, response):
        filename = "ighab-ighab"
        del_item(filename)
        page = 1
        url1 = 'https://www.ighab.com/product-category/%DA%AF%D9%88%D8%B4%DB%8C-%D9%87%D8%A7%DB%8C-%D9%87%D9%88%D8%B4%D9%85%D9%86%D8%AF/page/'
        while page < 8:
            link = url1 + str(page) + "/"
            new_request = Request(link, callback=self.parse_page, dont_filter=False)
            yield new_request
            page += 1

    def parse_page(self, response):
        blocks = response.xpath('//ul[@class="products columns-4"]/article[contains(@class, "indx-article")]')
        store = "آی قاب"
        city = "شیراز"
        filename = "ighab-ighab"
        folder = '20'
        print(len(blocks))
        if len(blocks) < 1:
            return
        for block in blocks:
            img = block.xpath('.//div[@class="img-post"]/a//img/@src').extract()[0]
            img = urlparse.urljoin(response.url, img)
            title = block.xpath('.//div[@class="title-post"]/a/h2/text()').extract()[0]
            title = ' '.join(title.split())
            link = block.xpath('.//div[@class="img-post"]/a/@href').extract()[0]
            link = urlparse.urljoin(response.url, link)
            price = block.xpath('.//div[@class="price-post"]/span/span[@class="price"]//span[contains(@class, "amount")]//text()').extract()
            if len(price) > 0:
                price = price[0]
                price = fatoen(price)
                price = price.replace(",".decode('utf-8'), "")
                price = price.replace("تومان".decode('utf-8'), "")
                price = ' '.join(price.split())
                price += '0'
            else:
                continue
            rand = '20'
            rand += str(randint(10000, 999999))
            ids = rand
            img = get_image(img, ids, folder)
            save_item(ids.encode('utf-8'), title.encode('utf-8'), price.encode('utf-8'), img.encode('utf-8')
                      , link.encode('utf-8'), filename, store, city)
