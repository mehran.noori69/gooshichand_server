#!/usr/bin/env python
# -*- coding: utf-8 -*-
from utility import *


class ScrapySpider(scrapy.Spider):
    name = "banehtak"
    start_urls = [
        "http://banehtak.com/"
    ]

    def parse(self, response):
        filename = "bt-banehtak"
        del_item(filename)
        page = 1
        url1 = 'http://banehtak.com/122-%D9%85%D9%88%D8%A8%D8%A7%DB%8C%D9%84?p='
        while page < 10:
            link = url1 + str(page)
            new_request = Request(link, callback=self.parse_page, dont_filter=False)
            yield new_request
            page += 1

    def parse_page(self, response):
        blocks = response.xpath('//div[@class="product-container"]')
        store = "بانه تک"
        city = "کردستان"
        filename = "bt-banehtak"
        folder = '14'
        if len(blocks) < 1:
            return
        for block in blocks:
            img = block.xpath('.//div[@class="left-block"]/div[@class="product-image-container"]/a/img/@src').extract()[0]
        #     img = urlparse.urljoin(response.url, img)
            title = block.xpath('.//div[@class="right-block"]/h5/a/text()').extract()[0]
            title = ' '.join(title.split())
            title = correction(title)
            link = block.xpath('.//div[@class="left-block"]/div[@class="product-image-container"]/a/@href').extract()[0]
            link = urlparse.urljoin(response.url, link)
            price = block.xpath('.//div[@class="right-block"]/div[@class="content_price"]/span[@class="price product-price"]//text()').extract()
            if len(price) > 0:
                price = price[0]
                price = fatoen(price)
                price = price.replace(",".decode('utf-8'), "")
                price = price.replace("تومان".decode('utf-8'), "")
                price = ' '.join(price.split())
                if len(price) < 2:
                    continue
                price += '0'
            else:
                continue
            rand = '14'
            rand += str(randint(10000, 999999))
            ids = rand
            img = get_image(img, ids, folder)
            new_request = Request(link, callback=self.parse_pages, dont_filter=False)
            new_request.meta['title'] = title
            new_request.meta['price'] = price
            new_request.meta['link'] = link
            new_request.meta['ids'] = ids
            new_request.meta['img'] = img
            new_request.meta['filename'] = filename
            new_request.meta['store'] = store
            new_request.meta['city'] = city
            yield new_request

    def parse_pages(self, response):
        title = response.meta['title']
        price = response.meta['price']
        link = response.meta['link']
        ids = response.meta['ids']
        img = response.meta['img']
        filename = response.meta['filename']
        store = response.meta['store']
        city = response.meta['city']
        blocks = response.xpath('//span[@class="products-attribute-value"]//text()').extract()
        index = 0
        for block in blocks:
            if index == 1:
                title += ' ' + block
            index += 1
        save_item(ids.encode('utf-8'), title.encode('utf-8'), price.encode('utf-8'), img.encode('utf-8')
                  , link.encode('utf-8'), filename, store, city)
