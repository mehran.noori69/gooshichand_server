#!/usr/bin/env python
# -*- coding: utf-8 -*-
from utility import *


class ScrapySpider(scrapy.Spider):
    name = "19kala"
    start_urls = [
        "https://www.19kala.com/%D8%A8%D8%B1%D9%86%D8%AF%D9%87%D8%A7%DB%8C-%D9%85%D9%88%D8%A8%D8%A7%DB%8C%D9%84.htm"
    ]

    def parse(self, response):
        filename = "kala-19kala"
        del_item(filename)
        links = response.xpath('//div[@id="content"]/div/a/@href').extract()
        for link in links:
            new_request = Request(link, callback=self.parse_page, dont_filter=False)
            yield new_request

    def parse_page(self, response):
        blocks = response.xpath('//div[contains(@class, "main-products")]/div[contains(@class, "product-list-item")]')
        print(len(blocks))
        if len(blocks) < 1:
            return
        for block in blocks:
            title = block.xpath('.//div[@class="product-details"]/div[@class="caption"]/h4/a/text()').extract()[0]
            title = ' '.join(title.split())
            title = title.replace('-', ' - ')
            title = title.replace('  ', ' ')
            title = correction(title)
            link = block.xpath('.//div[@class="image"]/a/@href').extract()[0]
            link = urlparse.urljoin(response.url, link)
            price = block.xpath('.//div[@class="product-details"]/div[@class="caption"]/p[@class="price"]/span[@class="price-new"]//text()').extract()
            if len(price) < 1:
                price = block.xpath('.//div[@class="product-details"]/div[@class="caption"]/p[@class="price"]//text()').extract()
            if len(price) > 0:
                price = price[0]
                price = fatoen(price)
                price = price.replace(",".decode('utf-8'), "")
                price = price.replace("تومان".decode('utf-8'), "")
                price = ' '.join(price.split())
                if 'بدون قیمت'.decode('utf-8') in price:
                    continue
                price += '0'
            else:
                continue
            new_request = Request(link, callback=self.parse_page_details, dont_filter=False)
            new_request.meta['title'] = title
            new_request.meta['price'] = price
            new_request.meta['link'] = link
            yield new_request

    def parse_page_details(self, response):
        title = response.meta['title']
        price = response.meta['price']
        link = response.meta['link']
        img = response.xpath('.//img[@itemprop="image"]/@src').extract()
        img = img[0].split("/", 2)[2]
        img = img.replace("www", "http://www")
        store = "19 کالا"
        city = "شیراز"
        filename = "kala-19kala"
        folder = '21'
        rand = '21'
        rand += str(randint(10000, 999999))
        ids = rand
        img = get_image(img, ids, folder)
        save_item(ids.encode('utf-8'), title.encode('utf-8'), price.encode('utf-8'), img.encode('utf-8')
                  , link.encode('utf-8'), filename, store, city)