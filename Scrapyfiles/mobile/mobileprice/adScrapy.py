#!/usr/bin/env python
# -*- coding: utf-8 -*-
from utility import *


class ScrapySpider(scrapy.Spider):
    name = "alldigitall"
    start_urls = [
        "http://alldigitall.ir/"
    ]

    def parse(self, response):
        filename = "ad-alldigitall"
        del_item(filename)
        pages = ['206', '247', '328', '331', '333', '344', '348', '349', '350', '353', '430', '1240', '1629', '1815'
            , '1946', '1947', '1948']
        url1 = 'http://www.alldigitall.ir/index.php?route=product/asearch&filtering=C&filter_stock=yes&path='
        for page in pages:
            link = url1 + page
            new_request = Request(link, callback=self.parse_page, dont_filter=True)
            yield new_request

    def parse_page(self, response):
        blocks = response.xpath('//div[@class="products-block col-nopadding"]/div[@class="product_box"]')
        store = "آل دیجیتال"
        city = "کرمان"
        filename = "ad-alldigitall"
        folder = '10'
        if len(blocks) < 1:
            return
        for block in blocks:
            img = block.xpath('.//div[@class="block-img text-center"]/div[contains(@class, "image")]/a/img/@src').extract()[0]
            img = urlparse.urljoin(response.url, img)
            brand = block.xpath('.//div[@class="product-meta"]/div[@class="top"]/h3/a/text()').extract()[0]
            brand = ' '.join(brand.split())
            title = block.xpath('.//div[@class="product-meta"]/div[@class="top"]/h3/a/text()').extract()[1]
            title = brand + ' ' + ' '.join(title.split())
            title = correction(title)
            link = block.xpath('.//div[@class="block-img text-center"]/div[contains(@class, "image")]/a/@href').extract()[0]
            link = urlparse.urljoin(response.url, link)
            price = block.xpath('.//div[@class="product-meta"]/div[@class="top"]/div[@class="price"]/span//text()').extract()
            if len(price) > 0:
                price = price[0]
                price = fatoen(price)
                price = price.replace(",".decode('utf-8'), "")
                price = price.replace("تومان".decode('utf-8'), "")
                price = ' '.join(price.split())
                price += '0'
            else:
                continue
            rand = '10'
            rand += str(randint(10000, 999999))
            ids = rand
            img = get_image(img, ids, folder)
            save_item(ids.encode('utf-8'), title.encode('utf-8'), price.encode('utf-8'), img.encode('utf-8')
                      , link.encode('utf-8'), filename, store, city)
