#!/usr/bin/env python
# -*- coding: utf-8 -*-
from utility import *


class ScrapySpider(scrapy.Spider):
    name = "salambaba"
    start_urls = [
        "https://salambaba.com/"
    ]

    def parse(self, response):
        filename = "sb-salambaba"
        del_item(filename)
        page = 1
        url1 = 'https://www.salambaba.com/80-phone#/موجود_بودن-موجودی/page-'
        while page < 2:  # 9
            link = url1 + str(page)
            new_request = Request(link, callback=self.parse_page, dont_filter=True)
            yield new_request
            page += 1

    def parse_page(self, response):
        blocks = response.xpath('//ul[contains(@class, "product_list")]/li')
        store = "سلام بابا"
        city = "کردستان"
        filename = "sb-salambaba"
        folder = '26'
        print(len(blocks))
        if len(blocks) < 1:
            return
        for block in blocks:
            img = block.xpath('.//div[@class="product-container"]/div[@class="left-block"]'
                              '//a[@class="product_img_link"]/img/@src').extract()[0]
            # img = urlparse.urljoin(response.url, img)
            title = block.xpath('.//div[@class="product-container"]/div[@class="right-block"]/h5/a/text()').extract()[0]
            title = ' '.join(title.split())
            link = block.xpath('.//div[@class="product-container"]/div[@class="left-block"]'
                              '//a[@class="product_img_link"]/@href').extract()[0]
            link = urlparse.urljoin(response.url, link)
            price = block.xpath('.//div[@class="product-container"]/div[@class="right-block"]/'
                                'div[@class="content_price"]/span[@class="price product-price"]/text()').extract()
            if len(price) > 0:
                price = price[0]
                if not price.encode('utf-8').isdigit():
                    return
                price = fatoen(price)
                price = price.replace(",".decode('utf-8'), "")
                price = price.replace("تومان".decode('utf-8'), "")
                price = ' '.join(price.split())
                price += '0'
            else:
                continue
            if price == '00':
                continue
            rand = '26'
            rand += str(randint(10000, 999999))
            ids = rand
            img = get_image(img, ids, folder)
            save_item(ids.encode('utf-8'), title.encode('utf-8'), price.encode('utf-8'), img.encode('utf-8')
                      , link.encode('utf-8'), filename, store, city)

