#!/usr/bin/env python
# -*- coding: utf-8 -*-
from utility import *


class ScrapySpider(scrapy.Spider):
    name = "datisonline"
    start_urls = [
        "http://datisonline.com/mobile-phones"
    ]

    def parse(self, response):
        filename = "do-datisonline"
        del_item(filename)
        page = 1
        url1 = 'http://datisonline.com/mobile-phones?&p='
        while page < 21:
            link = url1 + str(page)
            new_request = Request(link, callback=self.parse_page, dont_filter=False)
            yield new_request
            page += 1

    def parse_page(self, response):
        blocks = response.xpath('//div[@id="CPH_divProducts"]/div[@class="col-md-3 col-sm-6"]')
        store = "داتیس آنلاین"
        city = "تهران"
        filename = "do-datisonline"
        folder = '17'
        print(len(blocks))
        if len(blocks) < 1:
            return
        for block in blocks:
            img = block.xpath('.//div[@class="single-shop-product"]/div[@class="product-upper"]/a/img/@src').extract()[0]
        #     img = urlparse.urljoin(response.url, img)
            title = block.xpath('.//div[@class="single-shop-product"]/h2/a/text()').extract()[0]
            title = ' '.join(title.split())
            link = block.xpath('.//div[@class="single-shop-product"]/div[@class="product-upper"]/a/@href').extract()[0]
            link = urlparse.urljoin(response.url, link)
            price = block.xpath('.//div[@class="single-shop-product"]/div[@class="product-carousel-price"]/ins//text()').extract()
            if len(price) > 0:
                price = price[0]
                price = fatoen(price)
                price = price.replace(",".decode('utf-8'), "")
                price = price.replace("تومان".decode('utf-8'), "")
                price = price.replace("قیمت گوشی موبایل".decode('utf-8'), "")
                price = ' '.join(price.split())
                if '-'.decode('utf-8') in price:
                    continue
                price += '0'
            else:
                continue
            rand = '17'
            rand += str(randint(10000, 999999))
            ids = rand
            img = get_image(img, ids, folder)
            title = link.split("/")[4].replace("-", " ")
            save_item(ids.encode('utf-8'), title.encode('utf-8'), price.encode('utf-8'), img.encode('utf-8')
                      , link.encode('utf-8'), filename, store, city)
