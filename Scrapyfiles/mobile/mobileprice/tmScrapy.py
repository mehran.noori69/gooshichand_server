#!/usr/bin/env python
# -*- coding: utf-8 -*-
from utility import *


class ScrapySpider(scrapy.Spider):
    name = "tomanak"
    start_urls = [
        "https://tomanak.com/"
    ]

    def parse(self, response):
        filename = "tm-tomanak"
        del_item(filename)
        page = 1
        url1 = 'https://tomanak.com/10-%D9%85%D9%88%D8%A8%D8%A7%DB%8C%D9%84-%D9%88-%D8%AA%D8%A8%D9%84%D8%AA#/page-'
        while page < 2:  # 5
            link = url1 + str(page)
            new_request = Request(link, callback=self.parse_page, dont_filter=True)
            yield new_request
            page += 1

    def parse_page(self, response):
        blocks = response.xpath('//ul[contains(@class, "product_list")]/li')
        store = "تُمنک"
        city = "تهران"
        filename = "tm-tomanak"
        folder = '27'
        print(len(blocks))
        if len(blocks) < 1:
            return
        for block in blocks:
            img = block.xpath('.//div[@class="left-block"]/div[@class="product-image-container"]/a/img/@src').extract()[0]
        #     img = urlparse.urljoin(response.url, img)
            title = block.xpath('.//div[@class="right-block"]/h5/a/text()').extract()[0]
            title = ' '.join(title.split())
            link = block.xpath('.//div[@class="left-block"]/div[@class="product-image-container"]/a/@href').extract()[0]
            link = urlparse.urljoin(response.url, link)
            price = block.xpath('.//div[@class="right-block"]/div[@class="content_price"]//span[@class="price product-price"]//text()').extract()
            if len(price) > 0:
                price = price[0]
                # price = fatoen(price)
                price = price.replace(",".decode('utf-8'), "")
                price = price.replace("تومان".decode('utf-8'), "")
                # price = price.replace("قیمت گوشی موبایل".decode('utf-8'), "")
                price = ' '.join(price.split())
                if 'به زودی'.decode('utf-8') in price:
                    continue
                price += '0'
            else:
                continue
            rand = '27'
            rand += str(randint(10000, 999999))
            ids = rand
            img = get_image(img, ids, folder)
            save_item(ids.encode('utf-8'), title.encode('utf-8'), price.encode('utf-8'), img.encode('utf-8')
                      , link.encode('utf-8'), filename, store, city)

