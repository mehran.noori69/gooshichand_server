#!/usr/bin/env python
# -*- coding: utf-8 -*-
from utility import *


class ScrapySpider(scrapy.Spider):
    name = "gooshishop"
    start_urls = [
        "https://gooshishop.com/"
    ]

    def parse(self, response):
        filename = "gs-gooshishop"
        del_item(filename)
        page = 1
        url1 = 'https://www.gooshishop.com/category/mobile/in-stock/brand-samsung/brand-apple/brand-sony/brand-lg/' \
               'brand-htc/brand-nokia/brand-huawei/brand-blackberry/brand-motorola/brand-asus/brand-xiaomi/brand-glx/' \
               'orderby-latest/page-'
        while page < 10:
            link = url1 + str(page)
            new_request = Request(link, callback=self.parse_page, dont_filter=False)
            yield new_request
            page += 1

    def parse_page(self, response):
        blocks = response.xpath('//ul[@id="items"]/li')
        store = "گوشی شاپ"
        city = "تهران"
        filename = "gs-gooshishop"
        folder = '19'
        print(len(blocks))
        if len(blocks) < 1:
            return
        for block in blocks:
            img = block.xpath('.//div[@class="product-image"]/a/img/@data-original').extract()[0]
            img = urlparse.urljoin(response.url, img)
            title = block.xpath('.//div[@class="product-info"]/h2[@class="product-title"]/a/text()').extract()[0]
            title = ' '.join(title.split())
            link = block.xpath('.//div[@class="product-info"]/h2[@class="product-title"]/a/@href').extract()[0]
            link = urlparse.urljoin(response.url, link)
            price = block.xpath('.//div[@class="product-info"]/div[@class="product-price"]/text()').extract()[0]
            price = price.replace(",".decode('utf-8'), "")
            price = price.replace("تومان".decode('utf-8'), "")
            price = ' '.join(price.split())
            price += '0'
            rand = '19'
            rand += str(randint(10000, 999999))
            ids = rand
            img = get_image(img, ids, folder)
            save_item(ids.encode('utf-8'), title.encode('utf-8'), price.encode('utf-8'), img.encode('utf-8')
                      , link.encode('utf-8'), filename, store, city)
