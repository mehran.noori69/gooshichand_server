#!/usr/bin/env python
# -*- coding: utf-8 -*-
from utility import *


class ScrapySpider(scrapy.Spider):
    name = "amoyadegar"
    start_urls = [
        "https://www.amoyadegar.com/186-%D8%A8%D8%B1-%D8%A7%D8%B3%D8%A7%D8%B3-%D9%86%D9%88%D8%B9-%DA%A9%D8%A7%D8%B1%D8%A7%DB%8C%DB%8C?id_category=186&n=160"
    ]

    def parse(self, response):
        filename = "ay-amoyadegar"
        del_item(filename)
        blocks = response.xpath('//ul[@class="product_list grid row "]/li')
        store = "عمو یادگار"
        city = "کردستان"
        folder = '12'
        print('***********************************************')
        print(response.url)
        print(len(blocks))
        if len(blocks) < 1:
            return
        for block in blocks:
            img = block.xpath('.//div[@class="left-block"]/div[@class="product-image-container"]/a/img/@src').extract()[0]
        #     img = urlparse.urljoin(response.url, img)
            title = block.xpath('.//div[@class="right-block"]/h2/a/text()').extract()[0]
            title = ' '.join(title.split())
            title = correction(title)
            link = block.xpath('.//div[@class="left-block"]/div[@class="product-image-container"]/a/@href').extract()[0]
            link = urlparse.urljoin(response.url, link)
            price = block.xpath('.//div[@class="right-block"]/div[@class="content_price"]/span[@class="price product-price"]//text()').extract()
            if len(price) > 0:
                price = price[0]
                if 'در انبار موجود نیست'.decode('utf-8') in price:
                    continue
                price = fatoen(price)
                price = price.replace(",".decode('utf-8'), "")
                price = price.replace("تومان".decode('utf-8'), "")
                price = ' '.join(price.split())
                price += '0'
            else:
                continue
            rand = '12'
            rand += str(randint(10000, 999999))
            ids = rand
            img = get_image(img, ids, folder)
            save_item(ids.encode('utf-8'), title.encode('utf-8'), price.encode('utf-8'), img.encode('utf-8')
                      , link.encode('utf-8'), filename, store, city)
