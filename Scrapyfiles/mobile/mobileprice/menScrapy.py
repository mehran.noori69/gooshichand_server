#!/usr/bin/env python
# -*- coding: utf-8 -*-
from utility import *


class ScrapySpider(scrapy.Spider):
    name = "menegin"
    start_urls = [
        "http://menegin.com/"
    ]

    def parse(self, response):
        filename = "menegin"
        del_item(filename)
        url1 = 'http://www.menegin.com/%D9%85%D9%88%D8%A8%D8%A7%DB%8C%D9%84-mobile/page/'
        url2 = '?mfp=stock_status[1]'
        index = 0
        while index < 6:
            link = url1 + str(index) + url2
            new_request = Request(link, callback=self.parse_page, dont_filter=False)
            yield new_request
            index += 1

    def parse_page(self, response):
        blocks = response.xpath('//div[contains(@class, "tb_products")]/div[contains(@class, "tb_product")]')
        for block in blocks:
            title = block.xpath('.//a[@class="tb_thumb"]/img/@alt').extract()[0]
            title = ' '.join(title.split())
            link = block.xpath('.//a[@class="tb_thumb"]/@href').extract()[0]
            img = response.xpath('.//a[@class="tb_thumb"]/img/@src').extract()[0]
            store = "نگین خاورمیانه"
            city = "تهران"
            filename = "menegin"
            folder = '37'
            rand = '37'
            rand += str(randint(10000, 999999))
            ids = rand
            price = block.xpath('.//p[@class="tb_price"]/text()').extract()
            if len(price) < 1:
                continue
            price = price[0]
            price = ' '.join(price.split())
            if price == '0':
                continue
            price = fatoen(price)
            price = price.replace(",".decode('utf-8'), "")
            price = price.replace("تومان".decode('utf-8'), "")
            price = ' '.join(price.split())
            price += '0'
            # print(title)
            # print(link)
            # print(img)
            # print(price)
            img = get_image(img, ids, folder)
            save_item(ids.encode('utf-8'), title.encode('utf-8'), price.encode('utf-8'), img.encode('utf-8')
              , link.encode('utf-8'), filename, store, city)
