#!/usr/bin/env python
# -*- coding: utf-8 -*-
from utility import *


class ScrapySpider(scrapy.Spider):
    name = "meghdadit"
    start_urls = [
        "http://meghdadit.com/"
    ]

    def parse(self, response):
        filename = "mi-meghdadit"
        del_item(filename)
        urls = ["http://meghdadit.com/productlist/37/im.true/", "http://meghdadit.com/productlist/37/im.true/page.2/",
                "http://meghdadit.com/productlist/37/im.true/page.3/",
                "http://meghdadit.com/productlist/37/im.true/page.4/",
                "http://meghdadit.com/productlist/37/im.true/page.5/",
                "http://meghdadit.com/productlist/37/im.true/page.6/",
                "http://meghdadit.com/productlist/37/im.true/page.7/"]
        for link in urls:
            new_request = Request(link, callback=self.parse_page, dont_filter=False)
            yield new_request

    def parse_page(self, response):
        blocks = response.xpath('//div[@id="ContentPlaceHolder1_divThumbnailView"]/div[@class="item"]')
        store = "مقداد آی تی"
        city = "تهران"
        filename = "mi-meghdadit"
        folder = '33'
        print(len(blocks))
        if len(blocks) < 1:
            return
        for block in blocks:
            title = block.xpath('.//a[contains(@id, "ContentPlaceHolder1_rptList_hlkTitle_")]/text()').extract()[0]
            title = ' '.join(title.split())
            print(title)
            img = block.xpath('.//a[contains(@id, "ContentPlaceHolder1_rptList_hlkThumbnail_")]/img/@src').extract()[0]
            img = urlparse.urljoin(response.url, img)
            # print(img)
            link = block.xpath('.//a[contains(@id, "ContentPlaceHolder1_rptList_hlkTitle_")]/@href').extract()[0]
            link = urlparse.urljoin(response.url, link)
            # print(link)
            price = block.xpath('.//span[contains(@id, "ContentPlaceHolder1_rptList_lblLowestPrice_")]//text()').extract()
            if len(price) > 0:
                price = price[0]
                # if 'تماس بگیرید'.decode('utf-8') in price:
                #     continue
                # else:
                price = price.replace(",".decode('utf-8'), "")
                price = price.replace("تومان".decode('utf-8'), "")
                price = ' '.join(price.split())
                price = fatoen(price)
                price += '0'
            else:
                continue
            print(price)
            rand = '33'
            rand += str(randint(10000, 999999))
            ids = rand
            img = get_image(img, ids, folder)
            save_item(ids.encode('utf-8'), title.encode('utf-8'), price.encode('utf-8'), img.encode('utf-8')
                      , link.encode('utf-8'), filename, store, city)
