#!/usr/bin/env python
# -*- coding: utf-8 -*-
from utility import *


class ScrapySpider(scrapy.Spider):
    name = "kalabala"
    start_urls = [
        "https://kalabala.ir/"
    ]

    def parse(self, response):
        filename = "kb-kalabala"
        del_item(filename)
        page = 1
        url1 = 'https://kalabala.ir/product-category/%DA%A9%D8%A7%D9%84%D8%A7%DB%8C-' \
               '%D8%AF%DB%8C%D8%AC%DB%8C%D8%AA%D8%A7%D9%84/mobile/page/'
        while page < 10:
            link = url1 + str(page) + "/"
            new_request = Request(link, callback=self.parse_page, dont_filter=False)
            yield new_request
            page += 1

    def parse_page(self, response):
        blocks = response.xpath('//ul[contains(@class, "products")]/li/div[@class="product-inner"]')
        store = "کالابالا"
        city = "اهواز"
        filename = "kb-kalabala"
        folder = '22'
        print(len(blocks))
        if len(blocks) < 1:
            return
        for block in blocks:
            img = block.xpath('.//div[@class="product-thumb"]/a[@class="thumb-link"]/img/@data-src').extract()[0]
            # img = urlparse.urljoin(response.url, img)
            print(img)
            title = block.xpath('.//div[@class="product-info equal-elem"]/h3[@class="product-name product_title"]/a/text()').extract()[0]
            title = ' '.join(title.split())
            title = correction(title)
            print(title)
            link = block.xpath('.//div[@class="product-thumb"]/a/@href').extract()[0]
            link = urlparse.urljoin(response.url, link)
            price = block.xpath('.//div[@class="product-info equal-elem"]/span[@class="price"]/span[@class="woocommerce-Price-amount amount"]//text()').extract()
            if len(price) > 0:
                price = price[0]
                if 'تماس بگیرید'.decode('utf-8') in price:
                    continue
                else:
                    price = price.replace(",".decode('utf-8'), "")
                    price = price.replace("تومان".decode('utf-8'), "")
                    price = ' '.join(price.split())
                    price += '0'
            else:
                continue
            rand = '22'
            rand += str(randint(10000, 999999))
            ids = rand
            img = get_image(img, ids, folder)
            save_item(ids.encode('utf-8'), title.encode('utf-8'), price.encode('utf-8'), img.encode('utf-8')
                      , link.encode('utf-8'), filename, store, city)

