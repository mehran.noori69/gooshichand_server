#!/usr/bin/env python
# -*- coding: utf-8 -*-
from utility import *


class ScrapySpider(scrapy.Spider):
    name = "atramart"
    start_urls = [
        "http://atramart.com/"
    ]

    def parse(self, response):
        filename = "am-atramart"
        del_item(filename)
        page = 1
        url = 'http://www.atramart.com/fa/product/cat/36.html&pagesize=80&stock=1&ajax=ok?_=1525505865447'
        link = url
        new_request = Request(link, callback=self.parse_page, dont_filter=True)
        yield new_request

    def parse_page(self, response):
        blocks = response.xpath('//div[@class="product-list"]/div')
        store = "آترامارت"
        city = "تهران"
        filename = "am-atramart"
        folder = '11'
        print('***********************************************')
        print(response.url)
        print(len(blocks))
        if len(blocks) < 1:
            return
        for block in blocks:
            img = block.xpath('.//div[@class="product-info"]/div[@class="bbp"]/div[@class="iw"]/a/img/@src').extract()[0]
            img = urlparse.urljoin(response.url, img)
            title = block.xpath('.//div[@class="product-info"]/div[@class="bbp"]/div[@class="ip"]/h3/a/text()').extract()[0]
            title = ' '.join(title.split())
            link = block.xpath('.//div[@class="product-info"]/div[@class="bbp"]/div[@class="iw"]/a/@href').extract()[0]
            link = urlparse.urljoin(response.url, link)
            price = block.xpath('.//div[@class="product-info"]/div[@class="bbp"]/div[@class="ip"]/span[@class="price"]//text()').extract()
            if len(price) > 0:
                price = price[0]
                price = fatoen(price)
                price = price.replace(",".decode('utf-8'), "")
                price = price.replace("تومان".decode('utf-8'), "")
                price = ' '.join(price.split())
                price += '0'
            else:
                continue
            rand = '11'
            rand += str(randint(10000, 999999))
            ids = rand
            img = get_image(img, ids, folder)
            save_item(ids.encode('utf-8'), title.encode('utf-8'), price.encode('utf-8'), img.encode('utf-8')
                      , link.encode('utf-8'), filename, store, city)
