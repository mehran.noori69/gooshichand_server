#!/usr/bin/env python
# -*- coding: utf-8 -*-
from utility import *


class ScrapySpider(scrapy.Spider):
    name = "saymandigital"
    start_urls = [
        'https://saymandigital.com/'
    ]

    def parse(self, response):
        filename = "sd-saymandigital"
        del_item(filename)
        page = 1
        url1 = 'https://saymandigital.com/product-category/mobile/page/'
        while page < 12:
            link = url1 + str(page) + '/'
            new_request = Request(link, callback=self.parse_page, dont_filter=True)
            yield new_request
            page += 1

    def parse_page(self, response):
        blocks = response.xpath('//div[contains(@class, "products")]/div[contains(@class, "product")]')
        store = "سایمان دیجیتال"
        city = "آذربایجان شرقی"
        filename = "sd-saymandigital"
        folder = '35'
        print(len(blocks))
        if len(blocks) < 1:
            return
        for block in blocks:
            img = block.xpath('.//a/div[@class="thholder"]/img/@src').extract()[0]
            img = urlparse.urljoin(response.url, img)
            title = block.xpath('.//a/h2[@class="title"]/text()').extract()[0]
            title = ' '.join(title.split())
            title = correction(title)
            link = block.xpath('.//a/@href').extract()[0]
            # link = urlparse.urljoin(response.url, link)
            price = block.xpath('.//a/span[@class="price"]/span[@class="woocommerce-Price-amount amount"]/text()').extract()
            if len(price) > 0:
                price = price[0]
                price = fatoen(price)
                price = price.replace(",".decode('utf-8'), "")
                price = price.replace("تومان".decode('utf-8'), "")
                price = price.replace("ریال".decode('utf-8'), "")
                price = ' '.join(price.split())
                price += '0'
            else:
                continue
            rand = '35'
            rand += str(randint(10000, 999999))
            ids = rand
            img = get_image(img, ids, folder)
            save_item(ids.encode('utf-8'), title.encode('utf-8'), price.encode('utf-8'), img.encode('utf-8')
                      , link.encode('utf-8'), filename, store, city)
