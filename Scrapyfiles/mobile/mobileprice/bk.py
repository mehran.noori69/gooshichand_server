#!/usr/bin/python
# -*- coding: utf-8 -*-
from utility import *


def get_item(page):
    filename = "bk-banekala"
    payload = '{"MinPrice":"0","MaxPrice":"7812000","Brands":"","TakeItems":"20","Page":' + str(page) +\
              ',"GroupID":"228","Available":"false","Property":"","BooleanProperty":"",' \
              '"MySortID":"NewProduct","MyOrderBy":"desc"}'
    headers = {
        'content-type': "application/json",
        'cache-control': "no-cache"
    }
    r = requests.post('https://banekala.ir/AllProducts.aspx/GetProducts', data=payload, headers=headers)
    blocks = json.loads(r.text)['d']['ProductList']
    city = 'تهران'
    store = 'بانه کالا'

    base_url = "https://banekala.ir"
    for block in blocks:
        title = block['Name']
        title = correction(title)
        minprice = block['BuyPrice']
        if minprice == 0 or minprice == '0':
            continue
        minprice += '0'
        link = base_url + block['HRef']
        img = block['Image']
        img = base_url + img
        rand = '30'
        folder = '30'
        rand += str(randint(10000, 999999))
        ids = rand
        img = get_image(img, ids, folder)
        r = requests.post("https://gooshichand.com/mobilesaveitem/", data={'ids': ids, 'title': title
            , 'price': minprice, 'img': img, 'link': link, 'path': filename, 'store': store, 'city': city})
        print(r.status_code)


filename = "bk-banekala"
r = requests.post("https://gooshichand.com/mobiledelitem/", data={'path': filename})
print(r.status_code)
index = 0
while index < 5:
    get_item(index)
    index += 1
