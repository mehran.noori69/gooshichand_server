#!/usr/bin/env python
# -*- coding: utf-8 -*-
from utility import *


class ScrapySpider(scrapy.Spider):
    name = "bprshop"
    start_urls = [
        "https://bprshop.com/"
    ]

    def parse(self, response):
        filename = "bs-bprshop"
        del_item(filename)
        links = ['https://www.bprshop.com/%D9%84%DB%8C%D8%B3%D8%AA-%D9%82%DB%8C%D9%85%D8%AA-%DA%AF%D9%88%D8%B4%DB%8C-'
                 '%D9%85%D9%88%D8%A8%D8%A7%DB%8C%D9%84-%D8%A7%D9%BE%D9%84',
                 'https://www.bprshop.com/%D9%84%DB%8C%D8%B3%D8%AA-%D9%82%DB%8C%D9%85%D8%AA-%DA%AF%D9%88%D8%B4%DB%8C-'
                 '%D9%85%D9%88%D8%A8%D8%A7%DB%8C%D9%84-%D8%B3%D8%A7%D9%85%D8%B3%D9%88%D9%86%DA%AF',
                 'https://www.bprshop.com/%D9%84%DB%8C%D8%B3%D8%AA-%D9%82%DB%8C%D9%85%D8%AA-%DA%AF%D9%88%D8%B4%DB%8C-'
                 '%D9%85%D9%88%D8%A8%D8%A7%DB%8C%D9%84-%D8%A7%D9%84-%D8%AC%DB%8C',
                 'https://www.bprshop.com/%D9%84%DB%8C%D8%B3%D8%AA-%D9%82%DB%8C%D9%85%D8%AA-%DA%AF%D9%88%D8%B4%DB%8C-'
                 '%D9%85%D9%88%D8%A8%D8%A7%DB%8C%D9%84-%D8%A7%DA%86-%D8%AA%DB%8C-%D8%B3%DB%8C',
                 'https://www.bprshop.com/%D9%84%DB%8C%D8%B3%D8%AA-%D9%82%DB%8C%D9%85%D8%AA-%DA%AF%D9%88%D8%B4%DB%8C-'
                 '%D9%85%D9%88%D8%A8%D8%A7%DB%8C%D9%84-%D8%B3%D9%88%D9%86%DB%8C',
                 'https://www.bprshop.com/%D9%84%DB%8C%D8%B3%D8%AA-%D9%82%DB%8C%D9%85%D8%AA-%DA%AF%D9%88%D8%B4%DB%8C-'
                 '%D9%85%D9%88%D8%A8%D8%A7%DB%8C%D9%84-%D9%87%D9%88%D8%A7%D9%88%DB%8C',
                 'https://www.bprshop.com/%D9%84%DB%8C%D8%B3%D8%AA-%D9%82%DB%8C%D9%85%D8%AA-%DA%AF%D9%88%D8%B4%DB%8C-'
                 '%D9%85%D9%88%D8%A8%D8%A7%DB%8C%D9%84-%D8%A8%D9%84%DA%A9-%D8%A8%D8%B1%DB%8C',
                 'https://www.bprshop.com/%D9%84%DB%8C%D8%B3%D8%AA-%D9%82%DB%8C%D9%85%D8%AA-%DA%AF%D9%88%D8%B4%DB%8C-'
                 '%D9%85%D9%88%D8%A8%D8%A7%DB%8C%D9%84-%D8%A7%DB%8C%D8%B3%D9%88%D8%B3',
                 'https://www.bprshop.com/%D9%84%DB%8C%D8%B3%D8%AA-%D9%82%DB%8C%D9%85%D8%AA-%DA%AF%D9%88%D8%B4%DB%8C-'
                 '%D9%85%D9%88%D8%A8%D8%A7%DB%8C%D9%84-%D9%84%D9%86%D9%88%D9%88',
                 'https://www.bprshop.com/%D9%84%DB%8C%D8%B3%D8%AA-%D9%82%DB%8C%D9%85%D8%AA-%DA%AF%D9%88%D8%B4%DB%8C-'
                 '%D9%85%D9%88%D8%A8%D8%A7%DB%8C%D9%84-%D9%85%D8%A7%DB%8C%DA%A9%D8%B1%D9%88%D8%B3%D8%A7%D9%81%D8%AA',
                 'https://www.bprshop.com/%D9%84%DB%8C%D8%B3%D8%AA-%D9%82%DB%8C%D9%85%D8%AA-%DA%AF%D9%88%D8%B4%DB%8C-'
                 '%D9%85%D9%88%D8%A8%D8%A7%DB%8C%D9%84-%D9%86%D9%88%DA%A9%DB%8C%D8%A7']
        for link in links:
            new_request = Request(link, callback=self.parse_page, dont_filter=False)
            yield new_request

    def parse_page(self, response):
        blocks = response.xpath('//table[@class="plem1-tbl wide-view"]//tr')
        if len(blocks) < 2:
            return
        ind = 0
        for block in blocks:
            if ind == 0:
                ind += 1
                continue
            title = block.xpath('.//td//a//text()').extract()[0]
            title = ' '.join(title.split())
            link = block.xpath('.//td//a//@href').extract()[0]
            link = urlparse.urljoin(response.url, link)
            price = block.xpath('.//td//text()').extract()
            for p in price:
                if ','.decode('utf-8') in p:
                    price = p
                    price = fatoen(price)
                    price = price.replace(",".decode('utf-8'), "")
                    price = price.replace("تومان".decode('utf-8'), "")
                    price = ' '.join(price.split())
                    price += '0'
            new_request = Request(link, callback=self.parse_page_details, dont_filter=False)
            new_request.meta['title'] = title
            new_request.meta['price'] = price
            new_request.meta['link'] = link
            yield new_request

    def parse_page_details(self, response):
        title = response.meta['title']
        price = response.meta['price']
        link = response.meta['link']
        print('***********************************************')
        print(response.url)
        img = response.xpath('//div[@class="product-image"]/img/@src').extract()[0]
        img = "https://www.bprshop.com/"+img
        store = "بهینه پردازش"
        city = "تهران"
        filename = "bs-bprshop"
        folder = '13'
        rand = '13'
        rand += str(randint(10000, 999999))
        ids = rand
        img = get_image(img, ids, folder)
        save_item(ids.encode('utf-8'), title.encode('utf-8'), price.encode('utf-8'), img.encode('utf-8')
          , link.encode('utf-8'), filename, store, city)
