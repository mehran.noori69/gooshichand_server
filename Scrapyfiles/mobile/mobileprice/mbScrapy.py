#!/usr/bin/env python
# -*- coding: utf-8 -*-
from utility import *


class ScrapySpider(scrapy.Spider):
    name = "mobilebama"
    start_urls = [
        "https://mobilebama.ir/"
    ]

    def parse(self, response):
        filename = "mb-mobilebama"
        del_item(filename)
        page = 1
        url1 = 'https://www.mobilebama.ir/product-category/mobile-phones/page/'
        while page < 6:
            link = url1 + str(page) + "/"
            new_request = Request(link, callback=self.parse_page, dont_filter=False)
            yield new_request
            page += 1

    def parse_page(self, response):
        blocks = response.xpath('//ul[contains(@class, "products")]//div[@class="pdinner"]')
        store = "موبایل باما"
        city = "شیراز"
        filename = "mb-mobilebama"
        folder = '24'
        print(len(blocks))
        if len(blocks) < 1:
            return
        for block in blocks:
            img = block.xpath('.//figure/a/img/@src').extract()
            if len(img) < 1:
                continue
            img = img[0]
            # img = img.replace("//", "")
            # img = img.replace("www", "http://www")
            # img = urlparse.urljoin(response.url, img)
            title = block.xpath('.//h5/a/text()').extract()[0]
            title = ' '.join(title.split())
            title = correction(title)
            # print(title)
            link = block.xpath('.//figure/a/@href').extract()[0]
            link = urlparse.urljoin(response.url, link)
            price = block.xpath('.//div[@class="price"]/span[contains(@class, "amount")]//text()').extract()
            if len(price) > 0:
                price = price[0]
                price = fatoen(price)
                price = price.replace(",".decode('utf-8'), "")
                price = price.replace("تومان".decode('utf-8'), "")
                price = ' '.join(price.split())
                price += '0'
            else:
                continue
            rand = '24'
            rand += str(randint(10000, 999999))
            ids = rand
            img = get_image(img, ids, folder)
            save_item(ids.encode('utf-8'), title.encode('utf-8'), price.encode('utf-8'), img.encode('utf-8')
                      , link.encode('utf-8'), filename, store, city)

