#!/usr/bin/env python
# -*- coding: utf-8 -*-
from utility import *


class ScrapySpider(scrapy.Spider):
    name = "tabletphone"
    start_urls = [
        "http://tabletphone.ir/"
    ]

    def parse(self, response):
        filename = "tp-tabletphone"
        del_item(filename)
        url1 = 'http://tabletphone.ir/price/phone/page/'
        index = 1
        while index < 7:
            link = url1 + str(index) + '/'
            new_request = Request(link, callback=self.parse_page, dont_filter=False)
            yield new_request
            index += 1

    def parse_page(self, response):
        blocks = response.xpath('//ul[contains(@class, "products")]/li[contains(@class, "product")]')
        print(len(blocks))
        for block in blocks:
            title = block.xpath('.//div[contains(@class, "pinside")]/a/div[@class="text"]/h2/text()').extract()[0]
            title = ' '.join(title.split())
            link = block.xpath('.//div[contains(@class, "pinside")]/a/@href').extract()[0]
            img = response.xpath('.//div[contains(@class, "pinside")]/a/div[@class="image"]/img/@src').extract()[0]
            store = "تبلت فون"
            city = "تهران"
            filename = "tp-tabletphone"
            folder = '38'
            rand = '38'
            rand += str(randint(10000, 999999))
            ids = rand
            price = block.xpath('.//div[contains(@class, "pinside")]/a//span[@class="price-number"]/text()').extract()
            if len(price) < 1:
                continue
            price = price[0]
            price = ' '.join(price.split())
            if price == '0':
                continue
            price = fatoen(price)
            price = price.replace(",".decode('utf-8'), "")
            price = price.replace("تومان".decode('utf-8'), "")
            price = ' '.join(price.split())
            price += '0'
            # print(title)
            # print(link)
            # print(img)
            # print(price)
            img = get_image(img, ids, folder)
            save_item(ids.encode('utf-8'), title.encode('utf-8'), price.encode('utf-8'), img.encode('utf-8')
              , link.encode('utf-8'), filename, store, city)
