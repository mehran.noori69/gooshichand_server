#!/usr/bin/python
# -*- coding: utf-8 -*-
import urllib2
from Scrapyfiles.mobile.mobileprice.utility import *
import json


def get_item():
    filename = "np-nikoopay"
    r = requests.post("https://gooshichand.com/mobiledelitem/", data={'path': filename})
    print(r.status_code)
    city = 'تهران'
    url = "https://www.nikoopay.com/SearchAPI/GetProducts?id=179&sid=0&ps=0&sb=1&sd=1&pz=200&pn=1&_=1510727664171"
    r = requests.get(url, headers={'User-agent': 'Mozilla/5.0'})
    content = r.content
    data = json.loads(content)
    base_url = "https://www.nikoopay.com/"
    base_image_url = "https://www.nikoopay.com/Content/Images/Uploads/Product/Gallery/MainZoom/"
    blocks = data['Products']
    for block in blocks:
        title = block['ProductName']
        title = correction(title)
        store = block['ShopName']
        price = int(block['FinalPriceWithDiscount'])
        price *= 10
        url = block['Url']
        link = base_url+url
        img = block['Image']
        img = base_image_url+img
        rand = '34'
        folder = '34'
        rand += str(randint(10000, 999999))
        ids = rand
        img = get_image(img, ids, folder)
        r = requests.post("https://gooshichand.com/mobilesaveitem/", data={'ids': ids, 'title': title
            , 'price': price, 'img': img, 'link': link, 'path': filename, 'store': store, 'city': city})
        print(r.status_code)


get_item()

