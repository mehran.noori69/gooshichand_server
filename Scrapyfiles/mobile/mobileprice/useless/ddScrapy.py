#!/usr/bin/env python
# -*- coding: utf-8 -*-
from Scrapyfiles.mobile.mobileprice.utility import *


class ScrapySpider(scrapy.Spider):
    name = "digido"
    start_urls = [
        "https://digido.ir/"
    ]

    def parse(self, response):
        filename = "dd-digido"
        del_item(filename)
        page = 1
        while page < 2:  # 10
            link = 'https://digido.ir/modules/blocklayered/blocklayered-ajax.php?id_category_layered=3&' \
                   'layered_price_slider=0_8600000&orderby=quantity&orderway=desc&n=12&p=' + str(page) + '&_=1524395816548'
            new_request = Request(link, callback=self.parse_page, dont_filter=True)
            yield new_request
            page += 1

    def parse_page(self, response):
        blocks = response.xpath('//ul[@class="product_list grid row"]/li')
        store = "دیجی دو"
        city = "تهران"
        filename = "dd-digido"
        folder = '15'
        print(len(blocks))
        if len(blocks) < 1:
            return
        for block in blocks:
            avail = block.xpath('.//div[@class="right-block"]//span[@class="available-now"]//text()').extract()
            if len(avail) < 1:
                continue
            title = block.xpath('.//div[@class="right-block"]/h5/a/text()').extract()[0]
            title = ' '.join(title.split())
            print(title)
            img = block.xpath('.//div[@class="left-block"]/div[@class="product-image-container"]/img/@src').extract()[0]
            img = urlparse.urljoin(response.url, img)
            link = block.xpath('.//div[@class="right-block"]/h5/a/@href').extract()[0]
            link = urlparse.urljoin(response.url, link)
            price = block.xpath('.//div[@class="right-block"]//span[@class="price product-price"]/text()').extract()[0]
            price = price.replace(",".decode('utf-8'), "")
            price = price.replace("تومان".decode('utf-8'), "")
            price = ' '.join(price.split())
            price += '0'
            rand = '15'
            rand += str(randint(10000, 999999))
            ids = rand
            img = get_image(img, ids, folder)
            save_item(ids.encode('utf-8'), title.encode('utf-8'), price.encode('utf-8'), img.encode('utf-8')
                      , link.encode('utf-8'), filename, store, city)
