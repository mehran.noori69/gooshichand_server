#!/usr/bin/env python
# -*- coding: utf-8 -*-
from Scrapyfiles.mobile.mobileprice.utility import *


class ScrapySpider(scrapy.Spider):
    name = "wekama"
    start_urls = [
        'http://www.wekama.com/Product/ResaultSearchPure?KeyWords=&category=Mobiles&MaxPrice=44150000&MinPrice=600000'
        '&SortTypeId=&Step=1&PageSize=120&filter=category_Mobiles%2C&MultiValues=&VOH=&InStock=2'
    ]

    def parse(self, response):
        filename = "wk-wekama"
        del_item(filename)
        blocks = response.xpath('//ul[contains(@class, "product-list")]/li')
        store = "ویکاما"
        city = "ساری"
        folder = '32'
        print(len(blocks))
        if len(blocks) < 1:
            return
        for block in blocks:
            img = block.xpath('.//div[@class="right-block"]/a/img/@src').extract()[0]
            img = urlparse.urljoin(response.url, img)
            title = block.xpath('.//div[@class="left-block"]/h5[@class="product-name"]/a/text()').extract()[0]
            title = ' '.join(title.split())
            link = block.xpath('.//div[@class="right-block"]/a/@href').extract()[0]
            link = urlparse.urljoin(response.url, link)
            price = block.xpath('.//div[@class="left-block"]/div[@class="content_price"]/span[@class="price product-price"]//text()').extract()
            if len(price) > 0:
                price = price[0]
                if price == '0' or price == '0 ریال'.decode('utf-8'):
                    continue
                else:
                    price = price.replace(",".decode('utf-8'), "")
                    price = price.replace("تومان".decode('utf-8'), "")
                    price = price.replace("ریال".decode('utf-8'), "")
                    price = ' '.join(price.split())
                    # price += '0'
            else:
                continue
            rand = '32'
            rand += str(randint(10000, 999999))
            ids = rand
            img = get_image(img, ids, folder)
            save_item(ids.encode('utf-8'), title.encode('utf-8'), price.encode('utf-8'), img.encode('utf-8')
                      , link.encode('utf-8'), filename, store, city)
