#!/usr/bin/python
# -*- coding: utf-8 -*-
import urllib
import urllib2
from Scrapyfiles.mobile.mobileprice.utility import *


def get_item():
    filename = "tt-teektaak"
    index = 0
    r = requests.post("https://gooshichand.com/mobiledelitem/", data={'path': filename})
    post_data = {'limit_hikashop_category_information_menu_504_141': 0, 'limitstart': 0,
                 'limitstart_hikashop_category_information_menu_504_141': 0,
                 'filter_order_hikashop_category_information_menu_504_141': 'a.ordering',
                 'filter_order_Dir_hikashop_category_information_menu_504_141': 'DESC',
                 'ddee16329ca0943ecd044fb696a0fac1': 1}
    result = urllib2.urlopen('https://www.teektaak.ir/کالای-دیجیتال/موبایل', urllib.urlencode(post_data))
    content = result.read()
    # print(content)
    store = "تیک تاک"
    city = "اصفهان"
    blocks = content.split('<div class="item"')
    # print(len(blocks))
    for block in blocks:
        if index >= 1:
            lblock = block.split('<div class="bottom-border">')[0]
            img = lblock.split('<!-- PRODUCT IMG -->')[1].split('<!-- EO PRODUCT IMG -->')[0]
            img = img.split('src="')[1].split('"')[0]
            img = "https://www.teektaak.ir" + img
            link = lblock.split('<!-- PRODUCT IMG -->')[1].split('<!-- EO PRODUCT IMG -->')[0]
            link = link.split('href="')[1].split('"')[0]
            link = "https://www.teektaak.ir" + link
            title = lblock.split('<!-- PRODUCT NAME -->')[1].split('<!-- EO PRODUCT NAME -->')[0]
            title = title.split('title="')[1].split('"')[0]
            title = correction(title)
            price = lblock.split('<!-- PRODUCT PRICE -->')[1].split('<!-- EO PRODUCT PRICE -->')[0]
            t = price.split('price_with_discount">')
            if len(t) < 2:
                t = price.split('hikashop_product_price_0">')
                if len(t) < 2:
                    continue
            price = t[1].split('</span')[0]
            price = price.split('<span class=')[0]
            price = price.replace(",", "")
            price = price.replace("تومان", "")
            price = ' '.join(price.split())
            price += '0'
            rand = '36'
            folder = '36'
            rand += str(randint(10000, 999999))
            ids = rand
            img = get_image(img, ids, folder)
            r = requests.post("https://gooshichand.com/mobilesaveitem/", data={'ids': ids, 'title': title
                , 'price': price, 'img': img, 'link': link, 'path': filename, 'store': store, 'city': city})
        index += 1


get_item()

