#!/usr/bin/env python
# -*- coding: utf-8 -*-
from Scrapyfiles.mobile.mobileprice.utility import *


class ScrapySpider(scrapy.Spider):
    name = "parsismobile"
    start_urls = [
        "http://www.parsismobile.com/"
    ]

    def parse(self, response):
        filename = "pm-parsismobile"
        # del_item(filename)
        url1 = 'https://www.parsismobile.com/list/Mobile/in-stock?page='
        index = 1
        while index < 3:
            link = url1 + str(index)
            new_request = Request(link, callback=self.parse_page, dont_filter=False)
            yield new_request
            index += 1

    def parse_page(self, response):
        blocks = response.xpath('//div[contains(@class, "product-list")]//div[contains(@class, "product-item")]')
        print(len(blocks))
        for block in blocks:
            title = block.xpath('.//img[contains(@class, "product-list-img")]/@alt').extract()[0]
            title = ' '.join(title.split())
            link = block.xpath('.//h3/a/@href').extract()[0]
            link = "https://www.parsismobile.com/" + link
            img = response.xpath('.//img[contains(@class, "product-list-img")]/@src').extract()[0]
            img = img.replace("~/", "")
            img = "https://www.parsismobile.com/" + img
            store = "پارسیس موبایل"
            city = "تهران"
            filename = "pm-parsismobile"
            folder = '39'
            rand = '39'
            rand += str(randint(10000, 999999))
            ids = rand
            price = block.xpath('.//div[contains(@class, "pi-price text-center")]/text()').extract()
            if len(price) < 1:
                continue
            price = price[0]
            price = ' '.join(price.split())
            if price == '0':
                continue
            price = fatoen(price)
            price = price.replace(",".decode('utf-8'), "")
            price = price.replace("تومان".decode('utf-8'), "")
            price = ' '.join(price.split())
            price += '0'
            print(title)
            print(link)
            print(img)
            print(price)
            # img = get_image(img, ids, folder)
            # save_item(ids.encode('utf-8'), title.encode('utf-8'), price.encode('utf-8'), img.encode('utf-8')
            #   , link.encode('utf-8'), filename, store, city)
