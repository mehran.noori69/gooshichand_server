#!/usr/bin/env python
# -*- coding: utf-8 -*-
from Scrapyfiles.mobile.mobileprice.utility import *


class ScrapySpider(scrapy.Spider):
    name = "zanbil"
    start_urls = [
        "https://zanbil.ir/"
    ]

    def parse(self, response):
        filename = "zb-zanbil"
        del_item(filename)
        page = 0
        url1 = 'https://www.zanbil.ir/filter/p62,stexists?page='
        while page < 1:
            link = url1 + str(page)
            new_request = Request(link, callback=self.parse_page, dont_filter=True)
            yield new_request
            page += 1

    def parse_page(self, response):
        blocks = response.xpath('//ul[contains(@class, "showbiz")]/li[@class="span3"]')
        store = "زنبیل"
        city = "تهران"
        filename = "zb-zanbil"
        folder = '29'
        print(len(blocks))
        if len(blocks) < 1:
            return
        for block in blocks:
            img = block.xpath('.//div[@class="thumbnail mediaholder"]/a/img/@src').extract()[0]
            # img = urlparse.urljoin(response.url, img)
            brand = block.xpath('.//div[@class="thumbnail mediaholder"]/a/div[@class="title"]/h4/span/text()').extract()[0]
            brand = ' '.join(brand.split())
            title = block.xpath('.//div[@class="thumbnail mediaholder"]/a/div[@class="title"]/h4/span/text()').extract()[1]
            title = brand + ' ' + ' '.join(title.split())
            link = block.xpath('.//div[@class="thumbnail mediaholder"]/a/@href').extract()[0]
            link = urlparse.urljoin(response.url, link)
            price = block.xpath('.//div[@class="thumbnail mediaholder"]/a/div[@class="title"]/'
                                'span[@class="price"]/span[@class="current"]/span/text()').extract()
            if len(price) > 0:
                price = price[0]
                price = fatoen(price)
                price = price.replace(",".decode('utf-8'), "")
                price = price.replace("تومان".decode('utf-8'), "")
                price = price.replace("ریال".decode('utf-8'), "")
                price = ' '.join(price.split())
                # price += '0'
            else:
                continue
            rand = '29'
            rand += str(randint(10000, 999999))
            ids = rand
            img = get_image(img, ids, folder)
            save_item(ids.encode('utf-8'), title.encode('utf-8'), price.encode('utf-8'), img.encode('utf-8')
                      , link.encode('utf-8'), filename, store, city)

