#!/usr/bin/env python
# -*- coding: utf-8 -*-
from Scrapyfiles.mobile.mobileprice.utility import *


class ScrapySpider(scrapy.Spider):
    name = "dailyoffers"
    start_urls = [
        "https://dailyoffers.ir/"
    ]

    def parse(self, response):
        filename = "dof-dailyoffers"
        del_item(filename)
        page = 1
        url1 = 'https://dailyoffers.ir/Mobile/Mobile-Phone?page='
        while page < 4:
            link = url1 + str(page)
            new_request = Request(link, callback=self.parse_page, dont_filter=False)
            yield new_request
            page += 1

    def parse_page(self, response):
        blocks = response.xpath('//div[@id="products"]//div[contains(@class, "product-col")]')
        store = "دیلی آفرز"
        city = "تهران"
        filename = "dof-dailyoffers"
        folder = '16'
        print(len(blocks))
        if len(blocks) < 1:
            return
        for block in blocks:
            img = block.xpath('.//div[@class="block-img text-center"]/div[@class="image"]/a/img/@src').extract()[0]
            img = urlparse.urljoin(response.url, img)
            title = block.xpath('.//div[@class="product-meta"]/div[@class="top"]/h3/a/text()').extract()[0]
            title = ' '.join(title.split())
            link = block.xpath('.//div[@class="block-img text-center"]/div[@class="image"]/a/@href').extract()[0]
            link = urlparse.urljoin(response.url, link)
            price = block.xpath('.//div[@class="product-meta"]/div[@class="top"]/div[@class="price"]//span[@class="price-new"]//text()').extract()
            if len(price) > 0:
                price = price[0]
                price = price.replace(",".decode('utf-8'), "")
                price = price.replace("تومان".decode('utf-8'), "")
                price = ' '.join(price.split())
                price += '0'
            else:
                continue
            rand = '16'
            rand += str(randint(10000, 999999))
            ids = rand
            img = get_image(img, ids, folder)
            save_item(ids.encode('utf-8'), title.encode('utf-8'), price.encode('utf-8'), img.encode('utf-8')
                      , link.encode('utf-8'), filename, store, city)
