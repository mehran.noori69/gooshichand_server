#!/usr/bin/env python
# -*- coding: utf-8 -*-
from Scrapyfiles.mobile.mobileprice.utility import *


class ScrapySpider(scrapy.Spider):
    name = "kimiaonline"
    start_urls = [
        "https://kimiaonline.com/"
    ]

    def parse(self, response):
        filename = "ko-kimiaonline"
        del_item(filename)
        page = 1
        url1 = 'https://kimiaonline.com/List/m301/mobile-%DA%AF%D9%88%D8%B4%DB%8C-%D9%85%D9%88%D8%A8%D8%A7%DB%8C%D9%84/page-'
        while page < 11:
            link = url1 + str(page)
            new_request = Request(link, callback=self.parse_page, dont_filter=False)
            yield new_request
            page += 1

    def parse_page(self, response):
        blocks = response.xpath('//div[@class="prods-cnt"]//a')
        print(len(blocks))
        if len(blocks) < 1:
            return
        for block in blocks:
            title = block.xpath('.//div[@class="prod-panel-content"]/div[@class="prod-name"]/h3/span/text()').extract()[0]
            title = ' '.join(title.split())
            link = block.xpath('.//@href').extract()[0]
            link = urlparse.urljoin(response.url, link)
            price = block.xpath('.//div[@class="prod-panel-content"]/div[@class="prod-price-cnt"]//div[@class="prod-price"]/text()').extract()
            if len(price) > 0:
                price = price[0]
                price = fatoen(price)
                price = price.replace(",".decode('utf-8'), "")
                price = price.replace("تومان".decode('utf-8'), "")
                price = ' '.join(price.split())
                price += '0'
            else:
                continue
            new_request = Request(link, callback=self.parse_page_details, dont_filter=False)
            new_request.meta['title'] = title
            new_request.meta['price'] = price
            new_request.meta['link'] = link
            yield new_request

    def parse_page_details(self, response):
        title = response.meta['title']
        price = response.meta['price']
        link = response.meta['link']
        img = response.xpath('//a[contains(@class, "PleaseZoom")]/img/@src').extract()[0]
        img = urlparse.urljoin(response.url, img)
        store = "کیمیا آنلاین"
        city = "تهران"
        filename = "ko-kimiaonline"
        folder = '23'
        rand = '23'
        rand += str(randint(10000, 999999))
        ids = rand
        img = get_image(img, ids, folder)
        save_item(ids.encode('utf-8'), title.encode('utf-8'), price.encode('utf-8'), img.encode('utf-8')
                  , link.encode('utf-8'), filename, store, city)

