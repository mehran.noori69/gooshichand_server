#!/usr/bin/env python
# -*- coding: utf-8 -*-
from Scrapyfiles.mobile.mobileprice.utility import *


class ScrapySpider(scrapy.Spider):
    name = "wegobazaar"
    start_urls = [
        "https://wegobazaar.com/"
    ]

    def parse(self, response):
        filename = "wgb-wegobazaar"
        del_item(filename)
        page = 0
        url1 = 'https://www.wegobazaar.com/search/MAIN-MOBILE-PHONES?category=main-mobile-phones&exists=true&from='
        while page < 4:
            link = url1 + str(page)
            new_request = Request(link, callback=self.parse_page, dont_filter=True)
            yield new_request
            page += 1

    def parse_page(self, response):
        blocks = response.xpath('//section[@class="utility-text-align-center"]/a')
        store = "ویگوبازار"
        city = "تهران"
        filename = "wgb-wegobazaar"
        folder = '28'
        print(len(blocks))
        if len(blocks) < 1:
            return
        for block in blocks:
            img = block.xpath('.//figure[@class="image"]/img/@src').extract()[0]
            img = urlparse.urljoin(response.url, img)
            title = block.xpath('.//h3[@class="name"]/span[@class="pname"]/text()').extract()[0]
            title = ' '.join(title.split())
            title = correction(title)
            link = block.xpath('.//@href').extract()[0]
            link = urlparse.urljoin(response.url, link)
            price = block.xpath('.//div[@class="price"]/span/text()').extract()
            if len(price) > 0:
                price = price[0]
                price = fatoen(price)
                price = price.replace(",".decode('utf-8'), "")
                price = price.replace("تومان".decode('utf-8'), "")
                price = ' '.join(price.split())
                price += '0'
            else:
                continue
            rand = '28'
            rand += str(randint(10000, 999999))
            ids = rand
            img = get_image(img, ids, folder)
            save_item(ids.encode('utf-8'), title.encode('utf-8'), price.encode('utf-8'), img.encode('utf-8')
                      , link.encode('utf-8'), filename, store, city)

