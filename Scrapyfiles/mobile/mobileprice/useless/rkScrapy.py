#!/usr/bin/env python
# -*- coding: utf-8 -*-
from Scrapyfiles.mobile.mobileprice.utility import *


class ScrapySpider(scrapy.Spider):
    name = "redkala"
    start_urls = [
        "http://redkala.com/"
    ]

    def parse(self, response):
        filename = "rd-redkala"
        del_item(filename)
        page = 1
        url1 = 'http://redkala.com/product-category/%D9%85%D9%88%D8%A8%D8%A7%DB%8C%D9%84/1-1-0-15/page-'
        while page < 4:
            link = url1 + str(page)
            new_request = Request(link, callback=self.parse_page, dont_filter=True)
            yield new_request
            page += 1

    def parse_page(self, response):
        blocks = response.xpath('//div[@id="divItemRepeater"]/div[@class="template-vitrin"]')
        store = "ردکالا"
        city = "تهران"
        filename = "rd-redkala"
        folder = '25'
        print(len(blocks))
        if len(blocks) < 1:
            return
        for block in blocks:
            img = block.xpath('.//figure[@class="img-show"]/a/img/@src').extract()[0]
            img = urlparse.urljoin(response.url, img)
            title = block.xpath('.//a[@class="TiT"]/text()').extract()[0]
            title = ' '.join(title.split())
            link = block.xpath('.//figure[@class="img-show"]/a/@href').extract()[0]
            link = urlparse.urljoin(response.url, link)
            price = block.xpath('.//p[@class="Price"]/span[@class="price"]/text()').extract()
            if len(price) > 0:
                price = price[0]
                price = fatoen(price)
                price = price.replace(",".decode('utf-8'), "")
                price = price.replace("تومان".decode('utf-8'), "")
                price = ' '.join(price.split())
                price += '0'
            else:
                continue
            rand = '25'
            rand += str(randint(10000, 999999))
            ids = rand
            img = get_image(img, ids, folder)
            save_item(ids.encode('utf-8'), title.encode('utf-8'), price.encode('utf-8'), img.encode('utf-8')
                      , link.encode('utf-8'), filename, store, city)

