#!/usr/bin/env python
# -*- coding: utf-8 -*-
from utility import *


class ScrapySpider(scrapy.Spider):
    name = "images"
    start_urls = [
        "http://www.google.com/"
    ]

    def parse(self, response):
        brands = requests.get('https://gooshichand.com/getitemsbrand/')
        brands = json.loads(brands.content)['results']
        for brand in brands:
            brd = brand[0]
            items = requests.get('https://gooshichand.com/getitemsspec/?path='+brd)
            items = json.loads(items.content)['results']
            for item in items:
                title = item[0]
                link = item[1]
                new_request = Request(link, callback=self.parse_page, dont_filter=True)
                new_request.meta['title'] = title
                yield new_request

    def parse_page(self, response):
        title = response.meta['title']
        imgs = []
        blocks = response.xpath('//a[@class="head-image"]/img/@src').extract()
        for blk in blocks:
            imgs.append(blk)
        index = 0
        gallery = ''
        for img in imgs:
            if index > 0:
                append = '-'+str(index)
            else:
                append = ''
            if "https:" not in img:
                img = "https:" + img
            gallery += get_image(img, title.replace(' ', '_')+append) + ','
            index += 1
        gallery = gallery[:-1]
        result = requests.post('https://gooshichand.com/specchangeimg/', data={'gallery': gallery, 'title': title})
        print(result.content)
