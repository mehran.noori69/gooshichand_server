#!/usr/bin/env python
# -*- coding: utf-8 -*-
from utility import *


class ScrapySpider(scrapy.Spider):
    name = "HTC"
    start_urls = [
        "https://phonearena.com/"
    ]

    def parse(self, response):
        page = 1
        url1 = 'https://www.phonearena.com/phones/manufacturers/HTC/page/'
        while page < page_size:
            link = url1 + str(page)
            new_request = Request(link, callback=self.parse_page, dont_filter=True)
            yield new_request
            page += 1

    def parse_page(self, response):
        blocks = response.xpath('.//div[contains(@class, "widget-tilePhoneCard")]//a[@class="thumbnail"]')
        print(len(blocks))
        if len(blocks) < 1:
            return
        for block in blocks:
            link = block.xpath('.//@href').extract()[0]
            title = block.xpath('.//section/p/text()').extract()[0]
            title = ' '.join(title.split())
            link = urlparse.urljoin(response.url, link)
            img = block.xpath('.//img/@data-src').extract()[0]
            new_request = Request(link, callback=self.parse_pages, dont_filter=True)
            new_request.meta['title'] = title
            new_request.meta['img'] = img
            yield new_request

    def parse_pages(self, response):
        title = response.meta['title']
        img = response.meta['img']
        link = response.url
        camera_back = response.xpath(
            './/div[contains(@class, "field-610")]//span[@class="size-dimensions-container"]/text()').extract()
        if len(camera_back) > 0:
            camera_back = camera_back[0]
        else:
            camera_back = '0.0'
        camera_back = ' '.join(camera_back.split())

        camera_front = response.xpath(
            './/div[contains(@class, "field-377")]//span[@class="size-dimensions-container"]/text()').extract()
        if len(camera_front) > 0:
            camera_front = camera_front[0]
        else:
            camera_front = '0.0'
        camera_front = ' '.join(camera_front.split())

        temps = response.xpath('.//div[contains(@class, "meta-dates")]//span[@class="meta-date"]//text()').extract()
        release_date = 'Jan 1, 2010'
        for temp in temps:
            release_date = ' '.join(temp.split())

        os = response.xpath(
            './/div[contains(@class, "field-119")]//span[@class="size-dimensions-container"]/text()').extract()
        if len(os) > 0:
            os = ' '.join(os[0].split())

        dims = response.xpath(
            './/div[@class="media field-6"]//span[@class="size-dimensions-container"]/text()').extract()
        if len(dims) > 0:
            dims = ' '.join(dims[0].split())

        weight = response.xpath(
            './/div[@class="media field-166"]//span[@class="size-dimensions-container"]/text()').extract()
        if len(weight) > 0:
            weight = ' '.join(weight[0].split())

        inch = response.xpath(
            './/div[@class="media field-253"]//span[@class="size-dimensions-container"]/text()').extract()
        if len(inch) > 0:
            inch = ' '.join(inch[0].split())

        reso = response.xpath(
            './/div[@class="media field-12"]//span[@class="size-dimensions-container"]/text()').extract()
        if len(reso) > 0:
            reso = ' '.join(reso[0].split())

        pd = response.xpath(
            './/div[@class="media field-500"]//span[@class="size-dimensions-container"]/text()').extract()
        if len(pd) > 0:
            pd = ' '.join(pd[0].split())

        stbratio = response.xpath(
            './/div[@class="media field-552"]//span[@class="size-dimensions-container"]/text()').extract()
        if len(stbratio) > 0:
            stbratio = ' '.join(stbratio[0].split())  # screen to body ratio

        cpuchip = response.xpath(
            './/div[@class="media field-573"]//span[@class="size-dimensions-container"]/text()').extract()
        if len(cpuchip) > 0:
            cpuchip = ' '.join(cpuchip[0].split())

        cpu_processor = response.xpath(
            './/div[@class="media field-351"]//span[@class="size-dimensions-container"]/text()').extract()
        if len(cpu_processor) > 0:
            cpu_processor = ' '.join(cpu_processor[0].split())

        gpu = response.xpath(
            './/div[@class="media field-353"]//span[@class="size-dimensions-container"]/text()').extract()
        if len(gpu) > 0:
            gpu = ' '.join(gpu[0].split())

        memory = response.xpath(
            './/div[@class="media field-122"]//span[@class="size-dimensions-container"]/text()').extract()
        if len(memory) > 0:
            memory = ' '.join(memory[0].split())

        storage = response.xpath(
            './/div[@class="media field-774"]//span[@class="size-dimensions-container"]/text()').extract()
        if len(storage) > 0:
            storage = ' '.join((storage[0].split(',')[0]).split())

        battery_capacity = response.xpath(
            './/div[@class="media field-55"]//span[@class="size-dimensions-container"]/text()').extract()
        if len(battery_capacity) > 0:
            battery_capacity = ' '.join(battery_capacity[0].split())

        battery_type = response.xpath(
            './/div[@class="media field-692"]//span[@class="size-dimensions-container"]/text()').extract()
        if len(battery_type) > 0:
            battery_type = ' '.join(battery_type[0].split())

        battery_talk_time = response.xpath(
            './/div[@class="media field-291"]//span[@class="size-dimensions-container"]/text()').extract()
        if len(battery_talk_time) > 0:
            battery_talk_time = ' '.join(battery_talk_time[0].split())

        data = response.xpath(
            './/div[@class="media field-84"]//span[@class="size-dimensions-container"]/text()').extract()
        if len(data) > 0:
            data = ' '.join(data[0].split())

        temps = response.xpath(
            './/div[@class="media field-602"]//span[@class="size-dimensions-container"]/text()').extract()
        nano = False
        if len(temps) > 0:
            temp = ' '.join(temps[0].split())
            nano = temp
            if 'Nano' in nano:
                nano = True
            else:
                nano = False

        bluetooth = response.xpath(
            './/div[@class="media field-33"]//span[@class="size-dimensions-container"]/text()').extract()
        if len(bluetooth) > 0:
            bluetooth = ' '.join(bluetooth[0].split())

        camera_back = camera_back.replace("VGA", "")
        camera_back = camera_back.replace("MP", "")
        camera_back = camera_back.replace('megapixels', '')
        camera_back = camera_back.replace("megapixel", "")
        camera_back = ' '.join(camera_back.split())
        if len(camera_back) < 1:
            camera_back = '0.0'

        camera_front = camera_front.replace("VGA", "")
        camera_front = camera_front.replace("MP", "")
        camera_front = camera_front.replace('megapixels', '')
        camera_front = camera_front.replace("megapixel", "")
        camera_front = camera_front.replace("front", "")
        camera_front = ' '.join(camera_front.split())
        if len(camera_front) < 1 or camera_front == 'Other':
            camera_front = '0.0'

        release_date = datetime.strptime(release_date, '%b %d, %Y')
        if release_date.year < 2014:
            return

        battery_capacity = battery_capacity.replace('mAh', '')
        battery_capacity = ' '.join(battery_capacity.split())

        reso = reso.replace('pixels', '')
        reso = reso.replace(' x ', '*')
        reso = ' '.join(reso.split())

        weight = weight.split("(")[1].split(")")[0]
        weight = weight.replace("g", "")
        weight = ' '.join(weight.split())

        inch = inch.replace("inches", "")
        inch = ' '.join(inch.split())

        pd = pd.replace("ppi", "")
        pd = ' '.join(pd.split())

        stbratio = stbratio.replace("%", "")
        stbratio = ' '.join(stbratio.split())

        memory = memory.split("/")[0]
        memory = memory.replace("GB", "")
        memory = memory.replace("RAM", "")
        memory = memory.replace("MB", "")
        memory = memory.replace("ROM", "")
        memory = ' '.join(memory.split())
        if len(memory) < 1:
            memory = '0.0'
        storage = storage.replace("MB", "")
        storage = storage.replace("ROM", "")
        storage = storage.replace("GB", "")
        storage = ' '.join(storage.split())

        bluetooth = bluetooth.replace("EDR", "")
        bluetooth = bluetooth.replace(",", "")
        bluetooth = ' '.join(bluetooth.split())
        if bluetooth == 'Yes':
            bluetooth = "1"

        if len(cpu_processor) == 0:
            cpu_freq = '0'
            cpu_core = '1'
        else:
            if 'Deca' in cpu_processor:
                cpu_core = '10'
            elif 'Dual' in cpu_processor:
                cpu_core = '2'
            elif 'Hexa' in cpu_processor:
                cpu_core = '16'
            elif 'Octa' in cpu_processor:
                cpu_core = '8'
            elif 'Quad' in cpu_processor:
                cpu_core = '4'
            else:
                cpu_core = '1'
            if 'MHz' not in cpu_processor:
                cpu_freq = '0'
            else:
                cpu_p = cpu_processor.split(",")
                if len(cpu_p) == 1:
                    cpu_freq = cpu_p[0]
                else:
                    if "MHz" in cpu_p[0]:
                        cpu_freq = cpu_p[0]
                    else:
                        cpu_freq = cpu_p[1]
        cpu_freq = cpu_freq.replace("MHz", "")
        cpu_freq = ' '.join(cpu_freq.split())
        cpu_core = ' '.join(cpu_core.split())

        img = get_image(img, title.replace(' ', '_'))
        print(title)
        print(img)
        print(link)
        print(camera_back)
        print(camera_front)
        print(release_date)
        print(os)
        print(dims)
        print(weight)
        print(inch)
        print(reso)
        print(pd)
        print(stbratio)
        print(cpuchip)
        print(cpu_processor)
        print(cpu_freq)
        print(cpu_core)
        print(gpu)
        print(memory)
        print(storage)
        print(battery_capacity)
        print(battery_type)
        print(battery_talk_time)
        print(data)  # needed
        print(nano)
        print(bluetooth)
        print("--------------------------------")
        filename = self.name
        requests.post("https://gooshichand.com/specsaveitem/", data={'title': title, 'img': img, 'link': link,
                                                                     'camera_back': camera_back,
                                                                     'camera_front': camera_front,
                                                                     'release_date': release_date, 'os': os,
                                                                     'dims': dims, 'weight': weight, 'inch': inch,
                                                                     'reso': reso, 'pd': pd, 'stbratio': stbratio,
                                                                     'cpuchip': cpuchip,
                                                                     'cpu_processor': cpu_processor, 'gpu': gpu,
                                                                     'memory': memory, 'storage': storage,
                                                                     'battery_capacity': battery_capacity,
                                                                     'battery_type': battery_type,
                                                                     'battery_talk_time': battery_talk_time,
                                                                     'data': data, 'nano': nano,
                                                                     'bluetooth': bluetooth, 'path': filename,
                                                                     'cpu_freq': cpu_freq, 'cpu_core': cpu_core})
