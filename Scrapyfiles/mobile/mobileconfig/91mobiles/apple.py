#!/usr/bin/env python
# -*- coding: utf-8 -*-
from utility import *


class ScrapySpider(scrapy.Spider):
    name = "Apple"
    start_urls = [
        "https://phonearena.com/"
    ]

    def parse(self, response):
        filename = 'Apple'
        r = requests.post("http://gooshichand.ir/specdelitem/", data={'path': filename})
        page = 1
        url1 = 'https://www.phonearena.com/phones/manufacturers/Apple/page/'
        while page < 3:
            link = url1 + str(page)
            new_request = Request(link, callback=self.parse_page, dont_filter=True)
            yield new_request
            page += 1

    def parse_page(self, response):
        blocks = response.xpath('.//div[@class="s_listing"]//a[@class="s_thumb"]')
        print(len(blocks))
        if len(blocks) < 1:
            return
        for block in blocks:
            link = block.xpath('.//@href').extract()[0]
            title = block.xpath('.//img/@alt').extract()[0]
            title = ' '.join(title.split())
            link = urlparse.urljoin(response.url, link)
            img = block.xpath('.//img/@src').extract()[0]
            img = urlparse.urljoin(response.url, img)
            new_request = Request(link, callback=self.parse_pages, dont_filter=True)
            new_request.meta['title'] = title
            new_request.meta['img'] = img
            yield new_request

    def parse_pages(self, response):
        title = response.meta['title']
