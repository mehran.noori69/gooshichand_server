#!/usr/bin/env python
# -*- coding: utf-8 -*-
from utility import *


class ScrapySpider(scrapy.Spider):
    name = "nodud"
    start_urls = [
        "http://nodud.com/"
    ]

    def parse(self, response):
        # source_name_file = 'nodud'
        # del_item(source_name_file)
        url = "http://nodud.com/tags/"
        items = ['1791']
        for item in items:
            link = url + item + '/'
            new_request = Request(link, callback=self.parse_list, dont_filter=True)
            yield new_request

    def parse_list(self, response):
        blocks = response.xpath('//ul[@class="tagbox"]/li/div[@class="card"]/div[@class="row"]')
        # print(len(blocks))
        for block in blocks:
            img = block.xpath('.//div[@class="photo"]/a/img/@data-srcset').extract()
            if img is None or len(img) < 1:
                continue
            img = img[0].split(" ")[4]
            title = block.xpath('.//div[@class="photo"]/a/img/@alt').extract()[0]
            title = ' '.join(title.split())
            link = block.xpath('.//div[@class="photo"]/a/@href').extract()[0]
            link = urlparse.urljoin(response.url, link)
            short_desc = block.xpath('.//p/text()').extract()[0]
            short_desc = short_desc.split(" - ")[1]
            short_desc = ' '.join(short_desc.split())
            source_ids = link.split("/")[5]
            new_request = Request(link, callback=self.parse_page, dont_filter=True)
            new_request.meta['title'] = title
            new_request.meta['link'] = link
            new_request.meta['img'] = img
            new_request.meta['short_desc'] = short_desc
            new_request.meta['source_ids'] = source_ids
            yield new_request

    def parse_page(self, response):
        source_name = 'نوداد'
        source_name_file = 'nodud'
        folder = '11'
        title = response.meta['title']
        link = response.meta['link']
        img = response.meta['img']
        short_desc = response.meta['short_desc']
        source_news_id = response.meta['source_ids']
        our_news_id = '11' + str(randint(10000000, 99999999))
        big_desc = ''
        short_desc = response.xpath('//p[@class="lead"]/text()').extract()[0]
        short_desc = ' '.join(short_desc.split())
        articles = response.xpath('//div[@class="body-text"]/p//text()').extract()
        for p in articles:
            if len(p) > 50:
                big_desc += '\n' + ' '.join(p.split())
            else:
                big_desc += ' ' + ' '.join(p.split())

        # print(source_name_file)
        # print(source_name)
        # print(image)
        # print(title)
        # print(link)
        # print(short_desc)
        # print(source_news_id)
        # print(our_news_id)
        # print(big_desc)
        # img = get_image(img, our_news_id, folder)
        save_item(source_name_file, source_name, img, title.encode('utf-8'), link, short_desc.encode('utf-8'),
                  our_news_id, big_desc.encode('utf-8'), folder)
