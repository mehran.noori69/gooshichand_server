#!/usr/bin/env python
# -*- coding: utf-8 -*-
from utility import *


class ScrapySpider(scrapy.Spider):
    name = "click"
    start_urls = [
        "https://click.ir/"
    ]

    def parse(self, response):
        # source_name_file = 'click'
        # del_item(source_name_file)
        url = "https://click.ir/tag/"
        # items = ['سامسونگ']
        items = ['سامسونگ', 'samsung', 'آیفون', 'هوآوی', 'huawei', 'ال-جی', 'ایسوس', 'بلک-بری', 'موتورولا',
                 'شیائومی', 'وان-پلاس', 'htc', 'nokia', 'sony', 'apple', 'اپل', 'گوشی']
        for item in items:
            link = url + item + '/'
            new_request = Request(link, callback=self.parse_list, dont_filter=True)
            yield new_request

    def parse_list(self, response):
        blocks = response.xpath('//article[contains(@class, "jeg_pl_md_2")]')
        # print(len(blocks))
        for block in blocks:
            img = block.xpath('.//div[@class="jeg_thumb"]/a//img/@src').extract()
            if img is None or len(img) < 1:
                continue
            img = img[0]
            title = block.xpath('.//div[@class="jeg_postblock_content"]/h3[@class="jeg_post_title"]/a/text()').extract()[0]
            title = ' '.join(title.split())
            title = title.replace('/', '')
            title = ' '.join(title.split())
            link = block.xpath('.//div[@class="jeg_thumb"]/a/@href').extract()[0]
            link = urlparse.urljoin(response.url, link)
            short_desc = block.xpath('.//div[@class="jeg_postblock_content"]/div[@class="jeg_post_excerpt"]/p/text()').extract()[0]
            short_desc = ' '.join(short_desc.split())
            short_desc = short_desc.replace('رسانه کلیک - '.decode('utf-8'), '')
            short_desc = ' '.join(short_desc.split())
            # source_ids = link.split("/")[5]
            new_request = Request(link, callback=self.parse_page, dont_filter=True)
            new_request.meta['title'] = title
            new_request.meta['link'] = link
            new_request.meta['img'] = img
            new_request.meta['short_desc'] = short_desc
            # new_request.meta['source_ids'] = source_ids
            yield new_request

    def parse_page(self, response):
        source_name = 'کلیک'
        source_name_file = 'click'
        folder = '12'
        title = response.meta['title']
        link = response.meta['link']
        img = response.meta['img']
        short_desc = response.meta['short_desc']
        # source_news_id = response.meta['source_ids']
        our_news_id = '12' + str(randint(10000000, 99999999))
        big_desc = ''
        # short_desc = response.xpath('//p[@class="content-inner"]/text()').extract()[0]
        # short_desc = ' '.join(short_desc.split())
        articles = response.xpath('//div[@class="content-inner "]/p//text()').extract()
        index = 0
        for p in articles:
            if 'رسانه کلیک'.decode('utf-8') in p:
                continue
            if len(p) > 50 and index != 0:
                big_desc += '\n' + ' '.join(p.split())
            else:
                big_desc += ' ' + ' '.join(p.split())
            index += 1

        # print(source_name_file)
        # print(source_name)
        # print(image)
        # print(title)
        # print(link)
        # print(short_desc)
        # print(source_news_id)
        # print(our_news_id)
        # print(big_desc)
        # img = get_image(image, our_news_id, folder)
        save_item(source_name_file, source_name, img, title.encode('utf-8'), link, short_desc.encode('utf-8'),
                  our_news_id, big_desc.encode('utf-8'), folder)
