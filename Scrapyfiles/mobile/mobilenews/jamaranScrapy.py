#!/usr/bin/env python
# -*- coding: utf-8 -*-
from utility import *


class ScrapySpider(scrapy.Spider):
    name = "jamaran"
    start_urls = [
        "https://jamaran.ir/"
    ]

    def parse(self, response):
        # source_name_file = 'jamaran'
        # del_item(source_name_file)
        url = "https://jamaran.ir/tags/"
        # items = ['گوشی']
        items = ['سامسونگ', 'samsung', 'آیفون', 'هوآوی', 'huawei', 'ال-جی', 'ایسوس', 'بلک-بری', 'موتورولا',
                 'شیائومی', 'وان-پلاس', 'htc', 'nokia', 'sony', 'apple', 'اپل', 'گوشی']
        for item in items:
            link = url + item
            new_request = Request(link, callback=self.parse_list, dont_filter=True)
            yield new_request

    def parse_list(self, response):
        blocks = response.xpath('//ul[@class="main-news-list clearbox mt24"]/li/article')
        print(len(blocks))
        for block in blocks:
            img = block.xpath('.//a[@class="block wrapimg ml8 float"]/img/@src').extract()
            if img is None or len(img) < 1:
                continue
            img = img[0]
            title = block.xpath('.//a[@class="block wrapimg ml8 float"]/img/@alt').extract()[0]
            title = ' '.join(title.split())
            link = block.xpath('.//a[@class="block wrapimg ml8 float"]/@href').extract()[0]
            link = urlparse.urljoin(response.url, link)
            short_desc = block.xpath('.//p[@class="lead"]/text()').extract()
            if len(short_desc) > 0:
                short_desc = short_desc[0]
                short_desc = ' '.join(short_desc.split())
            else:
                short_desc = ''
            # source_ids = link.split("/")[5]
            new_request = Request(link, callback=self.parse_page, dont_filter=True)
            new_request.meta['title'] = title
            new_request.meta['link'] = link
            new_request.meta['img'] = img
            new_request.meta['short_desc'] = short_desc
            # new_request.meta['source_ids'] = source_ids
            yield new_request

    def parse_page(self, response):
        source_name = 'جماران'
        source_name_file = 'jamaran'
        folder = '13'
        title = response.meta['title']
        link = response.meta['link']
        img = response.meta['img']
        short_desc = response.meta['short_desc']
        # source_news_id = response.meta['source_ids']
        our_news_id = '13' + str(randint(10000000, 99999999))
        big_desc = ''
        # short_desc = response.xpath('//p[@class="content-inner"]/text()').extract()[0]
        # short_desc = ' '.join(short_desc.split())
        articles = response.xpath('.//article[contains(@class, "article")]//section[contains(@class, "article")]//text()').extract()
        for p in articles:
            p = ' '.join(p.split())
            if len(p) < 5:
                continue
            if 'تنظیم و انتشار'.decode('utf-8') in p:
                continue
            if 'انتهای پیام'.decode('utf-8') in p:
                continue
            if len(p) > 50:
                big_desc += '\n' + ' '.join(p.split())
            else:
                big_desc += ' ' + ' '.join(p.split())

        # print(source_name_file)
        # print(source_name)
        # print(image)
        # print(title)
        # print(link)
        # print(short_desc)
        # print(source_news_id)
        # print(our_news_id)
        # print(big_desc)
        # img = get_image(img, our_news_id, folder)
        save_item(source_name_file, source_name, img, title.encode('utf-8'), link, short_desc.encode('utf-8'),
                  our_news_id, big_desc.encode('utf-8'), folder)
