#!/usr/bin/env python
# -*- coding: utf-8 -*-
import urlparse
import scrapy
from scrapy import Spider, Item, Request
from random import *
import requests
import shutil


def get_image(img, ids, folder):
    r = requests.get(img, stream=True, headers={'User-agent': 'Mozilla/5.0'})
    with open('/root/gooshichand_server/static/website/img/news/' + folder + '/' + ids + '.jpg', 'wb') as output:
        r.raw.decode_content = True
        shutil.copyfileobj(r.raw, output)
    img = "https://gooshichand.com/static/website/img/news/" + folder + '/' + ids + ".jpg"
    return img


def del_item(filename):
    r = requests.post("https://gooshichand.com/mobilenewsdelitem/", data={'path': filename})


def save_item(source_name_file, source_name, img, title, link, short_desc, our_news_id, big_desc, folder):
    r = requests.post("https://gooshichand.com/mobilenewssaveitem/",
                      data={'source_name_file': source_name_file, 'source_name': source_name, 'img': img,
                            'title': title, 'link': link, 'short_desc': short_desc, 'our_news_id': our_news_id,
                            'big_desc': big_desc, 'folder': folder})
