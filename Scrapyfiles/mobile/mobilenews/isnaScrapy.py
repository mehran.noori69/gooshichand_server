#!/usr/bin/env python
# -*- coding: utf-8 -*-
from utility import *


class ScrapySpider(scrapy.Spider):
    name = "isna"
    start_urls = [
        "https://www.isna.ir/"
    ]

    def parse(self, response):
        # source_name_file = 'isna'
        # del_item(source_name_file)
        url = "https://www.isna.ir/tag/"
        items = ['اپل', 'سامسونگ', 'ال جی', 'ایسوس', 'شیائومی', 'هوآوی', 'نوکیا', 'سوني']
        for item in items:
            link = url + item
            new_request = Request(link, callback=self.parse_list, dont_filter=True)
            yield new_request

    def parse_list(self, response):
        blocks = response.xpath('//div[@class="items"]/ul/li')
        # print(len(blocks))
        for block in blocks:
            img = block.xpath('.//figure/a/img/@src').extract()
            if img is None or len(img) < 1:
                continue
            img = img[0]
            title = block.xpath('.//figure/a/img/@alt').extract()[0]
            title = ' '.join(title.split())
            title = title.replace('/', '')
            title = ' '.join(title.split())
            link = block.xpath('.//figure/a/@href').extract()[0]
            link = urlparse.urljoin(response.url, link)
            short_desc = block.xpath('.//div[@class="desc"]/p/text()').extract()[0]
            source_ids = link.split("/")[4]
            short_desc = ' '.join(short_desc.split())
            new_request = Request(link, callback=self.parse_page, dont_filter=True)
            new_request.meta['title'] = title
            new_request.meta['link'] = link
            new_request.meta['img'] = img
            new_request.meta['short_desc'] = short_desc
            new_request.meta['source_ids'] = source_ids
            yield new_request

    def parse_page(self, response):
        source_name = 'ایسنا'
        source_name_file = 'isna'
        folder = '10'
        title = response.meta['title']
        link = response.meta['link']
        img = response.meta['img']
        short_desc = response.meta['short_desc']
        source_news_id = response.meta['source_ids']
        our_news_id = '10' + str(randint(10000000, 99999999))
        big_desc = ''
        articles = response.xpath('//div[@class="item-text"]/p//text()').extract()
        for p in articles:
            if 'انتهای پیام'.decode('utf-8') in p:
                continue
            if len(p) > 50:
                big_desc += '\n' + p
            else:
                big_desc += ' ' + p
        all_part = big_desc.split("\n")
        big_desc = ''
        for part in all_part:
            if len(part) > 1:
                big_desc += part + '\n'

        # print(source_name_file)
        # print(source_name)
        # print(image)
        # print(title)
        # print(link)
        # print(short_desc)
        # print(source_news_id)
        # print(our_news_id)
        # print(big_desc)
        # img = get_image(img, our_news_id, folder)
        save_item(source_name_file, source_name, img, title.encode('utf-8'), link, short_desc.encode('utf-8'),
                  our_news_id, big_desc.encode('utf-8'), folder)
