#!/usr/bin/env python
# -*- coding: utf-8 -*-
import json
from random import randint
from datetime import datetime, timedelta
import urlparse
import scrapy
from scrapy import Spider, Item, Request
import re
import requests
import shutil


def fatoen(inputs):
    inputs = inputs.replace('۱'.decode('utf-8'), '1')
    inputs = inputs.replace('۲'.decode('utf-8'), '2')
    inputs = inputs.replace('۳'.decode('utf-8'), '3')
    inputs = inputs.replace('۴'.decode('utf-8'), '4')
    inputs = inputs.replace('۵'.decode('utf-8'), '5')
    inputs = inputs.replace('۶'.decode('utf-8'), '6')
    inputs = inputs.replace('۷'.decode('utf-8'), '7')
    inputs = inputs.replace('۸'.decode('utf-8'), '8')
    inputs = inputs.replace('۹'.decode('utf-8'), '9')
    inputs = inputs.replace('۰'.decode('utf-8'), '0')
    return inputs


def monthofyear(inputs):
    inputs = inputs.replace('فروردین'.decode('utf-8'), '01')
    inputs = inputs.replace('اردیبهشت'.decode('utf-8'), '02')
    inputs = inputs.replace('خرداد'.decode('utf-8'), '03')
    inputs = inputs.replace('تیر'.decode('utf-8'), '04')
    inputs = inputs.replace('مرداد'.decode('utf-8'), '05')
    inputs = inputs.replace('شهریور'.decode('utf-8'), '06')
    inputs = inputs.replace('شهريور'.decode('utf-8'), '06')
    inputs = inputs.replace('مهر'.decode('utf-8'), '07')
    inputs = inputs.replace('آبان'.decode('utf-8'), '08')
    inputs = inputs.replace('آذر'.decode('utf-8'), '09')
    inputs = inputs.replace('دی'.decode('utf-8'), '10')
    inputs = inputs.replace('بهمن'.decode('utf-8'), '11')
    inputs = inputs.replace('اسفند'.decode('utf-8'), '12')
    return inputs


def isEnglish(s):
    try:
        s.encode(encoding='utf-8').decode('ascii')
    except UnicodeDecodeError:
        return False
    else:
        return True


def hasNumbers(inputString):
    return any(char.isdigit() for char in inputString)


def get_number(ids):
    url = "https://www.sheypoor.com/api/web/listings/"+ids+"/number"
    r = requests.get(url)
    js = json.loads(r.content)
    success = js['success']
    if success:
        phone = js['data']['mobileNumber']
        return phone
    else:
        return js['message']
