#!/usr/bin/env python
# -*- coding: utf-8 -*-
from utility import *


class ScrapySpider(scrapy.Spider):
    name = "j5"
    start_urls = [
        "https://www.sheypoor.com/شهر-تهران/موبایل-تبلت-لوازم/موبایل-تبلت?q=j5"
    ]

    def parse(self, response):
        blocks = response.xpath('//article[contains(@class, "serp-item")]')
        print(len(blocks))
        for block in blocks:
            title = block.xpath('.//div[contains(@class, "content")]//h2//a//text()').extract()[0]
            title = ' '.join(title.split())
            image = block.xpath('.//div[contains(@class, "image")]//a//span//img//@src').extract()[0]
            image = ' '.join(image.split())
            link = block.xpath('.//div[contains(@class, "image")]//a//@href').extract()[0]
            link = ' '.join(link.split())
            price = block.xpath('.//div[contains(@class, "content")]//strong[contains(@class, "item-price")]//text()').extract()
            if price is not None and len(price) > 0:
                price = price[0]
                price = ' '.join(price.split())
                price = price.replace(",".decode('utf-8'), "")
                price = fatoen(price)
            else:
                price = '0'
            ids = block.xpath('.//div[contains(@class, "content")]//span[contains(@class, "icon-phone")]//@data-reveal-number').extract()[0]
            time = block.xpath('.//div[contains(@class, "content")]//time//@datetime').extract()[0]
            phone = get_number(ids)
            city = block.xpath('.//div[contains(@class, "content")]//p/text()').extract()[0]
            city = ' '.join(city.split())
            print(ids)
            print(title)
            print(image)
            print(link)
            print(price)
            print(time)
            print(phone)
            print(city)
