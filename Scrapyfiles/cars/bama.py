#!/usr/bin/env python
# -*- coding: utf-8 -*-
import json
from utility import *
import jalali
import telegram
from InstagramAPI import InstagramAPI
import urllib
from random import *

# TOKEN = "327581446:AAFASroeDkOfzpxp6Ngmf_Y920yW7SZqNUU"
TOKEN = "595255997:AAHcV-leW1X0vksOv-l2lu0kQQC9H6oee8w"
bot = telegram.Bot(token=TOKEN)
username = 'dailycaroffers'
password = '66639558'
InstagramAPI = InstagramAPI(username, password)


class ScrapySpider(scrapy.Spider):
    name = "bama"
    start_urls = [
        "https://bama.ir/car?page=2"
    ]

    def parse(self, response):
        url = 'https://bama.ir/car/all-brands/all-models/all-trims?seller=1&pic=true&sort=2&page='
        # url = "https://bama.ir/car/all-brands/all-models?pic=true&sort=2&page="
        index = 1
        InstagramAPI.login()  # login
        while index < 2:
            link = url+str(index)
            new_request = Request(link, callback=self.parse_page, dont_filter=True)
            yield new_request
            index += 1

    def parse_page(self, response):
        lis = response.xpath('.//div[contains(@class, "eventlist")]/ul/li')
        for li in lis:

            link = li.xpath('.//span[@class="photo"]/a/@href').extract()
            if len(link) > 0:
                link = link[0]
                link = urlparse.urljoin(response.url, link)

            title = li.xpath('.//div[@class="detail"]//a[@class="cartitle cartitle-mobile"]/h2/text()').extract()
            if len(title) > 0:
                title = title[0]
                title = ' '.join(title.split())
            else:
                continue

            year = li.xpath('.//div[@class="detail"]//span[@class="year-label visible-xs"]/text()').extract()
            if len(year) > 0:
                year = year[0]
                year = ' '.join(year.split())

            usage = li.xpath('.//div[@class="detail"]//p[@class="price milage-text-mobile visible-xs price-milage-mobile"]//text()').extract()
            if len(usage) > 0:
                usage = usage[1]
                usage = ' '.join(usage.split())
                usage = usage.replace(",".decode('utf-8'), '')
            else:
                usage = '0'

            price = li.xpath('.//div[@class="overview"]//p[@class="cost"]/span/text()').extract()
            if len(price) > 0:
                price = price[0]
                price = ' '.join(price.split())
                price = price.replace(",".decode('utf-8'), '')
            else:
                continue

            if int(price) < 30000000:
                continue

            addresses = li.xpath('.//div[@class="detail"]//p[@class="provice"]//span//text()').extract()
            if len(addresses) > 0:
                city = addresses[0]
                city = city.replace("،".decode('utf-8'), "")
                city = ' '.join(city.split())
            if len(addresses) > 1:
                address = addresses[1]
                address = ' '.join(address.split())
            if len(addresses) > 2:
                address = addresses[2]
                address = ' '.join(address.split())
            if len(addresses) > 3:
                address = addresses[3]
                address = ' '.join(address.split())

            new_request = Request(link, callback=self.parse_pages, dont_filter=True)
            new_request.meta['title'] = title
            new_request.meta['year'] = year
            new_request.meta['usage'] = usage
            new_request.meta['price'] = price
            new_request.meta['city'] = city
            yield new_request

    def parse_pages(self, response):
        title = response.meta['title']
        year = response.meta['year']
        usage = response.meta['usage']
        price = response.meta['price']
        city = response.meta['city']
        # lis = response.xpath('.//span[contains(@class, "eventlist")]/ul/li')
        color = response.xpath('.//f[@itemprop="color"]/text()').extract()
        if len(color) > 0:
            color = color[0]
        descs = response.xpath('.//span[@itemprop="description"]/text()').extract()
        desc = ''
        for de in descs:
            de = ' '.join(de.split())
            desc += de + ' '
        img = response.xpath('//img[@id="main-image"]/@src').extract()
        if len(img) < 1:
            return
        else:
            img = img[0]
        body = response.xpath('//body//text()').extract()
        for p in body:
            if 'var phoneNo = ' in p and "#CaptchaTable').hide()" in p:
                phone = p.split("var phoneNo = ")[1].split(";")[0]
                phone = phone.replace("'", "")
                if '09' in phone and (city == 'تهران'.decode('utf-8') or city == 'البرز'.decode('utf-8')):
                    content = ''
                    content += title.encode('utf-8') + '\n'
                    content += '#' + city.encode('utf-8').replace(' ', "_") + '\n'
                    if price == '0':
                        return
                    else:
                        content += 'قیمت: ' + "{:,}".format(int(price)) + ' ' + 'تومان' + '\n'
                    content += 'مدل: ' + year.encode('utf-8') + '\n'
                    if usage == 'صفر'.decode('utf-8'):
                        content += 'کارکرد: ' + "0" + '\n'
                    else:
                        content += 'کارکرد: ' + "{:,}".format(int(usage)) + '\n'
                    content += 'تماس: ' + phone.encode('utf-8') + '\n'
                    content += 'رنگ: ' + color.encode('utf-8') + '\n'
                    today = str(datetime.today())
                    today = today.split(' ')[0]
                    today = jalali.Gregorian(today).persian_string()
                    content += 'امروز' + ' ' + str(today) + '\n'
                    caption = content + '\n' + 'خرید و فروش خودرو' + '\n'
                    caption += '@daily_cars' + '\n'
                    bot.send_photo(chat_id='@daily_cars', photo=img, caption=caption, disable_web_page_preview=True,
                                   disable_notification=True)
                    rand = randint(10000000, 100000000)
                    number = '/root/cars/' + str(rand) + '.jpg'
                    urllib.urlretrieve(img, number)
                    channel = '\n' + 'خرید و فروش خودرو' + '\n'
                    channel += '@dailycaroffers' + '\n'
                    insta_desc = content + channel
                    InstagramAPI.uploadPhoto(number, caption=content)
