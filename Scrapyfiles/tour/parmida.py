#!/usr/bin/env python
# -*- coding: utf-8 -*-
from utility import *
import jalali


class ScrapySpider(scrapy.Spider):
    name = "parmidatours"
    start_urls = [
        "http://www.parmidatours.com/tours"  # 5556
    ]

    def parse(self, response):
        agancy = 'آژانس مسافرتی پارمیدا'
        del_item(agancy)
        index = 0
        blocks = response.xpath(
            '//div[contains(@class, "view-content")]//div[contains(@class, "row")]'
            '/div[contains(@class, "col")]/div[contains(@class, "tour-list-jd")]')
        for block in blocks:
            link = block.xpath('.//a[contains(@class, "tour-cover")]//@href').extract_first()
            link = urlparse.urljoin(response.url, link)
            new_request = Request(link, callback=self.parse_page_detail, dont_filter=False)
            new_request.meta['index'] = index
            yield new_request
            index += 20

    def parse_page_detail(self, response):
        agancy = 'آژانس مسافرتی پارمیدا'
        index = response.meta['index']
        link = response.url
        title = response.xpath('//h1[contains(@class, "tour-title")]/a//text()').extract_first()
        nights = response.xpath(
            '//div[contains(@class, "date-tour")]//div[contains(@class, "date")]//text()').extract_first()
        temp = ' '.join(nights.split())
        start = temp.split('/')[0]
        start = ' '.join(start.split())
        nights = temp.split('/')[1]
        nightss = nights.split('شب'.decode('utf-8'))
        if len(nightss) > 1:
            nights = nightss[0]
        else:
            nightss = nights.split('روز'.decode('utf-8'))
            nights = nightss[0]

        if 'پکیج'.decode('utf-8') in nights:
            return

        airline = response.xpath(
            '//div[contains(@class, "airline-tour")]//div[contains(@class, "airline")]//text()').extract_first()
        title = ' '.join(title.split())
        if airline is None:
            airline = '-'
        airline = ' '.join(airline.split())

        agancy_address = 'تهران،بلوار میرداماد،نرسیده به میدان مادر، بین شنگرف وحصاری، پلاک 142،طبقه اول'
        agancy_phone = '021-22922017'

        hotels = response.xpath('.//div[contains(@class, "hotels-data")]//div[contains(@class, "hotel")]'
                                '//div[contains(@class, "hotel-details")]')
        name = hotels.xpath('.//div[contains(@class, "title-hotel")]//div[contains(@class, "title")]//a//text()').extract()
        if len(name) < 1:
            return
        name = name[0]
        name = ' '.join(name.split())
        names = [name]
        onebed = hotels.xpath('.//div[contains(@class, "price-hotel")]//div[contains(@class, "price")]/text()').extract()[0]
        onebed = ' '.join(onebed.split())
        price = onebed
        price = price.replace("تومان".decode('utf-8'), "")
        price = price.replace(".".decode('utf-8'), "")
        price = ' '.join(price.split())
        price = fatoen(price)
        temp = hotels.xpath('.//div[contains(@class, "price-hotel")]//div[contains(@class, "type-star")]//ul//li//text()').extract()[0]
        temp = ' '.join(temp.split())
        star = temp.split("*")[0]
        stars = [star]
        service = temp.split("*")[1]
        services = [service]
        count = 1

        content = "size=" + str(count) + "&hotelname="
        for n in names:
            content += n.encode('utf-8') + "-"
        content += "&services="
        for n in services:
            content += n.encode('utf-8') + "-"
        content += "&stars="
        for n in stars:
            content += str(n) + "-"
        content += "&price=" + price.encode('utf-8')

        city, country = set_tag(title)

        rand = '1007'
        rand += str(randint(1000, 9999))
        ids = rand
        save_item(title, city, country, agancy, agancy_phone, agancy_address
                  , link, nights, airline, start, content, ids)
        # print(title)
        # print(city)
        # print(coutry)
        # print(agancy)
        # print(agancy_phone)
        # print(agancy_address)
        # print(link)
        # print(nights)
        # print(airline)
        # print(start)
        # print(content)
