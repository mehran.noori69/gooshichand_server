#!/usr/bin/env python
# -*- coding: utf-8 -*-
from utility import *
import jalali


#kharab


class ScrapySpider(scrapy.Spider):
    name = "amitistravel"
    start_urls = [
        "http://www.amitistravel.com/"  # 3846
    ]

    def parse(self, response):
        agancy = 'آژانس گردشگری آمیتیس'
        del_item(agancy)
        index = 0
        page = 1
        while page < 3:
            link = 'http://www.amitistravel.com/page/214-' + str(page) + \
                   '/%D8%AA%D9%88%D8%B1%D9%87%D8%A7%DB%8C-%D8%A2%D9%85%DB%8C%D8%AA%DB%8C%D8%B3.aspx'
            new_request = Request(link, callback=self.func1, dont_filter=False)
            new_request.meta['index'] = index
            yield new_request
            index += 24000
            page += 1

    def func1(self, response):
        index = response.meta['index']
        findex = index
        blocks = response.xpath('//section[contains(@id, "PageArticleGroups")]/div[contains(@class, "row")]/article')
        for block in blocks:
            link = block.xpath('.//div[contains(@class, "cb-imageWrap")]//a//@href').extract()[0]
            link = urlparse.urljoin(response.url, link)
            new_request = Request(link, callback=self.func2, dont_filter=False)
            new_request.meta['index'] = findex
            yield new_request
            findex += 2000

    def func2(self, response):
        index = response.meta['index']
        findex = index
        blocks = response.xpath('//div[contains(@id, "PageArticleGroups")]/div[contains(@class, "row")]/article')
        for block in blocks:
            link = block.xpath('.//div[contains(@class, "cb-imageWrap")]//a//@href').extract()[0]
            link = urlparse.urljoin(response.url, link)
            new_request = Request(link, callback=self.func3, dont_filter=False)
            new_request.meta['index'] = findex
            yield new_request
            findex += 200

    def func3(self, response):
        index = response.meta['index']
        findex = index
        blocks = response.xpath(
            '//div[contains(@id, "page")]//div[contains(@class, "container")]//div[contains(@class, "row")]'
            '//div[contains(@class, "tls-List")]')
        for block in blocks:
            link = block.xpath('.//div[contains(@class, "vr-image")]//a//@href').extract()[0]
            link = urlparse.urljoin(response.url, link)
            title = block.xpath('.//div[contains(@class, "vr-text")]//a//h3//text()').extract()[0]
            title = ' '.join(title.split())
            temp = title.split('(')
            if len(temp) > 1:
                title = temp[0]
                start = temp[1].split(')')[0]
                if not hasNumbers(start):
                    start = '-'
            else:
                title = temp[0]
                start = '-'
            title = ' '.join(title.split())
            start = ' '.join(start.split())
            temp = block.xpath(
                './/div[contains(@class, "vr-text")]//div[contains(@class, "_fields")]'
                '//span[contains(@class, "airline")]//text()').extract()
            airline = ''
            for t in temp:
                airline += t
            airline = ' '.join(airline.split())
            if airline is None:
                airline = '-'
            airline = airline.replace('ایرلاین: '.decode('utf-8'), '')
            temp = block.xpath(
                './/div[contains(@class, "vr-text")]//div[contains(@class, "_fields")]'
                '/span[contains(@class, "nights")]//text()').extract()
            nights = ''
            for t in temp:
                nights += t
            nights = ' '.join(nights.split())
            nights = fatoen(nights)
            nightss = nights.split('شب'.decode('utf-8'))
            if len(nightss) > 1:
                nights = nightss[0]
            else:
                nightss = nights.split('روز'.decode('utf-8'))
                nights = nightss[0]

            new_request = Request(link, callback=self.parse_page_details, dont_filter=False)
            new_request.meta['index'] = findex
            new_request.meta['title'] = title
            new_request.meta['start'] = start
            new_request.meta['nights'] = nights
            new_request.meta['airline'] = airline
            yield new_request
            findex += 20

    def parse_page_details(self, response):
        filename = "amitistravel"
        agancy = 'آژانس گردشگری آمیتیس'
        index = response.meta['index']
        title = response.meta['title']
        start = response.meta['start']
        nights = response.meta['nights']
        airline = response.meta['airline']
        link = response.url

        phone = response.xpath('//footer[@id="footer"]//div[contains(@class, "container")]'
                               '//div[contains(@class, "fot-Content")]')
        phone = phone.xpath('.//p/text()').extract()
        agancy_address = ' '.join(phone[0].split())
        agancy_phone = ' '.join(phone[1].split())
        agancy_phone = agancy_phone.split(':'.decode('utf-8'))[1]
        agancy_phone = ' '.join(agancy_phone.split())
        agancy_phone = "02188532885".decode('utf-8')

        hotels = response.xpath('.//table[contains(@id, "responsive-dt")]'
                                '/tbody/tr[contains(@class, "package")]')

        trs = hotels[0].xpath('.//td[contains(@class, "hl-name")]/a/text()').extract()
        count = 0
        names = []
        for tr in trs:
            tr = ' '.join(tr.split())
            count += 1
            names.append(tr)

        trs = hotels[0].xpath('.//td[contains(@class, "hl-star")]')
        stars = []
        ind = 0
        while ind < count:
            tr = trs.xpath('.//span[contains(@class, "hl-star-item")]')
            p = tr[ind].xpath('.//i[contains(@class, "fa-star")]')
            xx = len(p)
            stars.append(xx)
            ind += 1

        trs = hotels[0].xpath('.//td[contains(@class, "hl-service")]')
        services = []
        ind = 0
        while ind < count:
            tr = trs.xpath('.//span[contains(@class, "hl-service-item")]//text()').extract()
            xx = tr[ind]
            services.append(xx)
            ind += 1

        trs = hotels[0].xpath('.//td[contains(@class, "hl-price")]//span[contains(@class, "hl-price-item")]')
        price = trs.xpath("./text()").extract()[0]
        price = ' '.join(price.split())
        price = price.replace("تومان".decode('utf-8'), "")
        price = price.replace(",".decode('utf-8'), "")
        price = ' '.join(price.split())
        price = fatoen(price)

        content = "size=" + str(count) + "&hotelname="
        for n in names:
            content += n.encode('utf-8') + "-"
        content += "&services="
        for n in services:
            content += n.encode('utf-8') + "-"
        content += "&stars="
        for n in stars:
            content += str(n) + "-"
        content += "&price=" + price.encode('utf-8')

        city, country = set_tag(title)

        rand = '1003'
        rand += str(randint(1000, 9999))
        ids = rand
        save_item(title, city, country, agancy, agancy_phone, agancy_address
                  , link, nights, airline, start, content, ids)
        # print(title)
        # print(city)
        # print(coutry)
        # print(agancy)
        # print(agancy_phone)
        # print(agancy_address)
        # print(link)
        # print(nights)
        # print(airline)
        # print(start)
        # print(content)
