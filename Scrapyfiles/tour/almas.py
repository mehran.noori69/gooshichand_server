#!/usr/bin/env python
# -*- coding: utf-8 -*-
from utility import *
import jalali


class ScrapySpider(scrapy.Spider):
    name = "almasneshantour"
    start_urls = [
        "https://www.almasneshantour.com/"  # 10137
    ]

    def parse(self, response):
        agancy = 'آژانس مسافرتی الماس نشان'
        del_item(agancy)
        index = 0
        blocks = response.xpath(
            '//div[contains(@class, "tab-content")]//div[contains(@id, "tab1success")]/ul/li')
        for block in blocks:
            link = block.xpath('.//a//@href').extract()[0]
            link = link.replace("../../..", "")
            link = urlparse.urljoin(response.url, link)
            new_request = Request(link, callback=self.parse_page_detail, dont_filter=False)
            new_request.meta['index'] = index
            yield new_request
            index += 1

    def parse_page_detail(self, response):
        filename = "almasneshantour"
        agancy = 'آژانس مسافرتی الماس نشان'
        index = response.meta['index']
        link = response.url

        phone = response.xpath('//footer//div[contains(@class, "container")]'
                               '//div[contains(@class, "footercols")]')[0]
        phone = phone.xpath('.//ul/li')
        xxx = phone[3].xpath(".//strong//text()").extract()[0]
        agancy_address = ' '.join(xxx.split())
        xxx = phone[4].xpath(".//strong//text()").extract()[0]
        agancy_address += " , "+' '.join(xxx.split())
        agancy_address = agancy_address.replace('[email protected]', '')
        agancy_address = ' '.join(agancy_address.split())
        xxx = phone[0].xpath(".//strong//text()").extract()[0]
        agancy_phone = ' '.join(xxx.split())
        xxx = phone[1].xpath(".//strong//text()").extract()[0]
        agancy_phone += " , "+' '.join(xxx.split())

        title = response.xpath('.//div[contains(@class, "summaryL")]//p[contains(@class, "p-title")]//a//text()').extract()[0]
        title = title.replace("بند ر".decode('utf-8'), "بندر".decode('utf-8'))
        title = ' '.join(title.split())
        airline = response.xpath('.//div[contains(@class, "summaryL")]//div[contains(@class, "DoRflt")]/span/text()').extract()[1]
        if airline is None:
            airline = '-'
        airline = ' '.join(airline.split())
        nights = response.xpath('.//div[contains(@class, "summaryL")]//div[contains(@class, "DoRflt")]/span/text()').extract()[2]
        nights = ' '.join(nights.split())
        nights = nights.replace('مدت اقامت:'.decode('utf-8'), '')
        nights = nights.replace('شب'.decode('utf-8'), '')
        nights = ' '.join(nights.split())
        start = response.xpath('.//div[contains(@class, "packMIM")]//div[contains(@class, "PackDet")]/h5/text()').extract()[0]
        start = ' '.join(start.split())
        start = start.replace('تاریخ شروع:'.decode('utf-8'), '')
        year = start.split("/")[0]
        mon = start.split("/")[1]
        day = start.split("/")[2]
        startdate = year + '-' + mon + '-' + day
        startdate = str(startdate)
        start = jalali.Persian(startdate).gregorian_string()

        hotels = response.xpath('.//div[contains(@class, "main")]//table[contains(@class, "tablestyle")]/tr')
        if len(hotels) < 1:
            return
        trs = hotels[1].xpath('.//td[contains(@class, "grytd2")]/a/text()').extract()
        count = 0
        names = []
        for tr in trs:
            tr = ' '.join(tr.split())
            count += 1
            names.append(tr)

        trs = hotels[1].xpath('.//td[contains(@class, "grytd2")]').extract()
        stars = []
        for tr in trs:
            tr = ' '.join(tr.split())
            brs = tr.split("<br>")
            ind = 0
            while ind < count:
                p = brs[2*ind+1]
                xx = p.count('fa-star')
                stars.append(xx)
                ind += 1

        trs = hotels[3].xpath('.//td[contains(@class, "grytd2")]//text()').extract()
        services = []
        for tr in trs:
            tr = ' '.join(tr.split())
            services.append(tr)

        trs = hotels[2].xpath('.//td[contains(@class, "grytd2")]//span[contains(@class, "brb")]')
        price = trs.xpath("./text()").extract()[0]
        price = ' '.join(price.split())
        price = price.replace("تومان".decode('utf-8'), "")
        price = price.replace(",".decode('utf-8'), "")
        price = ' '.join(price.split())
        price = fatoen(price)

        content = "size=" + str(count) + "&hotelname="
        for n in names:
            content += n.encode('utf-8') + "##"
        content += "&services="
        for n in services:
            content += n.encode('utf-8') + "##"
        content += "&stars="
        for n in stars:
            content += str(n) + "##"
        content += "&price=" + price.encode('utf-8')

        city, country = set_tag(title)

        rand = '1002'
        rand += str(randint(1000, 9999))
        ids = rand
        save_item(title, city, country, agancy, agancy_phone, agancy_address
                  , link, nights, airline, start, content, ids)
        # print(title)
        # print(city)
        # print(country)
        # print(agancy)
        # print(agancy_phone)
        # print(agancy_address)
        # print(link)
        # print(nights)
        # print(airline)
        # print(start)
        # print(content)
