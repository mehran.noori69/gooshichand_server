#!/usr/bin/env python
# -*- coding: utf-8 -*-
from random import randint
from datetime import datetime, timedelta
import urlparse

import requests
import scrapy
from scrapy import Spider, Item, Request
import re


def fatoen(inputs):
    inputs = inputs.replace('۱'.decode('utf-8'), '1')
    inputs = inputs.replace('۲'.decode('utf-8'), '2')
    inputs = inputs.replace('۳'.decode('utf-8'), '3')
    inputs = inputs.replace('۴'.decode('utf-8'), '4')
    inputs = inputs.replace('۵'.decode('utf-8'), '5')
    inputs = inputs.replace('۶'.decode('utf-8'), '6')
    inputs = inputs.replace('۷'.decode('utf-8'), '7')
    inputs = inputs.replace('۸'.decode('utf-8'), '8')
    inputs = inputs.replace('۹'.decode('utf-8'), '9')
    inputs = inputs.replace('۰'.decode('utf-8'), '0')
    return inputs


def monthofyear(inputs):
    inputs = inputs.replace('فروردین'.decode('utf-8'), '01')
    inputs = inputs.replace('اردیبهشت'.decode('utf-8'), '02')
    inputs = inputs.replace('خرداد'.decode('utf-8'), '03')
    inputs = inputs.replace('تیر'.decode('utf-8'), '04')
    inputs = inputs.replace('مرداد'.decode('utf-8'), '05')
    inputs = inputs.replace('شهریور'.decode('utf-8'), '06')
    inputs = inputs.replace('شهريور'.decode('utf-8'), '06')
    inputs = inputs.replace('مهر'.decode('utf-8'), '07')
    inputs = inputs.replace('آبان'.decode('utf-8'), '08')
    inputs = inputs.replace('آذر'.decode('utf-8'), '09')
    inputs = inputs.replace('دی'.decode('utf-8'), '10')
    inputs = inputs.replace('بهمن'.decode('utf-8'), '11')
    inputs = inputs.replace('اسفند'.decode('utf-8'), '12')
    return inputs


def isEnglish(s):
    try:
        s.encode(encoding='utf-8').decode('ascii')
    except UnicodeDecodeError:
        return False
    else:
        return True


def hasNumbers(inputString):
    return any(char.isdigit() for char in inputString)


def cleanhtml(raw_html):
    cleanr = re.compile('<.*?>')
    cleantext = re.sub(cleanr, '', raw_html)
    return cleantext


def set_tag(title):
    city = ''
    country = ''
    title = title.encode('utf-8')
    iran = ['کیش', 'مشهد', 'شیراز', 'یزد', 'ایران', 'چابهار', 'تبریز', 'اهواز', 'کرمان', 'اصفهان', 'اراک'
            , 'رامسر', 'ارومیه', 'بندرعباس', 'قشم']
    for i in iran:
        if i in title and 'ایران ایر' not in title:
            if i is not 'ایران':
                city += i+'-'
            if 'ایران' not in country:
                country += 'ایران-'
    armenia = ['ایروان', 'ارمنستان']
    for i in armenia:
        if i in title:
            if i is not 'ارمنستان':
                city += i+'-'
            if 'ارمنستان' not in country:
                country += 'ارمنستان-'
    vietnam = ['ویتنام']
    for i in vietnam:
        if i in title:
            if i is not 'ویتنام':
                city += i+'-'
            if 'ویتنام' not in country:
                country += 'ویتنام-'
    brazil = ['برزیل']
    for i in brazil:
        if i in title:
            if i is not 'برزیل':
                city += i+'-'
            if 'برزیل' not in country:
                country += 'برزیل-'
    italy = ['ایتالیا', 'میلان']
    for i in italy:
        if i in title:
            if i is not 'ایتالیا':
                city += i+'-'
            if 'ایتالیا' not in country:
                country += 'ایتالیا-'
    canada = ['کانادا']
    for i in canada:
        if i in title:
            if i is not 'کانادا':
                city += i+'-'
            if 'کانادا' not in country:
                country += 'کانادا-'
    turkey = ['استانبول', 'آنتالیا', 'کوش آداسی', 'بدروم', 'مارماریس', 'آنکارا', ' وان ', 'ترکیه']
    for i in turkey:
        if i in title:
            if i is not 'ترکیه':
                city += i+'-'
            if 'ترکیه' not in country:
                country += 'ترکیه-'
    serilanka = ['بنتوتا', 'کلمبو', 'کندی', 'سریلانکا']
    for i in serilanka:
        if i in title:
            if i is not 'سریلانکا':
                city += i+'-'
            if 'سریلانکا' not in country:
                country += 'سریلانکا-'
    malesia = ['کوالالامپور', 'مالزی', 'پنانگ', 'لنکاوی']
    for i in malesia:
        if i in title:
            if i is not 'مالزی':
                city += i+'-'
            if 'مالزی' not in country:
                country += 'مالزی-'
    azarba = ['باکو', 'آذربایجان']
    for i in azarba:
        if i in title:
            if i is not 'آذربایجان':
                city += i+'-'
            if 'آذربایجان' not in country:
                country += 'آذربایجان-'
    sangapor = ['سنگاپور']
    for i in sangapor:
        if i in title:
            if i is not 'سنگاپور':
                city += i+'-'
            if 'سنگاپور' not in country:
                country += 'سنگاپور-'
    emirate = ['دبی', 'امارات']
    for i in emirate:
        if i in title and 'پرواز امارات' not in title:
            if i is not 'امارات':
                city += i+'-'
            if 'امارات' not in country:
                country += 'امارات-'
    china = ['پکن', 'شانگهای', 'گوانگجو', 'چین', 'گوانجو']
    for i in china:
        if i in title:
            if i is not 'چین':
                city += i+'-'
            if 'چین' not in country:
                country += 'چین-'
    india = ['دهلی', 'آگرا', 'جیپور', 'هند']
    for i in india:
        if i in title:
            if i is not 'هند':
                city += i+'-'
            if 'هند' not in country:
                country += 'هند-'
    mexico = ['مکزیک']
    for i in mexico:
        if i in title:
            if i is not 'مکزیک':
                city += i+'-'
            if 'مکزیک' not in country:
                country += 'مکزیک-'
    spain = ['اسپانیا', 'بارسلونا']
    for i in spain:
        if i in title:
            if i is not 'اسپانیا':
                city += i+'-'
            if 'اسپانیا' not in country:
                country += 'اسپانیا-'
    swiss = ['سوئیس', 'لوزان']
    for i in swiss:
        if i in title:
            if i is not 'سوئیس':
                city += i+'-'
            if 'سوئیس' not in country:
                country += 'سوئیس-'
    germany = ['آلمان', 'دوسلدورف']
    for i in germany:
        if i in title:
            if i is not 'آلمان':
                city += i+'-'
            if 'آلمان' not in country:
                country += 'آلمان-'
    thailand = ['پوکت', 'پاتایا', 'بانکوک', 'سامویی', 'تایلند']
    for i in thailand:
        if i in title:
            if i is not 'تایلند':
                city += i+'-'
            if 'تایلند' not in country:
                country += 'تایلند-'
    gorjestan = ['تفلیس', 'گرجستان']
    for i in gorjestan:
        if i in title:
            if i is not 'گرجستان':
                city += i+'-'
            if 'گرجستان' not in country:
                country += 'گرجستان-'
    indonesia = ['بالی', 'اندونزی']
    for i in indonesia:
        if i in title:
            if i is not 'اندونزی':
                city += i+'-'
            if 'اندونزی' not in country:
                country += 'اندونزی-'
    france = ['پاریس', 'فلورانس', 'فرانسه']
    for i in france:
        if i in title:
            if i is not 'فرانسه':
                city += i+'-'
            if 'فرانسه' not in country:
                country += 'فرانسه-'
    nether = ['آمستردام', 'هلند']
    for i in nether:
        if i in title:
            if i is not 'هلند':
                city += i+'-'
            if 'هلند' not in country:
                country += 'هلند-'
    maldiv = ['مالدیو']
    for i in maldiv:
        if i in title:
            if i is not 'مالدیو':
                city += i+'-'
            if 'مالدیو' not in country:
                country += 'مالدیو-'
    ghebres = ['قبرس']
    for i in ghebres:
        if i in title:
            if i is not 'قبرس':
                city += i+'-'
            if 'قبرس' not in country:
                country += 'قبرس-'
    afrigha = ['آفریقا']
    for i in afrigha:
        if i in title:
            if i is not 'آفریقا':
                city += i+'-'
            if 'افریقا' not in country:
                country += 'افریقا-'
    moris = ['موریس']
    for i in moris:
        if i in title:
            if i is not 'موریس':
                city += i+'-'
            if 'موریس' not in country:
                country += 'موریس-'
    Seychelles = ['سیشل']
    for i in Seychelles:
        if i in title:
            if i is not 'سیشل':
                city += i+'-'
            if 'سیشل' not in country:
                country += 'سیشل-'
    russia = ['روسیه', 'مسکو']
    for i in russia:
        if i in title:
            if i is not 'روسیه':
                city += i+'-'
            if 'روسیه' not in country:
                country += 'روسیه-'
    marakesh = ['مراکش', 'کازابلانکا']
    for i in marakesh:
        if i in title:
            if i is not 'مراکش':
                city += i+'-'
            if 'مراکش' not in country:
                country += 'مراکش-'
    qatar = ['قطر', 'دوحه']
    for i in qatar:
        if i in title:
            if i is not 'قطر':
                city += i+'-'
            if 'قطر' not in country:
                country += 'قطر-'
    serbia = ['صربستان']
    for i in serbia:
        if i in title:
            if i is not 'صربستان':
                city += i+'-'
            if 'صربستان' not in country:
                country += 'صربستان-'
    return city, country


def del_item(agancy_name):
    r = requests.post("https://gooshichand.com/tours/del/", data={'agancy_name': agancy_name})


def save_item(title, city, country, agancy, agancy_phone, agancy_address, link, nights, airline, start, content, ids):
    r = requests.post("https://gooshichand.com/tours/save/", data={'ids': ids, 'title': title, 'country': country
        , 'agancy_name': agancy, 'link': link, 'agancy_phone': agancy_phone, 'agancy_address': agancy_address
        , 'city': city, 'nights': nights, 'airline': airline, 'start': start, 'content': content})



