#!/usr/bin/env python
# -*- coding: utf-8 -*-
from utility import *
import jalali


# kharab


class ScrapySpider(scrapy.Spider):
    name = "salamparvaz"
    start_urls = [
        "https://www.salamparvaz.com/Default.aspx"  # 4362
    ]

    def parse(self, response):
        agancy = 'آژانس سلام پرواز'
        del_item(agancy)
        index = 0
        blocks = response.xpath('//div[@class="hot-deals-list"]/div/div/article[@class="box "]')
        for block in blocks:
            title = block.xpath('.//div[@class="details"]/span/a/text()').extract()[0]
            title = ' '.join(title.split())
            link = block.xpath('.//div[@class="details"]/span/a/@href').extract()[0]
            link = urlparse.urljoin(response.url, link)
            new_request = Request(link, callback=self.func1, dont_filter=False)
            new_request.meta['index'] = index
            yield new_request
            index += 900

    def func1(self, response):
        index = response.meta['index']
        blocks = response.xpath('//div[@class="hot-deals-list"]//article[@class="box"]')
        for block in blocks:
            title = block.xpath('.//div[@class="details"]/span/a/text()').extract()[0]
            title = ' '.join(title.split())
            nights = block.xpath('.//div[@class="details"]/p[contains(@class, "description")]/text()').extract()[0]
            nights = nights.split("-")[0]
            nights = ' '.join(nights.split())
            price = block.xpath('.//div[@class="feedback"]/p[contains(@class, "description")]/text()').extract()[0]
            price = ' '.join(price.split())
            link = block.xpath('.//div[@class="details"]/span/a/@href').extract()[0]
            link = urlparse.urljoin(response.url, link)
            if '0' in price:
                new_request = Request(link, callback=self.parse_page_detail, dont_filter=False)
                new_request.meta['index'] = index
                new_request.meta['title'] = title
                new_request.meta['nights'] = nights
                new_request.meta['price'] = price
                yield new_request
                index += 30

    def parse_page_detail(self, response):
        filename = "salamparvaz"
        agancy = 'آژانس سلام پرواز'
        index = response.meta['index']
        title = response.meta['title']
        nights = response.meta['nights']
        price = response.meta['price']
        link = response.url

        price = price.replace("تومان".decode('utf-8'), "")
        price = price.replace(".".decode('utf-8'), "")
        price = price.replace(",".decode('utf-8'), "")
        price = price.replace("قیمت از :".decode('utf-8'), "")
        price = ' '.join(price.split())
        price = fatoen(price)

        start = response.xpath('//div[@class="table-cell"]//dl[contains(@class, "term-description")]'
                               '//dd//text()').extract()[0]
        start = start.split(" ")[0]
        start = ' '.join(start.split())
        airline = response.xpath('//div[@class="table-cell"]//dl[contains(@class, "term-description")]'
                               '//dd//text()').extract()[3]
        airline = ' '.join(airline.split())

        phone = response.xpath('//footer[@id="footer"]//div[contains(@class, "container")]'
                               '//span[contains(@itemprop, "streetAddress")]//text()').extract()[0]
        agancy_address = ' '.join(phone.split())
        phone = response.xpath('//footer[@id="footer"]//div[contains(@class, "container")]'
                               '//span[contains(@itemprop, "telephone")]//text()').extract()[0]
        agancy_phone = ' '.join(phone.split())

        hotels = response.xpath('//table[contains(@id, "tableExport")]//tbody[contains(@id, "Pacakgehotel")]/tr')

        names = []
        name = hotels[0].xpath('.//td')[0]
        name = name.xpath('.//text()').extract()
        count = 0
        for n in name:
            n = ' '.join(n.split())
            count += 1
            names.append(n)

        trs = hotels[0].xpath('.//td')[1].extract()
        stars = []
        brs = trs.split("<br>")
        ind = 0
        while ind < count:
            p = brs[ind]
            xx = p.count('<img')
            # if xx != 0:
            #     stars.append(xx)
            stars.append(xx)
            ind += 1

        trs = hotels[0].xpath('.//td')[2]
        trs = trs.xpath('.//text()').extract()
        services = []
        for tr in trs:
            tr = ' '.join(tr.split())
            services.append(tr)

        content = "size=" + str(count) + "&hotelname="
        for n in names:
            content += n.encode('utf-8') + "-"
        content += "&services="
        for n in services:
            content += n.encode('utf-8') + "-"
        content += "&stars="
        for n in stars:
            content += str(n) + "-"
        content += "&price=" + price.encode('utf-8')

        city, coutry = set_tag(title)

        rand = '1008'
        rand += str(randint(1000, 9999))
        ids = rand
        save_item(title, city, country, agancy, agancy_phone, agancy_address
                  , link, nights, airline, start, content, ids)
        # print(title)
        # print(city)
        # print(coutry)
        # print(agancy)
        # print(agancy_phone)
        # print(agancy_address)
        # print(link)
        # print(nights)
        # print(airline)
        # print(start)
        # print(content)
