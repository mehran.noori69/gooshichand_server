#!/usr/bin/env python
# -*- coding: utf-8 -*-
from utility import *
import jalali


class ScrapySpider(scrapy.Spider):
    name = "mehrparvaz"
    start_urls = [
        "https://www.mehrparvaz.com/"  # 6802
    ]

    def parse(self, response):
        agancy = 'آژانس مهر پرواز'
        del_item(agancy)
        index = 0
        blocks = response.xpath('//div[@id="lasttour"]/a')
        for block in blocks:
            link = block.xpath('.//@href').extract()[0]
            link = urlparse.urljoin(response.url, link)
            divs = block.xpath('./div[@class="last_tour_item"]/div')
            title = divs[0].xpath('.//text()').extract()[0]
            title = ' '.join(title.split())
            block = divs[1]
            nights = block.xpath('.//div[@class="last_tour_icons_place"]//text()').extract()[1]
            nights = ' '.join(nights.split())
            airline = block.xpath('.//div[@class="last_tour_icons_airline"]//text()').extract()[1]
            airline = ' '.join(airline.split())
            start = block.xpath('.//div[@class="last_tour_icons_date"]//text()').extract()[1]
            start = ' '.join(start.split())
            new_request = Request(link, callback=self.parse_page_detail, dont_filter=False)
            new_request.meta['index'] = index
            new_request.meta['title'] = title
            new_request.meta['nights'] = nights
            new_request.meta['airline'] = airline
            new_request.meta['start'] = start
            yield new_request
            index += 50
        index = 1000
        blocks = response.xpath('//div[@id="dakheli"]/a')
        for block in blocks:
            link = block.xpath('.//@href').extract()[0]
            link = urlparse.urljoin(response.url, link)
            divs = block.xpath('./div[@class="last_tour_item"]/div')
            title = divs[0].xpath('.//text()').extract()[0]
            title = ' '.join(title.split())
            block = divs[1]
            nights = block.xpath('.//div[@class="last_tour_icons"]//text()').extract()[1]
            nights = ' '.join(nights.split())
            airline = block.xpath('.//div[@class="last_tour_icons"]//text()').extract()[2]
            airline = ' '.join(airline.split())
            start = block.xpath('.//div[@class="last_tour_icons"]//text()').extract()[4]
            start = ' '.join(start.split())
            new_request = Request(link, callback=self.parse_page_detail, dont_filter=False)
            new_request.meta['index'] = index
            new_request.meta['title'] = title
            new_request.meta['nights'] = nights
            new_request.meta['airline'] = airline
            new_request.meta['start'] = start
            yield new_request
            index += 50

    def parse_page_detail(self, response):
        filename = "mehrparvaz"
        agancy = 'آژانس مهر پرواز'
        index = response.meta['index']
        title = response.meta['title']
        nights = response.meta['nights']
        airline = response.meta['airline']
        start = response.meta['start']
        link = response.url

        agancy_address = 'خیابان عباس آباد ، میدان تختی ، ابتدای سورنا ، نبش کوچه آریا وطنی پلاک 48'
        agancy_phone = '021-43638'

        hotels = response.xpath('.//table[contains(@class, "pckage_ways")]/tbody/tr')
        hotel = hotels[0]
        trs = hotel.xpath('.//td')[0]
        trs = trs.xpath('.//span/a/text()').extract()
        count = 0
        names = []
        for tr in trs:
            tr = ' '.join(tr.split())
            count += 1
            names.append(tr)

        trs = hotel.xpath('.//td')[0]
        trs = trs.xpath('.//span[contains(@class, "pckage_hotel")]').extract()
        stars = []
        for tr in trs:
            tr = ' '.join(tr.split())
            brs = tr.split("<br>")
            ind = 0
            while ind < count:
                p = brs[2*ind+1]
                xx = p.count('fa-star')
                stars.append(xx)
                ind += 1

        trs = hotel.xpath('.//td')[1]
        trs = trs.xpath('.//span//text()').extract()
        services = []
        ind = 0
        for tr in trs:
            while ind < count:
                tr = ' '.join(tr.split())
                services.append(tr)
                ind += 1

        trs = hotel.xpath('.//td')[2]
        price = trs.xpath('.//span//text()').extract()[0]
        price = ' '.join(price.split())
        price = price.replace("تومان".decode('utf-8'), "")
        price = price.replace(",".decode('utf-8'), "")
        price = ' '.join(price.split())
        price = fatoen(price)

        content = "size=" + str(count) + "&hotelname="
        for n in names:
            content += n.encode('utf-8') + "-"
        content += "&services="
        for n in services:
            content += n.encode('utf-8') + "-"
        content += "&stars="
        for n in stars:
            content += str(n) + "-"
        content += "&price=" + price.encode('utf-8')

        city, country = set_tag(title)

        rand = '1005'
        rand += str(randint(1000, 9999))
        ids = rand
        save_item(title, city, country, agancy, agancy_phone, agancy_address
                  , link, nights, airline, start, content, ids)
        # print(title)
        # print(city)
        # print(coutry)
        # print(agancy)
        # print(agancy_phone)
        # print(agancy_address)
        # print(link)
        # print(nights)
        # print(airline)
        # print(start)
        # print(content)
