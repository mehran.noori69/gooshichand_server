#!/usr/bin/env python
# -*- coding: utf-8 -*-
from utility import *
import jalali


class ScrapySpider(scrapy.Spider):
    name = "mstiran"
    start_urls = [
        "https://mstiran.com/"  # 4849
    ]

    def parse(self, response):
        agancy = 'مقتدر سیر تابان'
        del_item(agancy)
        filename = "mstiran"
        index = 0
        blocks = response.xpath('//article[contains(@class, "tourBox")]//div[contains(@class, "inner")]')
        for block in blocks:
            title = block.xpath('.//div[contains(@class, "right")]//h3[contains(@class, "title")]//a//text()').extract()[0]
            link = block.xpath('.//div[contains(@class, "right")]//h3[contains(@class, "title")]//a//@href').extract()[0]
            link = urlparse.urljoin(response.url, link)
            days = block.xpath('.//div[contains(@class, "right")]//div[contains(@class, "long")]//text()').extract()[0]
            start = block.xpath('.//div[contains(@class, "right")]//div[contains(@class, "date")]//span//text()').extract()[0]
            end = block.xpath('.//div[contains(@class, "right")]//div[contains(@class, "date")]//span//text()').extract()[1]
            airline = block.xpath('.//div[contains(@class, "right")]//div[contains(@class, "otherInfo")]/span/text()').extract_first()
            if airline is None:
                airline = '-'
            # days = days.split(" ")[3]
            days = fatoen(days)
            nightss = days.split('شب'.decode('utf-8'))
            if len(nightss) > 1:
                days = nightss[0]
            else:
                nightss = days.split('روز'.decode('utf-8'))
                days = nightss[0]
            # now = str(datetime.now()).split(" ")[0]
            # year = jalali.Gregorian(now).persian_year
            start = fatoen(start)
            start = monthofyear(start)
            start = start.encode('utf-8')
            start = start.replace('از ', '')
            start = start.replace(' ', '-')
            day = start.split("-")[0]
            mon = start.split("-")[1]
            year = start.split("-")[2]
            start = year+'-'+mon+'-'+day
            startdate = str(start)
            start = jalali.Persian(startdate).gregorian_string()
            title = ' '.join(title.split())
            start = ' '.join(start.split())
            nights = ' '.join(days.split())
            airline = ' '.join(airline.split())
            new_request = Request(link, callback=self.parse_page_detail, dont_filter=False)
            new_request.meta['index'] = index
            new_request.meta['title'] = title
            new_request.meta['start'] = start
            new_request.meta['nights'] = nights
            new_request.meta['airline'] = airline
            yield new_request
            index += 50

    def parse_page_detail(self, response):
        filename = "alefbatour"
        agancy = 'مقتدر سیر تابان'
        index = response.meta['index']
        title = response.meta['title']
        start = response.meta['start']
        nights = response.meta['nights']
        airline = response.meta['airline']
        link = response.url

        phone = response.xpath('//footer[@id="mainFooter"]//div[contains(@class, "footerDouble")]'
                               '//div[contains(@class, "contactItem")]')
        xxx = phone[0].xpath(".//text()").extract()[1]
        agancy_address = ' '.join(xxx.split())
        agancy_address = agancy_address.split(':'.decode('utf-8'))[1]
        agancy_address = ' '.join(agancy_address.split())
        xxx = phone[1].xpath('.//a[@class="footer-tel"]/text()').extract()[0]
        agancy_phone = ' '.join(xxx.split())
        agancy_phone = fatoen(agancy_phone)

        hotels = response.xpath('//div[contains(@class, "tableWrap")]/table[contains(@class, "table")]'
                                '/tbody/tr[contains(@class, "elementRow")]')

        trs = hotels[0].xpath('.//td[contains(@class, "bigger")]//div[contains(@class, "each-hotel")]')
        names = []
        stars = []
        services = []
        count = len(trs)
        for tr in trs:
            name = tr.xpath(".//text()").extract()[0]
            name = ' '.join(name.split())
            names.append(name)
            t = tr.xpath('.//span//i[contains(@class, "fa-star-o")]')
            star = 5-len(t)
            stars.append(star)
            service = tr.xpath('.//span[contains(@class, "displayBlock")]//text()').extract()[0]
            service = ' '.join(service.split())
            services.append(service)
        price = hotels[0].xpath('.//td//mark//text()').extract()[0]
        price = ' '.join(price.split())
        price = price.replace("تومان".decode('utf-8'), "")
        price = price.replace("٫".decode('utf-8'), "")
        price = ' '.join(price.split())
        price = fatoen(price)

        content = "size=" + str(count) + "&hotelname="
        for n in names:
            content += n.encode('utf-8') + "##"
        content += "&services="
        for n in services:
            content += n.encode('utf-8') + "##"
        content += "&stars="
        for n in stars:
            content += str(n) + "##"
        content += "&price=" + price.encode('utf-8')

        city, country = set_tag(title)

        rand = '1006'
        rand += str(randint(1000, 9999))
        ids = rand
        save_item(title, city, country, agancy, agancy_phone, agancy_address
                  , link, nights, airline, start, content, ids)
        # print(title)
        # print(city)
        # print(coutry)
        # print(agancy)
        # print(agancy_phone)
        # print(agancy_address)
        # print(link)
        # print(nights)
        # print(airline)
        # print(start)
        # print(content)
