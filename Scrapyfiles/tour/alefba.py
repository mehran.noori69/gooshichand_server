#!/usr/bin/env python
# -*- coding: utf-8 -*-
from utility import *
import jalali


class ScrapySpider(scrapy.Spider):
    name = "alefbatour"
    start_urls = [
        "https://www.alefbatour.com/"  # 1948
    ]

    def parse(self, response):
        agancy = 'الفبای سفر پارسیان'
        del_item(agancy)
        index = 0
        blocks = response.xpath(
            '//div[contains(@class, "tab-content")]//div[contains(@class, "tab-pane")]'
            '//div[contains(@class, "uptourlist")]')
        temp = blocks[0]
        blocks = temp.xpath('.//div//a')
        for block in blocks:
            title = block.xpath(
                './/div[contains(@class, "tour-lst")]//h3[contains(@class, "tline")]//text()').extract_first()
            title = ' '.join(title.split())
            temps = block.xpath(
                './/div[contains(@class, "tour-lst")]//div[contains(@class, "tour-info")]//text()').extract()
            price = ''
            nights = ''
            start = ''
            airline = ''
            temp_index = 0
            for temp in temps:
                temp = ' '.join(temp.split())
                if len(temp) > 0:
                    if temp_index == 0:
                        price = temp
                        price = price.replace('قیمت از'.decode('utf-8'), '')
                        price = ' '.join(price.split())
                    elif temp_index == 1:
                        nights = temp
                        nights = nights.replace('شب'.decode('utf-8'), '')
                        nights = ' '.join(nights.split())
                    elif temp_index == 2:
                        start = temp
                        start = start.replace('تاریخ رفت :'.decode('utf-8'), '')
                        start = ' '.join(start.split())
                    elif temp_index == 3:
                        airline = temp
                    temp_index += 1
            link = block.xpath('.//@href').extract_first()
            link = urlparse.urljoin(response.url, link)
            start = monthofyear(start)
            now = str(datetime.now()).split(" ")[0]
            year = jalali.Gregorian(now).persian_year
            start = start.replace(' ', '-')
            start += "-" + str(year)
            day = start.split("-")[0]
            mon = start.split("-")[1]
            year = start.split("-")[2]
            startdate = year + '-' + mon + '-' + day
            startdate = str(startdate)
            startdate = jalali.Persian(startdate).gregorian_string()
            new_request = Request(link, callback=self.parse_page_detail, dont_filter=False)
            new_request.meta['index'] = index
            new_request.meta['title'] = title
            new_request.meta['nights'] = nights
            new_request.meta['airline'] = airline
            new_request.meta['price'] = price
            new_request.meta['startdate'] = startdate
            yield new_request
            index += 1

    def parse_page_detail(self, response):
        filename = "alefbatour"
        agancy = 'الفبای سفر پارسیان'
        index = response.meta['index']
        title = response.meta['title']
        nights = response.meta['nights']
        airline = response.meta['airline']
        start = response.meta['startdate']
        price = response.meta['price']
        link = response.url

        phone = response.xpath('//footer//div[contains(@class, "container")]'
                               '//div[contains(@class, "footer-contact")]/ul/li')
        xxx = phone.xpath(".//text()").extract()[0]
        xxx = ' '.join(xxx.split())
        agancy_address = xxx.split(':'.decode('utf-8'))[1]
        agancy_address = ' '.join(agancy_address.split())
        xxx = phone.xpath(".//text()").extract()[3]
        agancy_phone = ' '.join(xxx.split())

        hotels = response.xpath('.//tbody[contains(@id, "Pacakgehotel")]/tr')

        trs = hotels[0].xpath('.//td[contains(@class, "tblcol")]/a/text()').extract()
        count = 0
        names = []
        for tr in trs:
            tr = ' '.join(tr.split())
            if len(tr) > 1:
                count += 1
                names.append(tr)

        trs = hotels[0].xpath('.//td[contains(@class, "tblcol")]').extract()
        stars = []
        for tr in trs:
            tr = ' '.join(tr.split())
            brs = tr.split("<br>")
            ind = 0
            while ind < count:
                p = brs[2*ind+1]
                xx = p.count('<img')
                stars.append(xx)
                ind += 1

        trs = hotels[0].xpath('.//td')[1]
        trs = trs.xpath("./text()").extract()
        services = []
        for tr in trs:
            tr = ' '.join(tr.split())
            services.append(tr)

        trs = hotels[0].xpath('.//td')[2]
        price = trs.xpath("./text()").extract()[0]
        price = ' '.join(price.split())
        price = price.replace("تومان".decode('utf-8'), "")
        price = price.replace(",".decode('utf-8'), "")
        price = ' '.join(price.split())
        price = fatoen(price)

        content = "size=" + str(count) + "&hotelname="
        for n in names:
            content += n.encode('utf-8') + "##"
        content += "&services="
        for n in services:
            content += n.encode('utf-8') + "##"
        content += "&stars="
        for n in stars:
            content += str(n) + "##"
        content += "&price=" + price.encode('utf-8')

        city, country = set_tag(title)

        rand = '1001'
        rand += str(randint(1000, 9999))
        ids = rand
        save_item(title, city, country, agancy, agancy_phone, agancy_address
                  , link, nights, airline, start, content, ids)
        # print(title)
        # print(city)
        # print(country)
        # print(agancy)
        # print(agancy_phone)
        # print(agancy_address)
        # print(link)
        # print(nights)
        # print(airline)
        # print(start)
        # print(content)
