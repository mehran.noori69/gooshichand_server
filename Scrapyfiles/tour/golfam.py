#!/usr/bin/env python
# -*- coding: utf-8 -*-
from utility import *
import jalali


class ScrapySpider(scrapy.Spider):
    name = "golfamsafar"
    start_urls = [
        "http://www.golfamsafar.com/tours/"  # 1731
    ]

    def parse(self, response):
        agancy = 'آژانس گلفام سفر'
        del_item(agancy)
        index = 0
        blocks = response.xpath('//div[@class="shop"]/div/div/div[@class="product"]')
        for block in blocks:
            title = block.xpath('.//div[@class="product-title"]//h3//a//text()').extract()[0]
            title = ' '.join(title.split())
            link = block.xpath('.//div[@class="product-title"]//h3//a//@href').extract()[0]
            link = urlparse.urljoin(response.url, link)
            new_request = Request(link, callback=self.func1, dont_filter=False)
            new_request.meta['index'] = index
            yield new_request
            index += 500

    def func1(self, response):
        index = response.meta['index']
        blocks = response.xpath('//div[@class="shop  "]/div/div/div[@class="product"]'
                                '/div[@class="product-description"]')
        for block in blocks:
            title = block.xpath('.//div[@class="product-title"]//h3//a//text()').extract()[0]
            title = ' '.join(title.split())
            link = block.xpath('.//div[@class="product-title"]//h3//a//@href').extract()[0]
            link = urlparse.urljoin(response.url, link)
            start = block.xpath('.//div[@class="product-category"]')[0]
            end = start.xpath('.//b')[1]
            start = start.xpath('.//b')[0]
            start = start.xpath('.//text()').extract()[0]
            start = ' '.join(start.split())
            year = start.split("/")[0]
            mon = start.split("/")[1]
            day = start.split("/")[2]
            startdate = year + '-' + mon + '-' + day
            startdate = str(startdate)
            start = jalali.Persian(startdate).gregorian_string()
            end = end.xpath('.//text()').extract()[0]
            end = ' '.join(end.split())
            new_request = Request(link, callback=self.parse_page_detail, dont_filter=False)
            new_request.meta['index'] = index
            new_request.meta['title'] = title
            new_request.meta['start'] = start
            new_request.meta['end'] = end
            yield new_request
            index += 50

    def parse_page_detail(self, response):
        filename = "golfamsafar"
        agancy = 'آژانس گلفام سفر'
        index = response.meta['index']
        title = response.meta['title']
        start = response.meta['start']
        link = response.url
        phone = response.xpath('//footer[contains(@class, "footer")]/div[contains(@class, "container")]'
                               '/div[contains(@style, "text-align: right")]')
        agancy_phone = phone.xpath('.//text()').extract()[3]
        agancy_phone = agancy_phone.split(':'.decode('utf-8'))[1]
        agancy_phone = ' '.join(agancy_phone.split())
        agancy_phone = agancy_phone.replace("-021".decode('utf-8'), "")
        agancy_phone = "021".decode('utf-8')+agancy_phone

        agancy_address = phone.xpath('.//text()').extract()[7]
        agancy_address = agancy_address.split(':'.decode('utf-8'))[1]
        agancy_address = ' '.join(agancy_address.split())

        row = response.xpath('//div[@class="row"]')[0]
        nights = row.xpath('.//span//text()').extract()[1]
        nightss = nights.split('شب'.decode('utf-8'))
        if len(nightss) > 1:
            nights = nightss[0]
        else:
            nightss = nights.split('روز'.decode('utf-8'))
            nights = nightss[0]
        airline = response.xpath('//div[@class="row"]//span//table/tr/th/text()').extract()
        if len(airline) > 1:
            airline = airline[1]
        else:
            airline = response.xpath('//div[@class="row"]//span//b/text()').extract()[0]
            airline = airline.split("پروازرفت وبرگشت ".decode('utf-8'))[1]
            airline = airline.split(" ".decode('utf-8'))[0]
            airline = airline.split("-".decode('utf-8'))[0]
        airline = ' '.join(airline.split())

        trs = response.xpath('//div[@class="shop-cart"]/div[contains(@class, "table")]'
                             '/table[contains(@class, "prc_tbl")]')
        ths = trs[0].xpath(".//th//text()").extract()
        tharray = []
        for th in ths:
            tharray.append(th)
        hotels = trs.xpath('.//tr[contains(@style, "border-right")]')
        names = hotels[0].xpath('.//td[contains(@class, "td_txt_top")]')[0]
        service = hotels[0].xpath('.//td[contains(@class, "td_txt_top")]')[1]
        prices = hotels[0].xpath('.//td[contains(@class, "td_txt_top")]')[2]
        trs = names.xpath('.//table[contains(@class, "htl_detail")]/tr/td/a/text()').extract()
        count = 0
        names = []
        for tr in trs:
            tr = ' '.join(tr.split())
            if len(tr) > 1:
                count += 1
                names.append(tr)
        trs = service.xpath('.//table[contains(@class, "htl_detail")]/tr/td/text()').extract()
        services = []
        for tr in trs:
            tr = ' '.join(tr.split())
            if len(tr) > 1:
                services.append(tr)
        inde = 0
        stars = []
        while inde < count:
            trs = service.xpath('.//table[contains(@class, "htl_detail")]/tr')[2*inde+1]
            star = trs.xpath('./td/span[contains(@class, "glyphicon")]')
            s = len(star)
            stars.append(s)
            inde += 1
        price = prices.xpath('.//b/text()').extract()[0]
        price = ' '.join(price.split())
        price = price.replace("تومان".decode('utf-8'), "")
        price = price.replace(",".decode('utf-8'), "")
        price = ' '.join(price.split())
        price = fatoen(price)

        content = "size="+str(count)+"&hotelname="
        for n in names:
            content += n.encode('utf-8')+"##"
        content += "&services="
        for n in services:
            content += n.encode('utf-8')+"##"
        content += "&stars="
        for n in stars:
            content += str(n)+"##"
        content += "&price="+price.encode('utf-8')

        city, country = set_tag(title)

        rand = '1004'
        rand += str(randint(1000, 9999))
        ids = rand
        save_item(title, city, country, agancy, agancy_phone, agancy_address
                  , link, nights, airline, start, content, ids)
        # print(title)
        # print(city)
        # print(country)
        # print(agancy)
        # print(agancy_phone)
        # print(agancy_address)
        # print(link)
        # print(nights)
        # print(airline)
        # print(start)
        # print(content)
