#!/usr/bin/env python
# -*- coding: utf-8 -*-
# import os
# os.environ['DJANGO_SETTINGS_MODULE'] = 'doresa.settings'
# import django
# django.setup()

from scrapy import Request
import scrapy
import cfscrape


class ScrapySpider(scrapy.Spider):
    name = "whtvps"
    start_urls = [
        "http://www.webhostingtalk.ir/forumdisplay.php?f=13"
    ]

    # def start_requests(self):
    #     cf_requests = []
    #     token, agent = cfscrape.get_tokens(self.start_urls[0], 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36'
    #                                             ' (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36,_optional_')
    #     cf_requests.append(Request(url=self.start_urls[0],
    #                                # cookies={'__cfduid': token['__cfduid']},
    #                                cookies=token,
    #                                headers={'User-Agent': agent}))
    #     print "useragent in cfrequest: ", agent
    #     print "token in cfrequest: ", token
    #     return cf_requests

    def parse(self, response):
        trs = response.css("ol#threads li.threadbit")
        ids = ""
        index = 0
        for tr in trs:
            title = tr.css("a.title::text").extract_first()
            title = ' '.join(title.split())
            user = tr.css("a.username::text").extract_first()
            user = ' '.join(user.split())
            link = tr.css("a.title::attr(href)").extract_first()
            link = ' '.join(link.split())
            link = link.split("&")[0]
            link = "http://www.webhostingtalk.ir/" + link
            ids = link.split("?t=")[1]
            dates = tr.css("span.label::text").extract()
            ind = 0
            for date in dates:
                if ind == 1:
                    date = date.replace(",", "")
                    date = ' '.join(date.split())
                ind += 1
            uls = tr.css("ul.threadstats")
            for ul in uls:
                lis = ul.css("li")
                ind = 0
                for li in lis:
                    if ind == 0:
                        txt = li.css("::text").extract_first()
                        txt = txt.split(":")[1]
                        txt = ' '.join(txt.split())
                        resp = txt
                    elif ind == 1:
                        txt = li.css("::text").extract_first()
                        txt = txt.split(":")[1]
                        txt = ' '.join(txt.split())
                        view = txt
                    ind += 1
            print(title)
            print(user)
            print(link)
            print(ids)
            print(date)
            print(resp)
            print(view)
            # new_request = Request(link, callback=self.parse_page_detail, dont_filter=True)
            # new_request.meta['title'] = title
            # new_request.meta['user'] = user
            # new_request.meta['link'] = link
            # new_request.meta['ids'] = ids
            # new_request.meta['date'] = date
            # new_request.meta['resp'] = resp
            # new_request.meta['view'] = view
            # new_request.meta['index'] = index
            # yield new_request
            index += 1
    #
    # @staticmethod
    # def parse_page_detail(response):
    #     title = response.meta['title']
    #     user = response.meta['user']
    #     link = response.meta['link']
    #     ids = response.meta['ids']
    #     date = response.meta['date']
    #     resp = response.meta['resp']
    #     view = response.meta['view']
    #     lists = response.css("ol#posts li.postbitim")
    #     index = 0
    #     content = ''
    #     for lis in lists:
    #         if index == 0:
    #             li = lis.css("blockquote.postcontent").extract()
    #             for l in li:
    #                 l = l.replace('<blockquote class="postcontent restore ">', "")
    #                 l = l.replace('<blockquote class="postcontent lastedited">', "")
    #                 l = l.replace('<span class="time">', "")
    #                 l = l.replace('</span>', "")
    #                 l = l.replace('</blockquote>', "")
    #                 l = l.replace('<br>', "")
    #                 l = l.replace('<b>', "")
    #                 l = l.replace('</b>', "")
    #                 l = ' '.join(l.split())
    #                 content += l+'\n'
    #         index += 1
    #
    #     filename = "whtvps"
    #     index = response.meta['index']
    #     f = open('/home/user1/server/' + filename + str(index), 'w')
    #     title = title.encode('utf-8')
    #     user = user.encode('utf-8')
    #     link = link.encode('utf-8')
    #     ids = ids.encode('utf-8')
    #     date = date.encode('utf-8')
    #     resp = resp.encode('utf-8')
    #     view = view.encode('utf-8')
    #     content = content.encode('utf-8')
    #     f.write(title + "\n")
    #     f.write(user + "\n")
    #     f.write(link + "\n")
    #     f.write(ids + "\n")
    #     f.write(date + "\n")
    #     f.write(resp + "\n")
    #     f.write(view + "\n")
    #     f.write(content + "\n")
    #     f.write('/=|' + "\n")
    #     f.close()
