# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.utils.translation import ugettext_lazy as _

from django.db import models


class OperatorInformation(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    phone = models.CharField(max_length=1000, blank=True, null=True)
    ids = models.CharField(max_length=1000, blank=True, null=True)
    code = models.CharField(max_length=1000, blank=True, null=True)
    verified = models.BooleanField(default=False)

    class Meta:
        ordering = ('-created',)
        verbose_name = _("Operator Information")
        verbose_name_plural = _("Operator Informations")


class UserStates(models.Model):
    user_id = models.CharField(max_length=1000, blank=True, null=True)
    step = models.CharField(max_length=1000, blank=True, null=True)
    phone = models.CharField(max_length=1000, blank=True, null=True)
    melli = models.CharField(max_length=1000, blank=True, null=True)
    code = models.CharField(max_length=1000, blank=True, null=True)

    @classmethod
    def get_user_state_with_id(cls, user_id):
        return cls.objects.get(user_id=user_id)

    class Meta:
        verbose_name = _("User State")
        verbose_name_plural = _("User States")
