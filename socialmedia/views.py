#!/usr/bin/env python
# -*- coding: utf-8 -*-
import csv
import json
import random
from datetime import datetime, timedelta

import requests
import telegram
from django.core.exceptions import ObjectDoesNotExist
from django.http import HttpResponse
from django.utils import timezone
from rest_framework import status
from rest_framework.views import APIView

from gooshichand.models import MobileItem, MobileSpecs, MobileNews
from gooshichand.settings import TELEGRAM_TOKEN
from socialmedia.bots.TelegramBot import TelegramRegisterPeople
from socialmedia.models import OperatorInformation, UserStates
from . import jalali

NOW = datetime.today()
START_OF_TODAY = NOW - timedelta(days=0, hours=NOW.hour - 1)


class InstaNewsBot(APIView):
    @staticmethod
    def get(request):
        items = MobileNews.get_mobile_news_after_date(START_OF_TODAY)
        items = list(items.values('title', 'tags', 'img', 'ids'))
        result = json.dumps(items, sort_keys=True, default=str)
        return HttpResponse(result, content_type='application/json')


class TGDailyNewsBot(APIView):
    @staticmethod
    def get(request):
        bot = telegram.Bot(token=TELEGRAM_TOKEN)
        items = MobileNews.get_mobile_news_after_date(START_OF_TODAY)
        for item in items:
            content = item.title + '\n'
            link = item.link_of_news_to_share + '/\n'
            content += 'متن کامل خبر '.decode('utf-8') + '\n' + link
            channel = '\n\n' + 'ارزانترین فروشنده های گوشی'.decode('utf-8') + '\n'
            channel += '@daily_mobile' + '\n'
            content += channel
            bot.send_photo(chat_id='@daily_mobile', photo=item.img, caption=content, disable_web_page_preview=True,
                           disable_notification=True)

        return HttpResponse("Done", status.HTTP_200_OK)


class InstaNewestBot(APIView):
    @staticmethod
    def get(request):
        items = MobileSpecs.get_newest_mobile_spec_from_date(START_OF_TODAY)
        items = list(items.values('title', 'release_date', 'inch', 'camera_back', 'img'))
        result = json.dumps(items, sort_keys=True, default=str)
        return HttpResponse(result, content_type='application/json')


class TGNewestBot(APIView):
    @staticmethod
    def get(request):
        bot = telegram.Bot(token=TELEGRAM_TOKEN)
        items = MobileSpecs.get_newest_mobile_spec_from_date(START_OF_TODAY)
        for item in items:
            content = 'نام: ' + item.title.encode('utf-8') + '\n'
            content += 'انتشار: ' + item.release_date.strftime('%B %Y') + '\n'
            content += 'صفحه: ' + str(item.inch) + ' اینچ' + '\n'
            content += 'دوربین: ' + str(item.camera_back) + ' مگاپیکسل' + '\n'
            link = item.link_of_page_to_share + '/\n'
            content += 'مشخصات ' + '\n' + link
            channel = '\n\n' + 'ارزانترین فروشنده های گوشی' + '\n'
            channel += '@daily_mobile' + '\n'
            content += channel
            bot.send_photo(chat_id='@daily_mobile', photo=item.img, caption=content, disable_web_page_preview=True,
                           disable_notification=True)

        return HttpResponse("Done", status.HTTP_200_OK)


class LastTenBrandModel(APIView):
    @staticmethod
    def get(request):
        bot = telegram.Bot(token=TELEGRAM_TOKEN)
        start_day = datetime.today() - timedelta(days=180, hours=NOW.hour, minutes=NOW.minute)
        specs = MobileSpecs.get_latest_mobile_spec_from_date(start_day)
        content = ''
        index = 0
        today = str(datetime.today())
        today = today.split(' ')[0]
        today = jalali.Gregorian(today).persian_string()
        content += 'امروز' + ' ' + str(today) + '\n'
        for item in specs:
            title = item.title.replace(item.path, '')
            title = ' '.join(title.split())
            mi = MobileItem.objects.filter(model__iexact=title, cats=item.path).order_by('price')
            if len(mi) > 0 and index < 25:
                cat = item.path
                content += '#' + cat.encode('utf-8').replace(' ', "_") + ' #' +\
                           item.title.encode('utf-8').replace(' ', "_") + '\n'
                title = mi[0].title
                content += title.encode('utf-8') + '\n'
                price = mi[0].price
                content += 'از: ' + "{:,}".format(price) + ' ' + 'ریال' + '\n'
                link = item.link_of_page + '/\n'
                content += 'مشخصات و فروشنده ها ' + '\n' + link
                content += '-----\n'
                index += 1

        channel = '\n' + 'ارزانترین فروشنده های گوشی' + '\n'
        channel += '@daily_mobile' + '\n'
        content += channel
        bot.send_message(chat_id='@daily_mobile', text=content, disable_web_page_preview=True,
                         disable_notification=True)

        return HttpResponse("Done", status.HTTP_200_OK)


class OperatorID(APIView):
    @staticmethod
    def post(request):
        phone = request.POST.get('phone', '0')
        melli = request.POST.get('melli', '0')
        if len(OperatorInformation.objects.filter(phone=phone, ids=melli)) > 0:
            return HttpResponse("شما قبلا یکبار ثبت نام کرده بودید", status=status.HTTP_200_OK)
        else:
            oi = OperatorInformation(created=timezone.now(), phone=phone, ids=melli, code='', verified=True)
            oi.save()
            return HttpResponse("success", status=status.HTTP_200_OK)


class SaveStates(APIView):
    @staticmethod
    def post(request):
        user_id = request.POST.get('user_id', '')
        step = request.POST.get('step', '')
        phone = request.POST.get('phone', '')
        melli = request.POST.get('melli', '')
        code = request.POST.get('code', '')
        try:
            us = UserStates.get_user_state_with_id(user_id)
            us.phone = phone
            us.step = step
            us.melli = melli
            us.code = code
            us.save()
            return HttpResponse("اطلاعات ذخیره شد", status=status.HTTP_200_OK)
        except ObjectDoesNotExist:
            return HttpResponse("یوزر پیدا نشد", status=status.HTTP_200_OK)


class GetStates(APIView):
    @staticmethod
    def post(request):
        user_id = request.POST.get('user_id', '0')
        try:
            us = UserStates.get_user_state_with_id(user_id)
            result = {'user_id': us.user_id, 'step': us.step, 'phone': us.phone, 'melli': us.melli, 'code': us.code}
            result = json.dumps(result, sort_keys=True, default=str)
            return HttpResponse(result, content_type='application/json')
        except ObjectDoesNotExist:
            UserStates(user_id=user_id, step=0, phone='', melli='', code='').save()
            result = {'user_id': user_id, 'step': 0, 'phone': '', 'melli': '', 'code': ''}
            result = json.dumps(result, sort_keys=True, default=str)
            return HttpResponse(result, status=status.HTTP_200_OK)


class OperatorCSV(APIView):
    @staticmethod
    def get(request):
        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename="result.csv"'
        ois = OperatorInformation.objects.all()
        writer = csv.writer(response)
        writer.writerow(['created', 'phone', 'ids', 'code', 'verified'])
        for oi in ois:
            writer.writerow([oi.created, oi.phone, oi.ids, oi.code, oi.verified])
        return response


def send_sms(phone, rand, template):
    api_token = "59334A2F647668614B50366A6E57487A3438634C6E627566456A5A7961724563"
    requests.post("https://api.kavenegar.com/v1/" + api_token + "/verify/lookup.json",
                  data={'receptor': phone, 'token': str(rand), 'template': template})


def process_message_for_operator_register(input_text, step, phone, melli, code):
    result = ""
    if step == 0 and input_text != "/start":
        result = "برای شروع " \
                 "/start " \
                 "را وارد نمایید"
    elif step == 0 and input_text == "/start":
        step = 1
        result = "شماره تماس 11 رقمی خود را وارد نمایید"
    elif step == 1:
        if len(input_text) != 11:
            step = 1
            result = "شماره تماس شما باید 11 رقم باشد"
        elif input_text[0] != '0':
            step = 1
            result = "شماره تماس شما باید با صفر شروع شود"
        else:
            rand = random.randint(1000, 9999)
            code = rand
            send_sms(input_text, rand, 'operatorverification')
            phone = input_text
            result = "کد تاییدی که برای شماره تماس شما ارسال شده است را وارد نمایید"
            step = 2
    elif step == 2:
        if input_text == "ارسال مجدد".decode('utf-8'):
            rand = random.randint(1000, 9999)
            code = rand
            send_sms(phone, rand, 'operatorverification')
            result = "کد تایید، مجدد به شماره شما ارسال شد، آن را وارد نمایید"
        elif input_text == str(code):
            step = 3
            code = input_text
            result = "کد ملی خود را وارد نمایید"
        elif len(input_text) != 4:
            step = 2
            result = "کد تایید 4 رقم می باشد" \
                     "\n" \
                     "اگر کد را دریافت ننموده اید " \
                     "برای ارسال مجدد کد تایید، کلمه ارسال مجدد را وارد نمایید"
        else:
            step = 2
            result = "کد تایید صحیح نمی باشد" \
                     "\n" \
                     "اگر کد را دریافت ننموده اید " \
                     "برای ارسال مجدد کد تایید، کلمه ارسال مجدد را وارد نمایید"
    elif step == 3:
        if len(input_text) != 10:
            step = 3
            result = "کد ملی باید 10 رقم باشد"
        else:
            step = 0
            melli = input_text
            result = "ثبت نام شما تکمیل شد " \
                     "شما می توانید با در دست داشتن شناسنامه خود به آدرس زیر برای تایید ثبت نام مراجعه نمایید" \
                     "\n" \
                     "باتشکر از ثبت نام شما" \
                     "\n" \
                     "برای شروع مجدد " \
                     "/start" \
                     " را وارد نمایید"
            url = "https://gooshichand.com/socialmedia/saveoperatorid/"
            data = {'phone': phone, 'melli': melli}
            requests.post(url=url, data=data)
    return result, step, phone, melli, code


class OperatorRegisterWebHook(APIView):

    @staticmethod
    def post(request):
        bot = TelegramRegisterPeople("662410533:AAEvdOOISPLfRvgb6JgHZcqN0EkNMx8pKJc")
        item = request.data

        try:
            message = item["message"]["text"]
        except KeyError:
            message = None
        from_ = item["message"]["from"]["id"]

        url = "https://gooshichand.com/socialmedia/getoperatorstates/"
        data = {'user_id': from_}
        result = requests.post(url=url, data=data)
        result = json.loads(result.content)
        step, phone, melli, code = result['step'], result['phone'], result['melli'], result['code']

        reply, step, phone, melli, code = process_message_for_operator_register(message, int(step), phone, melli, code)

        url = "https://gooshichand.com/socialmedia/saveoperatorstates/"
        data = {'user_id': from_, 'step': step, 'phone': phone, 'melli': melli, 'code': code}
        requests.post(url=url, data=data)
        bot.send_message(reply, from_)

        return HttpResponse(message, status=status.HTTP_200_OK)
