# -*- coding: utf-8 -*-
# Generated by Django 1.11.20 on 2019-04-10 16:04
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('socialmedia', '0003_operatorinformation_verified'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='operatorinformation',
            options={'ordering': ('-created',), 'verbose_name': 'Operator Information', 'verbose_name_plural': 'Operator Informations'},
        ),
    ]
