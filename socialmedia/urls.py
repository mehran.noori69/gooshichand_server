from django.conf.urls import url

from socialmedia import views
from socialmedia.views import *

urlpatterns = [
    url(r'^instanewsbot/$', views.InstaNewsBot.as_view(), name='instanewsbot'),
    url(r'^instanewestbot/$', InstaNewestBot.as_view(), name='instanewestbot'),
    url(r'^tgnewestbot/$', TGNewestBot.as_view(), name='tgnewestbot'),
    url(r'^tgdailynewsbot/$', TGDailyNewsBot.as_view(), name='tgdailynewsbot'),
    url(r'^botlastten/$', LastTenBrandModel.as_view(), name='botlastten'),

    url(r'^saveoperatorid/$', OperatorID.as_view(), name='saveoperatorid'),
    url(r'^saveoperatorstates/$', SaveStates.as_view(), name='saveoperatorstates'),
    url(r'^getoperatorstates/$', GetStates.as_view(), name='getoperatorstates'),
    url(r'^csvoperatorinfo/$', OperatorCSV.as_view(), name='csvoperatorinfo'),
    url(r'^operatorregwebhook/$', views.OperatorRegisterWebHook.as_view(), name='operator_save_webhook')
]
