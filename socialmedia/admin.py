# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

from socialmedia.models import OperatorInformation, UserStates


class OperatorInformationAdmin(admin.ModelAdmin):
    list_display = ('created', 'phone', 'ids', 'code', 'verified')
    list_filter = ['created', 'phone', 'ids', 'code', 'verified']
    search_fields = ['created', 'phone', 'ids', 'code', 'verified']


class UserStatesAdmin(admin.ModelAdmin):
    list_display = ('user_id', 'step', 'phone', 'melli', 'code')
    list_filter = ['user_id', 'step', 'phone', 'melli', 'code']
    search_fields = ['user_id', 'step', 'phone', 'melli', 'code']


admin.site.register(OperatorInformation, OperatorInformationAdmin)
admin.site.register(UserStates, UserStatesAdmin)
