import requests
import json


class TelegramRegisterPeople:

    def __init__(self, token):
        self.token = token
        self.base = "https://api.telegram.org/bot{}/".format(token)

    def get_updates(self, offset=None):
        url = self.base + "getUpdates?timeout=10"
        if offset:
            url = url + "&offset={}".format(offset + 1)
        r = requests.get(url)
        return json.loads(r.content)

    def send_message(self, msg, chat_id):
        url = self.base + "sendMessage?chat_id={}&text={}".format(chat_id, msg)
        if msg is not None:
            requests.get(url)

