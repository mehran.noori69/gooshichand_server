# coding=utf-8
import json
from random import randint

import requests
import telegram

TOKEN = "595255997:AAHcV-leW1X0vksOv-l2lu0kQQC9H6oee8w"
bot = telegram.Bot(token=TOKEN)

countries = ['ایران', 'ارمنستان', 'برزیل', 'ایتالیا', 'کانادا', 'ترکیه', 'سریلانکا', 'مالزی', 'آذربایجان', 'سنگاپور',
             'امارات', 'چین', 'هند', 'مکزیک', 'اسپانیا', 'سوئیس', 'آلمان', 'تایلند', 'گرجستان', 'اندونزی', 'فرانسه',
             'هلند', 'مالدیو', 'قبرس', 'آفریقا', 'موریس', 'سیشل', 'روسیه', 'مراکش']

tours = []
content = ''
country = ''
while len(tours) < 1:
    random_index = randint(0, 28)
    country = countries[random_index]
    r = requests.post("https://gooshichand.com/tour/getbycountry/", data={'country': country})
    tours = json.loads(r.text)


content += '#' + country + '\n\n'

for tour in tours:
    hotel_names = tour['contents'].split("&")[1]
    hotel_names = hotel_names.split("=")[1]
    hotel_names = hotel_names.split("##")[0:-1]
    length = len(hotel_names)
    hotel_services = tour['contents'].split("&")[2]
    hotel_services = hotel_services.split("=")[1]
    hotel_services = hotel_services.split("##")[0:-1]
    hotel_stars = tour['contents'].split("&")[3]
    hotel_stars = hotel_stars.split("=")[1]
    hotel_stars = hotel_stars.split("##")[0:-1]
    hotel_price = tour['contents'].split("&")[4]
    hotel_price = hotel_price.split("=")[1]
    index = 0
    content += '💎 ' + tour['title'].encode('utf-8') + '\n'
    content += '✅ ' + tour['nights'].encode('utf-8') + '\n'
    content += '📆 ' + tour['start'].encode('utf-8') + '\n'
    content += 'کمترین قیمت از: ' + '\n'
    content += '💵 ' + hotel_price.encode('utf-8') + '\n'
    while index < length:
        hotel = '🏨 ' + hotel_names[index].encode('utf-8') + '  🍽  ' + hotel_services[index].encode('utf-8') + '  ⭐  ' \
                + hotel_stars[index].encode('utf-8')
        content += hotel + '\n'
        index += 1
    content += '✈ ' + tour['airline'].encode('utf-8') + '\n'
    content += '💎 ' + tour['agancy_name'].encode('utf-8') + '\n'
    content += '📞 ' + tour['agancy_phone'].encode('utf-8') + '\n'
    content += '-----\n'

content += 'مشاهده سایر تورهای ' + country + '\n'
content += 'http://tourbeen.ir/tours/?country=' + country
channel = 'اطلاع رسانی تورهای روزانه' + '\n'
channel += '@daily_tour' + '\n'
content += '\n\n' + channel
bot.sendMessage(chat_id='@daily_tour', text=content, disable_notification=True)
