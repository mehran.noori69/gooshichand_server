#!/usr/bin/env python
# -*- coding: utf-8 -*-
import json

from django.core.cache import cache
from rest_framework.views import APIView

from android.models import AppConfig
from android.serializer import AppConfigSerializer, UserCommentSerializer
from android.spellcorrection import SpellCorrector
from android.utilities import *
from gooshichand.models import UserComment


class HomePageView(APIView):
    @staticmethod
    def get(request):
        res = dict()
        mobile_brands = get_mobile_brand_function()
        res['mobile_brands'] = mobile_brands
        results = slide_images_function()
        res['home_slide_images'] = results
        mobile_specs = recom_mobile_item_by_brand_function("all")
        res['mobile_recommanded'] = mobile_specs
        result = json.dumps(res, sort_keys=True, default=str)
        return HttpResponse(result, content_type='application/json')


class BrandPageView(APIView):
    """
        In mobile application there is a page for every brand which will show us first 20th mobile items
        for every categories including cats, newest, best cpu, best camera, etc
    """

    @staticmethod
    def post(request):
        brand = request.POST.get('brand', '')
        res = dict()
        pagesize = 10
        mobile_specs = get_mobile_item_by_brand_function(brand, 0)
        res['cats'] = mobile_specs
        mobile_specs = est_mobile_item_by_brand_function(brand, 'newest', 0, pagesize)
        res['newest'] = mobile_specs
        mobile_specs = est_mobile_item_by_brand_function(brand, 'cpu', 0, pagesize)
        res['cpu'] = mobile_specs
        mobile_specs = est_mobile_item_by_brand_function(brand, 'back_camera', 0, pagesize)
        res['back_camera'] = mobile_specs
        mobile_specs = est_mobile_item_by_brand_function(brand, 'front_camera', 0, pagesize)
        res['front_camera'] = mobile_specs
        mobile_specs = est_mobile_item_by_brand_function(brand, 'ram', 0, pagesize)
        res['ram'] = mobile_specs
        mobile_specs = est_mobile_item_by_brand_function(brand, 'inch', 0, pagesize)
        res['inch'] = mobile_specs
        mobile_specs = est_mobile_item_by_brand_function(brand, 'weight', 0, pagesize)
        res['weight'] = mobile_specs
        result = json.dumps(res, sort_keys=True, default=str)
        return HttpResponse(result, content_type='application/json')


class MobileByBrandView(APIView):
    @staticmethod
    def get(request):
        if 'get_mobile_brand' in cache:
            result = cache.get('get_mobile_brand')
        else:
            res = dict()
            mobile_brands = get_mobile_brand_function()
            res['mobile_brands'] = mobile_brands
            result = json.dumps(res, sort_keys=True, default=str)
            cache.set('get_mobile_brand', result, timeout=CACHE_TTL)
        return HttpResponse(result, content_type='application/json')


class SearchMobileItemView(APIView):
    @staticmethod
    def post(request):
        start = request.POST.get('start', '')
        query = request.POST.get('search', '')
        spell_corrector = SpellCorrector(query)
        spell_corrector.fatoen()
        spell_corrector.spellcorrect()
        query = spell_corrector.inputs
        start = int(start)
        mobile_specs = MobileSpecs.get_mobile_spec_items_by_title(query, '-title', True, start, start+paginationsize)
        serializer = MobileSpecsSerializer(mobile_specs, many=True)
        mobile_specs = serializer.data
        mobile_specs = set_hasprice_minprice_stores_in_mobile_specs(mobile_specs)
        result = json.dumps(mobile_specs, sort_keys=True, default=str)
        return HttpResponse(result, content_type='application/json')


class EstMobileItemByBrandView(APIView):
    @staticmethod
    def post(request):
        start = request.POST.get('start', '')
        which = request.POST.get('which', '')
        brand = request.POST.get('brand', '')
        pagesize = 20
        start = int(start)
        mobile_specs = est_mobile_item_by_brand_function(brand, which, start, pagesize)
        result = json.dumps(mobile_specs, sort_keys=True, default=str)
        return HttpResponse(result, content_type='application/json')


class ContactUsView(APIView):
    @staticmethod
    def get(request):
        ucs = UserComment.objects.all()
        serializer = UserCommentSerializer(ucs)
        return HttpResponse(serializer.data)

    @staticmethod
    def post(request):
        serializer = UserCommentSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save(response='', deviceid=request.data['device_id'])
            response = {'result': "از تماس شما متشکریم"}
            response.update(serializer.data)
            result = json.dumps(response, sort_keys=True, default=str)
        else:
            response = {'result': "پیام شما ذخیره نشد"}
            response.update(serializer.data)
            result = json.dumps(response, sort_keys=True, default=str)
        return HttpResponse(result)


class AppConfigView(APIView):
    @staticmethod
    def get(request):
        configs = AppConfig.objects.all()
        serializer = AppConfigSerializer(configs, many=True)
        return HttpResponse(json.dumps(serializer.data))


class GetCity(APIView):
    @staticmethod
    def get(request):
        string = [{"name": "Abbotsford", "state": "BC"}, {"name": "Agassiz", "state": "BC"},
                  {"name": "Anmore", "state": "BC"}, {"name": "Belcarra", "state": "BC"},
                  {"name": "Birken", "state": "BC"}, {"name": "Boston Bar", "state": "BC"},
                  {"name": "Boston Bar / Lytton", "state": "BC"}, {"name": "Bowen Island", "state": "BC"},
                  {"name": "Brackendale", "state": "BC"}, {"name": "Britannia Beach", "state": "BC"},
                  {"name": "Burnaby", "state": "BC"}, {"name": "Burns Lake", "state": "BC"},
                  {"name": "Chilliwack", "state": "BC"}, {"name": "Columbia Valley", "state": "BC"},
                  {"name": "Coquitlam", "state": "BC"}, {"name": "Cultus Lake", "state": "BC"},
                  {"name": "D'Arcy", "state": "BC"}, {"name": "Delta", "state": "BC"},
                  {"name": "Downtown", "state": "BC"}, {"name": "Egmont", "state": "BC"},
                  {"name": "Furry Creek", "state": "BC"}, {"name": "Gabriola Island", "state": "BC"},
                  {"name": "Galiano Island", "state": "BC"}, {"name": "Gambier Island", "state": "BC"},
                  {"name": "Garden Bay", "state": "BC"}, {"name": "Garibaldi Highlands", "state": "BC"},
                  {"name": "Gibsons", "state": "BC"}, {"name": "Granthams Landing", "state": "BC"},
                  {"name": "Halfmoon Bay", "state": "BC"}, {"name": "Harrison Hot Springs", "state": "BC"},
                  {"name": "Harrison Mills", "state": "BC"}, {"name": "Hope", "state": "BC"},
                  {"name": "Keats Island", "state": "BC"}, {"name": "Ladner", "state": "BC"},
                  {"name": "Laidlaw", "state": "BC"}, {"name": "Langdale", "state": "BC"},
                  {"name": "Langley", "state": "BC"}, {"name": "Lindell Beach", "state": "BC"},
                  {"name": "Lions Bay", "state": "BC"}, {"name": "Mackenzie - Rural", "state": "BC"},
                  {"name": "Madeira Park", "state": "BC"}, {"name": "Maple Ridge", "state": "BC"},
                  {"name": "Mayne Island", "state": "BC"}, {"name": "Mission", "state": "BC"},
                  {"name": "Mount Currie", "state": "BC"}, {"name": "Nelson Island", "state": "BC"},
                  {"name": "New Westminster", "state": "BC"}, {"name": "No City Value", "state": "BC"},
                  {"name": "North Vancouver", "state": "BC"}, {"name": "Pemberton", "state": "BC"},
                  {"name": "Pender Harbour", "state": "BC"}, {"name": "Pender Island", "state": "BC"},
                  {"name": "Pitt Meadows", "state": "BC"}, {"name": "Port Coquitlam", "state": "BC"},
                  {"name": "Port Moody", "state": "BC"}, {"name": "Richmond", "state": "BC"},
                  {"name": "Roberts Creek", "state": "BC"}, {"name": "Rosedale", "state": "BC"},
                  {"name": "Ryder Lake", "state": "BC"}, {"name": "Salt Spring Island", "state": "BC"},
                  {"name": "Sardis", "state": "BC"}, {"name": "Sardis - Chwk River Valley", "state": "BC"},
                  {"name": "Sardis - Greendale", "state": "BC"}, {"name": "Sechelt", "state": "BC"},
                  {"name": "Squamish", "state": "BC"}, {"name": "Sunshine Valley", "state": "BC"},
                  {"name": "Surrey", "state": "BC"}, {"name": "Tsawwassen", "state": "BC"},
                  {"name": "Vancouver", "state": "BC"}, {"name": "West Vancouver", "state": "BC"},
                  {"name": "Whistler", "state": "BC"}, {"name": "White Rock", "state": "BC"},
                  {"name": "Yale", "state": "BC"}, {"name": "Yarrow", "state": "BC"}]
        response = HttpResponse(json.dumps(string))
        response["Access-Control-Allow-Origin"] = "*"
        return response


class GetNeighbor(APIView):
    @staticmethod
    def get(request):
        string = [{"id": 886, "name": "Out of Town", "city": "No City Value", "state": "BC"},
                  {"id": 887, "name": "Pender Harbour Egmont", "city": "Pender Harbour", "state": "BC"},
                  {"id": 888, "name": "Gambier Island", "city": "Gambier Island", "state": "BC"},
                  {"id": 889, "name": "Pacific Douglas", "city": "Surrey", "state": "BC"},
                  {"id": 890, "name": "Gibsons & Area", "city": "Gibsons", "state": "BC"},
                  {"id": 891, "name": "Galiano Island", "city": "Galiano Island", "state": "BC"},
                  {"id": 892, "name": "Pender Harbour Egmont", "city": "Madeira Park", "state": "BC"},
                  {"id": 893, "name": "Whalley", "city": "Surrey", "state": "BC"},
                  {"id": 894, "name": "Main", "city": "Vancouver", "state": "BC"},
                  {"id": 895, "name": "Annieville", "city": "Delta", "state": "BC"},
                  {"id": 896, "name": "Pender Harbour Egmont", "city": "Garden Bay", "state": "BC"},
                  {"id": 897, "name": "Yaletown", "city": "Vancouver", "state": "BC"},
                  {"id": 898, "name": "Sechelt District", "city": "Sechelt", "state": "BC"},
                  {"id": 899, "name": "Renfrew VE", "city": "Vancouver", "state": "BC"},
                  {"id": 900, "name": "Cambie", "city": "Vancouver", "state": "BC"},
                  {"id": 901, "name": "Grandview VE", "city": "Vancouver", "state": "BC"},
                  {"id": 902, "name": "Websters Corners", "city": "Maple Ridge", "state": "BC"},
                  {"id": 903, "name": "South Granville", "city": "Vancouver", "state": "BC"},
                  {"id": 904, "name": "Oakridge VW", "city": "Vancouver", "state": "BC"},
                  {"id": 905, "name": "WedgeWoods", "city": "Whistler", "state": "BC"},
                  {"id": 906, "name": "Woodland Acres PQ", "city": "Port Coquitlam", "state": "BC"},
                  {"id": 907, "name": "Coquitlam West", "city": "Coquitlam", "state": "BC"},
                  {"id": 908, "name": "Anmore", "city": "Anmore", "state": "BC"},
                  {"id": 909, "name": "Pender Island", "city": "Pender Island", "state": "BC"},
                  {"id": 910, "name": "Mission BC", "city": "Mission", "state": "BC"},
                  {"id": 911, "name": "Shaughnessy", "city": "Vancouver", "state": "BC"},
                  {"id": 912, "name": "Thornhill MR", "city": "Maple Ridge", "state": "BC"},
                  {"id": 913, "name": "Thornhill MR", "city": "Mackenzie - Rural", "state": "BC"},
                  {"id": 914, "name": "University VW", "city": "Vancouver", "state": "BC"},
                  {"id": 915, "name": "Ladner Rural", "city": "Delta", "state": "BC"},
                  {"id": 916, "name": "FVREB Out of Town", "city": "No City Value", "state": "BC"},
                  {"id": 917, "name": "Abbotsford East", "city": "Abbotsford", "state": "BC"},
                  {"id": 918, "name": "White Rock", "city": "White Rock", "state": "BC"},
                  {"id": 919, "name": "Olde Caulfeild", "city": "West Vancouver", "state": "BC"},
                  {"id": 920, "name": "Upper Squamish", "city": "Squamish", "state": "BC"},
                  {"id": 921, "name": "Central Pt Coquitlam", "city": "Port Coquitlam", "state": "BC"},
                  {"id": 922, "name": "Garibaldi Highlands", "city": "Squamish", "state": "BC"},
                  {"id": 923, "name": "South Cambie", "city": "Vancouver", "state": "BC"},
                  {"id": 924, "name": "South Slope", "city": "Burnaby", "state": "BC"},
                  {"id": 925, "name": "Collingwood VE", "city": "Vancouver", "state": "BC"},
                  {"id": 926, "name": "Halfmn Bay Secret Cv Redroofs", "city": "Halfmoon Bay", "state": "BC"},
                  {"id": 927, "name": "Grandview Surrey", "city": "Surrey", "state": "BC"},
                  {"id": 928, "name": "Salt Spring Island", "city": "Salt Spring Island", "state": "BC"},
                  {"id": 929, "name": "Tsawwassen Central", "city": "Delta", "state": "BC"},
                  {"id": 930, "name": "Bowen Island", "city": "Bowen Island", "state": "BC"},
                  {"id": 931, "name": "Murrayville", "city": "Langley", "state": "BC"},
                  {"id": 932, "name": "Fraser VE", "city": "Vancouver", "state": "BC"},
                  {"id": 933, "name": "North Coquitlam", "city": "Coquitlam", "state": "BC"},
                  {"id": 934, "name": "Westside", "city": "Whistler", "state": "BC"},
                  {"id": 935, "name": "D'Arcy", "city": "Pemberton", "state": "BC"},
                  {"id": 936, "name": "Central Coquitlam", "city": "Coquitlam", "state": "BC"},
                  {"id": 937, "name": "East Richmond", "city": "Richmond", "state": "BC"},
                  {"id": 938, "name": "Cottonwood MR", "city": "Maple Ridge", "state": "BC"},
                  {"id": 939, "name": "Knight", "city": "Vancouver", "state": "BC"},
                  {"id": 940, "name": "Islands Other", "city": "No City Value", "state": "BC"},
                  {"id": 941, "name": "Lynnmour", "city": "North Vancouver", "state": "BC"},
                  {"id": 942, "name": "Lake Errock", "city": "Mission", "state": "BC"},
                  {"id": 943, "name": "Blueberry Hill", "city": "Whistler", "state": "BC"},
                  {"id": 944, "name": "Cedar Hills", "city": "Surrey", "state": "BC"},
                  {"id": 945, "name": "Eagle Harbour", "city": "West Vancouver", "state": "BC"},
                  {"id": 946, "name": "Clayton", "city": "Surrey", "state": "BC"},
                  {"id": 947, "name": "Connaught Heights", "city": "New Westminster", "state": "BC"},
                  {"id": 948, "name": "South Vancouver", "city": "Vancouver", "state": "BC"},
                  {"id": 949, "name": "Marpole", "city": "Vancouver", "state": "BC"},
                  {"id": 950, "name": "McNair", "city": "Richmond", "state": "BC"},
                  {"id": 951, "name": "Caulfeild", "city": "West Vancouver", "state": "BC"},
                  {"id": 952, "name": "Willoughby Heights", "city": "Langley", "state": "BC"},
                  {"id": 953, "name": "Moody Park", "city": "New Westminster", "state": "BC"},
                  {"id": 954, "name": "Glenwood PQ", "city": "Port Coquitlam", "state": "BC"},
                  {"id": 955, "name": "Kerrisdale", "city": "Vancouver", "state": "BC"},
                  {"id": 956, "name": "Hastings", "city": "Vancouver", "state": "BC"},
                  {"id": 957, "name": "Spring Creek", "city": "Whistler", "state": "BC"},
                  {"id": 958, "name": "Big Bend", "city": "Burnaby", "state": "BC"},
                  {"id": 959, "name": "Roberts Creek", "city": "Roberts Creek", "state": "BC"},
                  {"id": 960, "name": "Renfrew Heights", "city": "Vancouver", "state": "BC"},
                  {"id": 961, "name": "King George Corridor", "city": "Surrey", "state": "BC"},
                  {"id": 962, "name": "Benchlands", "city": "Whistler", "state": "BC"},
                  {"id": 963, "name": "Bradner", "city": "Abbotsford", "state": "BC"},
                  {"id": 964, "name": "Granville", "city": "Richmond", "state": "BC"},
                  {"id": 965, "name": "Alpine Meadows", "city": "Whistler", "state": "BC"},
                  {"id": 966, "name": "Fraser Heights", "city": "Surrey", "state": "BC"},
                  {"id": 967, "name": "Brighouse South", "city": "Richmond", "state": "BC"},
                  {"id": 968, "name": "Rainbow", "city": "Whistler", "state": "BC"},
                  {"id": 969, "name": "Broadmoor", "city": "Richmond", "state": "BC"},
                  {"id": 970, "name": "Hamilton RI", "city": "Richmond", "state": "BC"},
                  {"id": 971, "name": "Steveston North", "city": "Richmond", "state": "BC"},
                  {"id": 972, "name": "Brighouse", "city": "Richmond", "state": "BC"},
                  {"id": 973, "name": "Canterbury WV", "city": "West Vancouver", "state": "BC"},
                  {"id": 974, "name": "Victoria VE", "city": "Vancouver", "state": "BC"},
                  {"id": 975, "name": "Chartwell", "city": "West Vancouver", "state": "BC"},
                  {"id": 976, "name": "Campbell Valley", "city": "Langley", "state": "BC"},
                  {"id": 977, "name": "Whistler Creek", "city": "Whistler", "state": "BC"},
                  {"id": 978, "name": "Albion", "city": "Maple Ridge", "state": "BC"},
                  {"id": 979, "name": "Burnaby Lake", "city": "Burnaby", "state": "BC"},
                  {"id": 980, "name": "Brennan Center", "city": "Squamish", "state": "BC"},
                  {"id": 981, "name": "Point Grey", "city": "Vancouver", "state": "BC"},
                  {"id": 982, "name": "Fairview VW", "city": "Vancouver", "state": "BC"},
                  {"id": 983, "name": "Lincoln Park PQ", "city": "Port Coquitlam", "state": "BC"},
                  {"id": 984, "name": "Maillardville", "city": "Coquitlam", "state": "BC"},
                  {"id": 985, "name": "Boston Bar - Lytton", "city": "Boston Bar / Lytton", "state": "BC"},
                  {"id": 986, "name": "Riverdale RI", "city": "Richmond", "state": "BC"},
                  {"id": 987, "name": "Bridgeview", "city": "Surrey", "state": "BC"},
                  {"id": 988, "name": "Upper Caulfeild", "city": "West Vancouver", "state": "BC"},
                  {"id": 989, "name": "Poplar", "city": "Abbotsford", "state": "BC"},
                  {"id": 990, "name": "Bolivar Heights", "city": "Surrey", "state": "BC"},
                  {"id": 991, "name": "Keats Island", "city": "Keats Island", "state": "BC"},
                  {"id": 992, "name": "Hamilton", "city": "North Vancouver", "state": "BC"},
                  {"id": 993, "name": "Bear Creek Green Timbers", "city": "Surrey", "state": "BC"},
                  {"id": 994, "name": "Hastings East", "city": "Vancouver", "state": "BC"},
                  {"id": 995, "name": "Mount Pleasant VE", "city": "Vancouver", "state": "BC"},
                  {"id": 996, "name": "Whistler Cay Heights", "city": "Whistler", "state": "BC"},
                  {"id": 997, "name": "Quilchena RI", "city": "Richmond", "state": "BC"},
                  {"id": 998, "name": "Pemberton", "city": "Pemberton", "state": "BC"},
                  {"id": 999, "name": "Fraserview VE", "city": "Vancouver", "state": "BC"},
                  {"id": 1000, "name": "The Heights NW", "city": "New Westminster", "state": "BC"},
                  {"id": 1001, "name": "British Properties", "city": "West Vancouver", "state": "BC"},
                  {"id": 1002, "name": "Lower Lonsdale", "city": "North Vancouver", "state": "BC"},
                  {"id": 1003, "name": "Blueridge NV", "city": "North Vancouver", "state": "BC"},
                  {"id": 1004, "name": "Durieu", "city": "Mission", "state": "BC"},
                  {"id": 1005, "name": "Emerald Estates", "city": "Whistler", "state": "BC"},
                  {"id": 1006, "name": "Bridgeport RI", "city": "Richmond", "state": "BC"},
                  {"id": 1007, "name": "Deer Lake", "city": "Burnaby", "state": "BC"},
                  {"id": 1008, "name": "Whitby Estates", "city": "West Vancouver", "state": "BC"},
                  {"id": 1009, "name": "South Meadows", "city": "Pitt Meadows", "state": "BC"},
                  {"id": 1010, "name": "Pebble Hill", "city": "Delta", "state": "BC"},
                  {"id": 1011, "name": "Mission-West", "city": "Mission", "state": "BC"},
                  {"id": 1012, "name": "Eastern Hillsides", "city": "Chilliwack", "state": "BC"},
                  {"id": 1013, "name": "Indian Arm", "city": "North Vancouver", "state": "BC"},
                  {"id": 1014, "name": "Langley City", "city": "Langley", "state": "BC"},
                  {"id": 1015, "name": "Hope Silver Creek", "city": "Hope", "state": "BC"},
                  {"id": 1016, "name": "Columbia Valley", "city": "Columbia Valley", "state": "BC"},
                  {"id": 1017, "name": "Elgin Chantrell", "city": "Surrey", "state": "BC"},
                  {"id": 1018, "name": "Killarney VE", "city": "Vancouver", "state": "BC"},
                  {"id": 1019, "name": "Woodwards", "city": "Richmond", "state": "BC"},
                  {"id": 1020, "name": "Hatzic", "city": "Mission", "state": "BC"},
                  {"id": 1021, "name": "Brookswood Langley", "city": "Langley", "state": "BC"},
                  {"id": 1022, "name": "Ambleside", "city": "West Vancouver", "state": "BC"},
                  {"id": 1023, "name": "West End VW", "city": "Vancouver", "state": "BC"},
                  {"id": 1024, "name": "East Cambie", "city": "Richmond", "state": "BC"},
                  {"id": 1025, "name": "Guildford", "city": "Surrey", "state": "BC"},
                  {"id": 1026, "name": "Serpentine", "city": "Surrey", "state": "BC"},
                  {"id": 1027, "name": "Lindell Beach", "city": "Lindell Beach", "state": "BC"},
                  {"id": 1028, "name": "Pender Harbour Egmont", "city": "Egmont", "state": "BC"},
                  {"id": 1029, "name": "Sullivan Station", "city": "Surrey", "state": "BC"},
                  {"id": 1030, "name": "Hemlock", "city": "Agassiz", "state": "BC"},
                  {"id": 1031, "name": "Quay", "city": "New Westminster", "state": "BC"},
                  {"id": 1032, "name": "Fleetwood Tynehead", "city": "Surrey", "state": "BC"},
                  {"id": 1033, "name": "Kitsilano", "city": "Vancouver", "state": "BC"},
                  {"id": 1034, "name": "Gibsons & Area", "city": "Granthams Landing", "state": "BC"},
                  {"id": 1035, "name": "West Central", "city": "Maple Ridge", "state": "BC"},
                  {"id": 1036, "name": "Bayshores", "city": "Whistler", "state": "BC"},
                  {"id": 1037, "name": "Whistler Village", "city": "Whistler", "state": "BC"},
                  {"id": 1038, "name": "Glenmore", "city": "West Vancouver", "state": "BC"},
                  {"id": 1039, "name": "Cloverdale BC", "city": "Surrey", "state": "BC"},
                  {"id": 1040, "name": "Nordel", "city": "Delta", "state": "BC"},
                  {"id": 1041, "name": "Queen Mary Park Surrey", "city": "Surrey", "state": "BC"},
                  {"id": 1042, "name": "Bayridge", "city": "West Vancouver", "state": "BC"},
                  {"id": 1043, "name": "Silver Valley", "city": "Maple Ridge", "state": "BC"},
                  {"id": 1044, "name": "Rosedale Popkum", "city": "Rosedale", "state": "BC"},
                  {"id": 1045, "name": "Nordic", "city": "Whistler", "state": "BC"},
                  {"id": 1046, "name": "Dundarave", "city": "West Vancouver", "state": "BC"},
                  {"id": 1047, "name": "Coal Harbour", "city": "Vancouver", "state": "BC"},
                  {"id": 1048, "name": "Stave Falls", "city": "Mission", "state": "BC"},
                  {"id": 1049, "name": "Abbotsford West", "city": "Abbotsford", "state": "BC"},
                  {"id": 1050, "name": "North Meadows PI", "city": "Pitt Meadows", "state": "BC"},
                  {"id": 1051, "name": "Burke Mountain", "city": "Coquitlam", "state": "BC"},
                  {"id": 1052, "name": "Westwood Plateau", "city": "Coquitlam", "state": "BC"},
                  {"id": 1053, "name": "East Burnaby", "city": "Burnaby", "state": "BC"},
                  {"id": 1054, "name": "Central BN", "city": "Burnaby", "state": "BC"},
                  {"id": 1055, "name": "Sardis West Vedder Rd", "city": "Sardis", "state": "BC"},
                  {"id": 1056, "name": "Panorama Ridge", "city": "Surrey", "state": "BC"},
                  {"id": 1057, "name": "East Central", "city": "Maple Ridge", "state": "BC"},
                  {"id": 1058, "name": "MacKenzie Heights", "city": "Vancouver", "state": "BC"},
                  {"id": 1059, "name": "East Newton", "city": "Surrey", "state": "BC"},
                  {"id": 1060, "name": "Edgemont", "city": "North Vancouver", "state": "BC"},
                  {"id": 1061, "name": "Quilchena", "city": "Vancouver", "state": "BC"},
                  {"id": 1062, "name": "Arbutus", "city": "Vancouver", "state": "BC"},
                  {"id": 1063, "name": "Southlands", "city": "Vancouver", "state": "BC"},
                  {"id": 1064, "name": "Northyards", "city": "Squamish", "state": "BC"},
                  {"id": 1065, "name": "Downtown SQ", "city": "Squamish", "state": "BC"},
                  {"id": 1066, "name": "Chelsea Park", "city": "West Vancouver", "state": "BC"},
                  {"id": 1067, "name": "Lions Bay", "city": "West Vancouver", "state": "BC"},
                  {"id": 1068, "name": "Plateau", "city": "Squamish", "state": "BC"},
                  {"id": 1069, "name": "Harbour Chines", "city": "Coquitlam", "state": "BC"},
                  {"id": 1070, "name": "Lindell Beach", "city": "Cultus Lake", "state": "BC"},
                  {"id": 1071, "name": "Upper Deer Lake", "city": "Burnaby", "state": "BC"},
                  {"id": 1072, "name": "Garibaldi Estates", "city": "Squamish", "state": "BC"},
                  {"id": 1073, "name": "Downtown VW", "city": "Vancouver", "state": "BC"},
                  {"id": 1074, "name": "Seafair", "city": "Richmond", "state": "BC"},
                  {"id": 1075, "name": "Cypress Park Estates", "city": "West Vancouver", "state": "BC"},
                  {"id": 1076, "name": "False Creek", "city": "Vancouver", "state": "BC"},
                  {"id": 1077, "name": "Cultus Lake", "city": "Cultus Lake", "state": "BC"},
                  {"id": 1078, "name": "Northeast", "city": "Maple Ridge", "state": "BC"},
                  {"id": 1079, "name": "Saunders", "city": "Richmond", "state": "BC"},
                  {"id": 1080, "name": "Oaklands", "city": "Burnaby", "state": "BC"},
                  {"id": 1081, "name": "Norgate", "city": "North Vancouver", "state": "BC"},
                  {"id": 1082, "name": "Central Abbotsford", "city": "Abbotsford", "state": "BC"},
                  {"id": 1083, "name": "East Delta", "city": "Delta", "state": "BC"},
                  {"id": 1084, "name": "Metrotown", "city": "Burnaby", "state": "BC"},
                  {"id": 1085, "name": "Garden City", "city": "Richmond", "state": "BC"},
                  {"id": 1086, "name": "Cape Horn", "city": "Coquitlam", "state": "BC"},
                  {"id": 1087, "name": "Crescent Bch Ocean Pk.", "city": "Surrey", "state": "BC"},
                  {"id": 1088, "name": "Aberdeen", "city": "Abbotsford", "state": "BC"},
                  {"id": 1089, "name": "Pemberton Meadows", "city": "Pemberton", "state": "BC"},
                  {"id": 1090, "name": "Otter District", "city": "Langley", "state": "BC"},
                  {"id": 1091, "name": "Hawthorne", "city": "Delta", "state": "BC"},
                  {"id": 1092, "name": "Whonnock", "city": "Maple Ridge", "state": "BC"},
                  {"id": 1093, "name": "Uptown NW", "city": "New Westminster", "state": "BC"},
                  {"id": 1094, "name": "Morgan Creek", "city": "Surrey", "state": "BC"},
                  {"id": 1095, "name": "Dollarton", "city": "North Vancouver", "state": "BC"},
                  {"id": 1096, "name": "Salmon River", "city": "Langley", "state": "BC"},
                  {"id": 1097, "name": "South Arm", "city": "Richmond", "state": "BC"},
                  {"id": 1098, "name": "University Highlands", "city": "Squamish", "state": "BC"},
                  {"id": 1099, "name": "Lions Bay", "city": "Lions Bay", "state": "BC"},
                  {"id": 1100, "name": "Forest Glen BS", "city": "Burnaby", "state": "BC"},
                  {"id": 1101, "name": "Sardis East Vedder Rd", "city": "Sardis", "state": "BC"},
                  {"id": 1102, "name": "Whytecliff", "city": "West Vancouver", "state": "BC"},
                  {"id": 1103, "name": "West Cambie", "city": "Richmond", "state": "BC"},
                  {"id": 1104, "name": "Mid Meadows", "city": "Pitt Meadows", "state": "BC"},
                  {"id": 1105, "name": "Coquitlam East", "city": "Coquitlam", "state": "BC"},
                  {"id": 1106, "name": "Vedder S Watson-Promontory", "city": "Sardis", "state": "BC"},
                  {"id": 1107, "name": "County Line Glen Valley", "city": "Langley", "state": "BC"},
                  {"id": 1108, "name": "Sperling-Duthie", "city": "Burnaby", "state": "BC"},
                  {"id": 1109, "name": "Dunbar", "city": "Vancouver", "state": "BC"},
                  {"id": 1110, "name": "Chilliwack N Yale-Well", "city": "Chilliwack", "state": "BC"},
                  {"id": 1111, "name": "Hope Center", "city": "Hope", "state": "BC"},
                  {"id": 1112, "name": "Riverwood", "city": "Port Coquitlam", "state": "BC"},
                  {"id": 1113, "name": "Lackner", "city": "Richmond", "state": "BC"},
                  {"id": 1114, "name": "Rockridge", "city": "West Vancouver", "state": "BC"},
                  {"id": 1115, "name": "Furry Creek", "city": "Furry Creek", "state": "BC"},
                  {"id": 1116, "name": "Promontory", "city": "Sardis", "state": "BC"},
                  {"id": 1117, "name": "Cliff Drive", "city": "Tsawwassen", "state": "BC"},
                  {"id": 1118, "name": "Howe Sound", "city": "West Vancouver", "state": "BC"},
                  {"id": 1119, "name": "Furry Creek", "city": "West Vancouver", "state": "BC"},
                  {"id": 1120, "name": "Fort Langley", "city": "Langley", "state": "BC"},
                  {"id": 1121, "name": "Chilliwack E Young-Yale", "city": "Chilliwack", "state": "BC"},
                  {"id": 1122, "name": "Hazelmere", "city": "Surrey", "state": "BC"},
                  {"id": 1123, "name": "Edmonds BE", "city": "Burnaby", "state": "BC"},
                  {"id": 1124, "name": "Hope Kawkawa Lake", "city": "Hope", "state": "BC"},
                  {"id": 1125, "name": "Upper Lonsdale", "city": "North Vancouver", "state": "BC"},
                  {"id": 1126, "name": "Vancouver Heights", "city": "Burnaby", "state": "BC"},
                  {"id": 1127, "name": "Ironwood", "city": "Richmond", "state": "BC"},
                  {"id": 1128, "name": "Highgate", "city": "Burnaby", "state": "BC"},
                  {"id": 1129, "name": "Harrison Hot Springs", "city": "Harrison Hot Springs", "state": "BC"},
                  {"id": 1130, "name": "Ladner Elementary", "city": "Delta", "state": "BC"},
                  {"id": 1131, "name": "Matsqui", "city": "Abbotsford", "state": "BC"},
                  {"id": 1132, "name": "Westridge BN", "city": "Burnaby", "state": "BC"},
                  {"id": 1133, "name": "Delta Manor", "city": "Delta", "state": "BC"},
                  {"id": 1134, "name": "Gilmore", "city": "Richmond", "state": "BC"},
                  {"id": 1135, "name": "Aldergrove Langley", "city": "Langley", "state": "BC"},
                  {"id": 1136, "name": "Nelson Island", "city": "Nelson Island", "state": "BC"},
                  {"id": 1137, "name": "Queens", "city": "West Vancouver", "state": "BC"},
                  {"id": 1138, "name": "Terra Nova", "city": "Richmond", "state": "BC"},
                  {"id": 1139, "name": "Simon Fraser Univer.", "city": "Burnaby", "state": "BC"},
                  {"id": 1140, "name": "West Meadows", "city": "Pitt Meadows", "state": "BC"},
                  {"id": 1141, "name": "Eagle Ridge CQ", "city": "Coquitlam", "state": "BC"},
                  {"id": 1142, "name": "Northwest Maple Ridge", "city": "Maple Ridge", "state": "BC"},
                  {"id": 1143, "name": "Capitol Hill BN", "city": "Burnaby", "state": "BC"},
                  {"id": 1144, "name": "Beach Grove", "city": "Delta", "state": "BC"},
                  {"id": 1145, "name": "West Newton", "city": "Surrey", "state": "BC"},
                  {"id": 1146, "name": "Queensborough", "city": "New Westminster", "state": "BC"},
                  {"id": 1147, "name": "Woodlands-Sunshine-Cascade", "city": "North Vancouver", "state": "BC"},
                  {"id": 1148, "name": "Central Meadows", "city": "Pitt Meadows", "state": "BC"},
                  {"id": 1149, "name": "Brackendale", "city": "Squamish", "state": "BC"},
                  {"id": 1150, "name": "Hemlock", "city": "Mission", "state": "BC"},
                  {"id": 1151, "name": "Cedardale", "city": "West Vancouver", "state": "BC"},
                  {"id": 1152, "name": "Deer Ridge WV", "city": "West Vancouver", "state": "BC"},
                  {"id": 1153, "name": "Buckingham Heights", "city": "Burnaby", "state": "BC"},
                  {"id": 1154, "name": "Parkcrest", "city": "Burnaby", "state": "BC"},
                  {"id": 1155, "name": "Ladner Rural", "city": "Ladner", "state": "BC"},
                  {"id": 1156, "name": "S.W. Marine", "city": "Vancouver", "state": "BC"},
                  {"id": 1157, "name": "Majuba Hill", "city": "Yarrow", "state": "BC"},
                  {"id": 1158, "name": "McLennan", "city": "Richmond", "state": "BC"},
                  {"id": 1159, "name": "McLennan North", "city": "Richmond", "state": "BC"},
                  {"id": 1160, "name": "Anmore", "city": "Port Moody", "state": "BC"},
                  {"id": 1161, "name": "Central Lonsdale", "city": "North Vancouver", "state": "BC"},
                  {"id": 1162, "name": "Sentinel Hill", "city": "West Vancouver", "state": "BC"},
                  {"id": 1163, "name": "Fraserview NW", "city": "New Westminster", "state": "BC"},
                  {"id": 1164, "name": "Government Road", "city": "Burnaby", "state": "BC"},
                  {"id": 1165, "name": "Chilliwack Mountain", "city": "Chilliwack", "state": "BC"},
                  {"id": 1166, "name": "White Gold", "city": "Whistler", "state": "BC"},
                  {"id": 1167, "name": "Mount Pleasant VW", "city": "Vancouver", "state": "BC"},
                  {"id": 1168, "name": "Sardis East Vedder Rd", "city": "Chilliwack", "state": "BC"},
                  {"id": 1169, "name": "Park Royal", "city": "West Vancouver", "state": "BC"},
                  {"id": 1170, "name": "Downtown NW", "city": "New Westminster", "state": "BC"},
                  {"id": 1171, "name": "Port Moody Centre", "city": "Port Moody", "state": "BC"},
                  {"id": 1172, "name": "Champlain Heights", "city": "Vancouver", "state": "BC"},
                  {"id": 1173, "name": "Lake Errock", "city": "Harrison Mills", "state": "BC"},
                  {"id": 1174, "name": "Chilliwack River Valley", "city": "Chilliwack", "state": "BC"},
                  {"id": 1175, "name": "Queensbury", "city": "North Vancouver", "state": "BC"},
                  {"id": 1176, "name": "Willingdon Heights", "city": "Burnaby", "state": "BC"},
                  {"id": 1177, "name": "Southwest Maple Ridge", "city": "Maple Ridge", "state": "BC"},
                  {"id": 1178, "name": "Upper Delbrook", "city": "North Vancouver", "state": "BC"},
                  {"id": 1179, "name": "Altamont", "city": "West Vancouver", "state": "BC"},
                  {"id": 1180, "name": "Canyon Heights NV", "city": "North Vancouver", "state": "BC"},
                  {"id": 1181, "name": "Yarrow", "city": "Yarrow", "state": "BC"},
                  {"id": 1182, "name": "Greendale Chilliwack", "city": "Sardis - Greendale", "state": "BC"},
                  {"id": 1183, "name": "Central Park BS", "city": "Burnaby", "state": "BC"},
                  {"id": 1184, "name": "Hope Sunshine Valley", "city": "Sunshine Valley", "state": "BC"},
                  {"id": 1185, "name": "White Rock", "city": "Surrey", "state": "BC"},
                  {"id": 1186, "name": "Brentwood Park", "city": "Burnaby", "state": "BC"},
                  {"id": 1187, "name": "Princess Park", "city": "North Vancouver", "state": "BC"},
                  {"id": 1188, "name": "Sunshine Hills Woods", "city": "Delta", "state": "BC"},
                  {"id": 1189, "name": "Dewdney Deroche", "city": "Mission", "state": "BC"},
                  {"id": 1190, "name": "Chilliwack W Young-Well", "city": "Chilliwack", "state": "BC"},
                  {"id": 1191, "name": "Boyd Park", "city": "Richmond", "state": "BC"},
                  {"id": 1192, "name": "Out of Town", "city": "Vancouver", "state": "BC"},
                  {"id": 1193, "name": "North Maple Ridge", "city": "Maple Ridge", "state": "BC"},
                  {"id": 1194, "name": "Montecito", "city": "Burnaby", "state": "BC"},
                  {"id": 1195, "name": "Cariboo", "city": "Burnaby", "state": "BC"},
                  {"id": 1196, "name": "Sunnyside Park Surrey", "city": "Surrey", "state": "BC"},
                  {"id": 1197, "name": "Valleycliffe", "city": "Squamish", "state": "BC"},
                  {"id": 1198, "name": "Walnut Grove", "city": "Langley", "state": "BC"},
                  {"id": 1199, "name": "Queens Park", "city": "New Westminster", "state": "BC"},
                  {"id": 1200, "name": "Port Kells", "city": "Surrey", "state": "BC"},
                  {"id": 1201, "name": "Neilsen Grove", "city": "Delta", "state": "BC"},
                  {"id": 1202, "name": "Roche Point", "city": "North Vancouver", "state": "BC"},
                  {"id": 1203, "name": "Vedder S Watson-Promontory", "city": "Chilliwack", "state": "BC"},
                  {"id": 1204, "name": "Lynn Valley", "city": "North Vancouver", "state": "BC"},
                  {"id": 1205, "name": "Canyon Springs", "city": "Coquitlam", "state": "BC"},
                  {"id": 1206, "name": "Deer Lake Place", "city": "Burnaby", "state": "BC"},
                  {"id": 1207, "name": "Westwind", "city": "Richmond", "state": "BC"},
                  {"id": 1208, "name": "Meadow Brook", "city": "Coquitlam", "state": "BC"},
                  {"id": 1209, "name": "Forest Hills NV", "city": "North Vancouver", "state": "BC"},
                  {"id": 1210, "name": "Tsawwassen East", "city": "Delta", "state": "BC"},
                  {"id": 1211, "name": "Cheakamus Crossing", "city": "Whistler", "state": "BC"},
                  {"id": 1212, "name": "Sumas Prairie", "city": "Abbotsford", "state": "BC"},
                  {"id": 1213, "name": "Hope Laidlaw", "city": "Hope", "state": "BC"},
                  {"id": 1214, "name": "Burnaby Hospital", "city": "Burnaby", "state": "BC"},
                  {"id": 1215, "name": "Sullivan Heights", "city": "Burnaby", "state": "BC"},
                  {"id": 1216, "name": "Ranch Park", "city": "Coquitlam", "state": "BC"},
                  {"id": 1217, "name": "North Shore Pt Moody", "city": "Port Moody", "state": "BC"},
                  {"id": 1218, "name": "Hope Laidlaw", "city": "Laidlaw", "state": "BC"},
                  {"id": 1219, "name": "Mary Hill", "city": "Port Coquitlam", "state": "BC"},
                  {"id": 1220, "name": "Scottsdale", "city": "Delta", "state": "BC"},
                  {"id": 1221, "name": "Scott Creek", "city": "Coquitlam", "state": "BC"},
                  {"id": 1222, "name": "Green Lake Estates", "city": "Whistler", "state": "BC"},
                  {"id": 1223, "name": "New Horizons", "city": "Coquitlam", "state": "BC"},
                  {"id": 1224, "name": "Harbour Place", "city": "Coquitlam", "state": "BC"},
                  {"id": 1225, "name": "Mount Currie", "city": "Pemberton", "state": "BC"},
                  {"id": 1226, "name": "Hockaday", "city": "Coquitlam", "state": "BC"},
                  {"id": 1227, "name": "Capilano NV", "city": "North Vancouver", "state": "BC"},
                  {"id": 1228, "name": "Hope Sunshine Valley", "city": "Hope", "state": "BC"},
                  {"id": 1229, "name": "Park Royal", "city": "North Vancouver", "state": "BC"},
                  {"id": 1230, "name": "Britannia Beach", "city": "Squamish", "state": "BC"},
                  {"id": 1231, "name": "Agassiz", "city": "Agassiz", "state": "BC"},
                  {"id": 1232, "name": "Cliff Drive", "city": "Delta", "state": "BC"},
                  {"id": 1233, "name": "Pemberton NV", "city": "North Vancouver", "state": "BC"},
                  {"id": 1234, "name": "Little Mountain", "city": "Chilliwack", "state": "BC"},
                  {"id": 1235, "name": "Royal Heights", "city": "Surrey", "state": "BC"},
                  {"id": 1236, "name": "Tantalus", "city": "Squamish", "state": "BC"},
                  {"id": 1237, "name": "Port Guichon", "city": "Delta", "state": "BC"},
                  {"id": 1238, "name": "Citadel PQ", "city": "Port Coquitlam", "state": "BC"},
                  {"id": 1239, "name": "English Bluff", "city": "Tsawwassen", "state": "BC"},
                  {"id": 1240, "name": "Brio", "city": "Whistler", "state": "BC"},
                  {"id": 1241, "name": "GlenBrooke North", "city": "New Westminster", "state": "BC"},
                  {"id": 1242, "name": "Belcarra", "city": "Belcarra", "state": "BC"},
                  {"id": 1243, "name": "English Bluff", "city": "Delta", "state": "BC"},
                  {"id": 1244, "name": "Deep Cove", "city": "North Vancouver", "state": "BC"},
                  {"id": 1245, "name": "Simon Fraser Hills", "city": "Burnaby", "state": "BC"},
                  {"id": 1246, "name": "Downtown VE", "city": "Vancouver", "state": "BC"},
                  {"id": 1247, "name": "Heritage Woods PM", "city": "Port Moody", "state": "BC"},
                  {"id": 1248, "name": "Cloverdale BC", "city": "Langley", "state": "BC"},
                  {"id": 1249, "name": "Gleneagles", "city": "West Vancouver", "state": "BC"},
                  {"id": 1250, "name": "Hawthorne", "city": "Ladner", "state": "BC"},
                  {"id": 1251, "name": "The Crest", "city": "Burnaby", "state": "BC"},
                  {"id": 1252, "name": "Steveston Village", "city": "Richmond", "state": "BC"},
                  {"id": 1253, "name": "Rosedale Popkum", "city": "Chilliwack", "state": "BC"},
                  {"id": 1254, "name": "Lillooet Lake", "city": "Pemberton", "state": "BC"},
                  {"id": 1255, "name": "Oxford Heights", "city": "Port Coquitlam", "state": "BC"},
                  {"id": 1256, "name": "Boundary Beach", "city": "Delta", "state": "BC"},
                  {"id": 1257, "name": "Columbia Valley", "city": "Chilliwack", "state": "BC"},
                  {"id": 1258, "name": "Westhill", "city": "West Vancouver", "state": "BC"},
                  {"id": 1259, "name": "Panorama Village", "city": "West Vancouver", "state": "BC"},
                  {"id": 1260, "name": "Holly", "city": "Delta", "state": "BC"},
                  {"id": 1261, "name": "Steveston South", "city": "Richmond", "state": "BC"},
                  {"id": 1262, "name": "Westmount WV", "city": "West Vancouver", "state": "BC"},
                  {"id": 1263, "name": "Ryder Lake", "city": "Ryder Lake", "state": "BC"},
                  {"id": 1264, "name": "Seymour NV", "city": "North Vancouver", "state": "BC"},
                  {"id": 1265, "name": "Sapperton", "city": "New Westminster", "state": "BC"},
                  {"id": 1266, "name": "Cultus Lake", "city": "Chilliwack", "state": "BC"},
                  {"id": 1267, "name": "Birchland Manor", "city": "Port Coquitlam", "state": "BC"},
                  {"id": 1268, "name": "Suncrest", "city": "Burnaby", "state": "BC"},
                  {"id": 1269, "name": "Greentree Village", "city": "Burnaby", "state": "BC"},
                  {"id": 1270, "name": "Pemberton Heights", "city": "North Vancouver", "state": "BC"},
                  {"id": 1271, "name": "Fairfield Island", "city": "Chilliwack", "state": "BC"},
                  {"id": 1272, "name": "Mountain Meadows", "city": "Port Moody", "state": "BC"},
                  {"id": 1273, "name": "Dentville", "city": "Squamish", "state": "BC"},
                  {"id": 1274, "name": "Westlynn", "city": "North Vancouver", "state": "BC"},
                  {"id": 1275, "name": "Lower Mary Hill", "city": "Port Coquitlam", "state": "BC"},
                  {"id": 1276, "name": "Indian River", "city": "North Vancouver", "state": "BC"},
                  {"id": 1277, "name": "Sumas Mountain", "city": "Abbotsford", "state": "BC"},
                  {"id": 1278, "name": "West End NW", "city": "New Westminster", "state": "BC"},
                  {"id": 1279, "name": "Tempe", "city": "North Vancouver", "state": "BC"},
                  {"id": 1280, "name": "Boulevard", "city": "North Vancouver", "state": "BC"},
                  {"id": 1281, "name": "East Chilliwack", "city": "Chilliwack", "state": "BC"},
                  {"id": 1282, "name": "Chineside", "city": "Coquitlam", "state": "BC"},
                  {"id": 1283, "name": "West Bay", "city": "West Vancouver", "state": "BC"},
                  {"id": 1284, "name": "Out of Town", "city": "Downtown", "state": "BC"},
                  {"id": 1285, "name": "Scottsdale", "city": "Surrey", "state": "BC"},
                  {"id": 1286, "name": "Neilsen Grove", "city": "Ladner", "state": "BC"},
                  {"id": 1287, "name": "Horseshoe Bay WV", "city": "West Vancouver", "state": "BC"},
                  {"id": 1288, "name": "Calverhall", "city": "North Vancouver", "state": "BC"},
                  {"id": 1289, "name": "Garden Village", "city": "Burnaby", "state": "BC"},
                  {"id": 1290, "name": "North Arm", "city": "New Westminster", "state": "BC"},
                  {"id": 1291, "name": "Heritage Mountain", "city": "Port Moody", "state": "BC"},
                  {"id": 1292, "name": "College Park PM", "city": "Port Moody", "state": "BC"},
                  {"id": 1293, "name": "Rosedale Center", "city": "Rosedale", "state": "BC"},
                  {"id": 1294, "name": "Columbia Valley", "city": "Cultus Lake", "state": "BC"},
                  {"id": 1295, "name": "Upper Eagle Ridge", "city": "Coquitlam", "state": "BC"},
                  {"id": 1296, "name": "Yale - Dogwood Valley", "city": "Yale", "state": "BC"},
                  {"id": 1297, "name": "Clayton", "city": "Langley", "state": "BC"},
                  {"id": 1298, "name": "Eagleridge", "city": "West Vancouver", "state": "BC"},
                  {"id": 1299, "name": "Nordel", "city": "Surrey", "state": "BC"},
                  {"id": 1300, "name": "River Springs", "city": "Coquitlam", "state": "BC"},
                  {"id": 1301, "name": "Gabriola Island", "city": "Gabriola Island", "state": "BC"},
                  {"id": 1302, "name": "Chilliwack River Valley", "city": "Sardis - Chwk River Valley", "state": "BC"},
                  {"id": 1303, "name": "Barber Street", "city": "Port Moody", "state": "BC"},
                  {"id": 1304, "name": "Gibsons & Area", "city": "Langdale", "state": "BC"},
                  {"id": 1305, "name": "Paradise Valley", "city": "Squamish", "state": "BC"},
                  {"id": 1306, "name": "Whistler Cay Estates", "city": "Whistler", "state": "BC"},
                  {"id": 1307, "name": "Halfmn Bay Secret Cv Redroofs", "city": "Sechelt", "state": "BC"},
                  {"id": 1308, "name": "Cypress", "city": "West Vancouver", "state": "BC"},
                  {"id": 1309, "name": "Steelhead", "city": "Mission", "state": "BC"},
                  {"id": 1310, "name": "Sea Island", "city": "Richmond", "state": "BC"},
                  {"id": 1311, "name": "Pebble Hill", "city": "Tsawwassen", "state": "BC"},
                  {"id": 1312, "name": "Black Tusk - Pinecrest", "city": "Whistler", "state": "BC"},
                  {"id": 1313, "name": "Alta Vista", "city": "Whistler", "state": "BC"},
                  {"id": 1314, "name": "Crescent Bch Ocean Pk.", "city": "White Rock", "state": "BC"},
                  {"id": 1315, "name": "Britannia Beach", "city": "Britannia Beach", "state": "BC"},
                  {"id": 1316, "name": "Central Abbotsford", "city": "Mission", "state": "BC"},
                  {"id": 1317, "name": "Braemar", "city": "North Vancouver", "state": "BC"},
                  {"id": 1318, "name": "Morgan Creek", "city": "White Rock", "state": "BC"},
                  {"id": 1319, "name": "Sardis West Vedder Rd", "city": "Chilliwack", "state": "BC"},
                  {"id": 1320, "name": "Grouse Woods", "city": "North Vancouver", "state": "BC"},
                  {"id": 1321, "name": "Sunnyside Park Surrey", "city": "White Rock", "state": "BC"},
                  {"id": 1322, "name": "Promontory", "city": "Chilliwack", "state": "BC"},
                  {"id": 1323, "name": "Delbrook", "city": "North Vancouver", "state": "BC"},
                  {"id": 1324, "name": "Yale - Dogwood Valley", "city": "Hope", "state": "BC"},
                  {"id": 1325, "name": "Brentwood Park", "city": "Vancouver", "state": "BC"},
                  {"id": 1326, "name": "Murrayville", "city": "Surrey", "state": "BC"},
                  {"id": 1327, "name": "Ladner Elementary", "city": "Ladner", "state": "BC"},
                  {"id": 1328, "name": "Nesters", "city": "Whistler", "state": "BC"},
                  {"id": 1329, "name": "Kerrisdale", "city": "North Vancouver", "state": "BC"},
                  {"id": 1330, "name": "Northlands", "city": "North Vancouver", "state": "BC"},
                  {"id": 1331, "name": "Forest Hills BN", "city": "Burnaby", "state": "BC"},
                  {"id": 1332, "name": "Brackendale", "city": "Brackendale", "state": "BC"},
                  {"id": 1333, "name": "Metrotown", "city": "Vancouver", "state": "BC"},
                  {"id": 1334, "name": "Windsor Park NV", "city": "North Vancouver", "state": "BC"},
                  {"id": 1335, "name": "Sunshine Hills Woods", "city": "Surrey", "state": "BC"},
                  {"id": 1336, "name": "Boston Bar - Lytton", "city": "Boston Bar", "state": "BC"},
                  {"id": 1337, "name": "Port Guichon", "city": "Ladner", "state": "BC"},
                  {"id": 1338, "name": "Garibaldi Highlands", "city": "Garibaldi Highlands", "state": "BC"},
                  {"id": 1339, "name": "Downtown NW", "city": "Vancouver", "state": "BC"},
                  {"id": 1340, "name": "Annieville", "city": "Surrey", "state": "BC"},
                  {"id": 1341, "name": "Ring Creek", "city": "Squamish", "state": "BC"},
                  {"id": 1342, "name": "Lynnmour", "city": "Vancouver", "state": "BC"},
                  {"id": 1343, "name": "Walkerville Estates", "city": "Pemberton", "state": "BC"},
                  {"id": 1344, "name": "Harrison Hot Springs", "city": "Harrison Mills", "state": "BC"},
                  {"id": 1345, "name": "Harrison Mills", "city": "Harrison Mills", "state": "BC"},
                  {"id": 1346, "name": "Glenayre", "city": "Port Moody", "state": "BC"},
                  {"id": 1347, "name": "Salt Spring Island", "city": "Vancouver", "state": "BC"},
                  {"id": 1348, "name": "Mt Woodside", "city": "Agassiz", "state": "BC"},
                  {"id": 1349, "name": "King George Corridor", "city": "White Rock", "state": "BC"},
                  {"id": 1350, "name": "D'Arcy", "city": "D'Arcy", "state": "BC"},
                  {"id": 1351, "name": "Chilliwack Yale Rd West", "city": "Chilliwack", "state": "BC"},
                  {"id": 1352, "name": "King George Corridor", "city": "Delta", "state": "BC"},
                  {"id": 1353, "name": "Birken", "city": "Pemberton", "state": "BC"},
                  {"id": 1354, "name": "Out of Town", "city": "North Vancouver", "state": "BC"},
                  {"id": 1355, "name": "Downtown VW", "city": "West Vancouver", "state": "BC"},
                  {"id": 1356, "name": "Beach Grove", "city": "Tsawwassen", "state": "BC"},
                  {"id": 1357, "name": "Oakdale", "city": "Burnaby", "state": "BC"},
                  {"id": 1358, "name": "Walkerville Estates", "city": "Mount Currie", "state": "BC"},
                  {"id": 1359, "name": "Elgin Chantrell", "city": "White Rock", "state": "BC"},
                  {"id": 1360, "name": "Hospital Hill", "city": "Squamish", "state": "BC"},
                  {"id": 1361, "name": "Summitt View", "city": "Coquitlam", "state": "BC"},
                  {"id": 1362, "name": "Columbia Valley", "city": "Lindell Beach", "state": "BC"},
                  {"id": 1363, "name": "Central Abbotsford", "city": "Langley", "state": "BC"},
                  {"id": 1364, "name": "Hamilton Heights", "city": "North Vancouver", "state": "BC"},
                  {"id": 1365, "name": "Yarrow", "city": "Chilliwack", "state": "BC"},
                  {"id": 1366, "name": "Birken", "city": "Birken", "state": "BC"},
                  {"id": 1367, "name": "Queen Mary Park Surrey", "city": "Langley", "state": "BC"},
                  {"id": 1368, "name": "Metrotown", "city": "Burns Lake", "state": "BC"},
                  {"id": 1369, "name": "Hope Laidlaw", "city": "Yale", "state": "BC"},
                  {"id": 1370, "name": "Central Pt Coquitlam", "city": "Vancouver", "state": "BC"},
                  {"id": 1371, "name": "Tsawwassen Central", "city": "Tsawwassen", "state": "BC"},
                  {"id": 1372, "name": "Norgate", "city": "West Vancouver", "state": "BC"},
                  {"id": 1373, "name": "Mayne Island", "city": "Mayne Island", "state": "BC"},
                  {"id": 1374, "name": "Bowen Island", "city": "North Vancouver", "state": "BC"},
                  {"id": 1375, "name": "Park Royal", "city": "Vancouver", "state": "BC"},
                  {"id": 1376, "name": "Coquitlam East", "city": "Port Coquitlam", "state": "BC"}]
        response = HttpResponse(json.dumps(string))
        response["Access-Control-Allow-Origin"] = "*"
        return response
