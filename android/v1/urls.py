from django.conf.urls import url
from android.v1 import views

urlpatterns = [
    url(r'homepage/$', views.HomePageView.as_view(), name='home_page'),

    # need to refactor
    url(r'brandpage/$', views.BrandPageView.as_view(), name='brand_page'),
    url(r'getmobilebrand/$', views.MobileByBrandView.as_view(), name='get_by_brand'),
    url(r'searchitembytitle/$', views.SearchMobileItemView.as_view(), name='search_by_name'),

    url(r'estitembytitle/$', views.EstMobileItemByBrandView.as_view(), name='est_by_brand'),
    url(r'getversion/$', views.AppConfigView.as_view(), name='get_config'),
    url(r'contactus/$', views.ContactUsView.as_view(), name='contact_us'),

    url(r'city/$', views.GetCity.as_view(), name='get_city'),
    url(r'neighborhood/$', views.GetNeighbor.as_view(), name='get_neighbor'),

]
