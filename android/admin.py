# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib import admin

from android.models import AppConfig, AppSlidingImages


class AppSlidingImagesAdmin(admin.ModelAdmin):
    list_display = ('time', 'number', 'link', 'model_pic')
    list_filter = ('time', 'number', 'link', 'model_pic')
    search_fields = ('time', 'number', 'link', 'model_pic')


class AppConfigAdmin(admin.ModelAdmin):
    list_editable = ('version_code', 'version_name', 'version_url', 'has_offer', 'offer_name')
    list_display = ('min_version_code', 'version_code', 'version_name', 'version_url', 'has_offer', 'offer_name')
    list_filter = ('min_version_code', 'version_code', 'version_name', 'version_url', 'has_offer', 'offer_name')
    search_fields = ('min_version_code', 'version_code', 'version_name', 'version_url', 'has_offer', 'offer_name')


admin.site.register(AppSlidingImages, AppSlidingImagesAdmin)
admin.site.register(AppConfig, AppConfigAdmin)
