# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.utils.translation import ugettext_lazy as _

from django.apps import AppConfig


class AndroidConfig(AppConfig):
    name = 'android'
    label = 'androidapp'
    verbose_name = _('android')

    def ready(self):
        import android.signals