# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from django.utils.translation import ugettext_lazy as _

from android.validator import validate_file_extension


class AppSlidingImages(models.Model):
    time = models.DateTimeField(auto_now_add=True)
    number = models.CharField(null=True, blank=True, max_length=1000)
    link = models.CharField(null=True, blank=True, max_length=1000)
    model_pic = models.ImageField(upload_to='img/slidingimg/', default='img/slidingimg/gc.jpg',
                                  validators=[validate_file_extension])

    def image_uploaded_link(self):
        return "https://gooshichand.com" + self.model_pic.url

    class Meta:
        ordering = ('-time',)
        verbose_name = _("Slide Image")
        verbose_name_plural = _("Slide Images")


class AppConfig(models.Model):
    min_version_code = models.IntegerField(blank=False)
    version_code = models.IntegerField(blank=False)
    version_name = models.CharField(blank=False, max_length=50)
    version_url = models.CharField(blank=False, max_length=100)
    update_text = models.TextField(blank=False)
    has_offer = models.BooleanField(default=False)
    offer_name = models.CharField(null=True, blank=True, max_length=100)

    class Meta:
        verbose_name = _("App Config")
        verbose_name_plural = _("App Configs")
