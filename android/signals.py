import requests
from django.db.models.signals import post_save, post_delete
from django.dispatch import receiver

from gooshichand.models import UserComment


def sending_email(destination, subject, body):
    requests.post(
        "https://gooshichand.com/sendemail/",
        data={"destination": [destination],
              "subject": subject,
              "body": body})


@receiver([post_save, post_delete], sender=UserComment)
def send_email(sender, instance, **kwargs):
    sending_email("mehran.noori2070@gmail.com", " New User Comment", instance.question)
