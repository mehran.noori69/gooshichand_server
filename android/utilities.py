import base64

from django.conf import settings
from django.contrib.auth import authenticate
from django.core.cache.backends.base import DEFAULT_TIMEOUT
from django.http import HttpResponse

from android.models import AppSlidingImages
from android.serializer import MobileSpecsSerializer
from gooshichand.models import MobileSpecs, MobileBrand, MobileItem, MobilePrice, MobileStore

paginationsize = 20


# add this if you want to ask user to enter their username and password in request header
def header_auth_view(request):
    auth_header = request.META['HTTP_AUTHORIZATION']
    encoded_credentials = auth_header.split(' ')[1]  # Removes "Basic " to isolate credentials
    decoded_credentials = base64.b64decode(encoded_credentials).decode("utf-8").split(':')
    username = decoded_credentials[0]
    password = decoded_credentials[1]
    feed_bot = authenticate(username=username, password=password)
    if feed_bot.username == username:
        return HttpResponse(feed_bot)
    else:
        return HttpResponse("error")


CACHE_TTL = getattr(settings, 'CACHE_TTL', DEFAULT_TIMEOUT)


def remove_t_from_datetime(time):
    return time.replace("T", " ")


def get_mobile_brand_function():
    mobile_brands = MobileBrand.get_visible_mobile_brand()
    mobile_brands = list(mobile_brands.values('name', 'imgUrl', 'visibility'))
    for mobile_brand in mobile_brands:
        items = MobileSpecs.get_mobile_spec_items_by_brand(mobile_brand['name'], '-release_date', False, 0, 0)
        if len(items) > 0:
            image = items.first().get_first_image_from_gallery_of_mobile_spec()
            if image is not '':
                mobile_brand['img'] = image
            else:
                mobile_brand['img'] = items.first().img
        else:
            mobile_brand['img'] = ''
        mobile_brand['specs_length'] = len(items)
    return mobile_brands


def prepare_list_of_price_and_dates(scales):
    return list(zip((str(t[0].year) + '-' + str(t[0].month) + '-' + str(t[0].day)
                     for t in scales.values_list('created')), (t[0] / 10 for t in scales.values_list('price'))))


def slide_images_function():
    ssi = AppSlidingImages.objects.all()
    results = []
    for s in ssi:
        dic = dict()
        dic['link'] = s.image_uploaded_link()
        results.append(dic)
    return results


def extract_details_of_stores(mobile_spec):
    for store in mobile_spec['stores']:
        item = MobileStore.get_store_by_name(store['store'])
        store['address'] = item.address
        store['phone'] = item.phone
        store['website'] = item.website


def recom_mobile_item_by_brand_function(brand):
    if brand == 'all':
        mobile_specs = MobileSpecs.get_mobile_spec_items_all('?', True, 0, 20)
    else:
        mobile_specs = MobileSpecs.get_mobile_spec_items_by_brand(brand, '?', True, 0, 20)
    serializer = MobileSpecsSerializer(mobile_specs, many=True)
    mobile_specs = serializer.data
    for mobile_spec in mobile_specs:
        mobile_spec['img'] = mobile_spec['first_img_in_gallery']
    return set_hasprice_minprice_stores_in_mobile_specs(mobile_specs)


def get_mobile_item_by_brand_function(brand, start):
    mobile_specs = MobileSpecs.get_mobile_spec_items_by_brand(brand, '-release_date', True, start, start+paginationsize)
    serializer = MobileSpecsSerializer(mobile_specs, many=True)
    mobile_specs = serializer.data
    return set_hasprice_minprice_stores_in_mobile_specs(mobile_specs)


def est_mobile_item_by_brand_function(brand, which, start, pagesize):
    mobile_specs = MobileSpecs.est_mobile_item_by_brand_function(brand, which, start, pagesize)
    serializer = MobileSpecsSerializer(mobile_specs, many=True)
    mobile_specs = serializer.data
    return set_hasprice_minprice_stores_in_mobile_specs(mobile_specs)


def set_hasprice_minprice_stores_in_mobile_specs(mobile_specs):
    for mobile_spec in mobile_specs:
        mobile_spec['release_date'] = remove_t_from_datetime(mobile_spec['release_date'])
        title_without_brand = mobile_spec['pure_title']
        mobile_spec['hasprice'] = MobileItem.see_if_mobile_spec_has_price(title_without_brand)
        if mobile_spec['hasprice'] > 0:
            # get min price
            mobile_spec['minprice'] = MobileItem.get_min_price_of_spec_in_mobile_items(title_without_brand)
            # get store contact detailes
            mobile_spec['stores'] = list(MobileItem.get_mobile_item_with_model(title_without_brand)
                                         .values('price', 'store', 'city', 'title'))
            extract_details_of_stores(mobile_spec)
        else:
            mobile_spec['minprice'] = -1
            mobile_spec['stores'] = []
        # add item price for Bar showing
        scales = MobilePrice.list_price_of_mobile_item_for_last_month(title_without_brand, mobile_spec['path'])
        mobile_spec['price_label'] = prepare_list_of_price_and_dates(scales)
    return mobile_specs
