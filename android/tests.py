#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.test import RequestFactory, TestCase, Client, override_settings
from rest_framework import status
from rest_framework.test import APIRequestFactory

from v1.views import *


class SimpleTest(TestCase):
    def setUp(self):
        print("run setup")
        self.factory = RequestFactory()
        self.APIfactory = APIRequestFactory()
        self.client = Client()
        UserComment.objects.create(
            name='Casper', email=3, section='Bull Dog', question='Black')
        UserComment.objects.create(
            name='Muffin', email=1, section='Gradane', question='Brown')
        UserComment.objects.create(
            name='Rambo', email=2, section='Labrador', question='Black')
        UserComment.objects.create(
            name='Ricky', email=6, section='Labrador', question='Brown')
        print(len(UserComment.objects.all()))

    @override_settings(DEBUG=True)
    def test_get_all_puppies(self):
        # get API response
        # response = self.client.get(reverse('contact_us'))
        response = self.client.get("https://gooshichand.com/android/v1/contactus/")
        print(response)
        # get data from db
        puppies = UserComment.objects.all()
        serializer = UserCommentSerializer(puppies, many=True)
        self.assertEqual(response, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    # def setUp(self):
        # Every test needs access to the request factory.
        # self.factory = RequestFactory()
        # self.APIfactory = APIRequestFactory()

    # @skip("Don't want to test")
    # def test_app_configs(self):
    #     request = self.APIfactory.get('/android/v1/getversion/', {})
    #     print(request)

    # @skipIf(True, "I don't want to run contact us test yet")
    # def test_contactus(self):
    #     request = self.APIfactory.post('/android/v1/contactus/', {'name': 'ali'})
    #     print(request)
    #     request = self.APIfactory.get('/android/v1/contactus/',)
    #     print(request)
        # ucs = UserComment.objects.all()
        # print(len(ucs))
        # for uc in ucs:
        #     print(uc.name)

    # @skip("Don't want to test")
    # def test_details(self):
    #     request = self.factory.get('https://gooshichand.com/android/v1/getversion')
    #     print(request)
    #     response = AppConfigView.as_view()(request)
    #     print(response)
    #     self.assertEqual(response.status_code, 200)
