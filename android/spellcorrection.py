#!/usr/bin/env python
# -*- coding: utf-8 -*-


class SpellCorrector:

    def __init__(self, inputs):
        self.inputs = inputs

    def fatoen(self):
        self.inputs = self.inputs.replace('۱'.decode('utf-8'), '1')
        self.inputs = self.inputs.replace('۲'.decode('utf-8'), '2')
        self.inputs = self.inputs.replace('۳'.decode('utf-8'), '3')
        self.inputs = self.inputs.replace('۴'.decode('utf-8'), '4')
        self.inputs = self.inputs.replace('۵'.decode('utf-8'), '5')
        self.inputs = self.inputs.replace('۶'.decode('utf-8'), '6')
        self.inputs = self.inputs.replace('۷'.decode('utf-8'), '7')
        self.inputs = self.inputs.replace('۸'.decode('utf-8'), '8')
        self.inputs = self.inputs.replace('۹'.decode('utf-8'), '9')
        self.inputs = self.inputs.replace('۰'.decode('utf-8'), '0')

    def spellcorrect(self):
        self.inputs = self.inputs.replace('ي'.decode('utf-8'), 'ی'.decode('utf-8'))
        self.inputs = self.inputs.replace('آ'.decode('utf-8'), 'ا'.decode('utf-8'))
        self.inputs = self.inputs.replace('ك'.decode('utf-8'), 'ک'.decode('utf-8'))
        self.inputs = self.inputs.replace('ي'.decode('utf-8'), 'ی'.decode('utf-8'))
        self.inputs = self.inputs.replace('ئ'.decode('utf-8'), 'ی'.decode('utf-8'))
        self.inputs = self.inputs.replace('ك'.decode('utf-8'), 'ک'.decode('utf-8'))
        self.inputs = self.inputs.replace('ﻚ'.decode('utf-8'), 'ک'.decode('utf-8'))
        self.inputs = self.inputs.replace('ﺑ'.decode('utf-8'), 'ب'.decode('utf-8'))
        self.inputs = self.inputs.replace('ﺎ'.decode('utf-8'), 'ا'.decode('utf-8'))
        self.inputs = self.inputs.replace('ﻧ'.decode('utf-8'), 'ن'.decode('utf-8'))
        self.inputs = self.inputs.replace('ﺧ'.decode('utf-8'), 'خ'.decode('utf-8'))
        self.inputs = self.inputs.replace('٠'.decode('utf-8'), '۰'.decode('utf-8'))
        self.inputs = self.inputs.replace('١'.decode('utf-8'), '۱'.decode('utf-8'))
        self.inputs = self.inputs.replace('٢'.decode('utf-8'), '۲'.decode('utf-8'))
        self.inputs = self.inputs.replace('٣'.decode('utf-8'), '۳'.decode('utf-8'))
        self.inputs = self.inputs.replace('۴'.decode('utf-8'), '۴'.decode('utf-8'))
        self.inputs = self.inputs.replace('۵'.decode('utf-8'), '۵'.decode('utf-8'))
        self.inputs = self.inputs.replace('۶'.decode('utf-8'), '۶'.decode('utf-8'))
        self.inputs = self.inputs.replace('٧'.decode('utf-8'), '۷'.decode('utf-8'))
        self.inputs = self.inputs.replace('٨'.decode('utf-8'), '۸'.decode('utf-8'))
        self.inputs = self.inputs.replace('٩'.decode('utf-8'), '۹'.decode('utf-8'))
        self.inputs = self.inputs.replace('إ'.decode('utf-8'), 'ا'.decode('utf-8'))
        self.inputs = self.inputs.replace('ؤ'.decode('utf-8'), 'و'.decode('utf-8'))
        self.inputs = self.inputs.replace('ۀ'.decode('utf-8'), 'ه'.decode('utf-8'))
        self.inputs = self.inputs.replace('أ'.decode('utf-8'), 'ا'.decode('utf-8'))
        self.inputs = self.inputs.replace('%'.decode('utf-8'), '٪'.decode('utf-8'))
        self.inputs = self.inputs.replace('?'.decode('utf-8'), '؟'.decode('utf-8'))
        self.inputs = self.inputs.replace('ً'.decode('utf-8'), ''.decode('utf-8'))
        self.inputs = self.inputs.replace('ٌ'.decode('utf-8'), ''.decode('utf-8'))
        self.inputs = self.inputs.replace('ٍ'.decode('utf-8'), ''.decode('utf-8'))
        self.inputs = self.inputs.replace('َ'.decode('utf-8'), ''.decode('utf-8'))
        self.inputs = self.inputs.replace('ُ'.decode('utf-8'), ''.decode('utf-8'))
        self.inputs = self.inputs.replace('ِ'.decode('utf-8'), ''.decode('utf-8'))
        self.inputs = self.inputs.replace(u"\uFEFF", " ")  # removing zero width non breakable space character
        self.inputs = self.inputs.replace(u"\u200F", "")  # removing rtl mark character
        self.inputs = self.inputs.replace(u"\u200E", "")  # removing ltr mark character
        self.inputs = self.inputs.replace(u"\u200D", "")  # removing zero width joiner
        self.inputs = self.inputs.replace(u"\u200B", " ")  # removing zero width space
        self.inputs = self.inputs.replace(u"\u200A", " ")  # removing hair space
        self.inputs = self.inputs.replace(u"\u200C", " ")  # removing zero width non joiner space character
        self.inputs = self.inputs.replace(u"\u00A0", " ")  # removing non break space
        self.inputs = self.inputs.replace(u"\u0640", "")  # removing arabic tatweel
