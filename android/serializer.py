from rest_framework import serializers
from .models import AppConfig
from gooshichand.models import UserComment, MobileSpecs, MobileItem


class AppConfigSerializer(serializers.ModelSerializer):

    class Meta:
        model = AppConfig
        fields = '__all__'


class UserCommentSerializer(serializers.ModelSerializer):

    class Meta:
        model = UserComment
        fields = '__all__'


class MobileSpecsSerializer(serializers.ModelSerializer):
    first_img_in_gallery = serializers.ReadOnlyField(source='first_image_in_gallery')
    pure_title = serializers.ReadOnlyField(source='title_without_brand')

    class Meta:
        model = MobileSpecs
        fields = '__all__'
        extra_fields = ['first_img_in_gallery', 'pure_title']


class MobileItemSerializer(serializers.ModelSerializer):

    class Meta:
        model = MobileItem
        fields = '__all__'
