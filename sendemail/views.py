# -*- coding: utf-8 -*-
import requests
from django.http import HttpResponse
from rest_framework.views import APIView
from gooshichand.settings import MAILGUN_API_KEY


class SendEmail(APIView):
    @staticmethod
    def post(request):
        destination_email = request.POST.get('destination', None)
        subject = request.POST.get('subject', None)
        body = request.POST.get('body', None)
        result = "message is not sent"
        if destination_email is not None and subject is not None and body is not None:
            result = requests.post(
                "https://api.mailgun.net/v3/uniboards.ir/messages",
                auth=("api", MAILGUN_API_KEY),
                data={"from": "Gooshichand <info@gooshichand.com>",
                      "to": [destination_email],
                      "subject": subject,
                      "text": body})
        return HttpResponse(result)


class SendSMS(APIView):
    @staticmethod
    def post(request):
        phone = request.POST.get('phone', None)
        token = request.POST.get('token', None)
        template = request.POST.get('template', None)
        api_token = "59334A2F647668614B50366A6E57487A3438634C6E627566456A5A7961724563"
        requests.post("https://api.kavenegar.com/v1/" + api_token + "/verify/lookup.json",
                      data={'receptor': phone, 'token': token, 'template': template})


