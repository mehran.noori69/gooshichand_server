from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.SendEmail.as_view(), name='send_email'),
    url(r'^$', views.SendSMS.as_view(), name='send_sms'),
]
