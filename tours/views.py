#!/usr/bin/env python
# -*- coding: utf-8 -*-
import json
from django.http import HttpResponse
from rest_framework import status
from tours.models import TourItems
from gooshichand.settings import PAGINATION_SIZE
from rest_framework.views import APIView


class SaveTour(APIView):
    @staticmethod
    def post(request):
        ids = request.POST.get('ids', None)
        title = request.POST.get('title', None)
        country = request.POST.get('country', None)
        agancy_name = request.POST.get('agancy_name', None)
        link = request.POST.get('link', None)
        agancy_phone = request.POST.get('agancy_phone', None)
        agancy_address = request.POST.get('agancy_address', None)
        city = request.POST.get('city', None)
        nights = request.POST.get('nights', None)
        airline = request.POST.get('airline', None)
        start = request.POST.get('start', None)
        content = request.POST.get('content', None)
        ti = TourItems(ids=ids, title=title, nights=nights, start=start, contents=content, airline=airline,
                       agancy_name=agancy_name, agancy_phone=agancy_phone, agancy_address=agancy_address, city=city,
                       country=country)
        ti.save()
        return HttpResponse('saved', status=status.HTTP_200_OK)


class DelTour(APIView):
    @staticmethod
    def post(request):
        agancy_name = request.POST.get('agancy_name', None)
        TourItems.get_tour_items_by_agancy_name(agancy_name).delete()
        return HttpResponse('deleted', status=status.HTTP_200_OK)


class GetTourItem(APIView):
    @staticmethod
    def post(request):
        query = request.POST.get('q', None)
        city = request.POST.get('city', None)
        country = request.POST.get('country', None)
        nights = request.POST.get('nights', None)
        agancy = request.POST.get('agancy', None)
        page = request.POST.get('page', '0')
        start = int(page) * PAGINATION_SIZE
        items = TourItems.get_tour_items().order_by('start')
        if query is not None and query is not '':
            items = items.filter(title__icontains=query).order_by('start')
        if city is not None and city is not '':
            items = items.filter(city__icontains=city).order_by('start')
        if country is not None and country is not '':
            items = items.filter(country__icontains=country).order_by('start')
        if nights is not None and nights is not '':
            items = items.filter(nights__icontains=nights).order_by('start')
        if agancy is not None and agancy is not '':
            items = items.filter(agancy_name__icontains=agancy).order_by('start')
        items = items[start:start + PAGINATION_SIZE]
        items = list(items.values('created', 'title', 'nights', 'start', 'contents', 'airline', 'agancy_name',
                                  'agancy_phone', 'agancy_address', 'city', 'country'))
        for item in items:
            item['price'] = item['contents'].split("price=")[1]
            if '€'.decode('utf-8') not in item['price'] and '$'.decode('utf-8') not in item['price'] \
                    and 'یورو'.decode('utf-8') not in item['price'] and 'دلار'.decode('utf-8') not in item['price']:
                item['price'] += " تومان".decode('utf-8')
            item['price'] = item['price'].replace('$'.decode('utf-8'), 'دلار'.decode('utf-8'))
            item['price'] = item['price'].replace('€'.decode('utf-8'), 'یورو'.decode('utf-8'))
            item['size'] = item['contents'].split("size=")[1].split("&")[0]
            item['hotelname'] = item['contents'].split("hotelname=")[1].split("&")[0].replace("##", "-")
            item['services'] = item['contents'].split("services=")[1].split("&")[0]
            item['stars'] = item['contents'].split("stars=")[1].split("&")[0]

        result = json.dumps(items, sort_keys=True, default=str)
        response = HttpResponse(result, content_type='application/json')
        # response["Access-Control-Allow-Origin"] = "http://tourbeen.ir"
        response["Access-Control-Allow-Origin"] = "*"
        return response
