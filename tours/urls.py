from django.conf.urls import url
from tours import views
from tours.views import *

urlpatterns = [
    url(r'^save/$', SaveTour.as_view(), name='savetour'),
    url(r'^del/$', DelTour.as_view(), name='deltour'),
    url(r'^gettouritems/$', GetTourItem.as_view(), name='gettouritems'),
]
