# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib import admin

from tours.models import TourItems


class TourItemsAdmin(admin.ModelAdmin):
    list_display = ('created', 'ids', 'title', 'nights', 'start', 'contents', 'airline', 'agancy_name', 'agancy_phone',
                    'city', 'country', 'agancy_address', 'sent')
    list_filter = ['created', 'ids', 'title', 'nights', 'start', 'contents', 'airline', 'agancy_name', 'agancy_phone',
                   'city', 'country', 'agancy_address', 'sent']
    search_fields = ['created', 'ids', 'title', 'nights', 'start', 'contents', 'airline', 'agancy_name', 'agancy_phone',
                     'city', 'country', 'agancy_address', 'sent']


admin.site.register(TourItems, TourItemsAdmin)
