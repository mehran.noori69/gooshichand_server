# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from django.utils.translation import ugettext_lazy as _


class TourItems(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    ids = models.CharField(max_length=500, blank=True, null=True)
    title = models.CharField(max_length=500, blank=True, null=True)
    nights = models.CharField(max_length=100, blank=True, null=True)
    start = models.CharField(max_length=500, blank=True, null=True)
    contents = models.TextField(blank=True, null=True)
    airline = models.CharField(max_length=500, blank=True, null=True)
    agancy_name = models.CharField(max_length=500, blank=True, null=True)
    agancy_phone = models.CharField(max_length=500, blank=True, null=True)
    agancy_address = models.CharField(max_length=2000, blank=True, null=True)
    city = models.CharField(max_length=1000, blank=True, null=True)
    country = models.CharField(max_length=1000, blank=True, null=True)
    sent = models.IntegerField(blank=True, null=True, default=0)

    @classmethod
    def get_tour_items(cls):
        return cls.objects.all()

    @classmethod
    def get_tour_items_by_agancy_name(cls, agancy_name):
        return cls.objects.filter(agancy_name=agancy_name)

    @classmethod
    def get_tour_items_by_agancy_title(cls, title):
        return cls.objects.filter(title__icontains=title)

    @classmethod
    def get_tour_items_by_agancy_city(cls, city):
        return cls.objects.filter(city__icontains=city)

    @classmethod
    def get_tour_items_by_agancy_country(cls, country):
        return cls.objects.filter(country__icontains=country)

    @classmethod
    def get_tour_items_by_agancy_night(cls, night):
        return cls.objects.filter(nights__icontains=night)

    class Meta:
        ordering = ('-created',)
        verbose_name = _("Tour Item")
        verbose_name_plural = _("Tour Items")
