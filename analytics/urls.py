from django.conf.urls import url
from django.contrib.auth.decorators import login_required

from analytics.views import *

urlpatterns = [
    # work with IPs
    url(r'^$', login_required(Tools.as_view()), name='tools'),
    url(r'^havebrandnotmodel/$', login_required(HaveBrandNotModel.as_view()), name='havebrandnotmodel'),
    url(r'^countipperday/$', login_required(CountIPPerDays.as_view()), name='countipperday'),
    url(r'^listallstores/$', login_required(StoreList.as_view()), name='listallstores'),
    url(r'^outdated_items/$', login_required(OutdatedItems.as_view()), name='outdated_items'),
    url(r'^todaycalcusertime/$', login_required(TodayDailyUserTime.as_view()), name='todaycalcusertime'),
    url(r'^calcusertime/$', login_required(DailyUserTime.as_view()), name='calcusertime'),
    url(r'^todaymodelssearch/$', login_required(TodayModelSearch.as_view()), name='todaymodelssearch'),
    url(r'^modelssearch/$', login_required(ModelSearch.as_view()), name='modelssearch'),
    url(r'^modelszerosearch/$', login_required(ModelZeroSearch.as_view()), name='modelszerosearch'),
    url(r'^todayadvancedsearch/$', login_required(TodayAdvancedSearch.as_view()), name='todayadvancedsearch'),
    url(r'^advancedsearch/$', login_required(AdvancedSearch.as_view()), name='advancedsearch'),
    url(r'^mostspectodayview/$', login_required(MostSpecTodayView.as_view()), name='mostspectodayview'),
    url(r'^todayviewreq/$', login_required(TodayViewReq.as_view()), name='todayviewreq'),
    url(r'^viewreq/$', login_required(ViewReq.as_view()), name='viewreq'),
    url(r'^searchref/$', login_required(SearchByRef.as_view()), name='searchref'),
    url(r'^compareoutdigiato/$', login_required(CompareOutDigiato.as_view()), name='compareoutdigiato'),
    url(r'^delgbot/$', login_required(DelGoogleBot.as_view()), name='delgbot'),
    url(r'^delip/$', login_required(DelWebReqIP.as_view()), name='delip'),
    url(r'^ips/all/$', login_required(AllIP.as_view()), name='ipsall'),
    url(r'^ips/today/$', login_required(AllToday.as_view()), name='ipstoday'),
    url(r'^ips/(?P<ip>[^/]+)/$', login_required(ThisIP.as_view()), name='ipsip'),
]
