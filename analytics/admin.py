# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib import admin

from analytics.mongomodels import MongoWebRequest


class WebRequestAdmin(admin.ModelAdmin):
    list_display = ('time', 'section', 'ipaddress', 'ref', 'browser')
    list_filter = ['time', 'section', 'ipaddress', 'ref', 'browser']
    search_fields = ['time', 'section', 'ipaddress', 'ref', 'browser']


admin.site.register(MongoWebRequest, WebRequestAdmin)
