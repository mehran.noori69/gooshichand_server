#!/usr/bin/env python
# -*- coding: utf-8 -*-
import operator
from datetime import datetime, timedelta

from django.http import HttpResponse
from django.views import View
from django.views.generic import TemplateView
from rest_framework import status

from analytics.mongomodels import MongoWebRequest
from gooshichand.models import MobileItem, WebRequest


NOW = datetime.today()
START_OF_TODAY = NOW - timedelta(hours=NOW.hour-1)
THIRTY_DAYS_AGO = NOW - timedelta(days=30, hours=NOW.hour-1)


class Tools(TemplateView):
    template_name = "tools/tools.html"
    items = ''

    def dispatch(self, *args, **kwargs):
        return super(Tools, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(Tools, self).get_context_data(**kwargs)
        context['items'] = self.items
        return context


class HaveBrandNotModel(TemplateView):
    template_name = "tools/reqlist.html"
    mobile_items = []

    def dispatch(self, *args, **kwargs):
        mis = MobileItem.objects.filter(model__exact='').exclude(cats__exact='').exclude(cats__exact='Kenxinda') \
            .exclude(cats__exact='GLX').exclude(cats__exact='Tecno').exclude(cats__exact='Smart') \
            .exclude(cats__exact='Marshal').exclude(cats__exact='Blu').exclude(cats__exact='Caterpillar') \
            .exclude(cats__exact='Fly').exclude(cats__exact='Alcatel').exclude(cats__exact='lenovo').exclude(
            cats__exact='OnePlus')
        self.mobile_items = []
        for mi in mis:
            new = [mi.path, mi.title]
            self.mobile_items.append(new)
        return super(HaveBrandNotModel, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(HaveBrandNotModel, self).get_context_data(**kwargs)
        context['items'] = self.mobile_items
        return context


class StoreList(TemplateView):
    template_name = "tools/reqlist.html"
    stores = []

    def dispatch(self, *args, **kwargs):
        mis = MobileItem.get_mobile_item_sorted_by_store()
        store_names = []
        for r in mis:
            if r.store not in store_names:
                store_names.append(r.store)
        for name in store_names:
            length = MobileItem.get_length_of_mobile_item_by_store_name(name)
            new = [name, length]
            self.stores.append(new)
        self.stores.sort(key=lambda x: x[0])
        return super(StoreList, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(StoreList, self).get_context_data(**kwargs)
        context['items'] = self.stores
        return context


class OutdatedItems(TemplateView):
    template_name = "tools/reqlist.html"
    items = []

    def dispatch(self, *args, **kwargs):
        mis = MobileItem.get_mobile_item_before_today()
        for mi in mis:
            new = [mi.title, mi.store]
            self.items.append(new)
        return super(OutdatedItems, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(OutdatedItems, self).get_context_data(**kwargs)
        context['items'] = self.items
        return context


class TodayDailyUserTime(TemplateView):
    template_name = "tools/reqlist.html"
    items = []

    def dispatch(self, *args, **kwargs):
        reqs = MongoWebRequest.objects(time__gte=START_OF_TODAY, time__lte=NOW).order_by('-time')
        ips = []
        for r in reqs:
            if r.ipaddress not in ips:
                ips.append(r.ipaddress)
        for ip in ips:
            req = MongoWebRequest.objects(ipaddress=ip, time__gte=START_OF_TODAY, time__lte=NOW).order_by('-time')
            diff = req.first().time - req.last().time
            new = [ip, diff]
            self.items.append(new)
        return super(TodayDailyUserTime, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(TodayDailyUserTime, self).get_context_data(**kwargs)
        context['items'] = self.items
        return context


class DailyUserTime(TemplateView):
    template_name = "tools/reqlist.html"
    reqs = []

    def dispatch(self, *args, **kwargs):
        reqs = MongoWebRequest.objects(time__gte=THIRTY_DAYS_AGO).order_by('-time')
        ips = []
        for r in reqs:
            if r.ipaddress not in ips:
                ips.append(r.ipaddress)
        for ip in ips:
            req = MongoWebRequest.objects(time__gte=THIRTY_DAYS_AGO, ipaddress=ip).order_by('-time')
            diff = req.first().time - req.last().time
            new = [ip, diff]
            self.reqs.append(new)
        return super(DailyUserTime, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(DailyUserTime, self).get_context_data(**kwargs)
        context['items'] = self.reqs
        return context


class CountIPPerDays(TemplateView):
    template_name = "tools/countip.html"
    reqs = []

    def dispatch(self, *args, **kwargs):
        i = 0
        while i < 30:
            n = datetime.now()
            today = n - timedelta(days=i, hours=n.hour, minutes=n.minute) + timedelta(hours=1)
            today_wanted_format = datetime.strptime(str(today.year) + '-' + str(today.month) + '-' + str(today.day),
                                                    '%Y-%m-%d').strftime('%Y-%m-%d')
            today_count = len(MongoWebRequest.objects(time__gt=today, time__lte=today + timedelta(days=1)))
            today_unique_count = len(
                MongoWebRequest.objects(time__gt=today, time__lte=today + timedelta(days=1)).order_by().values_list(
                    'ipaddress').distinct(field="ipaddress"))
            t = (today_wanted_format, today_count, today_unique_count)
            self.reqs.append(t)
            i += 1
        return super(CountIPPerDays, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(CountIPPerDays, self).get_context_data(**kwargs)
        context['items'] = self.reqs
        return context


class TodayModelSearch(TemplateView):
    template_name = "tools/modelpage.html"
    reqs = []

    def dispatch(self, *args, **kwargs):
        web_reqs = MongoWebRequest.objects(time__gte=START_OF_TODAY, time__lte=NOW, section__contains="model=")\
            .order_by('-time')
        for web_req in web_reqs:
            model = web_req.section.split("model=")[1].split("&")[0]
            minprice = web_req.section.split("minprice=")[1].split("&")[0]
            maxprice = web_req.section.split("maxprice=")[1].split("&")[0]
            store = web_req.section.split("store=")[1].split("&")[0]
            city = web_req.section.split("city=")[1].split("&")[0]
            brand = web_req.section.split("brand=")[1].split("&")[0]
            page = web_req.section.split("page=")[1].split("&")[0]
            result = web_req.section.split("result=")
            if len(result) > 1:
                result = result[1].split("&")[0]
            else:
                result = '0'
            src = web_req.ipaddress
            time = web_req.time
            new = [time, src, model, brand, store, city, minprice, maxprice, result, page]
            self.reqs.append(new)
        return super(TodayModelSearch, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(TodayModelSearch, self).get_context_data(**kwargs)
        context['items'] = self.reqs
        return context


class ModelZeroSearch(TemplateView):
    template_name = "tools/modelpage.html"
    reqs = []

    def dispatch(self, *args, **kwargs):
        web_reqs = MongoWebRequest.objects(time__gte=THIRTY_DAYS_AGO, section__contains="model=").order_by('-time')
        for web_req in web_reqs:
            model = web_req.section.split("model=")[1].split("&")[0]
            minprice = web_req.section.split("minprice=")[1].split("&")[0]
            maxprice = web_req.section.split("maxprice=")[1].split("&")[0]
            store = web_req.section.split("store=")[1].split("&")[0]
            city = web_req.section.split("city=")[1].split("&")[0]
            brand = web_req.section.split("brand=")[1].split("&")[0]
            page = web_req.section.split("page=")[1].split("&")[0]
            result = web_req.section.split("result=")
            if len(result) > 1:
                result = result[1].split("&")[0]
            else:
                result = '0'
            src = web_req.ipaddress
            time = web_req.time
            new = [time, src, model, brand, store, city, minprice, maxprice, result, page]
            if int(result) == 0:
                self.reqs.append(new)
        return super(ModelZeroSearch, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(ModelZeroSearch, self).get_context_data(**kwargs)
        context['items'] = self.reqs
        return context


class ModelSearch(TemplateView):
    template_name = "tools/modelpage.html"
    reqs = []

    def dispatch(self, *args, **kwargs):
        web_reqs = MongoWebRequest.objects(time__gte=THIRTY_DAYS_AGO, section__contains="model=").order_by('-time')
        for web_req in web_reqs:
            model = web_req.section.split("model=")[1].split("&")[0]
            minprice = web_req.section.split("minprice=")[1].split("&")[0]
            maxprice = web_req.section.split("maxprice=")[1].split("&")[0]
            store = web_req.section.split("store=")[1].split("&")[0]
            city = web_req.section.split("city=")[1].split("&")[0]
            brand = web_req.section.split("brand=")[1].split("&")[0]
            page = web_req.section.split("page=")[1].split("&")[0]
            result = web_req.section.split("result=")
            if len(result) > 1:
                result = result[1].split("&")[0]
            else:
                result = '0'
            src = web_req.ipaddress
            time = web_req.time
            new = [time, src, model, brand, store, city, minprice, maxprice, result, page]
            self.reqs.append(new)
        return super(ModelSearch, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(ModelSearch, self).get_context_data(**kwargs)
        context['items'] = self.reqs
        return context


class TodayAdvancedSearch(TemplateView):
    template_name = "tools/advancedsearch.html"
    reqs = []

    def dispatch(self, *args, **kwargs):
        web_reqs = MongoWebRequest.objects(time__gte=START_OF_TODAY, time__lte=NOW,
                                           section__contains="advancedsearch-").order_by('-time')
        for web_req in web_reqs:
            brand = web_req.section.split("brand=")[1].split("&")[0]
            mininch = web_req.section.split("mininch=")[1].split("&")[0]
            maxinch = web_req.section.split("maxinch=")[1].split("&")[0]
            minpd = web_req.section.split("minpd=")[1].split("&")[0]
            maxpd = web_req.section.split("maxpd=")[1].split("&")[0]
            minyear = web_req.section.split("minyear=")[1].split("&")[0]
            maxyear = web_req.section.split("maxyear=")[1].split("&")[0]
            minram = web_req.section.split("minram=")[1].split("&")[0]
            maxram = web_req.section.split("maxram=")[1].split("&")[0]
            minstorage = web_req.section.split("minstorage=")[1].split("&")[0]
            maxstorage = web_req.section.split("maxstorage=")[1].split("&")[0]
            mincore = web_req.section.split("mincore=")[1].split("&")[0]
            maxcore = web_req.section.split("maxcore=")[1].split("&")[0]
            minfreq = web_req.section.split("minfreq=")[1].split("&")[0]
            maxfreq = web_req.section.split("maxfreq=")[1].split("&")[0]
            minbcamera = web_req.section.split("minbcamera=")[1].split("&")[0]
            maxbcamera = web_req.section.split("maxbcamera=")[1].split("&")[0]
            minfcamera = web_req.section.split("minfcamera=")[1].split("&")[0]
            maxfcamera = web_req.section.split("maxfcamera=")[1].split("&")[0]
            sortdir = web_req.section.split("sortdir=")[1].split("&")[0]
            sort = web_req.section.split("sort=")[1].split("&")[0]
            os = web_req.section.split("os=")[1].split("&")[0]
            page = web_req.section.split("page=")[1].split("&")[0]
            result = web_req.section.split("result=")
            if len(result) > 1:
                result = result[1].split("&")[0]
            else:
                result = '0'
            src = web_req.ipaddress
            time = web_req.time
            new = [time, src, brand, mininch, maxinch, minpd, maxpd, minyear, maxyear, minram, maxram, minstorage,
                   maxstorage, mincore, maxcore, minfreq, maxfreq, minbcamera, maxbcamera, minfcamera, maxfcamera,
                   sortdir, sort, os, result, page]
            self.reqs.append(new)
        return super(TodayAdvancedSearch, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(TodayAdvancedSearch, self).get_context_data(**kwargs)
        context['items'] = self.reqs
        return context


class AdvancedSearch(TemplateView):
    template_name = "tools/advancedsearch.html"
    reqs = []

    def dispatch(self, *args, **kwargs):
        web_reqs = MongoWebRequest.objects(time__gte=THIRTY_DAYS_AGO,
                                           section__contains="advancedsearch-").order_by('-time')
        for web_req in web_reqs:
            brand = web_req.section.split("brand=")[1].split("&")[0]
            mininch = web_req.section.split("mininch=")[1].split("&")[0]
            maxinch = web_req.section.split("maxinch=")[1].split("&")[0]
            minpd = web_req.section.split("minpd=")[1].split("&")[0]
            maxpd = web_req.section.split("maxpd=")[1].split("&")[0]
            minyear = web_req.section.split("minyear=")[1].split("&")[0]
            maxyear = web_req.section.split("maxyear=")[1].split("&")[0]
            minram = web_req.section.split("minram=")[1].split("&")[0]
            maxram = web_req.section.split("maxram=")[1].split("&")[0]
            minstorage = web_req.section.split("minstorage=")[1].split("&")[0]
            maxstorage = web_req.section.split("maxstorage=")[1].split("&")[0]
            mincore = web_req.section.split("mincore=")[1].split("&")[0]
            maxcore = web_req.section.split("maxcore=")[1].split("&")[0]
            minfreq = web_req.section.split("minfreq=")[1].split("&")[0]
            maxfreq = web_req.section.split("maxfreq=")[1].split("&")[0]
            minbcamera = web_req.section.split("minbcamera=")[1].split("&")[0]
            maxbcamera = web_req.section.split("maxbcamera=")[1].split("&")[0]
            minfcamera = web_req.section.split("minfcamera=")[1].split("&")[0]
            maxfcamera = web_req.section.split("maxfcamera=")[1].split("&")[0]
            sortdir = web_req.section.split("sortdir=")[1].split("&")[0]
            sort = web_req.section.split("sort=")[1].split("&")[0]
            os = web_req.section.split("os=")[1].split("&")[0]
            page = web_req.section.split("page=")[1].split("&")[0]
            result = web_req.section.split("result=")
            if len(result) > 1:
                result = result[1].split("&")[0]
            else:
                result = '0'
            src = web_req.ipaddress
            time = web_req.time
            new = [time, src, brand, mininch, maxinch, minpd, maxpd, minyear, maxyear, minram, maxram, minstorage,
                   maxstorage, mincore, maxcore, minfreq, maxfreq, minbcamera, maxbcamera, minfcamera, maxfcamera,
                   sortdir, sort, os, result, page]
            self.reqs.append(new)
        return super(AdvancedSearch, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(AdvancedSearch, self).get_context_data(**kwargs)
        context['items'] = self.reqs
        return context


class ViewReq(TemplateView):
    template_name = "tools/reqlist.html"
    items = []

    def dispatch(self, *args, **kwargs):
        ips = MongoWebRequest.objects().item_frequencies('ipaddress')
        for ip in ips:
            new = [ip, ips[ip]]
            self.items.append(new)
        self.items.sort(key=operator.itemgetter(1), reverse=True)
        return super(ViewReq, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(ViewReq, self).get_context_data(**kwargs)
        context['items'] = self.items
        return context


class TodayViewReq(TemplateView):
    template_name = "tools/reqlist.html"
    items = []

    def dispatch(self, *args, **kwargs):
        ips = MongoWebRequest.objects(time__gte=START_OF_TODAY, time__lte=NOW).item_frequencies('ipaddress')
        for ip in ips:
            new = [ip, ips[ip]]
            self.items.append(new)
        self.items.sort(key=operator.itemgetter(1), reverse=True)
        return super(TodayViewReq, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(TodayViewReq, self).get_context_data(**kwargs)
        context['items'] = self.items
        return context


class MostSpecTodayView(TemplateView):
    template_name = "tools/reqlist.html"
    items = []

    def dispatch(self, *args, **kwargs):
        day = self.request.GET.get('day', '0')
        endday = NOW - timedelta(days=int(day))
        startday = NOW - timedelta(days=int(day), hours=endday.hour - 1)
        endday = endday - timedelta(hours=endday.hour)
        endday = endday + timedelta(days=1)
        ips = MongoWebRequest.objects(section__icontains='item-spec', time__gte=startday, time__lte=endday) \
            .item_frequencies('section')
        for ip in ips:
            new = [ip, ips[ip]]
            self.items.append(new)
        self.items.sort(key=operator.itemgetter(1), reverse=True)
        return super(MostSpecTodayView, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(MostSpecTodayView, self).get_context_data(**kwargs)
        context['items'] = self.items
        return context


class SearchByRef(TemplateView):
    template_name = "tools/iptables.html"
    items = []

    def dispatch(self, *args, **kwargs):
        ref = self.request.GET.get('ref', '')
        self.items = MongoWebRequest.objects(time__gte=THIRTY_DAYS_AGO, ref__icontains=ref).order_by('-time')
        return super(SearchByRef, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(SearchByRef, self).get_context_data(**kwargs)
        context['items'] = self.items
        return context


class CompareOutDigiato(TemplateView):
    template_name = "tools/iptables.html"
    items = []

    def dispatch(self, *args, **kwargs):
        self.items = MongoWebRequest.objects(section__icontains="items-compare-").filter(section__icontains=",")\
            .exclude(ref__icontains="digiato").order_by('-time')
        return super(CompareOutDigiato, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(CompareOutDigiato, self).get_context_data(**kwargs)
        context['items'] = self.items
        return context


class SomeIP(TemplateView):
    template_name = "tools/iptables.html"
    items = []

    def dispatch(self, *args, **kwargs):
        self.items = WebRequest.objects.order_by('-time').values('time', 'section', 'ipaddress')
        return super(SomeIP, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(SomeIP, self).get_context_data(**kwargs)
        context['items'] = self.items
        return context


class AllIP(TemplateView):
    template_name = "tools/iptables.html"
    items = []

    def dispatch(self, *args, **kwargs):
        ips = MongoWebRequest.objects(time__gte=THIRTY_DAYS_AGO).order_by('-time')
        for ip in ips:
            if 'model=' in ip.section:
                temp = ip.section
                temps = temp.split('&')
                xxx = ''
                for t in temps:
                    if 'model=' in t or 'store=' in t or 'brand=' in t or 'result=' in t:
                        xxx += t + '\n'
                ip.section = xxx
            item = dict()
            item['time'] = ip.time
            item['ipaddress'] = ip.ipaddress
            item['section'] = ip.section
            item['ref'] = ip.ref
            self.items.append(item)
        return super(AllIP, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(AllIP, self).get_context_data(**kwargs)
        context['items'] = self.items
        return context


class AllToday(TemplateView):
    template_name = "tools/iptables.html"
    items = []

    def dispatch(self, *args, **kwargs):
        startday = NOW - timedelta(hours=NOW.hour)
        ips = MongoWebRequest.objects(time__gte=startday, time__lte=NOW).order_by('-time')
        for ip in ips:
            if 'model=' in ip.section:
                temp = ip.section
                temps = temp.split('&')
                xxx = ''
                for t in temps:
                    if 'model=' in t or 'store=' in t or 'brand=' in t or 'result=' in t:
                        xxx += t + '\n'
                ip.section = xxx
            item = dict()
            item['time'] = ip.time
            item['ipaddress'] = ip.ipaddress
            item['section'] = ip.section
            item['ref'] = ip.ref
            self.items.append(item)
        return super(AllToday, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(AllToday, self).get_context_data(**kwargs)
        context['items'] = self.items
        return context


class ThisIP(TemplateView):
    template_name = "tools/iptables.html"
    items = []

    def dispatch(self, *args, **kwargs):
        ip = kwargs['ip']
        ips = MongoWebRequest.objects(ipaddress=ip).order_by('-time')
        for ip in ips:
            if 'model=' in ip.section:
                temp = ip.section
                temps = temp.split('&')
                xxx = ''
                for t in temps:
                    if 'model=' in t or 'store=' in t or 'brand=' in t or 'result=' in t:
                        xxx += t + '\n'
                ip.section = xxx
            item = dict()
            item['time'] = ip.time
            item['ipaddress'] = ip.ipaddress
            item['section'] = ip.section
            item['ref'] = ip.ref
            self.items.append(item)
        return super(ThisIP, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(ThisIP, self).get_context_data(**kwargs)
        context['items'] = self.items
        return context


class DelGoogleBot(View):
    @staticmethod
    def get(request):
        MongoWebRequest.objects(browser__icontains='Googlebot').delete()
        return HttpResponse("done", status.HTTP_200_OK)


class DelWebReqIP(View):
    @staticmethod
    def get(request):
        ip = request.GET.get('ip', None)
        if ip is not None:
            MongoWebRequest.objects(ipaddress=ip).delete()
        else:
            startday = NOW - timedelta(days=60)
            MongoWebRequest.objects(time__lte=startday).delete()
        return HttpResponse("done", status.HTTP_200_OK)
