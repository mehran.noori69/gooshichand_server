from mongoengine import *
from gooshichand import settings


connect('GC_Mongo_DB', host=settings.MONGO_IP, port=settings.MONGO_PORT)


class MongoWebRequest(Document):
    time = DateTimeField(auto_now_add=True)
    section = StringField(blank=True, max_length=1000, help_text='Section')
    ipaddress = StringField(blank=True, max_length=50, help_text='IP Address')
    ref = StringField(null=True, blank=True, max_length=1000, help_text='Reference')
    browser = StringField(blank=True, max_length=1000, help_text='User Browser')
